(function($) {
    Drupal.behaviors.rural_quickquote_counter_main = {
        attach: function(context, settings) {

			/**
			 *	SCRIPT INIT
			 */

			//Fetch passed module path var (used for ajax)
			var module_path = settings.rural_quickquote_counter_main.module_path;

			/** 
			 *	RESPONDERS
			 */

			$(document).ready(function(){

				//On 'Create Aggregator' click
				$(".create-container .create-aggregator").click(function(){
					_create_aggregator();
				});

				//On 'Stop Aggregator' click
				$(".aggregator-close .close").click(function(){
					_stop_aggregator($(this).data('aggregator-id'));
				});

				//On action button click
				$(".aggregator-actions .button").click(function(){
					_add_action($(this).data('type'), $(this).data('aggregator-id'));
				});

				//Load overall aggrigator stats
				$(".overall-stats").load(module_path + '/assets/ajax/rural_quickquote_counter.aggregator_overall.ajax.php');

				//Load aggrigator history into container
				$(".aggregator-history").load(module_path + '/assets/ajax/rural_quickquote_counter.aggregator_history.ajax.php');

			});

			/** 
			 *	HELPERS
			 */

			//Ajax function used to create an aggregator
			function _create_aggregator(){
				$.ajax({
		        	type: 'POST',
		        	cache: false,
		        	url: module_path + '/assets/ajax/rural_quickquote_counter.create_aggregator.ajax.php',
		        	dataType: "json",
		          	success: function(response){
		          		
		          		//Set the stats
		          		$(".aggregator-stats .aggregator-id span").html('#' + response.aggregator);
		          		$(".aggregator-stats .aggregator-cr span").html(response.num_call);
		          		$(".aggregator-stats .aggregator-cf span").html(response.num_closed);
		          		$(".aggregator-stats .aggregator-pc span").html('%0');

		          		//Set the close aggregator data attribute
		          		$(".aggregator-close .close").data('aggregator-id', response.aggregator);

		          		//Set the button aggregator data attributes
		          		$(".quote-open").data('aggregator-id', response.aggregator);
		          		$(".quote-fin").data('aggregator-id', response.aggregator);

		          		//Swap the containers
		          		$(".create-container").fadeOut('slow', function(){
							$(".main-container").fadeIn('slow');
						});

						//Load overall aggrigator stats
						$(".overall-stats").load(module_path + '/assets/ajax/rural_quickquote_counter.aggregator_overall.ajax.php');

		          	}
		        });
			}

			//Ajax function used to stop an aggregator
			function _stop_aggregator(aggregator_id){
				$.ajax({
		        	type: 'POST',
		        	cache: false,
		        	url: module_path + '/assets/ajax/rural_quickquote_counter.close_aggregator.ajax.php',
		        	data: {
		            	"aggregator_id": aggregator_id
		          	},
		          	success: function(){
		          		//Swap the containers
		          		$(".main-container").fadeOut('slow', function(){
							$(".create-container").fadeIn('slow');
						});
						//Load overall aggrigator stats
						$(".overall-stats").load(module_path + '/assets/ajax/rural_quickquote_counter.aggregator_overall.ajax.php');
						//Load aggrigator history into container
						$(".aggregator-history").load(module_path + '/assets/ajax/rural_quickquote_counter.aggregator_history.ajax.php');
		          	}
		        });
			}

			//Ajax function used to add action to an aggregator
			function _add_action(type, aggregator_id){
				$.ajax({
		        	type: 'POST',
		        	cache: false,
		        	url: module_path + '/assets/ajax/rural_quickquote_counter.add_action.ajax.php',
		        	data: {
		        		"type": type,
		            	"aggregator_id": aggregator_id
		          	},
		          	dataType: "json",
		          	success: function(response){

		          		if(response == "error"){
		          			alert("ERROR: Aggregator Stopped, Refresh page");	
		          		}

		          		//Calc %
		          		var aa_percent = (response.num_closed / response.num_call ) * 100;

		          		//Set the stats
		          		$(".aggregator-stats .aggregator-id span").html('#' + response.aggregator);
		          		$(".aggregator-stats .aggregator-cr span").html(response.num_call);
		          		$(".aggregator-stats .aggregator-cf span").html(response.num_closed);
		          		$(".aggregator-stats .aggregator-pc span").html('%' + aa_percent.toFixed(2));

		          		//Load overall aggrigator stats
						$(".overall-stats").load(module_path + '/assets/ajax/rural_quickquote_counter.aggregator_overall.ajax.php');
		          	}
		        });
			}

        }
    };
})(jQuery);