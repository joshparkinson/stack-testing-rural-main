<?php

//Bootstrap drupal
$path = $_SERVER['DOCUMENT_ROOT'];
chdir($path);
define('DRUPAL_ROOT', getcwd());
$base_url = 'http://'.$_SERVER['HTTP_HOST'];
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//Start session if necessary
if(session_status() == PHP_SESSION_NONE || session_id() == ''){
    session_start();
}

//Get the number of aggregators
$num_aggs = db_select('quickquote_aggregator', 'q');
$num_aggs->fields('q', array('qqa_id'));
$num_aggs = $num_aggs->execute();
$num_aggs = $num_aggs->rowCount();

//Get the number of calls
$num_calls = db_select('quickquote_counter', 'q');
$num_calls->fields('q', array('qqc_id'));
$num_calls->condition('type', 'open', '=');
$num_calls = $num_calls->execute();
$num_calls = $num_calls->rowCount();

//Get the number of covers
$num_cov = db_select('quickquote_counter', 'q');
$num_cov->fields('q', array('qqc_id'));
$num_cov->condition('type', 'close', '=');
$num_cov = $num_cov->execute();
$num_cov = $num_cov->rowCount();

//Get percentage
if($num_cov == 0){
	$percentage = 0;
}
else{
	$percentage = round(($num_cov / $num_calls) * 100, 2);
}

$output = '';
$output .= '<div class="stat-line">Aggregators Created: <span>'.$num_aggs.'</span></div>';
$output .= '<div class="stat-line">Calls Recieved: <span>'.$num_calls.'</span></div>';
$output .= '<div class="stat-line">On Cover: <span>'.$num_cov.'</span></div>';
$output .= '<div class="stat-line">% Converted: <span>%'.$percentage.'</span></div>';

echo $output;

?>