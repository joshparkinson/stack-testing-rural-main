<?php

/**
 *  ACCESS CHECKS
 */ 

//Stop direct access to the file (ie non ajax calls)
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'){
    _throw_error('403', 'No direct access');
}

//Kill call if no aggregator_id passed
if(!isset($_POST['aggregator_id']) || intval($_POST['aggregator_id']) == 0){
	_throw_error('403', 'Invalid Params');
}

//Kill call if no type passed or not as expected
if(!isset($_POST['type']) || ($_POST['type'] != 'open' && $_POST['type'] != 'close')){
	_throw_error('403', 'Invalid Params');
}

/**
 *	PROCESSING
 */

//Boostrap drupal
_bootstrap();

//Store the passed variables
$type = strtolower($_POST['type']);
$aggregator_id = intval($_POST['aggregator_id']);

//Get the current config
$config = _rqqc_fetch_config();

//Kill call if aggregator doesnt exists
if(!$config['aggregator']){
	echo json_encode("error");
	die();
}

//Check aggregator_id is current
if($aggregator_id != $config['aggregator']){
	$aggregator_id = $config['aggregator'];
}

//Insert the action
$add_action = db_insert('quickquote_counter');
$add_action->fields(array(
	'aggregator_id' => $aggregator_id,
	'type' => $type,
	'date' => time()
));
$add_action->execute();

//Get the number of calls
$num_calls = db_select('quickquote_counter', 'q');
$num_calls->fields('q', array('qqc_id'));
$num_calls->condition('type', 'open', '=');
$num_calls->condition('aggregator_id', $aggregator_id, '=');
$num_calls = $num_calls->execute();
$num_calls = $num_calls->rowCount();

//Get the number of covers
$num_cov = db_select('quickquote_counter', 'q');
$num_cov->fields('q', array('qqc_id'));
$num_cov->condition('type', 'close', '=');
$num_cov->condition('aggregator_id', $aggregator_id, '=');
$num_cov = $num_cov->execute();
$num_cov = $num_cov->rowCount();

//Update config
_rqqc_set_config_var('num_call', $num_calls);
_rqqc_set_config_var('num_closed', $num_cov);

//Get and return the updated config variable
echo json_encode(_rqqc_fetch_config());


/**
 *  HELPER FUNCTIONS
 */

//Drupal bootstraping function
function _bootstrap(){

    //Bootstrap drupal
    $path = $_SERVER['DOCUMENT_ROOT'];
    chdir($path);
    define('DRUPAL_ROOT', getcwd());
    $base_url = 'http://'.$_SERVER['HTTP_HOST'];
    require_once './includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    
    //Start session if necessary
    if(session_status() == PHP_SESSION_NONE || session_id() == ''){
        session_start();
    }

    //Include adpm tickets helper
    $base_path = drupal_get_path('module', 'rural_quickquote_counter');
    require_once($base_path.'/rural_quickquote_counter.module');

}

//HTML Error thrower
function _throw_error($errno, $msg = 'Nope'){
    header('HTTP/1.0 '.$errno.' '.$msg);
    die();
}

?>