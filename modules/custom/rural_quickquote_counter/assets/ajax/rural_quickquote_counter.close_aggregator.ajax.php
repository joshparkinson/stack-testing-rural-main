<?php

/**
 *  ACCESS CHECKS
 */ 

//Stop direct access to the file (ie non ajax calls)
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'){
    _throw_error('403', 'No direct access');
}

//Kill call if no aggregator_id passed
if(!isset($_POST['aggregator_id']) || intval($_POST['aggregator_id']) == 0){
	_throw_error('403', 'Invalid Params');
}

/**
 *	PROCESSING FUNCTIONS
 */

//Bootstrap drupal
_bootstrap();

//Store the aggregator id in a nice variable
$aggregator_id = intval($_POST['aggregator_id']);

//Close the aggregator
$close_aggregator = db_update('quickquote_aggregator');
$close_aggregator->fields(array(
	'closed' => time()
));
$close_aggregator->condition('qqa_id', $aggregator_id, '=');
$close_aggregator->execute();

//Set the new config variable
_rqqc_set_config_var('aggregator', FALSE);
_rqqc_set_config_var('num_call', 0);
_rqqc_set_config_var('num_closed', 0);


echo "success";

/**
 *  HELPER FUNCTIONS
 */

//Drupal bootstraping function
function _bootstrap(){

    //Bootstrap drupal
    $path = $_SERVER['DOCUMENT_ROOT'];
    chdir($path);
    define('DRUPAL_ROOT', getcwd());
    $base_url = 'http://'.$_SERVER['HTTP_HOST'];
    require_once './includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    
    //Start session if necessary
    if(session_status() == PHP_SESSION_NONE || session_id() == ''){
        session_start();
    }

    //Include adpm tickets helper
    $base_path = drupal_get_path('module', 'rural_quickquote_counter');
    require_once($base_path.'/rural_quickquote_counter.module');

}

//HTML Error thrower
function _throw_error($errno, $msg = 'Nope'){
    header('HTTP/1.0 '.$errno.' '.$msg);
    die();
}

?>