<?php

//Bootstrap drupal
$path = $_SERVER['DOCUMENT_ROOT'];
chdir($path);
define('DRUPAL_ROOT', getcwd());
$base_url = 'http://'.$_SERVER['HTTP_HOST'];
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//Start session if necessary
if(session_status() == PHP_SESSION_NONE || session_id() == ''){
    session_start();
}

//Fetch all aggregators
$aggregators = db_select('quickquote_aggregator', 'q');
$aggregators->fields('q');
$aggregators->condition('closed', NULL, 'IS NOT');
$aggregators->orderBy('qqa_id', DESC);
$aggregators = $aggregators->execute();

//Init output var
$output = '';

if($aggregators->rowCount() == 0){

	$output .= 'No previours aggregators found.';

}
else{

	while($aggregator = $aggregators->fetchAssoc()){

		//Get the number of calls
		$num_calls = db_select('quickquote_counter', 'q');
		$num_calls->fields('q', array('qqc_id'));
		$num_calls->condition('type', 'open', '=');
		$num_calls->condition('aggregator_id', $aggregator['qqa_id'], '=');
		$num_calls = $num_calls->execute();
		$num_calls = $num_calls->rowCount();

		//Get the number of covers
		$num_cov = db_select('quickquote_counter', 'q');
		$num_cov->fields('q', array('qqc_id'));
		$num_cov->condition('type', 'close', '=');
		$num_cov->condition('aggregator_id', $aggregator['qqa_id'], '=');
		$num_cov = $num_cov->execute();
		$num_cov = $num_cov->rowCount();

		//Get percentage
		if($num_cov == 0){
			$percentage = 0;
		}
		else{
			$percentage = round(($num_cov / $num_calls) * 100, 2);
		}

		$output .= '<div class="history-result">';
		$output .= '<div class="history-title">Aggregator #'.$aggregator['qqa_id'].'</h4></div>';
		$output .= '<div class="history-date">'.date('d/M/y H:i', $aggregator['open']).' - '.date('d M y  H:i', $aggregator['closed']).'</div>';
		$output .= '<div class="stat-line">Calls Recieved: <span>'.$num_calls.'</span></div>';
		$output .= '<div class="stat-line">On Cover: <span>'.$num_cov.'</span></div>';
		$output .= '<div class="stat-line">% Converted: <span>%'.$percentage.'</span></div>';
		$output .= '</div>';

	}

}

echo $output;

?>