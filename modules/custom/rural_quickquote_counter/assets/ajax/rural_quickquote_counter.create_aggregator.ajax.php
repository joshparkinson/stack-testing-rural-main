<?php

/**
 *  ACCESS CHECKS
 */ 

//Stop direct access to the file (ie non ajax calls)
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'){
    _throw_error('403', 'No direct access');
}

/**
 *	PROCESSING FUNCTIONS
 */

//Bootstrap drupal
_bootstrap();

//Fetch config
$config = _rqqc_fetch_config();

if(!$config['aggregator']){

    //Add the new aggregator to the database
    $add_aggregator = db_insert('quickquote_aggregator');
    $add_aggregator->fields(array('open'));
    $add_aggregator->values(array('open' => time()));
    $aggregator_id = $add_aggregator->execute();

    //Set the new config variable
    _rqqc_set_config_var('aggregator', $aggregator_id);
    _rqqc_set_config_var('num_call', 0);
    _rqqc_set_config_var('num_closed', 0);

    //Get the newest config var and return to it to the js
    echo json_encode(_rqqc_fetch_config());

}
else{
    echo json_encode($config);
}

/**
 *  HELPER FUNCTIONS
 */

//Drupal bootstraping function
function _bootstrap(){

    //Bootstrap drupal
    $path = $_SERVER['DOCUMENT_ROOT'];
    chdir($path);
    define('DRUPAL_ROOT', getcwd());
    $base_url = 'http://'.$_SERVER['HTTP_HOST'];
    require_once './includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    
    //Start session if necessary
    if(session_status() == PHP_SESSION_NONE || session_id() == ''){
        session_start();
    }

    //Include adpm tickets helper
    $base_path = drupal_get_path('module', 'rural_quickquote_counter');
    require_once($base_path.'/rural_quickquote_counter.module');

}

//HTML Error thrower
function _throw_error($errno, $msg = 'Nope'){
    header('HTTP/1.0 '.$errno.' '.$msg);
    die();
}

?>