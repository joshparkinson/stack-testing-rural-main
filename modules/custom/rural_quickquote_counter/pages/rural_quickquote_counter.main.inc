<?php

/* Counter Main Page */
function rural_quickquote_counter_page_main(){

	/**
	 *	PAGE INIT
	 */	

	//variable_del('quickquote_counter_config');

	//Get globals
	global $base_url;

	//Add necessary js
	$js_data = array();
	$js_data['module_path'] = $base_url."/".drupal_get_path('module', 'rural_quickquote_counter');
	drupal_add_js(array('rural_quickquote_counter_main' => $js_data), 'setting');
	drupal_add_js(drupal_get_path('module', 'rural_quickquote_counter').'/assets/js/rural_quickquote_counter.main.js');

	//Add necessary css
	drupal_add_css(drupal_get_path('module', 'rural_quickquote_counter').'/assets/css/rural_quickquote_counter.main.css');

	//Attempt to fetch aggregator config
	$config = _rqqc_fetch_config();

	//Determine container classes
	if(!$config['aggregator']){
		$create_class = 'show';
		$main_class = 'hide';
		$percentage = 0;
	}
	else{
		$create_class = 'hide';
		$main_class = 'show';
		if($config['num_closed'] == 0){
			$percentage = 0;
		}
		else{
			$percentage = round(($config['num_closed'] / $config['num_call']) * 100, 2);
		}
	}

	/**
	 *	OUTPUT PREPROCESSING
	 */

	//Init output var
	$output = '';

	//Build wrapper
	$output .= '<div class="clearfix">';

	//Build left side
	$output .= '<div class="left">';

	//Build the create aggregator container
	$output .= '<div class="create-container '.$create_class.'">';
	$output .= '<h3>No active aggregator found.</h3>';
	$output .= '<div class="create-aggregator button">Create Aggregator</div>';
	$output .= '</div>';

	//Build content container
	$output .= '<div class="main-container '.$main_class.'">';

	//Build aggeregator stats
	$output .= '<div class="aggregator-stats">';
	$output .= '<h3>Aggregator stats:</h3>';
	$output .= '<div class="aggregator-id stat-line clearfix">Aggregator ID: <span>#'.$config['aggregator'].'</span></div>';
	$output .= '<div class="aggregator-cr stat-line clearfix">Calls Recieved: <span>'.$config['num_call'].'</span></div>';
	$output .= '<div class="aggregator-cf stat-line clearfix">On Cover: <span>'.$config['num_closed'].'</span></div>';
	$output .= '<div class="aggregator-pc stat-line clearfix">% Converted: <span>%'.$percentage.'</span></div>';
	$output .= '</div>';

	//Build close aggregator
	$output .= '<div class="aggregator-close">';
	$output .= '<div class="close button" data-aggregator-id="'.$config['aggregator'].'">Stop Aggregator</div>';
	$output .= '</div>';

	//Build aggegator buttons
	$output .= '<div class="aggregator-actions">';
	$output .= '<div class="quote-open button" data-type="open" data-aggregator-id="'.$config['aggregator'].'">Call Received</div>';
	$output .= '<div class="quote-fin button" data-type="close" data-aggregator-id="'.$config['aggregator'].'">On Cover</div>';
	$output .= '</div>';

	//Close content container
	$output .= '</div>';

	//Close left
	$output .= '</div>';

	//Open right
	$output .= '<div class="right">';

	//Build overall stats
	$output .= '<div class="overall-container">';
	$output .= '<h3>Overall Stats</h3>';
	$output .= '<div class="overall-stats"></div>';
	$output .= '</div>';

	//Build history container
	$output .= '<div class="history-container">';
	$output .= '<h3>Aggregator History</h3>';
	$output .= '<div class="aggregator-history"></div>';
	$output .= '</div>';
	$output .= '</div>';

	//Close wrapper
	$output .= '</div>';

	/**
	 *	OUTPUT RENDER
	 */

	return $output;

}

?>