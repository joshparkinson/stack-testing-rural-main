<?php

/* SET FUNCTIONS */

//Using passed values update the counter config array
function _rqqc_set_config_var($attr, $val){
	$config = _rqqc_fetch_config();
	$config[$attr] = $val;
	variable_set('quickquote_counter_config', $config);
}

/* FETCH FUNCTIONS */

//Fetch and return the config variable
//If the config variable could not be fetched, set it, return it
function _rqqc_fetch_config(){
	
	//Attempt to fetch the config variable	
	$config = variable_get('quickquote_counter_config', FALSE);

	//If config could not be fetched, create it and set it
	if(!$config){

		//Create config var
		$config = array(
			'aggregator' => FALSE,
			'num_call' => 0,
			'num_closed' => 0
		);

		//Save the counter config variable
		variable_set('quickquote_counter_config', $config);

	}

	//Return the config variable
	return $config;

}

?>