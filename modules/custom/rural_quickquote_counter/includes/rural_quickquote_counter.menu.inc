<?php

//Implements hook_menu()
function rural_quickquote_counter_menu(){

	$items = array();

	/** COUNTER MAIN PAGE **/

	$items['r/qq/c/main'] = array(
		'title' => 'QuickQuote',
        'page callback' => 'rural_quickquote_counter_page_main',
        'file' => 'pages/rural_quickquote_counter.main.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
	);

	return $items;

}

?>