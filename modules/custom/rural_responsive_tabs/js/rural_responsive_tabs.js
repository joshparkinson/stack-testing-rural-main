(function($) {
    Drupal.behaviors.rural_responsive_tabs = {
        attach: function(context, settings) {
            var tabID = '#' + settings.rural_responsive_tabs.containerID + '-responsive-tabs';
            $(tabID).once('responsive-tabs', function(){
                $(tabID).easyResponsiveTabs();
            });
        }
    };
})(jQuery);