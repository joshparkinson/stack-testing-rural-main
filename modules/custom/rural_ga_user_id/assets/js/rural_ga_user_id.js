(function($){
    Drupal.behaviors.RuralGAUserID = {
        attach: function(context, settings) {

            $(document).ready(function(){
                if(typeof(ga) == "function"){
                    var gaUserId = settings.rural_ga_user_id.gaUserId;
                    ga('set', 'userId', gaUserId.toString());
                }
            });

        }
    };
})(jQuery);
