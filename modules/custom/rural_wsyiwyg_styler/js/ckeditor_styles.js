CKEDITOR.stylesSet.add('mycustomstyleset',
  [
    { name : 'Red', element : 'span', styles : {'color' : 'red' } },
    { name : 'CSS Style', element : 'span', attributes: { 'class' : 'my_style' } },
    { name : 'Marker: Yellow', element : 'span', styles : { 'background-color' : 'Yellow' } },
    { name : 'Heading 4' , element : 'h4' },
    { name : 'Blue Button', element : 'div', attributes : { 'class' : 'blue-button' } },
  ]);