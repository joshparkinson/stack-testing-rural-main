(function ($) {
    $(document).ready(function (){
        var mobile_menu = $("nav#mobile-menu");

        $(mobile_menu).mmenu({
            offCanvas: {
                position  : "right"
            }
        });

        $(mobile_menu).once('post-load', function(){
            $(mobile_menu).prepend('<div id="menu-user-details-wrapper"></div>');

            $.ajax({
                type: 'POST',
                url: '/rural_mobile_menu/ajax/load_user_details_block',
                dataType: 'json',
                success: function(data) {
                    if(data.output.length > 0){
                        $("#menu-user-details-wrapper").html(data.output);
                        $(mobile_menu).addClass('has-user-details');
                    }
                }
            });

            $(mobile_menu).append(
                '<div class="mobile-menu-info">' +
                '<p class="phone-number">0344 55 77 177</p>' +
                '<p class="opening-times">08:30 - 17:30 | Mon-Fri</p>' +
                '</div>'
            );
        });

    });
})(jQuery);