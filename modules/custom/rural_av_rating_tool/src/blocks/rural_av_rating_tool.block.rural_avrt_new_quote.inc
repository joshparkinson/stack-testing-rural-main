<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Builds the Rural AV Rating Tool new quote block.
 *
 * Block delta: rural_avrt_new_quote
 * Defined in:  src/hooks/rural_av_rating_tool.block.inc
 *
 * @return string The html representation of this blocks content
 */
function rural_avrt_new_quote_block_content(){
    $output = '<div class="title-block">';
    $output .= '<h3>Put your client’s agricultural vehicles on cover in <span>minutes</span> with an online <span>quick quote</span></h3>';
    $output .= '</div>';

    $output .= '<div class="intro">';
    $output .= '<p>Our quick quote tool is a fast and hassle free way to cover agricultural vehicles. Designed for an unlimited number of vehicles, we\'ll get you from quote to cover in minutes.</p>';
    $output .= '<a href="/'.RURAL_AV_RATING_TOOL_QUOTE_PAGE_URL.'">Get a quote</a>';
    $output .= '</div>';

    $output .= '<div class="covers">';
    $output .= '<ul>';
    $output .= '<li>Get an instant quote for single or multiple agri-vehicle risks</li>';
    $output .= '<li>Covers most agri-vehicle types including tractors, combine harvesters, forklift trucks, diggers, trailers, ATV’s and more</li>';
    $output .= '<li>Cover for farming only and agricultural contracting use up to <span>20%</span> of turnover</li>';
    $output .= '<li>Single vehicle value up to <span>£100,000</span></li>';
    $output .= '<li>Apply up to a <span>'.RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT.'%</span> commercial discount to our quoted premium to help you win business</li>';
    $output .= '<li>Premiums quoted inclusive of your standard Farm Motor commission with Rural Insurance</li>';
    $output .= '<li>Place your customer on cover instantly through our Covernotes system*</li>';
    $output .= '</ul>';
    $output .= '</div>';

    $output .= '<div class="caveat">';
    $output .= '<p><span>Please note:</span> We are unable to quote on Agriculutral Vehicle risks within Northern Ireland through our quick quote tool.</p>';
    $output .= '<p>* Requires Covernotes account</p>';
    $output .= '</div>';


    return $output;
}