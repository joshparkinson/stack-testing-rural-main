<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

//todo: comment
function rural_avrt_saved_quotes_block_content(){
    if(!user_is_logged_in()){
        return false;
    }

    rural_av_rating_tool_helper_modal_initialise_modals();

    global $user;

    $tableHeader = array(
        array('data' => 'Quote Ref', 'field' => 'id'),
        array('data' => 'Client', 'field' => 'client_name'),
        array('data' => 'Premium', 'field' => 'premium'),
        array('data' => 'Status', 'field' => 'status'),
        array('data' => 'Created', 'field' => 'created', 'sort' => 'desc'),
        array('data' => 'Actions', 'colspan' => 2, 'class' => 'center end'),
    );

    $submissionQuery = db_select('unity_av_rating_submissions', 's')->extend('PagerDefault')->extend('TableSort');
    $submissionQuery->addField('s', 'id');
    $submissionQuery->addField('s', 'client_name');
    $submissionQuery->addField('s', 'premium');
    $submissionQuery->addField('s', 'status');
    $submissionQuery->addField('s', 'created');
    $submissionQuery->addField('s', 'updated');
    $submissionQuery->condition('user_id', $user->uid, '=');
    $submissionQuery->condition('deleted', '0', '=');
    $submissionQuery->limit(15);
    $submissionQuery->orderByHeader($tableHeader);
    $savedSubmissions = $submissionQuery->execute();

    $tableRows = array();
    while($submission = $savedSubmissions->fetchAssoc()){

        $quoteRef = rural_av_rating_tool_helper_generate_quote_ref_from_int($submission['id']);

        $editPath = RURAL_AV_RATING_TOOL_QUOTE_PAGE_URL.'/'.$quoteRef;
        $editLink = ($submission['status'] == RURAL_AV_RATING_TOOL_STATUS_OPEN) ? l(t('Edit'), $editPath) : l(t('View'), $editPath);

        $deletePath = '/unity/av-rating-tool/delete/'.$quoteRef.'/nojs';
        $deleteLink = ctools_modal_text_button('Delete', $deletePath, 'Delete', 'ctools-modal-rural-av-rating-tool-modal');

        $client = (!empty($submission['client_name'])) ? $submission['client_name'] : 'N/A';
        $clientClass = (!empty($submission['client_name'])) ? 'client_name' : 'client_name default';

        $premium = 'N/A';
        $premiumClass = 'premium default';
        if(!empty($submission['premium'])){
            $premiumClass = 'premium';
            $submissionPremium = unserialize($submission['premium']);
            $premium = '£'.number_format((float)$submissionPremium['final_premium'], 2, '.', ',');
        }

        $tableRows[] = array(
            array('data' => $quoteRef),
            array('data' => $client, 'class' => $clientClass),
            array('data' => $premium, 'class' => $premiumClass),
            array('data' => ucfirst(strtolower($submission['status'])), 'class' => 'status '.strtolower($submission['status'])),
            array('data' => format_date($submission['created'], 'custom', 'd/m/y H:i')),
            array('data' => $editLink, 'class' => 'center end'),
            array('data' => $deleteLink, 'class' => 'center end'),
        );
    }

    if(empty($tableRows)){
        $tableRows[] = array(
            array('data' => 'No saved submissions', 'colspan' => 6)
        );
    }

    $output = '<h3>Recent saved submissions</h3>';
    $output .= theme('table', array(
        'header' => $tableHeader,
        'rows' => $tableRows,
        'sticky' => FALSE,
        'attributes' => array('class' => array('av-rating-tool-table'))
    )).theme('pager');

    $output .= '<p class="caveat">Our prices are calculated in real time and as such may be subject to change. If you return at a later date the price quoted today may differ.</p>';

    return $output;
}