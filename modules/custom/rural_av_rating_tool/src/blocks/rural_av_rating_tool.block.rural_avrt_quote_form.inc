<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Builds the Rural AV Rating Tool quote form block.
 *
 * Block delta: rural_avrt_quote_form
 * Defined in:  src/hooks/rural_av_rating_tool.block.inc
 *
 * @return string The html representation of this blocks content
 */
function rural_avrt_quote_form_block_content(){
    if(path_is_admin(current_path())){
        return "AV Rating Tool: Quote Form Block";
    }

    global $user;
    module_load_include('inc', 'rural_av_rating_tool', 'src/forms/rural_av_rating_tool.form.quote.master');

    drupal_add_library('system', 'effects.bounce');

    $quoteData = array();
    if(drupal_match_path(drupal_get_path_alias(), RURAL_AV_RATING_TOOL_RELOAD_QUOTE_URL_PATTERN)){

        $pathParts = explode('/', drupal_get_path_alias());
        if(sizeof($pathParts) > RURAL_AV_RATING_TOOL_RELOAD_QUOTE_SEGMENTS){
            drupal_goto('unity/av-rating-tool/quote/'.$pathParts[RURAL_AV_RATING_TOOL_RELOAD_QUOTE_WILDCARD_POS]);
        }

        $quoteData = rural_av_rating_tool_helper_get_quote_from_ref($pathParts[RURAL_AV_RATING_TOOL_RELOAD_QUOTE_WILDCARD_POS], $user->uid);
        if(!$quoteData){
            drupal_goto(RURAL_AV_RATING_TOOL_QUOTE_PAGE_URL);
        }
    }

    $avRatingQuoteForm = drupal_get_form('rural_av_rating_tool_quote_form', $quoteData);

    $output = "<noscript>Please enable javascript to use this tool</noscript>";
    $output .= "<div id='av-rating-tool-form-wrapper' class='hidden'>";
    $output .= drupal_render($avRatingQuoteForm);
    $output .= "</div>";

    return $output;
}