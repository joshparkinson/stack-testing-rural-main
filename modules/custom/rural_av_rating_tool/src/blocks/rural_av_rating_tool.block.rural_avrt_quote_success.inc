<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

function rural_avrt_quote_sucess_block_content(){
    global $user;
    $quoteReference = (!empty($_GET['ref'])) ? $_GET['ref'] : 'Unknown';

    $output = '<h2>Thank you for your submission</h2>';

    $output .= '<div class="quote-ref">';
    $output .= '<p>Quote reference: '.$quoteReference.'</p>';
    $output .= '</div>';

    $output .= '<div class="confirmation">';
    $output .= '<p>Your submission has been saved to your Unity account and confirmation of receipt has been sent to <span>'.$user->mail.'</span></p>';
    $output .= '</div>';

    return $output;
}