<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Callback Function.
 * Show global messages above the AV Rating Tool Form.
 *
 * The following function is used to show any non section specific
 * messages within the AV Rating Tool Quote Form. It should be used
 * to show global error messages and success responses etc etc.
 * Any section specific messages should be shown using the function:
 * rav_callback_messages_show_section_messages().
 *
 * Messages to display should be sent to this function as an array
 * key'd by the message type as expected by 'drupal_set_message()'.
 * The following message types are available:
 * - status, warning, error.
 *
 * If no messages are specified this function will render any status messages
 * already stored within the drupal status message system.
 *
 * If specified ($clearPreviousMessages) any previous messages
 * held within drupals status message system will be removed.
 * Removal is triggered via calling: drupal_get_messages()
 * These previous messages are removed as a default.
 *
 * @param array $messages               An array of messages to show key'd by the message type
 * @param bool  $clearPreviousMessages  Whether to clear any previous status messages
 *
 * @return array The ajax command definition array for use within the drupal form api.
 */
function rav_callback_messages_show_form_messages($messages = array(), $clearPreviousMessages = TRUE){
    if($clearPreviousMessages){
        drupal_get_messages();
    }

    foreach($messages as $messageType => $message){
        drupal_set_message($message, $messageType);
    }

    $commands = array(
        ajax_command_invoke(NULL, 'avrt_showFormMessages', array(theme('status_messages')))
    );

    drupal_get_messages();

    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Rural AV Rating Tool Callback Function.
 * Show section (element) specific form messages.
 *
 * The following function is used to display and stored status messages
 * within a section of the Rural AV Rating Tool. The function takes in
 * a section id parameter which is used to build the necessary element
 * selector which we pass to a javascript callback. avrt_showSectionMessages.
 *
 * We also allow the remove of any status messages after they have been
 * displayed within a form section via passing a boolean as a parameter.
 *
 * If messages are to be shown somewhere different to the default location
 * (after the sections introdcution) then an overriding element selector
 * can be passed to this function and the messages will be displayed
 * after that instead.
 *
 * @param string $sectionId                 The id of the section to show the messages in
 * @param bool   $clearMessagesAfterDisplay Wheteher or not to clear messages after showing them.
 * @param string $overridingElementSelector A jQuery ready element selector to use instead of the of the
 *                                          default created selector.
 *
 * @return array The ajax command definition array for use within the drupal form api.
 */
function rav_callback_messages_show_section_messages($sectionId, $clearMessagesAfterDisplay = TRUE, $overridingElementSelector = NULL){
    $commands = array();

    $elementSelector = (empty($overridingElementSelector)) ? '#edit-'.str_replace('_', '-', $sectionId).'-content .section-content-intro' : $overridingElementSelector;

    $commands[] = ajax_command_invoke(NULL, 'avrt_showSectionMessages', array(
        $elementSelector,
        theme('status_messages')
    ));

    if($clearMessagesAfterDisplay){
        drupal_get_messages();
    }

    return array('#type' => 'ajax', '#commands' => $commands);
}