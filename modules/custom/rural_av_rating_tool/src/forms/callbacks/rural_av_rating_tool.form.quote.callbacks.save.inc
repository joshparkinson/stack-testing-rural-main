<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Callback Function.
 * The save submission callback function.
 *
 * The following function is the last function called in the
 * 'save' button press function chain. This function will be
 * called after the save submissions '#validate' and '#submit'
 * functions have been ran as definined in the buttons form
 * definition array.
 *
 * This function will first check the form submission is not in a
 * state of referral. (As should all action buttons within this
 * tool.) If it is we must display the referral modal so that the
 * user knows there is action to take. If however the form is not
 * in a state of referral we trigger the display of the 'saved'
 * submission modal. Using the function: rav_callback_modal_display_modal().
 *
 * We pass the trigger to the modal which is 'save' and also the
 * generated quote reference for this submission. To generate the quote
 * reference we must first extract it from the drupal form state array
 * before passing it to the helper function which will generate the
 * reference from the stored integer id.
 *
 * @param array $form       The drupal form defintion array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax callbacks for drupal's form api to fire.
 */
function rural_av_rating_tool_save_callback($form, &$form_state){
    if(!empty($referralSection = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralSection);
    }

    $quoteReference = rural_av_rating_tool_helper_generate_quote_ref_from_int(rav_helper_get_id_from_storage($form_state));
    $quoteUrl = '/'.str_replace('*', $quoteReference, RURAL_AV_RATING_TOOL_RELOAD_QUOTE_URL_PATTERN);

    $ajaxCommands =  rav_callback_modal_display_modal('save', array(
        'quoteRef' => $quoteReference
    ));

    $ajaxCommands['#commands'][] = ajax_command_invoke(NULL, 'avrt_updatePageUrl', array($quoteUrl));

    return $ajaxCommands;
}

/**
 * Rural AV Rating Tool Callback Function.
 * The save submission submit handler function.
 *
 * The following function is called when a 'save' button is pressed
 * within the Rural AV Rating Tool form. It will only be called if
 * all validation passes as defined against the save button in its
 * '#validate' array.
 *
 * This function is used to persist any current submission data to the
 * database so that it can be revisted and continued as and when the user
 * feels necessary.
 *
 * First we must check the form is in a state where we process submit
 * handlers. This is done using the function: rav_helper_submit_can_process_submit
 * If this check fails we end the function call and any further
 * processing of the save function.
 *
 * If the form is in a state where we can process submission we proceed
 * to grab data from the form state storage array. We extract it's
 * 'current stage' the 'client name' if it has been set and the premium
 * data if that section has been reached. Based on the users current
 * position within the form we all grab any unsubmitted values from the
 * forms inputs and save them into the database.
 *
 * After all data has been stored to add/update the submission within the
 * database we then make a check to see if we need to add a new submission
 * record or whether we updating an already saved one. This is done by
 * checking the form states storage id attribut and if an id is found we
 * double check a quote exists with that id.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether the function ran successfully or not.
 */
function rural_av_rating_tool_save_section_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }

    global $user;

    //Grab the storage data
    $currentStage = $form_state['storage']['steps']['current'];

    //If not on the vehicle details stage, grab the values currently in the inputs
    if($currentStage != 'vehicle_details'){

        //If we're on the proposer details section, deal with that pesky date input, normalise it.
        if($currentStage == 'proposer_details'){
            $form_state['input'][$currentStage]['content']['cover_start_date'] = (!empty($form_state['input'][$currentStage]['content']['cover_start_date']['date']))
                ? DateTime::createFromFormat('d/m/y', $form_state['input'][$currentStage]['content']['cover_start_date']['date'])->format('Y-m-d')
                : NULL;
        }

        if(!empty($form_state['input'][$currentStage]['content'])){
            $form_state['storage']['values'][$currentStage] = $form_state['input'][$currentStage]['content'];
        }
    }

    //If vehicle details are stored in the form state add them to the storage values
    if(!empty($form_state['vehicles'])){
        $form_state['storage']['values']['vehicle_details']['vehicles'] = $form_state['vehicles'];
    }

    //Grab the storage data
    $data = $form_state['storage'];
    $clientName = (!empty($data['values']['proposer_details']['proposer_name'])) ? $data['values']['proposer_details']['proposer_name'] : NULL;
    $premium = (!empty($data['premium'])) ? serialize($data['premium']) : NULL;

    //Update a submission if the form state has an id, and this id is a valid quote id.
    if(!empty($data['id']) && rural_av_rating_tool_helper_quote_exists_with_id($data['id'], $user->uid)){
        $updateQuery = db_update(RURAL_AV_RATING_TOOL_DB_TABLE);
        $updateQuery->fields(array(
            'client_name' => $clientName,
            'premium' => $premium,
            'form_data' => serialize($data),
            'updated' => time(),
        ));
        $updateQuery->condition('id', $data['id']);
        $updateQuery->condition('user_id', $user->uid);
        $updateQuery->execute();

        return TRUE;
    }

    //Not updating a submission, so add a new
    //set the returned insert id to the form.
    $quoteId = db_insert(RURAL_AV_RATING_TOOL_DB_TABLE)
        ->fields(array(
            'user_id' => $user->uid,
            'client_name' => $clientName,
            'premium' => $premium,
            'form_data' => serialize($data),
            'status' => 'open',
            'created' => time(),
            'updated' => time(),
        ))
        ->execute();

    rav_helper_storage_set_id($quoteId, $form_state);
    return TRUE;
}