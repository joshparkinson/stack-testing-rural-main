<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Callback Function.
 * Show a read only submission callback.
 *
 * The following function is used to trigger the display of
 * a av rating tool form submission that is in a read only
 * state. It is defined as an ajax callback within the
 * 'show submission' button of a readonly form.
 *
 * NOTE: All callbacks should implement a check for a referral
 * trigger stored against the form submission however as this
 * callback is only used to show forms that are in a read only state
 * there is no reason to check for the referral trigger as no
 * processing can take place on the form.
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax commands to be invoked by drupal's form api.
 */
function rav_callback_readonly_show_readonly_submission_callback($form, &$form_state){
    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_toggleReadOnlyForm');
    return array('#type' => 'ajax', '#commands' => $commands);
}