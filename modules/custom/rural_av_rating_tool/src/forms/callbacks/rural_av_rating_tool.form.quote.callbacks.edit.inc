<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Callback Function.
 * The Edit button press ajax callback handler.
 *
 * The following is the last function called in the 'Edit' button press
 * 'function chain'. When the edit button is pressed the form will first
 * process any #validation and #submit handlers defined against it in its
 * form definition array. Regardless of validation errors this function will
 * run even if validation fails for the button and the submit handlers are
 * not triggered.
 *
 * As with all of the action callbacks defined within the AV rating tool the
 * first thing we must do is check there is no referral reason currently set
 * against the form/submission. If there is a referral reason set, we pass
 * that over to the modal callback function: rav_callback_modal_display_modal()
 * which will process the trigger and display the relevant modal.
 *
 * Assuming there is no referral modal to show we can process the rest of the
 * edit function callback. Firstly we check the edit button pressed has the
 * required data attribute set against it (data-section-id-segment). If that
 * is not set we end the call, if it is however we trigger the custom jQuery
 * method as definined in: ./assets/js/rural_av_rating_tool.form.callback_methods.js
 * to carry out the required animations etc to display the new section which
 * is to be edited.
 *
 * NOTE: No form state storage manipulation is carried out within this callback
 * all of that is handled within the 'edit' butonns submit handler.
 *
 * @param $form
 * @param $form_state
 *
 * @return array|bool
 */
function rural_av_rating_tool_edit_section_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state)) && $form_state['storage']['steps']['current'] == $referralTrigger){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    $commands = array();

    $idSegment = (!empty($form_state['triggering_element']['#attributes']['data-section-id-segment'])) ? $form_state['triggering_element']['#attributes']['data-section-id-segment'] : FALSE;

    if(!$idSegment){
        return rav_callback_messages_show_form_messages(array(
            'error' => 'Error processing edit of section',
        ));
    }

    $commands[] = ajax_command_invoke(NULL, 'avrt_editSection', array($idSegment));
    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Rural AV Rating Tool Callback Function.
 * The Edit button press submit handler.
 *
 * The following function is called when an 'Edit' button is pressed
 * on the AV Rating Tool form and any validation assigned to that button
 * has passed. As definined in its form definition array.
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * This function does nothing more than set the new current step value
 * within the forms storage array. This will only happen however if the
 * form is in an editiable state as detailed above and that the edit
 * button pressed has the required data attribute attached to it.
 * data-section.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array.
 *
 * @return bool Whether the submit handler was completed successfully or not.
 */
function rural_av_rating_tool_edit_section_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }

    if(empty($form_state['triggering_element']['#attributes']['data-section'])){
        return FALSE;
    }

    $form_state['storage']['steps']['current'] = $form_state['triggering_element']['#attributes']['data-section'];
    return TRUE;
}