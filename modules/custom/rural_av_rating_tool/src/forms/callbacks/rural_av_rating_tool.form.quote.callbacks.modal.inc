<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Display a modal.
 *
 * The following function is used to trigger the display of the
 * modal. It can be called from various area of the form
 * and is used to display any information required to be shown
 * above and outside of the form DOM. For example a reason for
 * referral or a save notification
 *
 * It defines drupal ajax commands to fire as part of a form sections
 * AJAX callback. It first clears any set system messages (that would
 * be displayed) on page reload. It then calls on the custom jQuery function
 * 'avrt_displayModal' as definined in:
 * ./assets/js/rural_av_rating_tool.form.callback_methods.js
 *
 * @param string $trigger The trigger for the modal used in its page callback
 *                        to display the correct information
 *
 * @return array An array of ajax commands for drupal to run
 */
function rav_callback_modal_display_modal($trigger, $paramaters = array()){
    drupal_get_messages();

    $parameterString = '';
    if(!empty($paramaters)){
        $parameterString = '?';
        foreach($paramaters as $key => $val){
            $parameterString .= $key.'='.$val.'&';
        }
        $parameterString = rtrim($parameterString, '&');
    }

    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_displayModal', array($trigger, $parameterString));
    return array('#type' => 'ajax', '#commands' => $commands);
}