<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Callback Function.
 * Process the movement from section to another via 'next'
 * button click.
 *
 * The following function is the last function to be called in the 'next'
 * button press function chain. It is called after the next buttons
 * '#validate' and '#submit' handlers have been run as defined within
 * the 'next' button's form definition array. Note: This callback
 * will run on every button press even if validation has failed etc
 * therefore we must check for errors in this function and carry out
 * the relevant actions.
 *
 * This function first checks (as all other actions should do) whether
 * or not the current form submission is in a state of referral. If it
 * is we grab the forms referral trigger and send it through the modal
 * callback function used to display the modal:
 * rav_callback_modal_display_modal()
 *
 * All buttons implementing this callback must provide the data
 * attributes: 'data-next-section-id-segment' and 'data-next-section-id-segment'
 * If either of these attributes could not be fetched we show a global
 * form message saying we couldn't process the move to the next section
 * of the form. To achieve this we leverage the following messages callback
 * function: rav_callback_messages_show_form_messages().
 * Passing it the message to show and its type, in this case an error.
 *
 * As mentioned this callback is triggered regardless of whether the section
 * has errors or not. So we check if the form has errors, using the
 * function: form_get_errors().
 * If the form does have errors we must show them within the form section
 * using the message callback function: rav_callback_messages_show_section_messages
 * Before calling this we must build our element selector to tell our form
 * what we need to show the messages after.
 *
 * Assuming the form is not in a state of referral and there are no form errors
 * to show, we trigger the movement onto the next section using the buttons
 * data attributes (which we ahve already checked exist).
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form statearray
 *
 * @return array
 */
function rav_callback_next_move_to_next_section_callback($form, &$form_state){
    if(!empty($referralSection = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralSection);
    }

    $nextSectionIdSegment = (!empty($form_state['triggering_element']['#attributes']['data-next-section-id-segment'])) ? $form_state['triggering_element']['#attributes']['data-next-section-id-segment'] : FALSE;
    $currentSectionIdSegment = (!empty($form_state['triggering_element']['#attributes']['data-current-section-id-segment'])) ? $form_state['triggering_element']['#attributes']['data-current-section-id-segment'] : FALSE;


    if(!$nextSectionIdSegment || !$currentSectionIdSegment){
        return rav_callback_messages_show_form_messages(array(
            'error' => 'Error processing move next section',
        ));
    }

    if(form_get_errors()){
        return rav_callback_messages_show_section_messages($currentSectionIdSegment);
    }


    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_moveToNextSection', array($currentSectionIdSegment, $nextSectionIdSegment));
    return array('#type' => 'ajax', '#commands' => $commands);
}