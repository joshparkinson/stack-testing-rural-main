<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/*
 * todo: add, we have saved this submission with quote reference xxx
 * todo: add quote reference into ajax callback in vehicles details
 */

/**
 * The premium section of the AV Rating Tool Form
 *
 * todo: comment properly
 *
 *
 * @param array $form_state The drupal form state array
 *
 * @return array The premium form definition to be merged into
 *                the master form.
 */
function rural_av_rating_tool_quote_form_premium($form, &$form_state){
    $quoteReference = rural_av_rating_tool_helper_generate_quote_ref_from_int(rav_helper_get_id_from_storage($form_state));

    $discountValue = (isset($form_state['storage']['premium']['discount'])) ? $form_state['storage']['premium']['discount'] : NULL;
    if(isset($discountValue) && $discountValue > RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT && rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){
        rav_helper_premium_generate_initial_premium($form_state, RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT);
        rural_av_rating_tool_save_section_submit($form, $form_state);
    }

    $initPremium = (!empty($form_state['storage']['premium']['init_premium'])) ? $form_state['storage']['premium']['init_premium'] : '0.00';
    if($initPremium != '0.00' && rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN) && rav_helper_storage_check_section_active($form_state, 'premium')){
        rav_helper_premium_generate_final_premium($initPremium, $form_state);
        rural_av_rating_tool_save_section_submit($form, $form_state);
    }

    $finalPremium = (!empty($form_state['storage']['premium']['final_premium'])) ? $form_state['storage']['premium']['final_premium'] : '0.00';
    $iptPercent = (!empty($form_state['storage']['premium']['ipt'])) ? $form_state['storage']['premium']['ipt'] : (rav_helper_premium_get_ipt_value(time()) * 100);
    $iptValue = (!empty($form_state['storage']['premium']['ipt_value'])) ? $form_state['storage']['premium']['ipt_value'] : '0.00';
    $ruralFee = (!empty($form_state['storage']['premium']['rural_fee'])) ? $form_state['storage']['premium']['rural_fee'] : RURAL_AV_RATING_TOOL_RATING_RURAL_FEE;
    $discountValue = (isset($form_state['storage']['premium']['discount'])) ? $form_state['storage']['premium']['discount'] : NULL;
    $dicountDisplayValue = ($discountValue != NULL) ? $discountValue.'%' : 'None';

    $form = array();
    $form['premium'] = rav_builder_global_build_section_container('premium', $form_state);
    $form['premium']['top'] = rav_builder_global_build_section_top('premium', '04. Premium', $form_state);
    $form['premium']['content'] = rav_builder_global_build_section_content('premium');

    $form['premium']['content']['display'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#attributes' => array(
            'class' => array('premium-display')
        ),
    );

    $form['premium']['content']['display']['breakdown'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="premium-breakdown">
        <div class="breakdown">
            <h2>Your Agricultural Vehicle quote</h2>
            <p class="final-premium">£<span class="val">'.number_format((float)$finalPremium, 2, '.', ',').'</span></p>
            <p>The quote provided is inclusive of broker commission at your standard Farm Motor rate.</p>
            <p>Our prices are calculated in real time and as such may be subject to change. If you return at a later date the price quoted today may differ.</p>
            <p class="quote-reference">This quote has been saved automatically with reference: <span class="val">'.$quoteReference.'<span></p>
        </div>
        <div class="breakdown-meta clearfix">
            <p class="total-pre-ipt">exc. IPT: £<span class="val">'.number_format((float)$initPremium, 2, '.', ',').'<span></p>
            <p class="ipt-value">IPT @ <span class="percent">'.$iptPercent.'</span>%: £<span class="val">'.number_format((float)$iptValue, 2, '.', ',').'</span></p>
            <p class="fee">Fee: £<span class="val">'.number_format((float)$ruralFee, 2, '.', ',').'</span></p>
            <p class="discount">Discount: <span class="val">'.$dicountDisplayValue.'</span></p>
        </div>
        </div>'

    );

    $form['premium']['content']['display']['discount'] = array(
        '#type' => 'container',
        '#attributes' => array(
            'class' => array('discount-container', 'clearfix')
        ),
    );

    $form['premium']['content']['display']['discount']['info'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="discount-info"><h3>Broker Trading Discount</h3>
        <p>If required, you may apply a commercial discount of up to '.RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT.'%</p></div>'
    );

    $form['premium']['content']['display']['discount']['value'] = array(
        '#title' => 'Discount',
        '#type' => 'textfield',
        '#maxlenght' => 1,
        '#attributes' => array(
            'class' => array('no-floatlabel')
        ),
        '#default_value' => $discountValue,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){

        $form['premium']['content']['display']['discount']['apply'] = array(
            '#type' => 'submit',
            '#name' => 'premium_apply_discount',
            '#value' => 'Apply',
            '#ajax' => array(
                'callback' => 'rural_av_rating_tool_premium_apply_discount_callback',
            ),
            '#limit_validation_errors' => array(
                array('avrt_premium_invalid_discount'),
            ),
            '#validate' => array('rural_av_rating_tool_premium_apply_discount_validate'),
            '#submit' => array('rural_av_rating_tool_premium_apply_discount_submit'),
        );

    }

    //todo: uncomment to show proper details breakdown
    //$proposerDetailsData = rav_helper_get_storage_values('proposer_details', $form_state);
    //$form['premium']['content']['proposer_details_breakdown'] = rav_builder_premium_build_proposer_details_breakdown_form_definition($proposerDetailsData);

    //Build the vehicle premium breakdown
    $vehicleData = rav_helper_get_storage_values('vehicle_details', $form_state);
    $vehiclePremiumData = (!empty($vehicleData['vehicles'])) ? $vehicleData['vehicles'] : array();
    $form['premium']['content']['vehicle_breakdown'] = rav_builder_premium_build_vehicle_premium_breakdown_form_definition($vehiclePremiumData);

    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){

        $form['premium']['content']['actions'] = rav_builder_global_build_section_actions('premium', array(), FALSE);

        $form['premium']['content']['actions']['back'] = array(
            '#type' => 'submit',
            '#name' => 'premium_go_back',
            '#value' => 'Back',
            '#attributes' => array(
                'class' => array('left', 'premium-back', 'action-back'),
            ),
            '#ajax' => array(
                'callback' => 'rural_av_rating_tool_premium_back_callback'
            ),
            '#limit_validation_errors' => array(),
            '#submit' => array('rural_av_rating_tool_premium_back_submit')
        );

        $form['premium']['content']['actions']['submit'] = array(
            '#type' => 'submit',
            '#name' => 'premium_submit_for_cover',
            '#value' => 'Submit for cover',
            '#attributes' => array(
                'class' => array('action-confirm')
            ),
            '#ajax' => array(
                'callback' => 'rural_av_rating_tool_submit_for_cover_callback'
            ),
            '#limit_validation_errors' => array(
                array('avrt_user_data_missing'),
                array('avrt_submit_for_cover_error')
            ),
            '#validate' => array('rural_av_rating_tool_submit_for_cover_validate'),
            '#submit' => array('rural_av_rating_tool_submit_for_cover_submit')
        );

    }

    return $form;

}

/**
 * todo: comment
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * @param $form
 * @param $form_state
 *
 * @return bool
 */
function rural_av_rating_tool_premium_back_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }
    $form_state['storage']['steps']['current'] = 'vehicle_details';
    return TRUE;
}

/**
 * AV Rating Tool: Premium section back button callback
 *
 * The following function is triggered after the back button is pressed on
 * the premium section (defined in this file) of the AV Rating Tool Form.
 * It will invoke the custom jquery function used to hide the premium
 * section of the form and show all of the other sections in a non displayed
 * but editable state.
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax commands used to invoke the back button
 *               functionality.
 */
function rural_av_rating_tool_premium_back_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }
    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_backFromPremium');
    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * AV Rating Tool: Premium discount application validation function.
 *
 * The following function is the first function to be called when a user attempts
 * to apply a discount to the premium that has been generated by the AV Rating
 * Tool. It will first check that the input (value) provided is a number, if it
 * isnt it will stop processing and define an error. If the value provided is a
 * number however we will check that the value entered isn't higher than the
 * maximum commercial discount. If it is we stop processing and throw an error.
 *
 * Any errors thrown in this function will be picked up and dealt with within
 * the premium discount application ajax callback function
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 */
function rural_av_rating_tool_premium_apply_discount_validate($form, &$form_state){
    if(!isset($form_state['values']['premium']['content']['display']['discount']['value'])){
        form_set_error('avrt_premium_invalid_discount', 'Invalid discount');
    }
    $discountValue = $form_state['values']['premium']['content']['display']['discount']['value'];
    if(!is_numeric($discountValue) || $discountValue < 0 || $discountValue > RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT){
        form_set_error('avrt_premium_invalid_discount', 'Invalid discount');
    }
}

/**
 * AV Rating Tool: Premium discount application submission function.
 *
 * The following function is the second function to be called when a user
 * attempts to apply a discount to a generated premium. This function will
 * only be called if the discount amount entered is a valid one.
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * Note: We only update/regenerate the premium if the discount to applied
 * is different to the one already applied.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 */
function rural_av_rating_tool_premium_apply_discount_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }

    $discountValue = $form_state['input']['premium']['content']['display']['discount']['value'];
    if($discountValue != $form_state['storage']['premium']['discount']){
        rav_helper_premium_generate_initial_premium($form_state, $discountValue);
        rural_av_rating_tool_save_section_submit($form, $form_state);
    }
    return TRUE;
}

/**
 * AV Rating Tool: Premium discount application callback function.
 *
 * The following function is called after the validation/submission
 * functions have been completed when a user attempts to apply a discount
 * to a generated premium. The function will first check for errors and
 * if any exist rather than rebuilding the premium breakdown information
 * it will simply bounce the discount input field so that the user knows
 * there was an error in applying the discount. If however there were
 * no errors the callback will grab the necessary data from the storage
 * array within the Drupal form state and rebuild the premium breakdown
 * information aswell as the vehicle premium breakdown table. This is
 * achieved via calling custom jQuery functions defined within this
 * module.
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * Note: form_get_errors() will only return the errors to be caught as
 * defined in the 'apply discount' button's '#limit_validation_errors'
 * definition.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax commands to fire
 */
function rural_av_rating_tool_premium_apply_discount_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    $commands = array();

    if(form_get_errors()){
        drupal_get_messages();
        $commands[] = ajax_command_invoke(NULL, 'avrt_bouncePremiumDiscountInput');
        return array('#type' => 'ajax', '#commands' => $commands);
    }

    $commands[] = ajax_command_invoke(NULL, 'avrt_populatePremiumBreakdown', array(
        number_format((float)$form_state['storage']['premium']['final_premium'], 2, '.', ','),
        number_format((float)$form_state['storage']['premium']['init_premium'], 2, '.', ','),
        $form_state['storage']['premium']['ipt'],
        number_format((float)$form_state['storage']['premium']['ipt_value'], 2, '.', ','),
        number_format((float)$form_state['storage']['premium']['rural_fee'], 2, '.', ','),
        $form_state['storage']['premium']['discount']
    ));

    $vehicleData = rav_helper_get_storage_values('vehicle_details', $form_state);
    $vehiclPremiumBreakdownTableHtml = rav_builder_premium_build_vehicle_premium_breakdown_table_html($vehicleData['vehicles']);
    $commands[] = ajax_command_invoke(NULL, 'avrt_buildPremiumVehicleBreakdownTable', array($vehiclPremiumBreakdownTableHtml));

    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * todo: comment
 *
 * @param $form
 * @param $form_state
 */
function rural_av_rating_tool_submit_for_cover_validate($form, &$form_state){
    //Grab the current user, ensure they have all data set
    global $user;
    $user = user_load($user->uid);
    if(empty($user->field_first_name['und'][0]['value'])
        || empty($user->field_last_name['und'][0]['value'])
        || empty($user->field_company_name['und'][0]['value'])
        || empty($user->field_region['und'][0]['value'])
    ){
        form_set_error('avrt_user_data_missing', 'Please update your account details to add any missing data.');
    }

    $quoteDataStorage = $form_state['storage'];
    $processingErrorKey = 'avrt_submit_for_cover_error';
    $processingErrorMessage = 'Error submitting quote for cover. Please try again';

    //Check non of the expected proposer details empty, remembering that the
    //previous insurer field can be empty (so we dont need to check that).
    if(empty($proposerDetailsValues = $quoteDataStorage['values']['proposer_details'])){
        form_set_error($processingErrorKey, $processingErrorMessage);
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }
    if(!isset($proposerDetailsValues['proposer_name'])
        || !isset($proposerDetailsValues['proposer_address'])
        || !isset($proposerDetailsValues['postcode'])
        || !isset($proposerDetailsValues['main_occupation'])
        || !isset($proposerDetailsValues['cover_start_date'])
    ){
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }

    //Check non of the expected statement of fact answers are yes
    if(empty($statementFactValues = $quoteDataStorage['values']['statement_of_fact'])){
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }
    foreach($statementFactValues as $key => $answer){
        if($answer == 'yes'){
            form_set_error($processingErrorKey, $processingErrorMessage);
            return FALSE;
        }
    }

    //Check all of the vehicles passed contain the expected values and we have atleast one tractor or combine harvestor
    if(empty($vehicleValues = $quoteDataStorage['values']['vehicle_details']['vehicles'])){
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }
    $hasTractorOrCombine = FALSE;
    foreach($vehicleValues as $vehicleData){
        if(!isset($vehicleData['reg'])
            || !isset($vehicleData['make_model'])
            || !isset($vehicleData['year'])
            || !isset($vehicleData['vehicle_type'])
            || !isset($vehicleData['value'])
            || !isset($vehicleData['ncb'])
            || !isset($vehicleData['premium'])
        ){
            form_set_error($processingErrorKey, $processingErrorMessage);
            return FALSE;
        }

        if(!$hasTractorOrCombine && ($vehicleData['vehicle_type'] == 'tractor' || $vehicleData['vehicle_type'] == 'combine_harvestor')){
            $hasTractorOrCombine = TRUE;
        }
    }
    if(!$hasTractorOrCombine){
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }

    //Check all the premium data exists as expected
    if(empty($premiumValues = $quoteDataStorage['premium'])){
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }
    if(!isset($premiumValues['ipt_value'])
        || !isset($premiumValues['ipt'])
        || !isset($premiumValues['rural_fee'])
        || !isset($premiumValues['final_premium'])
        || !isset($premiumValues['init_premium'])
        || !isset($premiumValues['discount'])
    ){
        form_set_error($processingErrorKey, $processingErrorMessage);
        return FALSE;
    }

    return TRUE;

}

/**
 * todo: comment
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * @param $form
 * @param $form_state
 *
 * @return bool
 */
function rural_av_rating_tool_submit_for_cover_submit($form, &$form_state){
    return (!rav_helper_submit_can_process_submit($form_state)) ? FALSE : TRUE;
}

//todo comment
function rural_av_rating_tool_submit_for_cover_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    if(form_get_errors()){
        return rav_callback_messages_show_form_messages(array(), FALSE);
    }

    $quoteId = rav_helper_get_id_from_storage($form_state);
    $quoteReference = rural_av_rating_tool_helper_generate_quote_ref_from_int($quoteId);

    return rav_callback_modal_display_modal('confirm_submit', array(
        'quoteRef' => $quoteReference
    ));
}