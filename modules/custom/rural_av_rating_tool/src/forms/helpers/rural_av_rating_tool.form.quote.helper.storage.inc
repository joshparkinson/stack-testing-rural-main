<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

//todo: rename functions
//todo: move all storage manipulation into functions within this file

/**
 * Rural AV Rating Tool Helper Function.
 * Get the submission/quote id from the form state storage array.
 *
 * @param array $form_state The drupal $form_state array
 * @param mixed $default    The default to return if id isnt found
 *
 * @return int|mixed The submission id or the provided default
 */
function rav_helper_get_id_from_storage($form_state, $default = NULL){
    return (!empty($form_state['storage']['id'])) ? $form_state['storage']['id'] : $default;
}

function rav_helper_storage_set_id($submissionId, &$form_state){
    $form_state['storage']['id'] = $submissionId;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Fetches a form section stored values from the Drupal
 * form state array.
 *
 * @param string $sectionId  The section we are fetching the values for
 * @param array  $form_state The drupal form state array
 *
 * @return array An array of form section values, empty if none found
 */
function rav_helper_get_storage_values($sectionId, $form_state){
    return (!empty($form_state['storage']['values'][$sectionId])) ? $form_state['storage']['values'][$sectionId] : array();
}

/**
 * Rural AV Rating Tool Helper Function.
 * Sets a form section's values to a storage array
 * within Drupal's form state array.
 *
 * This function will attempt to add all elements of the
 * 'values' array (within drupal form state) for the
 * specified section. To ignore specific elements of the
 * values array pass an array of element keys to exclude as
 * the third (optional) argument for this function.
 *
 * @param string $sectionId  The section we are setting values for
 * @param array  $form_state The drupal form state array, holds set values
 * @param array  $excludes   An array of value keys to exclude from storage
 */
function rav_helper_set_storage_values($sectionId, &$form_state, $excludes = array()){
    $excludes = array_merge($excludes, array('actions'));

    $form_state['storage']['values'][$sectionId] = array();

    foreach($form_state['values'][$sectionId]['content'] as $key => $value){
        if(in_array($key, $excludes)){
            continue;
        }
        $form_state['storage']['values'][$sectionId][$key] = $value;
    }

}

/**
 * Rural AV Rating Tool Helper Function.
 * Sets the submission status for the quote form.
 *
 * This function will first check we are trying to set
 * the submission status to a valid one, ignoring the
 * call if we are not. If a valid status is recieved it
 * will be stored in the form_state array.
 *
 * @param string $status     The status to set against the submission
 * @param array  $form_state The drupal form state array
 *
 * @return bool Whether the status was set successfully or not,.
 */
function rav_helper_set_storage_status($status, &$form_state){
    if(!rav_helper_submission_valid_status($status)){
        return FALSE;
    }

    $form_state['storage']['status'] = $status;
    return TRUE;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Gets the submission status for the quote form.
 *
 * This function will attempt to get the status of the submission
 * from the Drupal form state array, if it is not found it will
 * return a default.
 *
 * @param array $form_state The drupal form state array
 *
 * @return string The status of the submission
 */
function rav_helper_get_storage_status(&$form_state){
    return (!empty($form_state['storage']['status'])) ? $form_state['storage']['status'] : RURAL_AV_RATING_TOOL_STATUS_OPEN;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Check the current status of the form (in storage) against a value.
 *
 * This function will check the passed the status is equal to
 * the status stored within the storage section of the drupal
 * form state array
 *
 * @param array  $form_state The drupal form state array
 * @param string $status     The status to check against
 *
 * @return bool Whether the passed status and stored status are the same
 */
function rav_helper_check_storage_status(&$form_state, $status){
    return ($status == rav_helper_get_storage_status($form_state)) ? TRUE : FALSE;
}

//todo: comment
function rav_helper_get_storage_expire_reason($form_state, $default = FALSE){
    return (!empty($form_state['storage']['expire_reason'])) ? $form_state['storage']['expire_reason'] : $default;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Sets the trigger for a referral against the form in its storage array.
 *
 * @param array  $form_state The drupal form state array
 * @param string $sectionId  The id of the section that has triggered the referral
 */
function rav_helper_set_storage_referral(&$form_state, $sectionId){
    $form_state['storage']['referral'] = $sectionId;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Gets the trigger for a referral from the forms storage array
 *
 * @param array $form_state The drupal form state array
 * @param bool $default     The default to return if there is no referral trigger
 *
 * @return mixed The referral trigger or the default provided if there is non set.
 */
function rav_helper_get_storage_referral(&$form_state, $default = FALSE){
    return (!empty($form_state['storage']['referral'])) ? $form_state['storage']['referral'] : $default;
}

/**
 * Rural AV Rating Tool.
 * Check if the passed section (via id) is active.
 *
 * @param array  $form_state The drupal form state array
 * @param string $sectionId  The id of the section to check
 *
 * @return bool Whether the section has been completed or not
 */
function rav_helper_storage_check_section_active($form_state, $sectionId){
    return ($form_state['storage']['steps']['current'] == $sectionId);
}

/**
 * Rural AV Rating Tool.
 * Check if the passed section (via id) has been completed.
 *
 * @param array  $form_state The drupal form state array
 * @param string $sectionId  The id of the section to check
 *
 * @return bool Whether the section has been completed or not
 */
function rav_helper_storage_check_section_completed($form_state, $sectionId){
    return in_array($sectionId, $form_state['storage']['steps']['completed']);
}

/**
 * Rural AV Rating Tool.
 * Check if the passed section (via id) has been reached.
 *
 * @param array  $form_state The drupal form state array
 * @param string $sectionId  The id of the section to check
 *
 * @return bool Whether the section has been completed or not
 */
function rav_helper_storage_check_section_reached($form_state, $sectionId){
    return in_array($sectionId, $form_state['storage']['steps']['reached']);
}

/**
 * Rural AV Rating Tool Helper Function.
 * Sets the next step within the forms storage.
 *
 * The following function is usually called when an action
 * button that will take the user to the next step of the form has been
 * pressed.
 *
 * Any action button that uses this function must define the data
 * attributes: 'data-current-section' and 'data-next-section' which will
 * hold the form definition id's for the sections we are from and to
 * respectively. If either of these data attributes are missing from the
 * buttons definition array than the processing if the callback is ignored
 * via the 'return FALSE' statements.
 *
 * If both fo the required data attributes have been set we update the
 * storage definition with the passed next step. We then add the the
 * current step to the completed array within the storage and add the
 * step we are moving the user to the section reached array.
 *
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether the function ran successfully.
 */
function rav_helper_storage_set_next_step(&$form_state){
    if(empty($currentSection = $form_state['triggering_element']['#attributes']['data-current-section'])){
        return false;
    }
    if(empty($nextSection = $form_state['triggering_element']['#attributes']['data-next-section'])){
        return false;
    }

    $form_state['storage']['steps']['current'] = $nextSection;
    if(!in_array($currentSection, $form_state['storage']['steps']['completed'])){
        $form_state['storage']['steps']['completed'][] = $currentSection;
    }
    if(!in_array($nextSection, $form_state['storage']['steps']['reached'])){
        $form_state['storage']['steps']['reached'][] = $nextSection;
    }

    return true;
}