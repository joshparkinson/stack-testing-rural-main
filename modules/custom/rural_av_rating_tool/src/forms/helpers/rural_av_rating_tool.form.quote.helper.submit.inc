<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Check whether a form is a state where we can process submit handlers.
 *
 * The following function is called by the various action submission
 * handlers that are part of the AV Rating Tool. It is used to check
 * that the form or the submission currently being edited is in a
 * position where changes can be made to it.
 *
 * This function checks that the form has the status: open
 * And also that there is no referral currently set against the form.
 *
 * As the above are not by definition form errors they are not picked
 * up drupals system validation and submission handlers can still be
 * reached even if they are present. Because of this we must make a
 * 'hard' check for these value in each of the submission handlers
 * within the form.
 *
 * NOTE: ALL SUBMIT HANLDERS MUST MAKE THIS CALL BEFORE PROCESSING
 *
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether the form is in a state where its submission handlers
 *              can be processed or not.
 */
function rav_helper_submit_can_process_submit($form_state){
    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN) && !rav_helper_get_storage_referral($form_state, FALSE)){
        return TRUE;
    }
    return FALSE;
}