<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Generate a premium for an AV submission.
 *
 * The following function is used to generate a premium
 * for the AV Rating Tool based on the values stored
 * agains the submission.
 *
 * This function will first extract all of the vehicle data
 * form the submission before checking that the passed
 * discount is not higher than configured maximum in the .module
 * file.
 *
 * If the passed discount (percentage as integer) is higher than
 * the maximum allowed value we reset the discount value to the
 * configured max.
 *
 * Each vehicle saved against the submission is then loopover
 * over and a premium is generated for each of them. The
 * premium by vehicle value is stored against the vechile in the
 * forms storage system. If the vehicle we are processing happens to be
 * of type 'trailer_implement' we do not charge a premium for these
 * and that vehicles premium value is set to £0 and the function
 * begins to process the
 *
 * We calculate the value of the premium for each vehicle by doing
 * the following:
 * - Convert the vehicle value to an integer
 * - Get the rating value for the vehicle's type
 * - Get the rating value for the vehicle's ncb value
 * - Generate the premium:
 *     vehicle value * vehicle type rating * vehicle ncb rating * discount
 *
 * In the above the disocunt value is converted from a percentage integer
 * to a decimal value. For example a 5% discount percentage = a disocunt
 * value of 0.95.
 *
 * If the premium for the vehicle is calculated at a lower value than the
 * minimum premium config value (.module file) we set the vehicles premium
 * to the minimum premium value.
 *
 * Now that we have a premium value for each vehicle and a total premium
 * we can process to calculate the final premium value. Out total vehicle
 * premium becomes out initial premium, we then calculate the final premium
 * by adding the IPT to the initial premium and the rural fee.
 *
 * The full premium breakdown is then stored against the form submission. As
 * well as the update vehicle data (note the vehicles now have a premium
 * attached to them).
 *
 * @param array $form_state The drupal form state array
 * @param int   $discount   The discount integer value.
 */
function rav_helper_premium_generate_initial_premium(&$form_state, $discount = 0){
    $vehicleData = rav_helper_get_storage_values('vehicle_details', $form_state);
    
    if($discount > RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT){
        $discount = RURAL_AV_RATING_TOOL_VALIDATE_MAX_DISCOUNT;
    }

    $totalPremium = 0;
    foreach($vehicleData['vehicles'] as $vehicleIndex => &$vehicle){
        if($vehicle['vehicle_type'] == 'trailer_implement'){
            $vehicle['premium'] = 0;
            continue;
        }

        $vehicleValue = str_replace("£", "", str_replace(",", "", $vehicle['value']));
        $vehicleTypeRating = rav_helper_premium_get_vehicle_type_rating($vehicle['vehicle_type']);
        $vehicleNcbRating = rav_helper_premium_get_vehicle_ncb_rating($vehicle['ncb']);
        $vehiclePremium = $vehicleValue * $vehicleTypeRating * $vehicleNcbRating * ((100 - $discount) / 100);
        $vehicle['premium'] = ($vehiclePremium >= RURAL_AV_RATING_TOOL_RATING_MIN_PREMIUM) ? round($vehiclePremium, 2) : RURAL_AV_RATING_TOOL_RATING_MIN_PREMIUM;
        $totalPremium += $vehicle['premium'];
    }

    /* @TODO tighten this up, probably rework the save details callback */
    $form_state['storage']['values']['vehicle_details']['vehicles'] = $vehicleData['vehicles'];
    $form_state['vehicles'] = $vehicleData['vehicles'];


    $form_state['storage']['premium']['init_premium'] = round($totalPremium, 2);
    $form_state['storage']['premium']['discount'] = $discount;

    $update = db_update(RURAL_AV_RATING_TOOL_DB_TABLE)
        ->fields(array(
            'quote_date' => time(),
            'updated' => time()
        ))
        ->condition('id', rav_helper_get_id_from_storage($form_state), '=')
        ->execute();

    rav_helper_premium_generate_final_premium($totalPremium, $form_state);
}

function rav_helper_premium_generate_final_premium($initialPremium, &$form_state){
    $proposerDetailsData = rav_helper_get_storage_values('proposer_details', $form_state);
    $coverStartDateTimestamp = DateTime::createFromFormat('Y-m-d', $proposerDetailsData['cover_start_date'])->getTimestamp();
    $iptToApply = rav_helper_premium_get_ipt_value($coverStartDateTimestamp);

    $premiumIpt = $initialPremium * $iptToApply;
    $finalPremium = $initialPremium + $premiumIpt + RURAL_AV_RATING_TOOL_RATING_RURAL_FEE;

    $form_state['storage']['premium']['ipt_value'] = round($premiumIpt, 2);
    $form_state['storage']['premium']['ipt'] = $iptToApply * 100;
    $form_state['storage']['premium']['rural_fee'] = RURAL_AV_RATING_TOOL_RATING_RURAL_FEE;
    $form_state['storage']['premium']['final_premium'] = round($finalPremium, 2);
}

/**
 * Premium generation helper function to convert
 * a passed vehicle type to its defined rating.
 *
 * @param string $vehicleType The vehicle type to covert to a rating
 *
 * @return bool|float|int False if type not recognised else rating value
 */
function rav_helper_premium_get_vehicle_type_rating($vehicleType){
    switch($vehicleType){
        case 'atv':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_ATV;
        case 'bailer':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_BAILER;
        case 'combine_harvestor':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_COMBINE;
        case 'crawler':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_CRAWLER;
        case 'digger':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_DIGGER;
        case 'forklift':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_FORKLIFT;
        case 'loadall':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_LOADALL;
        case 'sprayer':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_SPRAYER;
        case 'spreader':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_SPREADER;
        case 'telehandler':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_TELEHANDLER;
        case 'tractor':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_TRACTOR;
        case 'trailer_implement':
            return RURAL_AV_RATING_TOOL_RATING_VTYPE_TRAILER;
        default:
            return FALSE;
    }
}

/**
 * Premium generator helper function to convert
 * a passed ncd value to its defined rating.
 *
 * @param int $ncd The vehicle NCD value to conver to a rating
 *
 * @return bool|float|int False if type not recognised else rating value
 */
function rav_helper_premium_get_vehicle_ncb_rating($ncb){
    switch($ncb){
        case 0:
            return RURAL_AV_RATING_TOOL_RATING_NCB_ZERO;
        case 1:
            return RURAL_AV_RATING_TOOL_RATING_NCB_ONE;
        case 2:
            return RURAL_AV_RATING_TOOL_RATING_NCB_TWO;
        default:
            return FALSE;
    }
}

/**
 * Premium generator helper function to get the IPT
 * percentage value based on the cover start date.
 *
 * This is down to the way in which IPT can change based
 * on the date a cover is to begin, its rare IPT values
 * change but we must falicate the option to apply different
 * IPT values based on the cover start date.
 *
 * To achieve this we first define an array (with highest
 * date first) of key values pairs where the key is the
 * timestamp the IPT value becomes valid and the value to
 * apply based on this timestamp check. We then use the
 * passed cover start date timestamp and go through the array
 * to find the correct value for the IPT value.
 *
 * @param int $coverStartDateTimestamp The timestamp for the coverstart date
 *
 * @return int|float The IPT percentage as a decimal.
 */
function rav_helper_premium_get_ipt_value($coverStartDateTimestamp){
    $defaultIptValue = '0.095';
    $iptValues = array(
        1475280000 => '0.1', //10% IPT from 01/10/2016
        0 => '0.095' //9.5% default IPT
    );

    foreach($iptValues as $iptValidFrom => $iptValue){
        if($coverStartDateTimestamp >= $iptValidFrom){
            return $iptValue;
        }
    }


    return $defaultIptValue;
}