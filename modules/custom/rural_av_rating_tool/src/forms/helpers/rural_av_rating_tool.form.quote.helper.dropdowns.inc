<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Get the available dropdown values based on the dropdownKey provided.
 *
 * This function is used to centralise all dropdown option definitions.
 *
 * @param string $dropdownKey The dropdown we are getting the values for
 *
 * @return array|bool An array of key => value (value => Label) pairs of
 *                    dropdown options. Returns false if an unknown
 *                    dropdownKey is provided.
 */
function rav_helper_get_dropdown_values($dropdownKey){
    switch($dropdownKey){
        case 'proposer_details_main_occupation':
            return array(
                'farming' => 'Farming (No Agricultural Contracting)',
                'farming_agricultural' => 'Farming (Plus Agricultual Contacting up to 20% of turnover)',
            );
        case 'vehicle_details_vehicle_type':
            return array(
                'tractor' => 'Tractor',
                'combine_harvestor' => 'Combine Harvestor',
                'atv' => 'ATV',
                'trailer_implement' => 'Trailer/Implement',
                'digger' => 'Digger',
                'loadall' => 'Loadall',
                'telehandler' => 'Telehandler',
                'forklift' => 'Forklift Truck',
                'bailer' => 'Bailer',
                'crawler' => 'Crawler',
                'sprayer' => 'Sprayer',
                'spreader' => 'Spreader',
            );
        case 'vehicle_details_ncb':
            return array(
                0 => '0 no claims',
                1 => '1 year no claims',
                2 => '2 or more years no claims'
            );
        default:
            return FALSE;
    }
}

/**
 * Rural AV Rating Tool Helper Function.
 * Get a the label (display value) for a passed dropdown value.
 *
 * This function is used to get the display value from the system value
 * for a chosen dropdown. It is used for display of dropdown value so
 * rather than showing the system value the label (display value) can
 * be used. Mainly to be used in confirmation email send etc etc.
 *
 * @param string $dropdownKey   The dropdown we are to fetch a label from
 * @param string $dropdownValue The value we are getting the label for
 *
 * @return string The label for the value or 'Unknown' if not found.
 */
function rav_helper_get_dropdown_value_label($dropdownKey, $dropdownValue){
    $dropdownValues = rav_helper_get_dropdown_values($dropdownKey);
    return (!empty($dropdownValues[$dropdownValue])) ? $dropdownValues[$dropdownValue] : 'Unknown';
}

/**
 * Rural AV Rating Tool Helper Function.
 * Validates a passed value against the available dropdown options.
 *
 * This function is used to ensure the value of a select box is in
 * the available options. i.e. avoid any DOM manipulation / misuse.
 *
 * @param string $value       The value of the dropdown to check against
 * @param string $dropdownKey The key of the dropdown we are checking the
 *                            value for.
 *
 * @return bool. True if the value is valid, false if not
 */
function rav_helper_validate_dropdown_value($value, $dropdownKey){
    $dropdownValues = rav_helper_get_dropdown_values($dropdownKey);

    if(!$dropdownValues || !array_key_exists($value, $dropdownValues)){
        return FALSE;
    }

    return TRUE;
}