<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * The proposer details section of the AV Rating Tool Form.
 *
 * @return array The proposer details form definition to be merged into
 *               the master form.
 */
function rural_av_rating_tool_quote_form_proposer_details($form, &$form_state){
    $savedData = rav_helper_get_storage_values('proposer_details', $form_state);

    $form = array();
    $form['proposer_details'] = rav_builder_global_build_section_container('proposer_details', $form_state, array('first', 'odd'));
    $form['proposer_details']['top'] = rav_builder_global_build_section_top('proposer_details', '01. Proposer details', $form_state);
    $form['proposer_details']['content'] = rav_builder_global_build_section_content('proposer_details');

    //The proposer name field
    $form['proposer_details']['content']['proposer_name'] = array(
        '#type' => 'textfield',
        '#title' => 'Proposer name',
        '#attributes' => array(
            'placeholder' => 'Proposer name*',
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['proposer_name'])) ? $savedData['proposer_name'] : NULL,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    //The proposer address field
    $form['proposer_details']['content']['proposer_address'] = array(
        '#type' => 'textfield',
        '#title' => 'Proposer address',
        '#attributes' => array(
            'placeholder' => 'Proposer address*',
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['proposer_address'])) ? $savedData['proposer_address'] : NULL,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['proposer_details']['content']['postcode'] = array(
        '#type' => 'textfield',
        '#title' => 'Postcode',
        '#attributes' => array(
            'placeholder' => 'Postcode*'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['postcode'])) ? $savedData['postcode'] : NULL,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['proposer_details']['content']['main_occupation'] = array(
        '#type' =>  'select',
        '#title' => 'Main occupation',
        '#empty_option' => 'Main occupation*',
        '#options' => rav_helper_get_dropdown_values('proposer_details_main_occupation'),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['main_occupation'])) ? $savedData['main_occupation'] : NULL,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['proposer_details']['content']['previous_insurer'] = array(
        '#type' => 'textfield',
        '#title' => 'Previous insurer',
        '#attributes' => array(
            'placeholder' => 'Previous insurer'
        ),
        '#default_value' => (!empty($savedData['previous_insurer'])) ? $savedData['previous_insurer'] : NULL,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['proposer_details']['content']['cover_start_date'] = array(
        '#type' => 'date_popup',
        '#date_type' => DATE_UNIX,
        '#date_format' => 'd/m/y',
        '#title' => 'Cover start date',
        '#attributes' => array(
            'placeholder' => 'Cover start date*'
        ),
        '#restrictions' => array(
            'min' => array(
                'type' => 'interval',
                'interval' => array('interval' => 0, 'period' => 'day')
            ),
            'max' => array(
                'type' => 'interval',
                'interval' => array('interval' => RURAL_AV_RATING_TOOL_VALIDATE_MAX_FUTURE_DAYS_COVER_START_DATE, 'period' => 'day')
            )
        ),
        '#required' => TRUE,
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );
    if(!empty($savedData['cover_start_date']) && !is_array($savedData['cover_start_date'])){
        $form['proposer_details']['content']['cover_start_date']['#default_value'] = $savedData['cover_start_date'];
    }

    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){

        //The action buttons container
        $form['proposer_details']['content']['actions'] = rav_builder_global_build_section_actions('proposer_details');

        //The next button for the proposer details section
        //The ajax callback resides in: rural_av_rating_tool.form.quote.shared_callbacks
        //The data-section-id-segment definied is used by the ajax callback to determine
        //  which section to open next
        //Note: We have to append the submission/validation handlers so that we can still leverage
        //      system validation etc
        $form['proposer_details']['content']['actions']['next'] = array(
            '#type' => 'submit',
            '#name' => 'proposer_details_next',
            '#value' => 'Next',
            '#attributes' => array(
                'data-current-section' => 'proposer_details',
                'data-current-section-id-segment' => 'proposer-details',
                'data-next-section' => 'statement_of_fact',
                'data-next-section-id-segment' => 'statement-of-fact',
                'class' => array('action-next')
            ),
            '#ajax' => array(
                'callback' => 'rav_callback_next_move_to_next_section_callback'
            ),
            '#limit_validation_errors' => array(
                array('proposer_details', 'content', 'proposer_name'),
                array('proposer_details', 'content', 'proposer_address'),
                array('proposer_details', 'content', 'postcode'),
                array('proposer_details', 'content', 'main_occupation'),
                array('proposer_details', 'content', 'previous_insurer'),
                array('proposer_details', 'content', 'cover_start_date'),
                array('proposer_details_invalid_postcode_format'),
                array('proposer_details_invalid_main_occupation'),
                array('proposer_details_cover_start_past'),
                array('proposer_details_cover_start_future')
            ),
            '#validate' => array('rural_av_rating_tool_quote_form_proposer_details_validate'),
            '#submit' => array('rural_av_rating_tool_quote_form_proposer_details_submit')
        );

    }

    //Return the form to the master function
    return $form;
}

/**
 * The Proposer Details Section validation handler.
 *
 * The following is the first function called after a 'next button press'.
 * It is used to set any custom validation errors for the 'Proposer details'
 * section of the form. These are any errors that are not picked up via Drupals
 * system validation as definied against the form element in its definition
 * array.
 *
 * NOTE: Any custom errors definied within this validation function must be added to
 * the '#limit_validation_errors' array definition for the 'next button' of this
 * form section.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether this validation was run or not.
 */
function rural_av_rating_tool_quote_form_proposer_details_validate($form, &$form_state){
    if(rav_helper_get_storage_referral($form_state) == 'postcode'){
        rav_helper_set_storage_referral($form_state, NULL);
    }

    //No need to process unless submission does not contain system errors
    if(form_get_errors()){
        return FALSE;
    }

    //Grab proposer details
    $proposerDetailsValues = $form_state['values']['proposer_details']['content'];

    //Validate the postcode using regex
    //Trim and strip any spaces from the postcode before before chekcing against pattern
    $postcodePattern = '/^([g][i][r][0][a][a])$|^((([a-pr-uwyz]{1}([0]|[1-9]\d?))|([a-pr-uwyz]{1}[a-hk-y]{1}([0]|[1-9]\d?))|([a-pr-uwyz]{1}[1-9][a-hjkps-uw]{1})|([a-pr-uwyz]{1}[a-hk-y]{1}[1-9][a-z]{1}))(\d[abd-hjlnp-uw-z]{2})?)$/i';
    if(!preg_match($postcodePattern, str_replace(' ', '', trim($proposerDetailsValues['postcode'])))){
        form_set_error('proposer_details_invalid_postcode_format', 'Invalid UK Postcode entered.');
    }

    //Postcode format is valid, check its not a NI postcode
    $postcodeRegionChars = substr(strtolower(trim($proposerDetailsValues['postcode'])), 0, 2);
    if($postcodeRegionChars == 'bt'){
        rav_helper_set_storage_referral($form_state, 'postcode');
    }

    //Ensure the main occupation value is a valid one
    //i.e. the option exists within the select list
    if(!rav_helper_validate_dropdown_value($proposerDetailsValues['main_occupation'], 'proposer_details_main_occupation')){
        form_set_error('proposer_details_invalid_main_occupation', 'Invalid main occupation entered.');
    }

    //Validate the cover start date, ensure it is not in the past
    //Convert the passed date to a timestamp
    //Check its not a date in the past
    //Check its not more days in the future than specified within the .module config
    $coverStartDateTimeStamp = DateTime::createFromFormat('Y-m-d', $proposerDetailsValues['cover_start_date'])->getTimestamp();
    $todayStartTimeStamp = strtotime('today');
    $maxFutureDateTimeStamp = strtotime('+'.RURAL_AV_RATING_TOOL_VALIDATE_MAX_FUTURE_DAYS_COVER_START_DATE.' days');
    if($coverStartDateTimeStamp < $todayStartTimeStamp){
        form_set_error('proposer_details_cover_start_past', 'Cover start date can not be set in the past.');
    }
    if($coverStartDateTimeStamp > $maxFutureDateTimeStamp){
        form_set_error(
            'proposer_details_cover_start_future',
            'Cover start date can only be set up to '.RURAL_AV_RATING_TOOL_VALIDATE_MAX_FUTURE_DAYS_COVER_START_DATE.' days in the futue'
        );
    }

    return TRUE;
}

/**
 * The Proposer Details Section submission handler.
 *
 * The following function is the second function called after a 'next button press'
 * The function is only called if the form has no errors whether these are triggered
 * by the drupal form API (required field missing) or custom errors are set in the
 * validation handler.
 *
 * This function will store all of the validated values for this section into the
 * $form_state['storage'] array, the array used for saving/reloading submission data
 * to and from the database.
 *
 * Before the validated values are placed in storage the next step of the form is
 * set into the $form_state array using the helper function: 'rav_helper_storage_set_next_step()'
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether the submission handler has completed successfully
 */
function rural_av_rating_tool_quote_form_proposer_details_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }
    rav_helper_storage_set_next_step($form_state);
    rav_helper_set_storage_values('proposer_details', $form_state);
    return TRUE;
}
