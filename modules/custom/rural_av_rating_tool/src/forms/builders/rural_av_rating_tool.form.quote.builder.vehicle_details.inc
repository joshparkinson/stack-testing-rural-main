<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Builder Function.
 * Builds the editable vehicle list for the vehicle details section of the tool.
 *
 * This function should be one of the initial calls of the vehicle details
 * section definition file. It is used to create a list of all vehicles
 * currently stored within the form state (passed as an array) which if
 * loaded from a saved submission is populated via the database. Depending
 * on the passed submission status the list (and its relevant actions) will
 * display in either a fully functional add/edit/delete list or as a
 * read only. For example the function  will return a read only list definition
 * if the submission status is 'Submitted' for example.
 *
 * @param array  $vehicles         An array of vehicle detail definitions fetched from drupal
 *                                 form state array.
 * @param string $submissionStatus The current status of the quote/submission/form the list is
 *                                 being built for.
 *
 * @return array The vehicle list definition array
 */
function rav_builder_vehicle_details_build_editable_vehicle_list($vehicles, $submissionStatus){

    $vehicleListDefinition = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#attributes' => array(
            'class' => (empty($vehicles)) ? array('vehicle-list', 'empty') : array('vehicle-list')
        ),
        '#prefix' => '<div id="vehicle-list">',
        '#suffix' => '</div>'
    );

    if(empty($vehicles)){
        return $vehicleListDefinition;
    }

    $vehicleListDefinition['header'] = array(
        '#type' => 'markup',
        '#markup' => '<h3>Your vehicle(s)</h3>'
    );

    $i = 0;
    foreach($vehicles as $vehicleIndex => $vehicleDetails){

        $classes = array('vehicle-details-list-row', 'clearfix', $vehicleDetails['vehicle_type']);
        if($i == 0){
            $classes[] = 'first';
        }
        if($i == (sizeof($vehicles) - 1)){
            $classes[] = 'last';
        }

        //Cleanup vehicle value
        $vehicleValue = str_replace("£", "", $vehicleDetails['value']);
        $vehicleValue = str_replace(",", "", $vehicleValue);
        $vehicleValue = '£'.number_format((float)$vehicleValue, 2, '.', ',');

        $vehicleListDefinition[$vehicleIndex] = array(
            '#type' => 'fieldset',
            '#tree' => TRUE,
            '#attributes' => array(
                'class' => $classes,
                'data-vehicle-index' => $vehicleIndex
            ),
        );

        $vehicleListDefinition[$vehicleIndex]['meta'] = array(
            '#type' => 'markup',
            '#markup' => '<div class="vehicle-meta">
                            <div class="reg-make-model"><p>'.$vehicleDetails['reg'].' - '.$vehicleDetails['make_model'].'</p></div>
                            <div class="sub">
                                <p>'.$vehicleDetails['year'].' | '.rav_helper_get_dropdown_value_label('vehicle_details_vehicle_type', $vehicleDetails['vehicle_type']).' | '.$vehicleValue.' | '.rav_helper_get_dropdown_value_label('vehicle_details_ncb', $vehicleDetails['ncb']).'</p>
                            </div>
                         </div>'
        );

        if($submissionStatus == RURAL_AV_RATING_TOOL_STATUS_OPEN){

            $vehicleListDefinition[$vehicleIndex]['actions'] = array(
                '#type' => 'container',
                '#attributes' => array(
                    'class' => array('vehicle-details-actions')
                ),
            );

            $vehicleListDefinition[$vehicleIndex]['actions']['edit'] = array(
                '#type' => 'submit',
                '#name' => 'edit_vehicle_'.$vehicleIndex,
                '#value' => 'Edit',
                '#attributes' => array(
                    'class' => array('edit-vehicle'),
                    'data-vehicle-index' => $vehicleIndex
                ),
                '#ajax' => array(
                    'callback' => 'vehicle_details_edit_vehicle_from_list_callback',
                ),
                '#limit_validation_errors' => array(),
                '#submit' => array()
            );

            $vehicleListDefinition[$vehicleIndex]['actions']['remove'] = array(
                '#type' => 'submit',
                '#name' => 'remove_vehicle_'.$vehicleIndex,
                '#value' => 'Remove',
                '#attributes' => array(
                    'class' => array('remove-vehicle'),
                    'data-vehicle-index' => $vehicleIndex
                ),
                '#ajax' => array(
                    'callback' => 'vehicle_details_remove_vehicle_from_list_callback',
                ),
                '#limit_validation_errors' => array(),
                '#submit' => array('vehicle_details_remove_vehicle_from_list_submit'),
            );

        }

        $i++;

    }

    if($submissionStatus == RURAL_AV_RATING_TOOL_STATUS_OPEN){

        $vehicleListDefinition['actions'] = array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => array('vehicle-list-actions', 'clearfix')
            ),
        );

        $vehicleListDefinition['actions']['add_another'] = array(
            '#type' => 'submit',
            '#name' => 'vehicle_details_add_another_vehicle',
            '#value' => 'Add another vehicle',
            '#attributes' => array(
                'id' => 'edit-vehicle-details-content-vehicle-list-actions-add-another',
                'class' => array('action-add-another-vehicle')
            ),
            '#ajax' => array(
                'callback' => 'vehicle_details_add_another_callback',
            ),
            '#limit_validation_errors' => array(),
            '#submit' => array(),
        );

    }

    return $vehicleListDefinition;

}