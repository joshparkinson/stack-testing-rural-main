<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Builds a sections outermost container definition.
 *
 * This function call should be one of the first calls in
 * a form section definition file. It is used to create the
 * outermost container whilst also checking various form_state
 * variables to apply the proper classes to the container
 * indicating whether this section has been reached,
 * whether the section is the currently active section and
 * whether the section has been completed.
 *
 * @param string $sectionId  The section id we are building the form section
 *                           container for. This should be equal to the $form['sectionID']
 *                           attribute used to build the container
 * @param array  $form_state The drupal form state array. Used to determine container classes.
 * @param array  $classes    An optional array of additional classes to add to the container
 *
 * @return array The container definition array
 */
function rav_builder_global_build_section_container($sectionId, $form_state, $classes = array()){
    $sectionIdDashed = trim(str_replace('_', '-', $sectionId));

    $sectionClasses = array_merge(array(
        'av-rating-tool-form-section',
        $sectionIdDashed
    ), $classes);

    if(!rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){
        $sectionClasses[] = 'read-only';
    }
    if(rav_helper_storage_check_section_reached($form_state, $sectionId)){
        $sectionClasses[] = 'section-reached';
    }
    if(rav_helper_storage_check_section_active($form_state, $sectionId)){
        $sectionClasses[] = 'section-active';
    }
    if(rav_helper_storage_check_section_completed($form_state, $sectionId)){
        $sectionClasses[] = 'section-completed';
    }
    if($form_state['storage']['steps']['current'] == 'premium' && $sectionId != 'premium'){
        $sectionClasses[] = 'section-hidden';
    }

    $sectionDefinition = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#attributes' => array(
            'class' => $sectionClasses
        ),
    );

    return $sectionDefinition;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Builds a section top (title bar) definition.
 *
 * The function should be called after the section container build
 * call/definition in a form section definition file.
 *
 * The following function will set a containers title whilst also
 * configuring its edit button with the correct data attributes.
 *
 * The edit button defined in this container uses the shared callbacks:
 * rural_av_rating_tool_edit_section_submit() && rural_av_rating_tool_edit_section_callback()
 * Stored in: [av rating tool base dir]/src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.edit.inc
 *
 * @param string $sectionId  The section ID we are building the top container for
 * @param string $title      The title of the section
 * @param array  $form_state The drupal form state array
 *
 * @return array The section's top container definition array
 */
function rav_builder_global_build_section_top($sectionId, $title, $form_state){
    $sectionIdDashed = trim(str_replace('_', '-', $sectionId));

    $topDefinition = array(
        '#type' => 'container',
        '#attributes' => array(
            'class' => array(
                'section-top'
            ),
        ),
    );

    $topDefinition['title'] = array(
        '#markup' => '<h2>'.$title.'</h2>'
    );

    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){

        $topDefinition['edit'] = array(
            '#type' => 'submit',
            '#name' => $sectionId.'_edit',
            '#value' => 'Edit',
            '#attributes' => array(
                'class' => array('section-edit'),
                'data-section' => $sectionId,
                'data-section-id-segment' => $sectionIdDashed
            ),
            '#ajax' => array(
                'callback' => 'rural_av_rating_tool_edit_section_callback'
            ),
            '#limit_validation_errors' => array(),
            '#submit' => array('rural_av_rating_tool_edit_section_submit'),
        );

    }

    return $topDefinition;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Builds a sections content container and introduction copy.
 *
 * This function shopuld be called after the section top container build
 * call/definition in a form section definition file.
 *
 * The following function will initialise a sections content container
 * and also set its introduction copy.
 *
 * @param string $sectionId   The ID of the section we are building the content
 *                            container for.
 * @param string $introText   Any additional text to be displayed in the introduction,.
 *
 * @return array The sections content container definition array
 */
function rav_builder_global_build_section_content($sectionId, $introText = ''){
    $sectionIdDashed = trim(str_replace('_', '-', $sectionId));

    $contentDefinition = array(
        '#type' => 'container',
        '#attributes' => array(
            'id' => 'edit-'.$sectionIdDashed.'-content',
            'class' => array(
                'section-content'
            ),
        ),
    );

    $introductionClasses = (!empty($introText)) ? 'intro no-margin' : 'intro';
    $contentIntroduction = '<p class="'.$introductionClasses.'">Please note all fields marked with a <span>*</span> are mandatory</p>';
    if(!empty($introText)){
        $contentIntroduction .= '<p class="intro">'.$introText.'</p>';
    }

    $contentDefinition['intro'] = array(
        '#markup' => '<div class="section-content-intro">'.$contentIntroduction.'</div>',
        '#attributes' => array(
            'class' => array(
                'section-intro'
            ),
        ),
    );

    return $contentDefinition;
}

/**
 * Rural AV Rating Tool Helper Function.
 * Builds a sections action container and save button.
 *
 * This function should be called when actions are required/needed
 * within a form section.
 *
 * @param string $sectionId     The ID of the section we are building the
 *                              actions for
 * @param array  $classes       Any aditional classes to add to the section
 *                              actions container
 * @param bool   $addSaveButton Indiciates whether or not to build a save button within the actions container
 *
 * @return array The sections action definition array
 */
function rav_builder_global_build_section_actions($sectionId, $classes = array(), $addSaveButton = TRUE){
    $actionDefinition = array(
        '#type' => 'container',
        '#attributes' => array(
            'class' => array_merge(array('section-actions', 'clearfix'), $classes)
        )
    );

    if($addSaveButton){
        $actionDefinition['save'] = array(
            '#type' => 'submit',
            '#name' => $sectionId.'_save',
            '#value' => 'Save',
            '#ajax' => array(
                'callback' => 'rural_av_rating_tool_save_callback'
            ),
            '#attributes' => array(
                'class' => array('action-save', 'left')
            ),
            '#limit_validation_errors' => array(),
            '#submit' => array('rural_av_rating_tool_save_section_submit'),
        );
    }

    return $actionDefinition;
}