<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

//todo: comment
function rav_builder_premium_build_proposer_details_breakdown_form_definition($proposerDetailsData){
    $proposerDetailsBreakdownDefinition = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#attributes' => array(
            'class' => array('premium-proposer-details-breakdown')
        ),
    );

    $proposerDetailsBreakdownDefinition['breakdown'] = array(
        '#type' => 'markup',
        '#markup' => rav_builder_premium_build_proposer_details_breakdown_html($proposerDetailsData)
    );

    return $proposerDetailsBreakdownDefinition;
}

//todo: comment
function rav_builder_premium_build_proposer_details_breakdown_html($proposerDetailsData){
    $name = (!empty($proposerDetailsData['proposer_name'])) ? $proposerDetailsData['proposer_name'] : NULL;
    $address = (!empty($proposerDetailsData['proposer_address'])) ? $proposerDetailsData['proposer_address'] : NULL;
    $postcode = (!empty($proposerDetailsData['postcode'])) ? $proposerDetailsData['postcode'] : NULL;
    $occupation = (!empty($proposerDetailsData['main_occupation'])) ? rav_helper_get_dropdown_value_label('proposer_details_main_occupation', $proposerDetailsData['main_occupation']) : NULL;
    $insurer = (!empty($proposerDetailsData['previous_insurer'])) ? $proposerDetailsData['previous_insurer'] : 'n/a';
    $coverStartDate = (!empty($proposerDetailsData['cover_start_date'])) ? DateTime::createFromFormat('Y-m-d', $proposerDetailsData['cover_start_date'])->format('d / m / Y') : NULL;

    $insurerClass = (!empty($insurer)) ? 'visible' : 'hidden';

    $proposerDetailsBreakdownHtml = '<div class="proposer-details-breakdown">';
    $proposerDetailsBreakdownHtml .= '<p class="name">Name: <span class="val">'.$name.'<span></p>';
    $proposerDetailsBreakdownHtml .= '<p class="address">Address: <span class="val">'.$address.', '.$postcode.'<span></p>';
    $proposerDetailsBreakdownHtml .= '<p class="occupation">Occupation: <span class="val">'.$occupation.'<span></p>';
    $proposerDetailsBreakdownHtml .= '<p class="insurer '.$insurerClass.'">Previous insurer: <span class="val">'.$insurer.'</span></p>';
    $proposerDetailsBreakdownHtml .= '<p class="start-date">Cover start date: <span class="val">'.$coverStartDate.'</span></p>';
    $proposerDetailsBreakdownHtml .= '</div>';

    $tableHeader = array(
        array('data' => 'Proposer details', 'class' => 'proposer-details'),
        array('data' => 'Cover start date', 'class' => 'cover-start-date end')
    );

    $tableRows = array(
        array(
            'data' => array(
                array('data' => $proposerDetailsBreakdownHtml, 'class' => 'proposer-details'),
                array('data' => $coverStartDate, 'class' => 'cover-start-date end')
            ),
            'class' => array('first last')
        )
    );

    return theme('table', array(
        'header' => $tableHeader,
        'rows' => $tableRows,
        'sticky' => FALSE,
        'attributes' => array('class' => array('av-rating-tool-table avrt-premium-proposer-details-table'))
    ));

}

/**
 * Rural AV Rating Tool Builder Function.
 * Builds the vehicle premium breakdown table for the premium section of the tool.
 *
 * This function should be called after showing the premium/discount application
 * within the premium section definition file. It is used to create a table of all
 * the vehicles that make up part of the premium/quote showing each of the premiums
 * per vehicle. There is no fallback for this table generation if the vehicleData
 * array passed to it is an empty as it is expected that a user can not reach this
 * section of the tool without adding atleast one vehicle to the quotation. This
 * table is non functional and is purely for providing more granularity to the
 * premium also displayed/defined within the premium section definition file.
 *
 * NOTE: This function leverages another helper function within this file
 * 'rav_helper_build_vehicl_premium_breakdown_table_html' to build the actual
 * table HTML. This is due to the fact that at various stages throughout the
 * form this table will need to be regenerated outside of the form definition
 * (via ajax callbacks) for example when a disocunt is applied to the premium.
 *
 * @param array $vehicleData An array of vehicle form data used to build the
 *                           breakdown table
 *
 * @return array The vehicle premium breakdown form definition array
 */
function rav_builder_premium_build_vehicle_premium_breakdown_form_definition($vehicleData){
    $vehiclePremiumBreakdownDefinition = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#attributes' => array(
            'class' => array('vehicle-premium-breakdown')
        ),
    );

    if(!empty($vehicleData)){
        $vehiclePremiumBreakdownDefinition['vehicle_breakdown_table'] = array(
            '#type' => 'markup',
            '#markup' => rav_builder_premium_build_vehicle_premium_breakdown_table_html($vehicleData)
        );
    }

    return $vehiclePremiumBreakdownDefinition;
}

/**
 * Rural AV Rating Tool Builder Function.
 * Builds the HTML for the vehicle breakdown table for the premium
 * section of the tool.
 *
 * The following function will take an array of vehicle data (which
 * arrives in an expected format) and covert it into a display ready
 * HTML definition of the vehicle premium breakdown table. This is
 * achieved via use of Drupal native themeing functions. This function
 * will be called at various stages throughout the form (mostly via ajax
 * callbacks).
 *
 * First a table header array is definied with column names and classes.
 * Then all passed 'vehicleData' is looped over to generate the rows for
 * the table, through use of counter variables etc we can determine first
 * and last rows and add required classes to them. The vehicle premium if
 * found (note vehicles added to the vehicles list but not yet quoted against
 * will be used to generate this table on initial page load but wont have
 * a premium attached to it) this premium value if formatted to a price. If not
 * found to avoid problems we default it to 0.00 (this default should never be
 * seen however).
 *
 * @param array $vehicleData An array of vehicle form data used to build the
 *                           breakdown table
 *
 * @return string The HTML definition for the table
 */
function rav_builder_premium_build_vehicle_premium_breakdown_table_html($vehicleData){
    $tableHeader = array(
        array('data' => 'Vehicle details', 'class' => 'vehicle-details'),
        array('data' => 'Premium (exc. IPT & Fee)', 'class' => 'premium end'),
    );

    $i = 0;
    $tableRows = array();
    foreach($vehicleData as $vehicle){
        $rowClasses = array();
        if($i == 0){ $rowClasses[] = 'first'; }
        if($i == (sizeof($vehicleData) - 1)){ $rowClasses[] = 'last'; }

        //Cleanup vehicle value
        $vehicleValue = str_replace("£", "", $vehicle['value']);
        $vehicleValue = str_replace(",", "", $vehicleValue);
        $vehicleValue = '£'.number_format((float)$vehicleValue, 2, '.', ',');

        $vehicleDetails = '<p class="reg-make-model">'.$vehicle['reg'].' - '.$vehicle['make_model'].'</p>';
        $vehicleDetails .= '<p class="sub">'.$vehicle['year'].' | '.rav_helper_get_dropdown_value_label('vehicle_details_vehicle_type', $vehicle['vehicle_type']).' | '.$vehicleValue.' | '.rav_helper_get_dropdown_value_label('vehicle_details_ncb', $vehicle['ncb']).'</p>';

        $vehiclePremium = (!empty($vehicle['premium'])) ? number_format((float)$vehicle['premium'], 2, '.', ',') : '0.00';

        $tableRows[] = array(
            'data' => array(
                array('data' => $vehicleDetails, 'class' => 'vehicle-details'),
                array('data' => '£'.$vehiclePremium, 'class' => 'premium end')
            ),
            'class' => $rowClasses
        );

        $i++;
    }

    return theme('table', array(
        'header' => $tableHeader,
        'rows' => $tableRows,
        'sticky' => FALSE,
        'attributes' => array('class' => array('av-rating-tool-table avrt-premium-breakdown-by-vehicle-table'))
    ));
}