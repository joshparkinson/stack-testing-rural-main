<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Builder Function.
 * Build the AV Rating Tool in a read only/'view details' state.
 *
 * The following function is called if an AV Rating Tool submission
 * is reloaded and viewed from the database that is in a non open
 * state. For example this can be when a form has been auto expired
 * during cron functionality system checks or if the quote has been
 * carried through to copmpleted and submitted to be put on cover.
 *
 * This function builds the status block display before wrapping
 * the form in a 'non-open' container and adding a button to view
 * the closed submission.
 *
 * @param array $defaultForm The default 'editable' form definition array
 * @param array $form_state  The drupal form state array
 *
 * @return array The read-only form definition array
 */
function rav_builder_readonly_build_readonly_form($defaultForm, $form_state){
    $submissionStatus = rav_helper_get_storage_status($form_state);
    $expireReason = rav_helper_get_storage_expire_reason($form_state, FALSE);

    $readOnlyForm = array();



    $readOnlyForm['status'] = array(
        '#type' => 'container',
        '#attributes' => array(
            'class' => array('readonly-status', strtolower($submissionStatus)),
        ),
    );

    $readOnlyForm['status']['header'] = array(
        '#type' => 'markup',
        '#markup' => '<h2>This quote has been <span>'.ucfirst($submissionStatus).'</span></h2>'
    );

    $readOnlyForm['status']['message'] = array(
        '#type' => 'markup',
        '#markup' => rav_builder_readonly_build_status_message($submissionStatus, $expireReason)
    );

    $readOnlyForm['status']['actions'] = array(
        '#type' => 'container',
        '#attributes' => array(
            'class' => array('status-actions clearfix')
        ),
    );

    $readOnlyForm['status']['actions']['show_non_open_submission'] = array(
        '#type' => 'submit',
        '#value' => 'Show submission',
        '#name' => 'avrt_show_non_open_submission',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => 'rav_callback_readonly_show_readonly_submission_callback',
        ),
    );

    $readOnlyForm['status']['actions']['new_quote'] = array(
        '#type' => 'markup',
        '#markup' => '<a href="/'.RURAL_AV_RATING_TOOL_QUOTE_PAGE_URL.'">Start a new quote</a>'
    );

    $readOnlyForm['form_wrapper_start'] = array(
        '#type' => 'markup',
        '#markup' => '<div id="avrt-non-open-wrapper">'
    );
    $readOnlyForm = array_merge($readOnlyForm, $defaultForm);
    $readOnlyForm['form_wrapper_end'] = array(
        '#type' => 'markup',
        '#markup' => '</div>'
    );

    return $readOnlyForm;
}

//todo: comment
function rav_builder_readonly_build_status_message($submissionStatus, $expireReason){
    switch(strtolower($submissionStatus)){
        case RURAL_AV_RATING_TOOL_STATUS_EXPIRED:
            if($expireReason == 'quote_date'){
                return '<p>This quotation has expired as the date of the quote has exceeded 30 days.</p>';
            }
            if($expireReason == 'start_date'){
                return '<p>This quotation has expired as the selected cover start date has elapsed.</p>';
            }
            break;
        case RURAL_AV_RATING_TOOL_STATUS_CLOSED:
            return '<p>Thank you for your business</p>';
            break;
    }
}