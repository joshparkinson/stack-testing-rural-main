<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * The statement of fact section of the AV Rating Tool Form.
 *
 * @return array The statement of fact form definition to be merged into
 *               the master form.
 */
function rural_av_rating_tool_quote_form_statement_of_fact($form, &$form_state){
    $savedData = rav_helper_get_storage_values('statement_of_fact', $form_state);

    $form = array();
    $form['statement_of_fact'] = rav_builder_global_build_section_container('statement_of_fact', $form_state, array('even'));
    $form['statement_of_fact']['top'] = rav_builder_global_build_section_top('statement_of_fact', '02. Eligibility & Statement of Fact', $form_state);
    $form['statement_of_fact']['content'] = rav_builder_global_build_section_content('statement_of_fact');

    $form['statement_of_fact']['content']['motor_lease'] = array(
        '#type' => 'radios',
        '#title' => t('Do you own or lease any motor vehicle(s) other than those declared?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['motor_lease'])) ? $savedData['motor_lease'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['no_claims_in_use'] = array(
        '#type' => 'radios',
        '#title' => t('Is the No Claims Bonus you intend to use in connection with each insured vehicle on this policy currently in use elsewhere?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['no_claims_in_use'])) ? $savedData['no_claims_in_use'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['trade_other_name'] = array(
        '#type' => 'radios',
        '#title' => t('Do you or have you ever traded under another name?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['trade_other_name'])) ? $savedData['trade_other_name'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['declared_bankrupt'] = array(
        '#type' => 'radios',
        '#title' => t('Have you or anyone directly connected with the business been declared bankrupt?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['declared_bankrupt'])) ? $savedData['declared_bankrupt'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['modified_vehicle'] = array(
        '#type' => 'radios',
        '#title' => t('Has any vehicle or trailer been modified from the manufacturers standard specification?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['modified_vehicle'])) ? $savedData['modified_vehicle'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['different_address'] = array(
        '#type' => 'radios',
        '#title' => t('Will any vehicle(s) be kept at any address other than shown on the schedule?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['different_address'])) ? $savedData['different_address'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['vehicle_usage'] = array(
        '#type' => 'radios',
        '#title' => t('Will any vehicle(s) be used by any driver other than in connection with:'),
        '#description' => '<ul><li>The policyholders business</li><li>Social, Domestic and Pleasure</li><li>Commuting to and from one place of work</li></ul>',
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['vehicle_usage'])) ? $savedData['vehicle_usage'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['vehicle_registered_to_other'] = array(
        '#type' => 'radios',
        '#title' => t('Are any vehicles registered in the name of anyone other than the policyholder?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['vehicle_registered_to_other'])) ? $savedData['vehicle_registered_to_other'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['driver_under_twenty_five'] = array(
        '#type' => 'radios',
        '#title' => t('In respect of Private Cars and Commercial Vehicles will any driver be under the age of 25?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['driver_under_twenty_five'])) ? $savedData['driver_under_twenty_five'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['goods_haulage'] = array(
        '#type' => 'radios',
        '#title' => t('Will any vehicle be used for the carriage of goods for hire and reward or haulage of third party goods?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['goods_haulage'])) ? $savedData['goods_haulage'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['tree_felling'] = array(
        '#type' => 'radios',
        '#title' => t('Will any vehicle be used for tree felling?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['tree_felling'])) ? $savedData['tree_felling'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['agricultural_contracting_exceeding_twenty'] = array(
        '#type' => 'radios',
        '#title' => t('Will the turnover generated from agricultural contracting exceed 20% of your total annual turnover?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['agricultural_contracting_exceeding_twenty'])) ? $savedData['agricultural_contracting_exceeding_twenty'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['northern_ireland_risk_address'] = array(
        '#type' => 'radios',
        '#title' => t('Is the risk address located in any area of Northern Ireland?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['northern_ireland_risk_address'])) ? $savedData['northern_ireland_risk_address'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['have_you_or'] = array(
        '#type' => 'radios',
        '#title' => t('Have you or any person who may drive:'),
        '#description' => '<ul>
            <li>Been refused motor insurance or had a motor policy cancelled, or ever had special terms imposed?</li>
            <li>Suffered from diabetes, epilepsy, heart condition, defective vision or hearing, loss of limb or any other physical, mental or alcoholic condition?</li>
            <li>Any unspent motoring offence, or have any prosecutions pending?</li>
            <li>Any unspent convictions in accordance with the Rehabilitation of Offenders Act?</li>
        </ul>',
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['have_you_or'])) ? $savedData['have_you_or'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['statement_of_fact']['content']['accidents_claims_losses'] = array(
        '#type' => 'radios',
        '#title' => t('Had any accidents, claims or losses in the past 3 years, whether to blame or not?'),
        '#options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        ),
        '#required' => TRUE,
        '#default_value' => (!empty($savedData['accidents_claims_losses'])) ? $savedData['accidents_claims_losses'] : 'no',
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){

        $form['statement_of_fact']['content']['actions'] = rav_builder_global_build_section_actions('statement_of_fact');

        $form['statement_of_fact']['content']['actions']['next'] = array(
            '#type' => 'submit',
            '#name' => 'statement_of_fact_next',
            '#value' => 'Next',
            '#attributes' => array(
                'data-current-section' => 'statement_of_fact',
                'data-current-section-id-segment' => 'statement-of-fact',
                'data-next-section' => 'vehicle_details',
                'data-next-section-id-segment' => 'vehicle-details',
                'class' => array('action-next')
            ),
            '#ajax' => array(
                'callback' => 'rav_callback_next_move_to_next_section_callback'
            ),
            '#limit_validation_errors' => array(
                array('statement_of_fact', 'content', 'motor_lease'),
                array('statement_of_fact', 'content', 'no_claims_in_use'),
                array('statement_of_fact', 'content', 'trade_other_name'),
                array('statement_of_fact', 'content', 'declared_bankrupt'),
                array('statement_of_fact', 'content', 'modified_vehicle'),
                array('statement_of_fact', 'content', 'different_address'),
                array('statement_of_fact', 'content', 'vehicle_usage'),
                array('statement_of_fact', 'content', 'vehicle_registered_to_other'),
                array('statement_of_fact', 'content', 'driver_under_twenty_five'),
                array('statement_of_fact', 'content', 'goods_haulage'),
                array('statement_of_fact', 'content', 'tree_felling'),
                array('statement_of_fact', 'content', 'agricultural_contracting_exceeding_twenty'),
                array('statement_of_fact', 'content', 'northern_ireland_risk_address'),
                array('statement_of_fact', 'content', 'have_you_or'),
                array('statement_of_fact', 'content', 'accidents_claims_losses'),
                array('statement_of_fact_ineligible')
            ),
            '#validate' => array('rural_av_rating_tool_quote_form_statement_of_fact_validate'),
            '#submit' => array('rural_av_rating_tool_quote_form_statement_of_fact_submit')
        );

    }

    return $form;
}

/**
 * The Statement of Fact section validation handler.
 *
 * The following is the first function call after the 'next' button is pressed
 * within the statement of fact form section. It is used to set any custom
 * validation errors for the 'Statement of Fact' section of the form. These
 * are any errors not picked up via Drupal's system validation as defined against
 * the form element in its definition array. (An empty required field for example).
 *
 * NOTE: Any custom errors defined within this validation must be added to the
 * '#limit_validation_errors' array within the form item definition for the
 * 'next' button within this section of the form.
 *
 * NOTE: Any validation handlers that set a referral state against the submission
 * must remove the referral state at the start of the function call. This stops
 * the system trapping the user within a form section.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether or not this validation function was run or not.
 */
function rural_av_rating_tool_quote_form_statement_of_fact_validate($form, &$form_state){
    if(rav_helper_get_storage_referral($form_state) == 'statement_of_fact'){
        rav_helper_set_storage_referral($form_state, NULL);
    }

    //No need to process unless submission does not contain system errors
    if(form_get_errors()){
        return FALSE;
    }

    //Only validation required for this step is to ensure that none of the statement
    //of fact answers have been set to yes. To save is checking each manually
    //loop over all the values (arrays are not sof radios) check if it is yes
    //if it throw an error and set the submission as a referred one.
    foreach($form_state['values']['statement_of_fact']['content'] as $answer){
        if(is_array($answer)){
            continue;
        }
        if($answer == "yes"){
            rav_helper_set_storage_referral($form_state, 'statement_of_fact');
            break;
        }
    }

    return TRUE;
}

/**
 * The Statement of Fact section submission handler.
 *
 * The following is the function called after the 'next button' is pressed.
 * The function is only called if the form section validation functions
 * (including drupal system validation) has not thrown any errors.
 *
 * This function is used to leverage the form helpers to store the values
 * for this section into the $form_state['storage'] array, the array used
 * for saving/reloading submission to and from the database.
 *
 * Before we save these values to the $form_state['storage'] array we have to
 * set the next step of the form in the storage array using the helper function:
 * 'rav_helper_storage_set_next_step'. This call requires the current form state and
 * for various data attribute to be set agains the next button in its definition
 * array.
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether the submit function was run successfully or not
 */
function rural_av_rating_tool_quote_form_statement_of_fact_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }
    rav_helper_storage_set_next_step($form_state);
    rav_helper_set_storage_values('statement_of_fact', $form_state);
}