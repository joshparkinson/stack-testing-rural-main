<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * The Vehicle Details section of the Rural AV Rating Tool form.
 * Pass in the form state by reference so that it can be ammended
 * used in building the form.
 *
 * @param array $form_state The drupal form state array
 *
 * @return array The renderable form array
 */
function rural_av_rating_tool_quote_form_vehicle_details($form, &$form_state){

    //Fetch any saved data for this section of the form. Saved data will be present when reopening/editing
    //a saved quote etc etc.
    //If no vehicle details are saved in the form state attempt to populate with the saved data or
    //initialise it as an empty array
    $savedData = $savedData = rav_helper_get_storage_values('vehicle_details', $form_state);
    if(!isset($form_state['vehicles'])){
        $form_state['vehicles'] = (!empty($savedData['vehicles'])) ? array_values($savedData['vehicles']) : array();
    }

    $form = array();
    $form['vehicle_details'] = rav_builder_global_build_section_container('vehicle_details', $form_state, array('odd', 'last'));
    $form['vehicle_details']['top'] = rav_builder_global_build_section_top('vehicle_details', '03. Vehicle details', $form_state);
    $form['vehicle_details']['content'] = rav_builder_global_build_section_content('vehicle_details', 'Your submission must include at least one vehicle which is <b style="font-weight:bold;text-decoration:underline;">not</b> a Trailer or ATV<br />Individual vehicles must not exceed £100,000 in value.');
    $form['vehicle_details']['content']['vehicle_list'] = rav_builder_vehicle_details_build_editable_vehicle_list($form_state['vehicles'], rav_helper_get_storage_status($form_state));


    $form['vehicle_details']['content']['vehicle'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle',
            'class' => (!empty($form_state['vehicles'])) ? array('hidden', 'vehicle-fieldset') : array('vehicle-fieldset')
        )
    );

    $form['vehicle_details']['content']['vehicle']['index'] = array(
        '#type' => 'hidden',
        '#default_value' => NULL,
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-index',
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['vehicle_details']['content']['vehicle']['reg'] = array(
        '#type' => 'textfield',
        '#title' => 'Reg or Serial No.',
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-reg',
            'placeholder' => 'Reg or Serial No.*'
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['vehicle_details']['content']['vehicle']['make_model'] = array(
        '#type' => 'textfield',
        '#title' => 'Make & Model',
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-make-model',
            'placeholder' => 'Make & Model*',
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['vehicle_details']['content']['vehicle']['year'] = array(
        '#type' => 'textfield',
        '#title' => 'Year',
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-year',
            'placeholder' => 'Year*',
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['vehicle_details']['content']['vehicle']['vehicle_type'] = array(
        '#type' => 'select',
        '#title' => 'Vehicle type',
        '#empty_option' => 'Vehicle type*',
        '#options' => rav_helper_get_dropdown_values('vehicle_details_vehicle_type'),
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-vehicle-type',
            'placeholder' => 'Vehicle Type*'
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['vehicle_details']['content']['vehicle']['value'] = array(
        '#type' => 'textfield',
        '#title' => 'Value (£)',
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-value',
            'placeholder' => 'Value (£)*'
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    $form['vehicle_details']['content']['vehicle']['ncb'] = array(
        '#type' => 'select',
        '#title' => 'No claims bonus',
        '#empty_option' => 'No claims bonus*',
        '#options' => rav_helper_get_dropdown_values('vehicle_details_ncb'),
        '#attributes' => array(
            'id' => 'edit-vehicle-details-content-vehicle-ncb',
            'placeholder' => 'No claims bonus*'
        ),
        '#disabled' => !rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN),
    );

    if(rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){

        $form['vehicle_details']['content']['vehicle']['actions'] = array(
            '#type' => 'container',
            '#attributes' => array(
                'class' => array('vehicle-actions', 'clearfix')
            ),
        );

        $form['vehicle_details']['content']['vehicle']['actions']['add_vehicle'] = array(
            '#type' => 'submit',
            '#name' => 'vehicle_details_add_vehicle',
            '#value' => 'Add Vehicle',
            '#attributes' => array(
                'id' => 'edit-vehicle-details-content-vehicle-actions-add-vehicle',
                'class' => array('action-add-vehicle'),
            ),
            '#ajax' => array(
                'callback' => 'vehicle_details_add_vehicle_callback',
                'wrapper' => 'edit-vehicle-details-content'
            ),
            '#limit_validation_errors' => array(
                array('vehicle_details', 'content', 'vehicle', 'index'),
                array('vehicle_details', 'content', 'vehicle', 'reg'),
                array('vehicle_details', 'content', 'vehicle', 'make_model'),
                array('vehicle_details', 'content', 'vehicle', 'year'),
                array('vehicle_details', 'content', 'vehicle', 'vehicle_type'),
                array('vehicle_details', 'content', 'vehicle', 'value'),
                array('vehicle_details', 'content', 'vehicle', 'ncb'),
                array('add_vehicle_invalid_year'),
                array('add_vehicle_year_in_future'),
                array('add_vehicle_invalid_value'),
                array('add_vehicle_value_too_high'),
                array('add_vehicle_missing_data'),
                array('add_vehicle_invalid_vehicle_type'),
                array('add_vehicle_invalid_vehicle_ncb')
            ),
            '#validate' => array('vehicle_details_add_vehicle_validate'),
            '#submit' => array('vehicle_details_add_vehicle_submit')
        );

        if(!empty($form_state['vehicles'])){
            $form['vehicle_details']['content']['vehicle']['actions']['cancel'] = array(
                '#type' => 'submit',
                '#name' => 'vehicle_details_cancel_vehicle',
                '#value' => 'Cancel',
                '#attributes' => array(
                    'id' => 'edit-vehicle-details-content-vehicle-actions-cancel',
                    'class' => array('action-cancel-add-vehicle')
                ),
                '#ajax' => array(
                    'callback' => 'vehicle_details_cancel_vehicle_callback',
                ),
                '#limit_validation_errors' => array(),
                '#submit' => array(),
            );

            $form['vehicle_details']['content']['actions'] = array(
                '#type' => 'container',
                '#attributes' => array(
                    'id' => 'edit-vehicle-details-content-actions',
                    'class' => array('section-actions', 'section-vehicle-actions', 'clearfix')
                ),
            );

            $form['vehicle_details']['content']['actions']['quote_error_anchor'] = array(
                '#type' => 'markup',
                '#markup' => '<div id="avrt-quote-gen-error-anchor"></div>'
            );

            $form['vehicle_details']['content']['actions']['save'] = array(
                '#type' => 'submit',
                '#name' => 'vehicle_details_save',
                '#value' => 'Save',
                '#attributes' => array(
                    'class' => array('action-save', 'left')
                ),
                '#ajax' => array(
                    'callback' => 'rural_av_rating_tool_save_callback'
                ),
                '#limit_validation_errors' => array(),
                '#submit' => array('rural_av_rating_tool_save_section_submit'),
            );

            $form['vehicle_details']['content']['actions']['confirm'] = array(
                '#type' => 'submit',
                '#name' => 'vehicle_details_confirm',
                '#value' => 'Confirm & View Quote',
                '#attributes' => array(
                    'id' => 'edit-vehicle-details-content-actions-confirm',
                    'class' => array('action-confirm'),
                    'data-current-section' => 'vehicle_details',
                    'data-current-section-id-segment' => 'vehicle-details',
                    'data-next-section' => 'premium',
                    'data-next-section-id-segment' => 'premium',
                ),
                '#ajax' => array(
                    'callback' => 'vehicle_details_confirm_callback',
                ),
                '#validate' => array('rural_av_rating_tool_quote_form_proposer_details_validate', 'rural_av_rating_tool_quote_form_statement_of_fact_validate', 'rural_av_rating_tool_vehicle_details_confirm_validate'),
                '#submit' => array('vehicle_details_confirm_submit'),
            );
        }

    }

    return $form;
}

/**
 * The add vehicle AJAX callback.
 * This is tightly coupled to the 'add vehicle submit handler'
 *
 * The following function is called as a final step of the
 * 'add vehicle' button press. The button is used to save vehicle details
 * entered as part of the vehicle details section form.
 *
 * If the form does not have errors we reload the vehicle details form.
 * This will update the 'vehicle list' table with our newly added vehicle.
 *
 * If however the form does have errors
 * i.e. Vehicle details have failed validation (missing fields etc)
 * Show the form errors above the vehicle details fieldset using drupals
 * ajax api.
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array
 */
function vehicle_details_add_vehicle_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    if(empty(form_get_errors())){
        return $form['vehicle_details']['content'];
    }

    return rav_callback_messages_show_section_messages('vehicle_details', TRUE, '#vehicle-list');
}

/**
 * The add vehicle validation handler.
 * This is tightly coupled to the 'add vehicle AJAX callback and submit handler'
 *
 * The following function is called after the 'add vehicle' button in the form
 * is pressed.
 *
 * After this button is pressed the details entered are validation, this
 * function will check:
 *   - The vehicle value is not greater than £100,000
 */
function vehicle_details_add_vehicle_validate($form, &$form_state){

    //Check all fields are filled
    if(empty($form_state['values']['vehicle_details']['content']['vehicle']['reg'])
        || empty($form_state['values']['vehicle_details']['content']['vehicle']['make_model'])
        || empty($form_state['values']['vehicle_details']['content']['vehicle']['year'])
        || empty($form_state['values']['vehicle_details']['content']['vehicle']['vehicle_type'])
        || $form_state['values']['vehicle_details']['content']['vehicle']['value'] == ''
        || $form_state['values']['vehicle_details']['content']['vehicle']['ncb'] == ''){
        form_set_error('add_vehicle_missing_data', 'Please ensure all fields are filled');
        return;
    }

    //Grab the vehicle year, check its a number, check its 4 digits, check its not in the future
    $vehicleYear = $form_state['values']['vehicle_details']['content']['vehicle']['year'];
    if(!is_numeric($vehicleYear) || strlen($vehicleYear) != 4){
        form_set_error('add_vehicle_invalid_year', 'Invalid vehicle year');
    }
    else if($vehicleYear > date("Y")){
        form_set_error('add_vehicle_year_in_future', 'Vehicle year can not be set to the future');
    }

    //Grab the vehcile type value, check it is a valid option
    $vehicleType = $form_state['values']['vehicle_details']['content']['vehicle']['vehicle_type'];
    if(!rav_helper_validate_dropdown_value($vehicleType, 'vehicle_details_vehicle_type')){
        form_set_error('add_vehicle_invalid_vehicle_type', 'Invalid vehicle type');
    }

    //Grab the vehicle ncb, check it is a valid option
    $vehicleNcb = $form_state['values']['vehicle_details']['content']['vehicle']['ncb'];
    if(!rav_helper_validate_dropdown_value($vehicleNcb, 'vehicle_details_ncb')){
        form_set_error('add_vehicle_invalid_vehicle_ncb', 'Invalid vehicle NCB');
    }


    //Grab the vehicle value, clean it up
    $vehicleValue = $form_state['values']['vehicle_details']['content']['vehicle']['value'];
    $vehicleValue = str_replace("£", "", $vehicleValue);
    $vehicleValue = str_replace(",", "", $vehicleValue);

    //Check the vehicle value is numeric after clean up, if acceptable value format it should be
    if(!is_numeric($vehicleValue)){
        form_set_error('add_vehicle_invalid_value', 'Invalid vehicle value');
    }

    //Check decimal places stored against value, match against expect £ format
    $vehicleValuesDecimalPlaces = strlen(substr(strrchr($vehicleValue, "."), 1));
    if($vehicleValuesDecimalPlaces != 0 && $vehicleValuesDecimalPlaces != 2){
        form_set_error('add_vehicle_invalid_value', 'Invalid vehicle value');
    }

    //Round vehicle value up to nearest whole number
    $vehicleValue = ceil($vehicleValue);

    //Check the vehicle value is not larger than the max value specified in config
    if($vehicleValue > RURAL_AV_RATING_TOOL_VALIDATE_MAX_VALUE){
        $formattedMaxValue = number_format((float)RURAL_AV_RATING_TOOL_VALIDATE_MAX_VALUE, 2, '.', ',');
        form_set_error('add_vehicle_value_too_high', 'Vehicle value can not be more than £'.$formattedMaxValue);
    }
}

/**
 * The add vehicle submit handler.
 * This is tighlty coupled to the 'add vehicle AJAX callback'
 *
 * The following function is called after the 'add vehicle' button in
 * the form is pressed assuming there are no validation errors produced
 * in the 'add vehicle validation handler'.
 * This button is used to save vehicle details against the quote.
 *
 * The function will first check if the vehicle has a 'vehicle index' attached
 * to it which is used as nothing more than to determine whether we are adding a new
 * vehicle or ammending one which has previously been saved/stored.
 *
 * If the vehicle details does have a 'vehicle index' attached to it we first check that
 * we have a stored vehicle with this index, if we do we update those details.
 *
 * If no 'vehicle index' is present or not vehicle exists with that index appened the
 * new details to the details stored in the drupal form state array.
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return bool
 */
function vehicle_details_add_vehicle_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }

    $form_state['rebuild'] = TRUE; //We tell out form we need to rebuild as the ajax attached returns updated form definitions

    //Force the reg to upper case
    $form_state['values']['vehicle_details']['content']['vehicle']['reg'] = strtoupper($form_state['values']['vehicle_details']['content']['vehicle']['reg']);

    if($form_state['values']['vehicle_details']['content']['vehicle']['index'] != NULL){
        $vehicleIndex = $form_state['values']['vehicle_details']['content']['vehicle']['index'];
        if(!empty($form_state['vehicles'][$vehicleIndex])){
            $form_state['vehicles'][$vehicleIndex] = $form_state['values']['vehicle_details']['content']['vehicle'];
            return true;
        }
    }

    $form_state['vehicles'][] = $form_state['values']['vehicle_details']['content']['vehicle'];
    return true;
}

/**
 * The 'cancel' vehicle AJAX callback.
 * The following is called when a vehicle is being added but the 'cancel' button
 * is pressed.
 *
 * This function defines ajax commands using the drupal ajax api.
 *
 * It:
 *   - Shows the 'add another vehicle' button
 *   - Resets all of the vehicle details input values
 *   - Triggers the 'change' event for these inputs to reset the float label state
 *   - Resets the save button text to 'Add Vehicle'
 *   - Removes any form errors from above the vehicle details fieldset
 *   - Hides (slidesUp) the vehicle details fieldset
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax commands to be fired by drupals ajax api
 */
function vehicle_details_cancel_vehicle_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_hideAddVehicleForm');
    $commands[] = ajax_command_invoke(NULL, 'avrt_showConfirmViewQuoteButton');
    $commands[] = ajax_command_invoke(NULL, 'avrt_resetAddVehicleForm');
    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * The 'add another vehicle' button AJAX callback.
 *
 * The following function defines ajax commands using the drupal ajax api.
 *
 * It:
 *   - Hides the 'add another vehicle button'
 *   - Resets all of the vehicle details input values
 *   - Triggers the 'change' for these inputs to reset the float label state
 *   - Resets the 'save' button to 'Add vehicle'
 *   - Removes any ajax errors above the vehicle details fieldset
 *   - Shows (slidesDown) the vehicle fieldset
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array An arra of ajax commands to be fired by drupals ajax api
 */
function vehicle_details_add_another_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_resetAddVehicleForm');
    $commands[] = ajax_command_invoke(NULL, 'avrt_showAddVehicleForm');
    $commands[] = ajax_command_invoke(NULL, 'avrt_hideConfirmViewQuoteButton');
    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * The edit saved vehicle details AJAX callback.
 * The following is triggered when the edit button for a saved vehicle is clicked.
 *
 * This callback relies on a data attribute 'data-vehicle-index' to exist against the button.
 * This data attribute is used to fetch the vehicle details by index from the stored values.
 * If this index does not exist again the button the callback is aborted (return false).
 *
 * If the index exists, but no vehicle details exist against that index the callback defaults
 * to the standard 'add another vehicle' ajax callback.
 *
 * If a vehicle with the specified index exists (data-vehicle-index) the following ajax commands
 * are fired using Drupal's ajax API:
 *   - Hide the 'add another vehicle button'
 *   - Sets the vehicle details input values to the values currently held for the vehicle
 *   - Triggers the change event for these inputs to reset the float label state
 *   - Shows (slideDown's) the vehicle fieldset
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array|bool An array of drupal ajax commands or false is the callback could not complete.
 */
function vehicle_details_edit_vehicle_from_list_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    if(!isset($form_state['triggering_element']['#attributes']['data-vehicle-index'])){
        return false;
    }

    $dataVehicleIndex = $form_state['triggering_element']['#attributes']['data-vehicle-index'];
    if(empty($vehicleDetails = $form_state['vehicles'][$dataVehicleIndex])){
        return vehicle_details_add_another_callback($form, $form_state);
    }

    $commands = array();
    $commands[] = ajax_command_invoke(NULL, 'avrt_populateAddVehicleForm', array(
        $dataVehicleIndex,
        $vehicleDetails['reg'],
        $vehicleDetails['make_model'],
        $vehicleDetails['year'],
        $vehicleDetails['vehicle_type'],
        $vehicleDetails['value'],
        $vehicleDetails['ncb']
    ));
    $commands[] = ajax_command_invoke(NULL, 'avrt_showAddVehicleForm');
    $commands[] = ajax_command_invoke(NULL, 'avrt_hideConfirmViewQuoteButton');
    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * The 'remove' vehicle from list AJAX callback.
 * This is tightly coupled with the 'remove' vehicle from list submit handler
 *
 * This callback relies on the remove button having a vehicle index data attribute
 * attached to it. If this data attribute does not exists cancel the callback (return false).
 *
 * If we're removing the vehicle that is currently open for editing we need to change the
 * 'save' buttons text from 'Update Vehicle' to 'Add Vehicle'
 *
 * Define ajax commands to slideUp and remove the vehicle details row from the vehicles list table.
 *
 * If we are removing the last vehicle from the list we must reset the vehicle details form section
 * to its initial state. To do this we must define ajax calls to do the following:
 *   - Hide the 'Add another vehicle' button
 *   - Reset the save vehicle details button value to 'Add Vehicle'
 *   - Hide the vehicle list container
 *   - Hide the canel button
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * Note: As AJAX callbacks can not manipulate the drupal form state array the actual removal
 * of the saved details is handled by the 'remove' vehicle submit handler
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array|bool An array of ajax commands to fire or false if callback failed.
 */
function vehicle_details_remove_vehicle_from_list_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    if(!isset($form_state['triggering_element']['#attributes']['data-vehicle-index'])){
        return false;
    }

    $commands = array();
    $dataVehicleIndex = $form_state['triggering_element']['#attributes']['data-vehicle-index'];


    $commands[] = ajax_command_invoke(NULL, 'avrt_removeVehicleFromVehicleDetailsList', array($dataVehicleIndex));

    if(empty($form_state['vehicles'])){
        $commands[] = ajax_command_invoke(NULL, "avrt_restartVehicleDetailsSection");
    }

    return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * The 'remove' vehicle from list submit handler
 * This is tighly coupled with the 'remove' vehicle from list AJAX callback.
 *
 * This callback relies on the remove button having a vehicle index data attribute
 * attached to it. If this data attribute does not exists cancel the callback (return false).
 *
 * If the data attribute exists remove any vehicle details stored under that vehicle index.
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * The AJAX callback will take care of the 'visual' removal
 * i.e. hiding the table row that holds this vehicle etc etc
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether or not this handler completed successfully.
 */
function vehicle_details_remove_vehicle_from_list_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }

    if(!isset($form_state['triggering_element']['#attributes']['data-vehicle-index'])){
        return false;
    }
    $dataVehicleIndex = $form_state['triggering_element']['#attributes']['data-vehicle-index'];
    unset($form_state['vehicles'][$dataVehicleIndex]);
    return true;
}

/**
 * todo: comment
 */
function rural_av_rating_tool_vehicle_details_confirm_validate($form, &$form_state){
    //Ensure we have atleast one vehicle to generate a premium against
    if(empty($form_state['vehicles'])){
        form_set_error('vehicle_details_no_vehicles', 'A minimum of 1 vehicle is required to generate a quote.');
        return FALSE;
    }

    //Ensure atleast one of the valid vehicle types are present
    //Get the list of vehicle types, remove any types that are not required to pass this validation
    //step. Loop over each vehicle, if it is one of the required types then set this step as pass
    //break out of loop and dont show an error.
    $hasRequiredVehicleType = FALSE;
    $requiredVehicleTypes = rav_helper_get_dropdown_values('vehicle_details_vehicle_type');
    unset($requiredVehicleTypes['atv']);
    unset($requiredVehicleTypes['trailer_implement']);
    foreach($form_state['vehicles'] as $vehicleDetails){
        if(array_key_exists($vehicleDetails['vehicle_type'], $requiredVehicleTypes)){
            $hasRequiredVehicleType = TRUE;
            break;
        }
    }
    if(!$hasRequiredVehicleType){
        form_set_error('vehicle_details_no_tractor_combine', 'To provide a quote, you must include at least one vehicle which is not a Trailer or ATV.');
        return FALSE;
    }

    return TRUE;
}

/**
 * Todo: comment
 *
 * As with all of the submission handlers within the AV Rating Tool
 * we must first check the form is an a state in which we should process
 * any submission handlers. This is done using the submit helper:
 * rav_helper_submit_can_process_submit(). This call checks that the
 * form/submission status is 'OPEN' and that there is no referral
 * reason currently set against the submission. Doing this check ensure
 * no form data can be manipulated whilst the form is in an uneditable
 * state.
 *
 * @param $form
 * @param $form_state
 *
 * @return bool Whether the submission was ran successfully or not.
 */
function vehicle_details_confirm_submit($form, &$form_state){
    if(!rav_helper_submit_can_process_submit($form_state)){
        return FALSE;
    }

    /* @todo make cleaner -- add to save */
    //todo: make into set storage call
    $form_state['storage']['values']['vehicle_details']['vehicles'] = $form_state['vehicles'];

    $discount = (isset($form_state['storage']['premium']['discount'])) ? $form_state['storage']['premium']['discount'] : 0;

    rav_helper_storage_set_next_step($form_state);
    rav_helper_set_storage_values('proposer_details', $form_state);
    rav_helper_set_storage_values('statement_of_fact', $form_state);
    rav_helper_premium_generate_initial_premium($form_state, $discount);
    rural_av_rating_tool_save_section_submit($form, $form_state);
}

/**
 *
 * todo: comment
 *
 * Before any callback functionality is processed we first check there is no
 * referral reason set against the form, if there is we must display the modal
 * using the helper funciton: rav_callback_modal_display_modal(). Passing it
 * the feched trigger (rav_helper_get_storage_referral()) as an agument.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function vehicle_details_confirm_callback($form, &$form_state){
    if(!empty($referralTrigger = rav_helper_get_storage_referral($form_state))){
        return rav_callback_modal_display_modal($referralTrigger);
    }

    if(form_get_errors()){
        return rav_callback_messages_show_section_messages('vehicle_details', TRUE, '#avrt-quote-gen-error-anchor');
    }

    //Build quote reference and url
    $quoteReference = rural_av_rating_tool_helper_generate_quote_ref_from_int(rav_helper_get_id_from_storage($form_state));
    $quoteUrl = '/'.str_replace('*', $quoteReference, RURAL_AV_RATING_TOOL_RELOAD_QUOTE_URL_PATTERN);

    //Build vehicle premium breakdown table html
    $vehicleData = rav_helper_get_storage_values('vehicle_details', $form_state);
    $vehiclPremiumBreakdownTableHtml = rav_builder_premium_build_vehicle_premium_breakdown_table_html($vehicleData['vehicles']);

    //Initialise commands
    $commands = array();

    //Send user to premium display
    $commands[] = ajax_command_invoke(NULL, 'avrt_goToPremium');

    //Populate/update premium breakdown data
    $commands[] = ajax_command_invoke(NULL, 'avrt_populatePremiumBreakdown', array(
        number_format((float)$form_state['storage']['premium']['final_premium'], 2, '.', ','),
        number_format((float)$form_state['storage']['premium']['init_premium'], 2, '.', ','),
        $form_state['storage']['premium']['ipt'],
        number_format((float)$form_state['storage']['premium']['ipt_value'], 2, '.', ','),
        number_format((float)$form_state['storage']['premium']['rural_fee'], 2, '.', ','),
        $form_state['storage']['premium']['discount'],
        $quoteReference
    ));

    //Populate/update vehicle premium breakdown table
    $commands[] = ajax_command_invoke(NULL, 'avrt_buildPremiumVehicleBreakdownTable', array($vehiclPremiumBreakdownTableHtml));

    //Update quote url, add quote reference
    $commands[] = ajax_command_invoke(NULL, 'avrt_updatePageUrl', array($quoteUrl));

    return array('#type' => 'ajax', '#commands' => $commands);
}
