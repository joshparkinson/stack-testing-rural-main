<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_form().
 * Build the main AV Rating Tool's form.
 *
 * The following pulls in the form step's definition arrays from
 * the various form building functions within this module.
 * Each of the form steps controls its own validation/submission handlers.
 * Shared callbacks are also included in this form.
 *
 * Start by initialising the form state storage array. Populate this with
 * data from the database or provided detaults.
 *
 * todo: add notes about setting referrals in validation functions
 * todo: add notes about checking whether the form can process submit functions in all action submit handlers
 * todo: add referral checks in all action ajax callbacks
 * todo: add can submit checks to all action submit handlers
 * todo: add no dynamic ajax buttons names, change these values with javascript
 *
 *
 * @param array $form       The drupal form definition arrya
 * @param array $form_state The drupal form state array
 * @param array $quoteData  An array of saved quote data, populated on form block load.
 *                          Empty array if no saved data is fetched from the database on load.
 *
 * @return array The form definition array
 */
function rural_av_rating_tool_quote_form($form, &$form_state, $quoteData = array()){
    rural_av_rating_tool_helper_modal_initialise_modals();

    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/rural_av_rating_tool.form.quote.master');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/rural_av_rating_tool.form.quote.proposer_details');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/rural_av_rating_tool.form.quote.statement_of_fact');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/rural_av_rating_tool.form.quote.vehicle_details');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/rural_av_rating_tool.form.quote.premium');

    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/builders/rural_av_rating_tool.form.quote.builder.global');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/builders/rural_av_rating_tool.form.quote.builder.premium');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/builders/rural_av_rating_tool.form.quote.builder.readonly');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/builders/rural_av_rating_tool.form.quote.builder.vehicle_details');

    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.edit');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.messages');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.modal');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.next');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.readonly');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/callbacks/rural_av_rating_tool.form.quote.callbacks.save');

    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.dropdowns');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.multistep');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.premium');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.referral');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.storage');
    form_load_include($form_state, 'inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.submit');

    if(!isset($form_state['storage'])){
        $form_state['storage'] = array(
            'id' => (!empty($quoteData['id'])) ? $quoteData['id'] : NULL,
            'steps' => array(
                'current' => (!empty($quoteData['steps']['current'])) ? $quoteData['steps']['current'] : 'proposer_details',
                'completed' => (!empty($quoteData['steps']['completed'])) ? $quoteData['steps']['completed'] : array(),
                'reached' => (!empty($quoteData['steps']['reached'])) ? $quoteData['steps']['reached'] : array(),
            ),
            'values' => (!empty($quoteData['values'])) ? $quoteData['values'] : array(),
            'status' => (!empty($quoteData['status'])) ? $quoteData['status'] : RURAL_AV_RATING_TOOL_STATUS_OPEN,
            'premium' => (!empty($quoteData['premium'])) ? $quoteData['premium'] : NULL,
            'referral' => (!empty($quoteData['referral'])) ? $quoteData['referral'] : NULL,
            'expire_reason' => (!empty($quoteData['expire_reason'])) ? $quoteData['expire_reason'] : NULL,
        );
    }

    $form = array_merge($form, rural_av_rating_tool_quote_form_proposer_details($form, $form_state));
    $form = array_merge($form, rural_av_rating_tool_quote_form_statement_of_fact($form, $form_state));
    $form = array_merge($form, rural_av_rating_tool_quote_form_vehicle_details($form, $form_state));
    $form = array_merge($form, rural_av_rating_tool_quote_form_premium($form, $form_state));

    $form['#attributes']['autocomplete'] = 'off';
    $form['#attached']['js'][drupal_get_path('module', 'rural_av_rating_tool').'/assets/js/rural_av_rating_tool.form.init.js'] = array('type' => 'file');
    $form['#attached']['js'][drupal_get_path('module', 'rural_av_rating_tool').'/assets/js/rural_av_rating_tool.form.callback_methods.js'] = array('type' => 'file');

    if(!rav_helper_check_storage_status($form_state, RURAL_AV_RATING_TOOL_STATUS_OPEN)){
        $form = rav_builder_readonly_build_readonly_form($form, $form_state);
    }

    return $form;
}