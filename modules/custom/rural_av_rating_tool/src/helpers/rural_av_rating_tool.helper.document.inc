<?php

use Dompdf\Dompdf;

function rural_av_rating_tool_helper_create_document($type, $params){
    libraries_load('dompdf');

    $documentHtml = _ravdochelper_create_doc_html($type, $params);

    $dompdf = new Dompdf();
    $dompdf->setPaper('A4');
    $dompdf->setBasePath(RURAL_AV_RATING_TOOL_DOC_BASE);
    $dompdf->loadHtml($documentHtml);
    $dompdf->render();

    $filePath = 'temporary://'.$type.'.pdf';
    file_unmanaged_save_data($dompdf->output(), $filePath, FILE_EXISTS_REPLACE);
    return $filePath;
}

function _ravdochelper_create_doc_html($type, $params){
    $baseHtml = file_get_contents(RURAL_AV_RATING_TOOL_DOC_BASE.'/templates/document_base.html');
    $headerHtml = file_get_contents(RURAL_AV_RATING_TOOL_DOC_BASE.'/templates/document_header.html');
    $footerHtml = file_get_contents(RURAL_AV_RATING_TOOL_DOC_BASE.'/templates/document_footer.html');

    switch($type){
        case 'schedule':
        default:
            $pageHtml = _ravdochelper_get_schedule($params);
            break;
    }

    return strtr($baseHtml, array(
        '[@header]' => $headerHtml,
        '[@footer]' => $footerHtml,
        '[@page]' => strtr($pageHtml, array(
            '[@pagebreak]' => '<div style="page-break-before: always"></div>',
        ))
    ));
}


function _ravdochelper_get_schedule($params){
    return strtr(
        file_get_contents(RURAL_AV_RATING_TOOL_DOC_BASE.'/templates/schedule.html'),
        array(
            '[@date]' => date('d / m / Y'),
            '[@quoteRef]' => $params['quoteReference'],
            '[@proposerName]' => $params['proposerDetails']['name'],
            '[@proposerAddress]' => $params['proposerDetails']['address'],
            '[@postcode]' => $params['proposerDetails']['postcode'],
            '[@coverStartDate]' => $params['proposerDetails']['cover_start_date'],
            '[@premiumExcludingIPT]' => $params['premiumDetails']['init_premium'],
            '[@IPT]' => $params['premiumDetails']['ipt_value'],
            '[@premium]' => $params['premiumDetails']['final_premium'],
            '[@ruralFee]' => $params['premiumDetails']['rural_fee'],
            '[@finalPremium]' => $params['premiumDetails']['final_premium']
        )
    );
}