<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/* @todo: comment - move into database / rename */

function rural_av_rating_tool_helper_generate_quote_ref_from_int($quoteInt){
    return RURAL_AV_RATING_TOOL_SUBMISSION_ID_PREFIX.str_pad($quoteInt, RURAL_AV_RATING_TOOL_SUBMISSION_ID_PADDING_LENGTH, '0', STR_PAD_LEFT);
}

function rural_av_rating_tool_helper_generate_int_from_quote_ref($quoteRef){
    return ltrim(str_replace(RURAL_AV_RATING_TOOL_SUBMISSION_ID_PREFIX, "", $quoteRef), '0');
}

function rural_av_rating_tool_helper_get_quote_from_ref($quoteRef, $userId){
    $quoteId = rural_av_rating_tool_helper_generate_int_from_quote_ref($quoteRef);
    if(!is_numeric($quoteId)){
        return FALSE;
    }

    $quoteQuery = db_select(RURAL_AV_RATING_TOOL_DB_TABLE, 't');
    $quoteQuery->addField('t', 'id');
    $quoteQuery->addField('t', 'form_data');
    $quoteQuery->addField('t', 'status');
    $quoteQuery->addField('t', 'premium');
    $quoteQuery->addField('t', 'expire_reason');
    $quoteQuery->condition('id', $quoteId, '=');
    $quoteQuery->condition('user_id', $userId, '=');
    $quoteQuery->condition('deleted', 0, '=');
    $quoteData = $quoteQuery->execute();

    if($quoteData->rowCount() == 0){
        return array();
    }

    $quoteData = $quoteData->fetchAssoc();
    $formData = unserialize($quoteData['form_data']);

    return array(
        'id' => $quoteData['id'],
        'steps' => $formData['steps'],
        'values' => $formData['values'],
        'status' => $quoteData['status'],
        'premium' => (!empty($quoteData['premium'])) ? unserialize($quoteData['premium']) : NULL,
        'referral' => $formData['referral'],
        'expire_reason' => $quoteData['expire_reason']
    );
}

function rural_av_rating_tool_helper_quote_exists_with_id($quoteId, $userId){
    $checkQuery = db_select(RURAL_AV_RATING_TOOL_DB_TABLE, 't');
    $checkQuery->addField('t', 'id');
    $checkQuery->condition('id', $quoteId);
    $checkQuery->condition('user_id', $userId);
    $checkResult = $checkQuery->execute();
    return (bool) $checkResult->rowCount();
}

//todo: dont move this!

/**
 * Rural AV Rating Tool Helper Function.
 * Check the validitiy of a passed status against
 * the allowed/expected Submission status.
 *
 * @param string $status The status to check
 *
 * @return bool Whether the status is valid or not
 */
function rav_helper_submission_valid_status($status){
    return in_array($status, array(
        RURAL_AV_RATING_TOOL_STATUS_OPEN,
        RURAL_AV_RATING_TOOL_STATUS_CLOSED,
        RURAL_AV_RATING_TOOL_STATUS_EXPIRED,
    ));
}