<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Modal initialisation function.
 *
 * The following function is used to initialise the modal
 * functionality to be used within the Rural AV Rating
 * Tool. It should be called within any section of this tool
 * that requires to the use of modal windows to display any
 * content/copy.
 *
 * The function adds the required chaos tools libraries so that
 * modals will function correctly as well as defining an array
 * of modal styles which can be used to dictate the way in which
 * the modal is rendered.
 */
function rural_av_rating_tool_helper_modal_initialise_modals(){
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    $modalStyles = array(
        'rural-av-rating-tool-modal' => array(
            'modalSize' => array(
                'type' => 'fixed',
                'width' => 750,
                'height' => 460,
                //'contentRight' => 32,
                //'contentBottom' => 32,
            ),
            'modalOptions' => array(
                'opacity' => 0.9,
                'background-color' => '#131a20',
            ),
            'animation' => 'fadeIn',
            'modalTheme' => 'rural_av_rating_tool_modal',
            'throbberTheme' => 'rural_av_rating_tool_modal_throbber',
        ),
    );

    drupal_add_js($modalStyles, 'setting');
    drupal_add_js(drupal_get_path('module', 'rural_av_rating_tool').'/assets/js/rural_av_rating_tool.modal.theme.js');
}