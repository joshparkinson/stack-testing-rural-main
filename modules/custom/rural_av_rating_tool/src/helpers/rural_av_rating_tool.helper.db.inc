<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Helper Function.
 * Update a saved submission with the passed status
 *
 * @param int    $quoteId The id of the quote we're updating
 * @param string $status  The status to set for the quote
 *
 * @return bool Wether the update was successful or not
 */
function rav_db_helper_update_quote_status($quoteId, $status){
    if(!rav_helper_submission_valid_status($status)){
        return FALSE;
    }

    $numUpdatedRows = db_update(RURAL_AV_RATING_TOOL_DB_TABLE)
        ->fields(array(
            'status' => $status,
            'updated' => time(),
        ))
        ->condition('id', intval($quoteId))
        ->execute();

    return ($numUpdatedRows > 0) ? TRUE : FALSE;
}