<?php

//todo: comment
function rural_av_rating_tool_callback_menu_delete_quote_modal($quoteReference, $js = FALSE){
    if(!user_is_logged_in()){
        drupal_access_denied();
        drupal_exit();
    }

    ctools_include('modal');
    ctools_include('ajax');

    global $user;

    $quoteId = rural_av_rating_tool_helper_generate_int_from_quote_ref($quoteReference);
    if(!is_numeric($quoteId) || !rural_av_rating_tool_helper_quote_exists_with_id($quoteId, $user->uid)){
        drupal_set_message('Error deleting submission: Invalid Quote Reference.', 'error');
        $commands = array(ctools_ajax_command_redirect('/unity/av-rating-tool'));
        print ajax_render($commands);
        return;
    }


    $output = '<div class="confirm-delete-modal">';
    $output .= '<div class="icon-wrapper confirm-delete"></div>';
    $output .= '<h1>Are you sure?</h1>';

    $output .= '<p>Please confirm you want to delete the following submission</p>';
    $output .= '<div class="quote-reference"><p>Quote reference: <span>'.$quoteReference.'</span></p></div>';

    $output .= '<div class="actions-wrapper clearfix">';
    $output .= '<a href="#" class="cancel ctools-close-modal">Cancel</a>';
    $output .= '<a href="/unity/av-rating-tool/delete/'.$quoteReference.'/confirm" class="delete">Delete</a>';
    $output .= '</div>';

    $output .= '</div>';

    ctools_modal_render(NULL, $output);
}

//todo: comment
function rural_av_rating_tool_callback_menu_delete_submission($quoteRef){
    if(!user_is_logged_in()){
        drupal_access_denied();
        drupal_exit();
    }

    global $user;

    $quoteId = rural_av_rating_tool_helper_generate_int_from_quote_ref($quoteRef);
    if(!is_numeric($quoteId) || !rural_av_rating_tool_helper_quote_exists_with_id($quoteId, $user->uid)){
        drupal_set_message('Error deleting submission: Invalid Quote Reference.', 'error');
        drupal_goto(RURAL_AV_RATING_TOOL_ENTRY_PAGE_URL);
    }

    $deleteQuery = db_update(RURAL_AV_RATING_TOOL_DB_TABLE)
        ->fields(array(
            'deleted' => 1,
            'updated' => time(),
        ))
        ->condition('id', $quoteId)
        ->condition('user_id', $user->uid)
        ->execute();

    drupal_set_message('Quote '.$quoteRef.' deleted successfully');
    drupal_goto(RURAL_AV_RATING_TOOL_ENTRY_PAGE_URL);
}