<?php

function rural_av_rating_tool_callback_menu_reports_export(){

    module_load_include('inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.dropdowns');

    $filename = 'ravrt_vehicle_premium_data_export_'.date('Ymd').'.csv';

    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'"');

    $fp = fopen('php://output', 'w');

    $exportHeader = array('Quote Ref', 'Reg', 'Make/Model', 'Year', 'Type',
        'Value', 'NCD', 'Premium (excl. IPT & Fee)', 'Proposer Postcode',
        'Proposer Occupation', 'Cover Start Date', 'Submission IPT %',
        'Submission IPT Value', 'Submission Discount %',
        'Submission Discount Value',
        'Submission Date',
        'Submission Timestamp');

    fputcsv($fp, $exportHeader);

    $submittedSubmissionsData = db_select(RURAL_AV_RATING_TOOL_DB_TABLE, 'ravrt');
    $submittedSubmissionsData->addField('ravrt', 'id');
    $submittedSubmissionsData->addField('ravrt', 'premium');
    $submittedSubmissionsData->addField('ravrt', 'form_data');
    $submittedSubmissionsData->addField('ravrt', 'updated', 'submitted_timestamp');
    $submittedSubmissionsData->condition('status', RURAL_AV_RATING_TOOL_STATUS_CLOSED, '=');
    $submittedSubmissionsData->orderBy('id', 'ASC');
    $submittedSubmissionsData = $submittedSubmissionsData->execute();

    while($submissionData = $submittedSubmissionsData->fetchAssoc()){
        $premiumData = unserialize($submissionData['premium']);
        $formData = unserialize($submissionData['form_data']);

        $quoteRef = rural_av_rating_tool_helper_generate_quote_ref_from_int($submissionData['id']);
        $proposerPostcode = $formData['values']['proposer_details']['postcode'];
        $proposerOccupation = rav_helper_get_dropdown_value_label('proposer_details_main_occupation', $formData['values']['proposer_details']['main_occupation']);
        $coverStartDate = DateTime::createFromFormat('Y-m-d', $formData['values']['proposer_details']['cover_start_date'])->format('d / m / Y');
        $submissionIptPercent = $premiumData['ipt'];
        $submissionIptValue = number_format((float)$premiumData['ipt_value'], 2, '.', '');
        $submissionDiscountPercent = $premiumData['discount'];
        $submissionDiscountValue = number_format((float)($premiumData['final_premium'] * ($premiumData['discount'] / 100)), 2, '.', '');
        $submissionDate = date('d / m / Y', $submissionData['submitted_timestamp']);
        $submissionTimestamp = $submissionData['submitted_timestamp'];

        foreach($formData['values']['vehicle_details']['vehicles'] as $vehicleData){
            $csvRow = array(
                $quoteRef,
                $vehicleData['reg'],
                $vehicleData['make_model'],
                $vehicleData['year'],
                rav_helper_get_dropdown_value_label('vehicle_details_vehicle_type', $vehicleData['vehicle_type']),
                number_format((float)str_replace("£", "", str_replace(",", "", $vehicleData['value'])), 2, '.', ''),
                $vehicleData['ncb'],
                number_format((float)$vehicleData['premium'], 2, '.', ''),
                $proposerPostcode,
                $proposerOccupation,
                $coverStartDate,
                $submissionIptPercent,
                $submissionIptValue,
                $submissionDiscountPercent,
                $submissionDiscountValue,
                $submissionDate,
                $submissionTimestamp
            );

            fputcsv($fp, $csvRow);
        }
    }

    drupal_exit();
}