<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Rural AV Rating Tool Page Callback Function.
 * AV Rating Tool modal base page.
 *
 * The following function is used to display the contents of a
 * modal which is to be displayed within the Rural AV Rating Tool.
 *
 * This function acts as a request handler and passes the building of
 * the actual modal content to the various builders based on the
 * $trigger it recieves.
 *
 * The $js boolean is just an indication of whether we the user
 * has javascript enabled thus can will be able to view the modal
 * as intended as the Rural AV Rating Tool requires that javascript
 * is enabled we can ignore this. It must be passed as part of
 * the choas tools modal functionality requirement.
 *
 * Inlcude the necessary chaos tools libraries so that we can
 * display the modal properly.
 *
 * @param string $triggeringSection
 * @param bool   $js
 */
function rural_av_rating_tool_modal_page($trigger, $js = FALSE){
    ctools_include('modal');
    ctools_include('ajax');

    switch($trigger){
        case 'postcode':
            rural_av_rating_tool_page_builder_modal_postcode();
            break;
        case 'statement_of_fact':
            rural_av_rating_tool_page_builder_modal_statement_of_fact();
            break;
        case 'save':
            rural_av_rating_tool_page_builder_modal_saved();
            break;
        case 'confirm_submit':
            rural_av_rating_tool_page_builder_modal_confirm_submit();
            break;
    }
}

function rural_av_rating_tool_page_builder_modal_confirm_submit(){
    if(empty($quoteReference = $_GET['quoteRef'])){
        return null;
    }

    $output = '<div class="confirm-submit-modal">';

    $output .= '<div class="icon-wrapper confirm-submit"></div>';

    $output .= '<h1>Are you sure you want to submit this quote and place it on cover?</h1>';

    $output .= '<div class="actions-wrapper clearfix">';
    $output .= '<a href="/unity/av-rating-tool/confirm-quote/'.$quoteReference.'" class="submit-to-cover">Submit for cover</a>';
    $output .= '</div>';

    $output .= '</div>';

    ctools_modal_render(NULL, $output);
}

function rural_av_rating_tool_page_builder_modal_postcode(){
    $output = '<div class="referral-modal">';

    $output .= '<div class="icon-wrapper decline"></div>';

    $output .= '<h1>Unfortunately, we are unable to quote on Agriculutral Vehicle risks within Northern Ireland through our quick quote tool.</h1>';

    $output .= '<p>Please contact our New Business Underwriting Team on<br /><span>0344 55 77 177</span> or email <a href="mailto:newbusiness@ruralinsurance.co.uk">newbusiness@ruralinsurance.co.uk</a></p>';

    $output .= '<div class="actions-wrapper clearfix">';
    $output .= '<a href="/unity/documentation/proposal-documents" class="download-submission">Download submission form</a>';
    $output .= '<a href="mailto:newbusiness@ruralinsurance.co.uk?subject=Agricultural Vehicle Submission" class="send-submission">Send submission form</a>';
    $output .= '</div>';

    $output .= '</div>';

    ctools_modal_render(NULL, $output);
}

//todo: rename functions, comment properly
function rural_av_rating_tool_page_builder_modal_saved(){
    $quoteReference = (!empty($_GET['quoteRef'])) ? $_GET['quoteRef'] : NULL;

    $output = '<div class="saved-modal">';

    $output .= '<div class="icon-wrapper saved"></div>';

    $output .= '<h1>Your quote has been saved</h1>';

    $output .= '<div class="quote-reference"><p>Your quote reference: <span>'.$quoteReference.'</span></p></div>';

    $output .= '<div class="actions-wrapper clearfix">';
    $output .= '<a href="/'.RURAL_AV_RATING_TOOL_ENTRY_PAGE_URL.'" class="saved-submissions">View saved quotes</a>';
    $output .= '<a href="/'.RURAL_AV_RATING_TOOL_QUOTE_PAGE_URL.'" class="new-quote">Start a new quote</a>';
    $output .= '</div>';

    $output .= '<p class="sub">Close this window to continue with your submission</p>';

    $output .= '</div>';

    ctools_modal_render(NULL, $output);
}

function rural_av_rating_tool_page_builder_modal_statement_of_fact(){
    $output = '<div class="referral-modal">';

    $output .= '<div class="icon-wrapper referral"></div>';

    $output .= '<h1>Based on the information provided, our underwriters will need to review this risk to provide a quotation.</h1>';

    $output .= '<p>Please contact our New Business Underwriting Team on<br /><span>0344 55 77 177</span> or email <a href="mailto:newbusiness@ruralinsurance.co.uk">newbusiness@ruralinsurance.co.uk</a></p>';

    $output .= '<div class="actions-wrapper clearfix">';
    $output .= '<a href="/unity/documentation/proposal-documents" class="download-submission">Download submission form</a>';
    $output .= '<a href="mailto:newbusiness@ruralinsurance.co.uk?subject=Agricultural Vehicle Submission" class="send-submission">Send submission form</a>';
    $output .= '</div>';

    $output .= '</div>';

    ctools_modal_render(NULL, $output);
}