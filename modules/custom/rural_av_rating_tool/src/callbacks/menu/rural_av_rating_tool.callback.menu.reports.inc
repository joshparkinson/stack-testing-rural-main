<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

function rural_av_rating_tool_callback_menu_reports_page(){

    /**
     * Submission Totals
     */

    $submissionTotalsData = db_select(RURAL_AV_RATING_TOOL_DB_TABLE);
    $submissionTotalsData->addExpression('COUNT(*)', 'total');
    $submissionTotalsData->addExpression('COUNT(IF(status = :open, 1, NULL))', 'open', array(':open' => RURAL_AV_RATING_TOOL_STATUS_OPEN));
    $submissionTotalsData->addExpression('COUNT(IF(status = :submitted, 1, NULL))', 'submitted', array(':submitted' => RURAL_AV_RATING_TOOL_STATUS_CLOSED));
    $submissionTotalsData->addExpression('COUNT(IF(status = :expired, 1, NULL))', 'expired', array(':expired' => RURAL_AV_RATING_TOOL_STATUS_EXPIRED));
    $submissionTotalsData = $submissionTotalsData->execute()->fetchAssoc();

    $submissionsTotalTable = theme('table', array(
        'header' => array(
            array('data' => 'Total submissions',     'class' => 'total'),
            array('data' => 'Open submissions',      'class' => 'open'),
            array('data' => 'Submitted submissions', 'class' => 'submitted'),
            array('data' => 'Expired submissions',   'class' => 'expired'),
        ),
        'rows' => array(
            array(
                array('data' => $submissionTotalsData['total'],     'class' => 'total'),
                array('data' => $submissionTotalsData['open'],      'class' => 'open'),
                array('data' => $submissionTotalsData['submitted'], 'class' => 'submitted'),
                array('data' => $submissionTotalsData['expired'],   'class' => 'expired')
            )
        ),
        'sticky' => FALSE,
    ));

    /**
     * Premium averages
     */

    $maxPremiumGenerated = 0;
    $minPremiumGenerated = 0;
    $sumPremiumGenerated = 0;

    $premiumAveragesData = db_select(RURAL_AV_RATING_TOOL_DB_TABLE, 'ravrt');
    $premiumAveragesData->addField('ravrt', 'id');
    $premiumAveragesData->addField('ravrt', 'premium');
    $premiumAveragesData->condition('ravrt.status', RURAL_AV_RATING_TOOL_STATUS_CLOSED, '=');
    $premiumAveragesData = $premiumAveragesData->execute()->fetchAllAssoc('id');

    foreach($premiumAveragesData as $premiumRow){
        $finalPremium = unserialize($premiumRow->premium)['final_premium'];
        $sumPremiumGenerated += $finalPremium;
        $maxPremiumGenerated = ($finalPremium > $maxPremiumGenerated) ? $finalPremium : $maxPremiumGenerated;
        $minPremiumGenerated = ($minPremiumGenerated == 0 || $finalPremium < $minPremiumGenerated) ? $finalPremium : $minPremiumGenerated;
    }

    $avgPremiumGenerated = '£'.number_format((float)($sumPremiumGenerated / sizeof($premiumAveragesData)), 2, '.', ',');
    $maxPremiumGenerated = '£'.number_format((float)$maxPremiumGenerated, 2, '.', ',');
    $minPremiumGenerated = '£'.number_format((float)$minPremiumGenerated, 2, '.', ',');
    $sumPremiumGenerated = '£'.number_format((float)$sumPremiumGenerated, 2, '.', ',');

    $premiumAveragesTable = theme('table', array(
        'header' => array(
            array('data' => 'Total premiums', 'class' => 'total'),
            array('data' => 'Average premium', 'class' => 'average'),
            array('data' => 'Max premium', 'class' => 'max'),
            array('data' => 'Min premium', 'class' => 'min')
        ),
        'rows' => array(
            array(
                array('data' => $sumPremiumGenerated, 'class' => 'total'),
                array('data' => $avgPremiumGenerated, 'class' => 'average'),
                array('data' => $maxPremiumGenerated, 'class' => 'max'),
                array('data' => $minPremiumGenerated, 'class' => 'min'),
            )
        ),
        'sticky' => FALSE,
    ));



    /**
     * Output
     */

    $output = '<h2>Submission Breakdown</h2>';
    $output .= $submissionsTotalTable;

    $output .= '<h2>Premium Breakdown</h2>';
    $output .= '<p>Please not all values show below are inclusive of IPT, Rural\'s Fee and any applied broker discount.</p>';
    $output .= $premiumAveragesTable;

    return $output;
}