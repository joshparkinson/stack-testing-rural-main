<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 28/09/2016
 * Time: 11:38
 */

function rural_av_rating_tool_callback_menu_confirm_quote_submission($quoteReference){
    global $user;

    //Attempt to get quote, check valid, if not send back to tool entry page
    $quoteData = rural_av_rating_tool_helper_get_quote_from_ref($quoteReference, $user->uid);
    if(empty($quoteData) || $quoteData['status'] != RURAL_AV_RATING_TOOL_STATUS_OPEN){
        drupal_goto(RURAL_AV_RATING_TOOL_ENTRY_PAGE_URL);
        return FALSE;
    }

    //Get the quote
    $quoteId = rural_av_rating_tool_helper_generate_int_from_quote_ref($quoteReference);

    //Get the submitting user
    $submittingUser = user_load($user->uid);

    //Include the helpers
    module_load_include('inc', 'rural_av_rating_tool', 'src/forms/helpers/rural_av_rating_tool.form.quote.helper.dropdowns');

    //Build the email and tracking vehicle details from the saved data
    $vehicleDetailsEmail = array();
    $vehicleDetailsTracking = array();
    foreach($quoteData['values']['vehicle_details']['vehicles'] as $vehicleData){
        $vehicleDetailsEmail[] = array(
            'reg' => $vehicleData['reg'],
            'make_model' => $vehicleData['make_model'],
            'year' => $vehicleData['year'],
            'vehicle_type' => rav_helper_get_dropdown_value_label('vehicle_details_vehicle_type', $vehicleData['vehicle_type']),
            'value' => '£'.number_format((float)str_replace('£', '', str_replace(',', '', $vehicleData['value'])), 2, '.', ','),
            'ncb' => rav_helper_get_dropdown_value_label('vehicle_details_ncb', $vehicleData['ncb']),
            'premium' => '£'.number_format((float)$vehicleData['premium'], 2, '.', ','),
        );

        $vehicleDetailsTracking[] = array(
            'name' => $vehicleData['make_model'],
            'sku' => 'AVRT-'.strtoupper(str_replace(' ', '', trim($vehicleData['reg']))),
            'category' => 'AVRT_'.strtolower(trim($vehicleData['vehicle_type'])),
            'price' => number_format((float)$vehicleData['premium'], 2, '.', ''),
        );
    }

    //Build the data we need to sent out in the emails
    $emailData = array(
        'quoteReference' => $quoteReference,
        'submittingUser' => array(
            'name'        => $submittingUser->field_first_name['und'][0]['value'].' '.$submittingUser->field_last_name['und'][0]['value'],
            'email'       => $submittingUser->mail,
            'company'     => $submittingUser->field_company_name['und'][0]['value'],
            'location'    => $submittingUser->field_region['und'][0]['value']
        ),
        'proposerDetails' => array(
            'name' => $quoteData['values']['proposer_details']['proposer_name'],
            'address' => $quoteData['values']['proposer_details']['proposer_address'],
            'postcode' => $quoteData['values']['proposer_details']['postcode'],
            'occupation' => rav_helper_get_dropdown_value_label('proposer_details_main_occupation', $quoteData['values']['proposer_details']['main_occupation']),
            'previous_insurer' => $quoteData['values']['proposer_details']['previous_insurer'],
            'cover_start_date' => DateTime::createFromFormat('Y-m-d', $quoteData['values']['proposer_details']['cover_start_date'])->format('d / m / Y'),
        ),
        'vehicleDetails' => $vehicleDetailsEmail,
        'premiumDetails' => array(
            'ipt_value' => '£'.number_format((float)$quoteData['premium']['ipt_value'], 2, '.', ','),
            'ipt' => 'IPT @ '.$quoteData['premium']['ipt'].'%: £'.number_format((float)$quoteData['premium']['ipt_value'], 2, '.', ','),
            'rural_fee' => 'Rural Fee: £'.number_format((float)$quoteData['premium']['rural_fee'], 2, '.', ','),
            'final_premium' => '£'.number_format((float)$quoteData['premium']['final_premium'], 2, '.', ','),
            'init_premium' => 'exc. IPT: £'.number_format((float)$quoteData['premium']['init_premium'], 2, '.', ','),
            'discount' => 'Discount: '.$quoteData['premium']['discount'].'%'
        )
    );

    //Store the rest of the tranaction tracking details
    $finalPremium = number_format((float)$quoteData['premium']['final_premium'], 2, '.', '');
    $iptValue = number_format((float)$quoteData['premium']['ipt_value'], 2, '.', '');
    $ruralFee = number_format((float)$quoteData['premium']['rural_fee'], 2, '.', '');

    //Build the success page url
    $successPageUrl = RURAL_AV_RATING_TOOL_SUCCESS_PAGE_URL.'?ref='.$quoteReference;

    //Send out the emails
    //Todo: uncomment set proper email addresses
    drupal_mail('rural_av_rating_tool', 'avrt_broker_confirmation', 'jparkinson@ruralinsurance.co.uk', 'en', $emailData, 'joshparkinson1991@gmail.com'); //- to the broker
    drupal_mail('rural_av_rating_tool', 'avrt_back_office', 'joshparkinson1991@gmail.com', 'en', $emailData, 'joshparkinson1991@gmail.com');             //- for newbusiness
    //drupal_mail('rural_av_rating_tool', 'avrt_back_office', 'marketing@ruralinsurance.co.uk', 'en', $emailData, 'joshparkinson1991@gmail.com');        //- for marketing

    //Close the submission
    //rav_db_helper_update_quote_status($quoteId, RURAL_AV_RATING_TOOL_STATUS_CLOSED);

    drupal_add_js(array('rural_av_rating_tool' => array(
        'successPageUrl' => $successPageUrl,
        'quoteReference' => $quoteReference,
        'finalPremium' => $finalPremium,
        'iptValue' => $iptValue,
        'ruralFee' => $ruralFee,
        'vehicleData' => $vehicleDetailsTracking
    )), 'setting');
    drupal_add_js(drupal_get_path('module', 'rural_av_rating_tool').'/assets/js/rural_av_rating_tool.form.callback_methods.js');
    drupal_add_js(drupal_get_path('module', 'rural_av_rating_tool').'/assets/js/rural_av_rating_tool.confirm_quote.js');

    return 'done';
}