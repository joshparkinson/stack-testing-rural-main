<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Define Rural AV Rating Tool Permissions
 *
 * @return array
 */
function rural_av_rating_tool_permission(){
    return array(
        'view avrt reports' => array(
            'title' => t('View Rural AV Rating Tool Reports'),
            'description' => t('Access and generate reports for the Rural AV Rating Tool.')
        ),
    );
}