<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 08/11/2016
 * Time: 20:13
 */

use Dompdf\Dompdf;

function rural_av_rating_tool_preprocess_page(&$vars){

    /*
    $params = array(
        'quoteReference' => 'PMOT00000015',
        'submittingUser' => array(
            'name'        => 'Josh Parkinson',
            'email'       => 'joshparkinson1991@gmail.com',
            'company'     => 'AIUA',
            'location'    => 'Liverpool'
        ),
        'proposerDetails' => array(
            'name' => 'Tim Burns',
            'address' => 'Apartment 524 Manor Mills, Ingram Street, Leeds, West Yorkshire',
            'postcode' => 'LS11 9BR',
            'occupation' => 'Farmer',
            'previous_insurer' => 'BIB',
            'cover_start_date' => '01 / 12 / 2016',
        ),
        'vehicleDetails' => array(
            array(
                'reg' => 'ABC 1234',
                'make_model' => 'Audi',
                'year' => '1990',
                'vehicle_type' => 'Tractor',
                'value' => '£20,000.00',
                'ncb' => '1 Year',
                'premium' => '£45.00',
            ),
            array(
                'reg' => 'DEF 5678',
                'make_model' => 'John Deere',
                'year' => '1950',
                'vehicle_type' => 'Tractor',
                'value' => '£20,000.00',
                'ncb' => '1 Year',
                'premium' => '£45.00',
            ),
        ),
        'premiumDetails' => array(
            'ipt_value' => '£10.00',
            'ipt' => 'IPT @ 10%: £10.00',
            'rural_fee' => 'Rural Fee: £25.00',
            'final_premium' => '£100.00',
            'init_premium' => 'exc. IPT: £90.00',
            'discount' => 'Discount: 5%'
        )
    );

    $documentHtml = _ravdochelper_create_doc_html('schedule', $params);

    libraries_load('dompdf');
    $dompdf = new Dompdf();
    $dompdf->setPaper('A4');
    $dompdf->setBasePath(RURAL_AV_RATING_TOOL_DOC_BASE);
    $dompdf->loadHtml($documentHtml);
    $dompdf->render();

    $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

    exit(0);
    */
}