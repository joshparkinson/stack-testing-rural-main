<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

//todo: comment
function rural_av_rating_tool_mail($key, &$message, $params){
    libraries_load('dompdf');

    switch($key){
        case 'avrt_broker_confirmation':
            $email = rural_av_rating_tool_build_broker_confirmation_mail($params);
            $message['subject'] = $email['subject'];
            $message['body'] = $email['body'];
            break;
        case 'avrt_back_office':
            $email = rural_av_rating_tool_build_back_office_mail($params);
            $message['subject'] = $email['subject'];
            $message['body'] = $email['body'];
            break;
    }

    $message['params']['attachments'][] = array(
        'filepath' => rural_av_rating_tool_helper_create_document('schedule', $params)
    );

}


//todo: comment
function rural_av_rating_tool_build_broker_confirmation_mail($params){
    $intro = _rav_mail_helper_build_intro('av-rating-tool-intro-brokers.html', $params);
    $vehicleDetails = _rav_mail_helper_build_vehicle_details('av-rating-tool-vehicle-details-brokers.html', $params);
    return array(
        'subject' => '['.$params['quoteReference'].'] Confirmation of your AV Rating Submission',
        'body' => _rav_mail_helper_build_email_body($intro, $vehicleDetails, $params),
    );
}





//todo: comment
function rural_av_rating_tool_build_back_office_mail($params){
    $intro = _rav_mail_helper_build_intro('av-rating-tool-intro-back-office.html', $params);
    $vehicleDetails = _rav_mail_helper_build_vehicle_details('av-rating-tool-vehicle-details-back-office.html', $params);
    return array(
        'subject' => '['.$params['quoteReference'].'] Unity: AV Rating Submission',
        'body' => _rav_mail_helper_build_email_body($intro, $vehicleDetails, $params),
    );
}




//todo: comment
function _rav_mail_helper_build_intro($templateFile, $params){
    $introTemplate = file_get_contents(RURAL_AV_RATING_TOOL_PATH_EMAIL_TEMPLATE.'/'.$templateFile);
    return strtr($introTemplate, array(
        '[@quoteReference]' => $params['quoteReference']
    ));
}





//todo: comment
function _rav_mail_helper_build_vehicle_details($templateFile, $params){
    $vehicleDetailsTemplate = file_get_contents(RURAL_AV_RATING_TOOL_PATH_EMAIL_TEMPLATE.'/'.$templateFile);
    $vehicleDetailsComplete = '';
    foreach($params['vehicleDetails'] as $vehicleDetails){
        $vehicleDetailsComplete .= strtr($vehicleDetailsTemplate, array(
            '[@reg]'      => $vehicleDetails['reg'],
            '[@make]'     => $vehicleDetails['make_model'],
            '[@year]'     => $vehicleDetails['year'],
            '[@type]'     => $vehicleDetails['vehicle_type'],
            '[@value]'    => $vehicleDetails['value'],
            '[@ncb]'      => $vehicleDetails['ncb'],
            '[@vPremium]' => $vehicleDetails['premium']
        ));
    }
    return $vehicleDetailsComplete;
}





//todo: comment
function _rav_mail_helper_build_email_body($intro, $vehicleDetails, $params){
    $baseEmailTemplate = file_get_contents(RURAL_AV_RATING_TOOL_PATH_EMAIL_TEMPLATE.'/av-rating-tool-base.html');
    return strtr($baseEmailTemplate, array(
        '[@intro]'          => $intro,

        '[@name]'           => $params['submittingUser']['name'],
        '[@email]'          => $params['submittingUser']['email'],
        '[@company]'        => $params['submittingUser']['company'],
        '[@location]'       => $params['submittingUser']['location'],

        '[@pname]'          => $params['proposerDetails']['name'],
        '[@paddress]'       => $params['proposerDetails']['address'],
        '[@ppostcode]'      => $params['proposerDetails']['postcode'],
        '[@poccupation]'    => $params['proposerDetails']['occupation'],
        '[@pinsurer]'       => $params['proposerDetails']['previous_insurer'],
        '[@startDate]'      => $params['proposerDetails']['cover_start_date'],

        '[@vehicleDetails]' => $vehicleDetails,

        '[@premium]'        => $params['premiumDetails']['final_premium'],
        '[@preIPT]'         => $params['premiumDetails']['init_premium'],
        '[@ipt]'            => $params['premiumDetails']['ipt'],
        '[@fee]'            => $params['premiumDetails']['rural_fee'],
        '[@discount]'       => $params['premiumDetails']['discount']
    ));
}