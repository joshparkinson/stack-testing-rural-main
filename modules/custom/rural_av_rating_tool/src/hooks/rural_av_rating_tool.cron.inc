<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_cron().
 *
 * The following function is used to check if any open submission
 * for the AV Rating Tool are in a state in which they need to be
 * expired.
 *
 * This is achieved by first retreiving any open submissions from
 * the AV Rating Tool submissions database. ($getOpenSubmissionsQuery)
 * These submissions are each processed to check if they have had a
 * cover start date set against them (note: some submissions may be
 * saved before a start date is entered). If they do we then convert
 * this start date to a timestamp and compare it against the definied
 * expiration period (RURAL_AV_RATING_TOOL_VALIDATE_EXPIRATION_PERIOD).
 * If the cover start date is before the expiration period the id of the
 * submission is stored within the ($expiredIds) array for expiration
 * setting. If the $expiredIds array is empty we have no submission to
 * expire so we can end the processing call. If there is however we
 * create an update query ($expireSubmissionsQuery) with these id's
 * setting each of their statuses to expired.
 *
 * @return bool Whether or any submissions have been expired by this
 *              cron processing function.
 */
function rural_av_rating_tool_cron(){

    $getOpenSubmissionsQuery = db_select(RURAL_AV_RATING_TOOL_DB_TABLE, 'av');
    $getOpenSubmissionsQuery->addField('av', 'id');
    $getOpenSubmissionsQuery->addField('av', 'quote_date');
    $getOpenSubmissionsQuery->addField('av', 'form_data');
    $getOpenSubmissionsQuery->condition('status', RURAL_AV_RATING_TOOL_STATUS_OPEN, '=');
    $getOpenSubmissionsResult = $getOpenSubmissionsQuery->execute();

    $toExpire = array();
    while($submission = $getOpenSubmissionsResult->fetchAssoc()){

        //Check the is hasnt been longer than the expiration period since the quote date
        if(!empty($submission['quote_date']) && $submission['quote_date'] < strtotime(RURAL_AV_RATING_TOOL_VALIDATE_EXPIRATION_PERIOD)){
            $toExpire[] = array(
                'reason' => 'quote_date',
                'id' => $submission['id']
            );
            continue;
        }

        //Check the cover start date hasnt passed if its been set
        $formData = unserialize($submission['form_data']);
        if(!empty($formData['values']['proposer_details']['cover_start_date'])){
            $coverStartDateTimeStamp = DateTime::createFromFormat('Y-m-d', $formData['values']['proposer_details']['cover_start_date'])->getTimestamp();
            if($coverStartDateTimeStamp < time()){
                $toExpire[] = array(
                    'reason' => 'start_date',
                    'id' => $submission['id']
                );
                continue;
            }
        }

    }

    if(empty($toExpire)){
        return FALSE;
    }

    //todo: make better
    foreach($toExpire as $expiredSubmission){
        $expireSubmissionsQuery = db_update(RURAL_AV_RATING_TOOL_DB_TABLE);
        $expireSubmissionsQuery->fields(array(
            'status' => RURAL_AV_RATING_TOOL_STATUS_EXPIRED,
            'expire_reason' => $expiredSubmission['reason'],
            'updated' => time()
        ));
        $expireSubmissionsQuery->condition('id', $expiredSubmission['id'], '=');
        $expireSubmissionsQuery->execute();
    }

    return TRUE;

}