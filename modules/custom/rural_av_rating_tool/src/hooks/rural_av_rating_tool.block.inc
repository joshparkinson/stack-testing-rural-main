<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_block_info().
 * Defines all blocks implemented by this module.
 */
function rural_av_rating_tool_block_info(){
    $blocks = array();
    $blocks['rural_avrt_quote_form'] = array(
        'info' => t('Rural AV Rating Tool: Quote Form')
    );
    $blocks['rural_avrt_new_quote'] = array(
        'info' => t('Rural AV Rating Tool: New Quote')
    );
    $blocks['rural_avrt_saved_quotes'] = array(
        'info' => t('Rural AV Rating Tool: Saved Submissions')
    );
    $blocks['rural_avrt_quote_success'] = array(
        'info' => t('Rural AV Rating Tool: Quote Success')
    );
    return $blocks;
}

/**
 * Implements hook_block_view().
 * Return a rednered or renderable view of a block.
 *
 * @param string $delta The block id
 *
 * @return array The block definition array
 */
function rural_av_rating_tool_block_view($delta = ''){
    $block = array();
    switch($delta){
        case 'rural_avrt_quote_form':
            module_load_include('inc', 'rural_av_rating_tool', 'src/blocks/rural_av_rating_tool.block.rural_avrt_quote_form');
            $block['subject'] = '';
            $block['content'] = rural_avrt_quote_form_block_content();
            break;
        case 'rural_avrt_new_quote':
            module_load_include('inc', 'rural_av_rating_tool', 'src/blocks/rural_av_rating_tool.block.rural_avrt_new_quote');
            $block['subject'] = '';
            $block['content'] = rural_avrt_new_quote_block_content();
            break;
        case 'rural_avrt_saved_quotes':
            module_load_include('inc', 'rural_av_rating_tool', 'src/blocks/rural_av_rating_tool.block.rural_avrt_saved_quotes');
            $block['subject'] = '';
            $block['content'] = rural_avrt_saved_quotes_block_content();
            break;
        case 'rural_avrt_quote_success':
            module_load_include('inc', 'rural_av_rating_tool', 'src/blocks/rural_av_rating_tool.block.rural_avrt_quote_success');
            $block['subject'] = '';
            $block['content'] = rural_avrt_quote_sucess_block_content();
            break;
    }
    return $block;
}
