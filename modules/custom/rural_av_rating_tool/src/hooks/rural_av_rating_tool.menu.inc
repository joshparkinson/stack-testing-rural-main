<?php
/**
 * Custom Module File
 * Module: Rural AV Rating Tool
 * Developed by: Josh Parkinson
 */

//todo: comment

function rural_av_rating_tool_menu(){
    $items = array();

    $items['unity/av-rating-tool/confirm-quote/%'] = array(
        'title' => 'Confirm AV Rating Tool Submission',
        'page callback' => 'rural_av_rating_tool_callback_menu_confirm_quote_submission',
        'page arguments' => array(3),
        'file' => 'src/callbacks/menu/rural_av_rating_tool.callback.menu.confirm_quote_submission.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK
    );

    $items['unity/av-rating-tool/delete/%/confirm'] = array(
        'title' => 'Delete AV Rating Submission',
        'page callback' => 'rural_av_rating_tool_callback_menu_delete_submission',
        'page arguments' => array(3),
        'file' => 'src/callbacks/menu/rural_av_rating_tool.callback.menu.delete.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK
    );

    $items['unity/av-rating-tool/delete/%/%ctools_js'] = array(
        'title' => 'Confirm Delete AV Rating Submission',
        'page callback' => 'rural_av_rating_tool_callback_menu_delete_quote_modal',
        'page arguments' => array(3, 4),
        'file' => 'src/callbacks/menu/rural_av_rating_tool.callback.menu.delete.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK
    );

    $items['unity/av-rating-tool/modal/%/%ctools_js'] = array(
        'title' => 'Modal',
        'page callback' => 'rural_av_rating_tool_modal_page',
        'page arguments' => array(3, 4),
        'file' => 'src/callbacks/menu/rural_av_rating_tool.callback.menu.modal.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );

    $items['admin/reports/av-rating-tool'] = array(
        'title' => 'Rural: AV Rating Tool',
        'description' => 'Reporting around the Rural: AV Rating Tool',
        'page callback' => 'rural_av_rating_tool_callback_menu_reports_page',
        'file' => 'src/callbacks/menu/rural_av_rating_tool.callback.menu.reports.inc',
        'access arguments' => array('view avrt reports'),
    );

    $items['admin/reports/av-rating-tool/export-vehicle-premium-data'] = array(
        'title' => 'Export Vehicle Premium Data',
        'page callback' => 'rural_av_rating_tool_callback_menu_reports_export',
        'file' => 'src/callbacks/menu/rural_av_rating_tool.callback.menu.reports.export.inc',
        'access arguments' => array('view avrt reports'),
        'type' => MENU_LOCAL_ACTION,
    );

    return $items;
}