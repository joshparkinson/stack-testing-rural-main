(function($){

    $(document).ready(function(){

        $('body').once('process-quote-submission', function(){

            var succesPageUrl = Drupal.settings.rural_av_rating_tool.successPageUrl;
            var quoteReference = Drupal.settings.rural_av_rating_tool.quoteReference;
            var finalPremium = Drupal.settings.rural_av_rating_tool.finalPremium;
            var iptValue = Drupal.settings.rural_av_rating_tool.iptValue;
            var ruralFee = Drupal.settings.rural_av_rating_tool.ruralFee;
            var vehicleData = Drupal.settings.rural_av_rating_tool.vehicleData;

            $().avrt_GaSendSubmissionTransaction(quoteReference, finalPremium, iptValue, ruralFee, vehicleData).avrt_redirectToPage(succesPageUrl, true);

        });

    });

})(jQuery);