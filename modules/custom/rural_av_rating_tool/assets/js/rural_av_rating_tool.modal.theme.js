(function($){

    Drupal.theme.prototype.rural_av_rating_tool_modal = function () {
        var html = '';
        html += '<div id="ctools-modal" class="av-rating-modal">';
        html += '  <div class="ctools-modal-content">';
        html += '    <a class="close av-rating-close-modal" href="#">Close</a>';
        html += '    <div id="modal-content" class="modal-content av-rating-modal"> </div>';
        html += '  </div>';
        html += '</div>';
        return html;
    };

    Drupal.theme.prototype.rural_av_rating_tool_modal_throbber = function () {
        var html = '';
        html += '  <div id="modal-throbber">';
        html += '    <div class="modal-throbber-wrapper">';
        html += '        <img src="' + Drupal.settings.basePath + 'sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/broker_area/icons/loader.gif" class="preloader" />';
        html += '    </div>';
        html += '  </div>';

        return html;
    };

    //Triggered on modal open, stops page scrolling
    $(document).on("CToolsAttachBehaviors", function(){
        $('body').css('overflow', 'hidden');
    });

    //Triggered on modal close, allows page scrolling
    $(document).on("CToolsDetachBehaviors", function(){
        $('body').css('overflow', 'auto');
    });

})(jQuery);