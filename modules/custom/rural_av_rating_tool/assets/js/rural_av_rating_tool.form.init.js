(function($){

    /**
     * On document ready (page load) initialise the form by:
     *   - Showing the form
     *   - Initialising the forms floatlabels
     *   - Initialising the forms fancySelects
     */
    $(document).ready(function(){
        showForm();
        initialiseFloatLabels();
        initialiseFancySelects();
        initialiseNextButtons();
    });

    $(document).ajaxComplete(function(){
        initialiseFloatLabels();
        initialiseFancySelects();
        initialiseNextButtons();
    });

    /**
     * showForm function
     *
     * The following function is used to remove the hidden class applied to
     * the form wrapper. The form leverages alot of javascript so unless
     * it is made visible using javascript (as a check) it is assumed the user
     * doesn't have javascript enabled and will see a fallback <noscript> content
     */
    function showForm(){
        $("#av-rating-tool-form-wrapper").removeClass('hidden');
    }

    /**
     * initialiseFloatLabels function
     *
     * The following function finds any inputs within the av rating tool
     * quote form and applies the floatlabel pattern to them.
     */
    function initialiseFloatLabels(){
        $("#rural-av-rating-tool-quote-form").find("input:not(.no-floatlabel)").floatlabel({
            'labelClass': 'float-label'
        });
    }

    /**
     * initialiseFancySelects function
     *
     * The following function is used to initialise the fancySelect library
     * for the select inputs in the av rating tool quote form.
     *
     * First, all selects in the form are looped over.
     * Each select is initialised with the fancySelect library.
     * It is then checked that is has a value, if it does it adds classes
     * to both the select trigger and the form-item (wrapper)
     *
     * Secondly, it responds to all select change events.
     * On change the same value check takes place, if the change results
     * in a value being set then a has-value class is added to the form-item,
     * if not it is removed.
     */
    function initialiseFancySelects(){

        $("#rural-av-rating-tool-quote-form").find("select").each(function(){
            $(this).fancySelect();
            if($(this).val() != ''){
                $(this).closest('.form-item').addClass('has-value');
                $(this).closest('.fancy-select').find('.trigger').addClass('selected');
            }
        });

        $('.fancy-select').on('change.fs', function(){
            if($(this).find('select').val() != '') {
                $(this).closest('.form-item').addClass('has-value');
            }
            else{
                $(this).closest('.form-item').removeClass('has-value');
            }
        });

    }

    /**
     * initialiseNextButtons function
     *
     * The following function is used to initialise all of the 'next' buttons
     * within the AV Rating Tool Quote form. We have to maniupulate the values
     * of the next buttons within a jQuery call as drupal's form api does
     * not play nice with dynamic ajax enable submit button values. A dynamic
     * submit button value can/will cause confusion for Drupal in terms of
     * which ajax callback it is expected to fire so to avoid any problems/
     * headaches on document ready we trigger this function to update any
     * completed sections next buttons to 'Update' rather than 'Next.
     *
     * NOTE: DO NOT USE DYNAMIC AJAX ENABLED SUBMIT BUTTON VALUES!!!!
     */
    function initialiseNextButtons(){
        $("#rural-av-rating-tool-quote-form").find('.section-completed').find('.action-next').val('Update');
    }

})(jQuery);