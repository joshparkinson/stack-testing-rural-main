(function($){

    var formId = '#rural-av-rating-tool-quote-form';
    var formTopOffsetNegative = 50;
    var ajaxMessageClass = '.form-ajax-messages';


    /**
     * Callback helper function.
     * Remove any ajax messages from the form.
     */
    function removeAjaxMessages(){
        $(formId + ' ' + ajaxMessageClass).remove();
    }

    /**
     * Callback helper function.
     * Move the user back to the top of the form.
     */
    function scrollToTopOfForm(){
        $(window).scrollTop($(formId).offset().top - formTopOffsetNegative);
    }

    /**
     * Function to display a read only form.
     *
     * This function is triggered when a user views a form that is not
     * currently open (set against its status). This displays the
     * 'reason' block and also a button to show the form. When this
     * button is pressed we trigger the below function via drupal
     * ajax command.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_toggleReadOnlyForm = function(){
        var nonOpenWrapper = formId + ' #avrt-non-open-wrapper';

        if($(nonOpenWrapper).is(':hidden')){
            $(formId + ' #avrt-non-open-wrapper').slideDown('slow');
            $('#edit-show-non-open-submission').val('Hide submission');
        }
        else{
            $(formId + ' #avrt-non-open-wrapper').slideUp('slow');
            $('#edit-show-non-open-submission').val('Show submission');
        }

        return this;
    };

    /**
     * Function to send the user to the premium breakdown section.
     *
     * This function will first fadeOut all sections of the form
     * before fading in the premium section and adding the active
     * class to it.
     *
     * @return {jQuery}
     */
    $.fn.avrt_goToPremium = function(){
        $(formId + ' .av-rating-tool-form-section:not(.premium)').fadeOut('slow', function(){
            $(formId + ' .av-rating-tool-form-section').removeClass('section-active');
            $(formId + ' .av-rating-tool-form-section.premium').addClass('section-active');
            $(formId + ' .av-rating-tool-form-section.premium').fadeIn('slow', function(){
                $(formId + ' .av-rating-tool-form-section.premium').addClass('section-active');
            });
            scrollToTopOfForm();
        });
    };

    /**
     * Function to return the user back to the form sections
     * to make any ammends after they have reached the premium
     * breakdown/confirmation page
     *
     * This function will first fadeOut the premium section of the form
     * before removing all 'section-active' classes from the form sections
     * making them all titles and edit buttons only. It then fades in
     * all of the other form sections apart from the premium section
     * and moves the user back to the top of the page
     *
     * @returns {jQuery}
     */
    $.fn.avrt_backFromPremium = function(){
        $(".av-rating-tool-form-section.premium").fadeOut('slow', function(){
            $(".av-rating-tool-form-section").removeClass('section-active').removeClass('section-hidden');
            $(".av-rating-tool-form-section.vehicle-details").addClass('section-active');
            $(".av-rating-tool-form-section:not(.premium)").fadeIn('slow');
            scrollToTopOfForm();
        });

        return this;
    };

    /**
     * Function to display any messages above the whole form.
     *
     * This should be used to display any non section specific messages
     * to the user within the AV Rating Tool. The function will first
     * clear out any ajax messages that already exist within the form
     * and then prepend the passed messages to the start (above)
     * the form's data capture.
     *
     * @param {string} messages The HTML for the messages to be displayed
     *
     * @returns {jQuery}
     */
    $.fn.avrt_showFormMessages = function(messages){
        removeAjaxMessages();
        $(formId).prepend('<div class="form-ajax-messages">' + messages + '</div>');

        return this;
    };

    /**
     * Function to display any messages for form sections
     * during quote submission.
     *
     * This function will first remove any messages that
     * are currently within the form DOM. It will then append
     * the passed messages to the passed selector
     *
     * @param {string} insertAfterElement A valid jQuery element selector used
     *                                    which the errors messages should be displayed
     *                                    after
     * @param {string} messages      The HTML for the error message that are to be
     *                                    displayed
     *
     * @returns {jQuery}
     */
    $.fn.avrt_showSectionMessages = function(insertAfterElement, messages){
        removeAjaxMessages();
        $(formId + ' ' + insertAfterElement).after('<div class="form-ajax-messages">' + messages + '</div>');

        return this;
    };

    /**
     * Function to move the quote form onto the next section.
     *
     * This function will remove any error messages that were displayed
     * (if we are moving the next section, validation is passed and
     * these are no longer needed). It will the move the user back to
     * the top of the form whilst sliding up (hiding) the section they
     * were currently on (passed as an argument). Once the section is
     * hidden it will remove the active class from the current section
     * and add the completed class to it aswell. It will then show the
     * next section (as specified as an argument) by sliding it down
     * and adding the active and reached classes to it
     *
     * @param {string} currentSection The current section id segment
     * @param {string} nextSection    The next section id segment
     *
     * @returns {jQuery}
     */
    $.fn.avrt_moveToNextSection = function(currentSection, nextSection){
        removeAjaxMessages();
        scrollToTopOfForm();
        $(formId + ' #edit-' + currentSection + '-content').slideUp('slow', function(){
            $('#edit-' + currentSection).removeClass('section-active').addClass('section-completed');
            $('#edit-' + nextSection + '-content').slideDown('slow');
            $('#edit-' + nextSection).addClass('section-active').addClass('section-reached');
        });

        return this;
    };

    /**
     * Function to edit a specific section within the quote form.
     *
     * This function will trigger the jquery actions required to open
     * and edit a specific section of the AV Rating Tool Form. It is
     * triggered when the 'edit' button within the title bar of a
     * completed or reached form is pressed.
     *
     * The function will first remove any ajax errors that have been
     * displayed within the form before moving the user back to the
     * top of the form. It then attempts to find any currently active
     * section (NOTE: there may not be one if the user has reached
     * the premium breakdown display), if there is an active section
     * we first slide that section up (hide it) whilst removing its
     * active class. Then we show the section to be edited. If no
     * active section is found we merely slide down and show the
     * section requested to be edited. The section to be edited
     * will have the active class added to it on display.
     *
     * @param {string} sectionIdSegment A segmeent of the element id for
     *                                  the section we are wanting to
     *                                  edit
     *
     * @returns {jQuery}
     */
     $.fn.avrt_editSection = function(sectionIdSegment){
         removeAjaxMessages();
         scrollToTopOfForm();

         var activeID = $(formId).find('.section-active').attr('id');
         if(activeID != undefined){
             $(formId + ' #' + activeID + '-content').slideUp('slow', function(){
                 $(formId + ' #' + activeID).removeClass('section-active');
                 $('#edit-' + sectionIdSegment + '-content').slideDown('slow', function(){
                     $('#edit-' + sectionIdSegment).addClass('section-active').find('input.action-next').val('Update');
                 });
             });
         }
         else{
             $('#edit-' + sectionIdSegment + '-content').slideDown('slow', function(){
                 $('#edit-' + sectionIdSegment).addClass('section-active').find('input.action-next').val('Update');
             });
         }

         return this;
     };

    /**
     * Restart 'Vehicle Details' form section function
     *
     * This function will trigger the jquery actions required to
     * reset the 'vehicle details' section of the form back to
     * its initial state. This happens when a user is on the the
     * vehicle details section and removes all vehicles from their
     * saved vehicles list. If there are no vehicles to quote against
     * we must reset the section essentially forcing the user to add
     * atleast one vehicle to quote against.
     *
     * The function will first reset the 'Add Vehicle' form to its
     * initial state by called this 'avrt_restartAddVehicleForm'
     * method on the document. It then hides the vehicle list and
     * all processing actions on the section. This does not include
     * the actions for the 'add vehicle' form however which is
     * shown again and has its hidden class removed.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_restartVehicleDetailsSection = function(){
        $(document).avrt_resetAddVehicleForm();
        $('#vehicle-list').hide();
        $('#edit-vehicle-details-content-vehicle-list-actions-add-another').hide();
        $('#edit-vehicle-details-content-vehicle-actions-cancel').hide();
        $('body').avrt_showConfirmViewQuoteButton();
        $('#edit-vehicle-details-content-actions').hide();
        $(formId + ' .vehicle-fieldset').show().removeClass('hidden');

        return this;
    };

    /**
     * Remove a vehicle from the vehicle details vehicle list function
     *
     * This function is triggered when a user clicks the 'remove' button
     * for a vehicle stored within the AV Rating Tool's 'vehicle details'
     * section's vehicle list. Its purpose is to trigger the jQuery commands
     * necessary to (as the name suggest) remove a vehicle from the saved
     * vehicle list.
     *
     * NOTE: The actual removal of data is handled in the PHP ajax callback
     * which triggers this method, this function does nothing more than
     * fire the 'animations' to hide the vehicle row from the vehicle list.
     *
     * This function, using the passed vehicle index will find the vehicle
     * row within the vehicle list it will then slide this row up (hide it)
     * before removing that element from DOM object. It also checks if the
     * vehicle being removed from the list is currently being edited if it
     * is it must reset the add vehicle submit button value to 'Add Vehicle'
     * from 'Update Vehicle'. This is done by checking the passed vehicleIndex
     * against the value stored within the hidden 'index' input in the
     * 'Add Vehicle' form.
     *
     * @param {int} vehicleIndex The index of the vehicle to be removed from the list
     *
     * @returns {jQuery}
     */
    $.fn.avrt_removeVehicleFromVehicleDetailsList = function(vehicleIndex){
        var vehicleRowSelector = formId + ' #vehicle-list .vehicle-details-list-row[data-vehicle-index="' + vehicleIndex + '"]';

        $(vehicleRowSelector).slideUp('slow', function(){
            $(vehicleRowSelector).remove();
        });

        if($('#edit-vehicle-details-content-vehicle-index').val() == vehicleIndex){
            $('#edit-vehicle-details-content-vehicle-actions-add-vehicle').val('Add Vehicle');
        }

        return this;
    };

    /**
     * Show 'Add Vehicle' form function
     *
     * This function is triggered when a user clicks the 'add another vehicle'
     * button within the 'vehicle details' section of the AV Rating Tool Form.
     *
     * This function hides the 'add another vehicle' button (the button being
     * clicked) and slides down the 'add vehicle' form fieldset whislt removing
     * its hidden class.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_showAddVehicleForm = function(){
        $(formId + ' #edit-vehicle-details-content-vehicle-list-actions-add-another').hide();
        $(formId + ' .vehicle-fieldset').slideDown('slow').removeClass('hidden');

        return this;
    };

    /**
     * Hide 'Add Vehicle' form function
     *
     * This function is triggered when a user clicks the 'canel' button within the
     * add vehicle fieldset or when they have successfully added a vehicle using
     * the add vehicle fieldset.
     *
     * This function will hide the 'add vehicle' fieldset whilst adding the
     * hidden class to it. It also makes the 'add another vehicle' button
     * visible again.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_hideAddVehicleForm = function(){
        $(formId + ' .vehicle-fieldset').slideUp('slow').addClass('hidden');
        $(formId + ' #edit-vehicle-details-content-vehicle-list-actions-add-another').show();

        return this;
    };

    /**
     * Show the 'Confirm & View Quote' button.
     *
     * This function is triggered when a user has finished adding or updating
     * a vehicle in the Vehicle details section of the form.
     *
     * The function will show the 'Confirm & View Quote' button so a user is
     * able to proceed to generate a Quote for their risk submission.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_showConfirmViewQuoteButton = function(){
        $(formId + ' #edit-vehicle-details-content-actions-confirm').show();

        return this;
    };

    /**
     * Hide the 'Confirm & View Quote' button
     *
     * This function is triggered when a user is adding or updating a vehicle
     * in the Vehicle details sectuib of the form.
     *
     * This function will hide the 'Confirm & View Quote' button so that a user
     * must complete or cancel the add/edit of a vehicle before they can proceed
     * to generate a quote.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_hideConfirmViewQuoteButton = function(){
        $(formId + ' #edit-vehicle-details-content-actions-confirm').hide();

        return this;
    };

    /**
     * Populate the 'Add Vehicle' form function
     *
     * This function is triggered when a user clicks the 'edit' button for a saved
     * vehicle that is in the vehicle list. It is used to prepopulate the 'add
     * vehicle' form with the data from the vehicle to be edited. The data is not
     * fetched using this method, this method is only used to set the input values
     * of the form.
     *
     * This function takes in all of the saved values for the vehicle to be edited
     * and using the '.val()' methods sets theses values as the defaults for the
     * 'add (in this case edit) vehicle form fieldset'. As we are editing a vehicle
     * we must also set the submit button text to 'Update Vehicle' instead of
     * 'Add Vehicle'. After setting all of our default values we must trigger the
     * change events for the fieldsets inputs and selects so that any jQuery
     * libraries wrapped around them (fancySelect, floatLabel) etc can carry out
     * the required functionality to set correct states etc etc etc.
     *
     * @param {int}    vehicleIndex     The index of the vehicle to be edited.
     * @param {string} vehicleReg       The saved registration/serial number value.
     * @param {string} vehicleMakeModel The make and model of the vehicle.
     * @param {string} vehicleYear      The year of the vehicle being edited.
     * @param {string} vehicleType      The vehicle type value.
     * @param {string} vehicleValue     The value of the vehicle being edited.
     * @param {string} vehicleNCB       The NCB value of the vehicle beign edited.
     *
     * @returns {jQuery}
     */
    $.fn.avrt_populateAddVehicleForm = function(vehicleIndex, vehicleReg, vehicleMakeModel, vehicleYear, vehicleType, vehicleValue, vehicleNCB){
        var addVehicleFormSelector = '#edit-vehicle-details-content-vehicle';

        $('#edit-vehicle-details-content-vehicle-index').val(vehicleIndex);
        $('#edit-vehicle-details-content-vehicle-reg').val(vehicleReg);
        $('#edit-vehicle-details-content-vehicle-make-model').val(vehicleMakeModel);
        $('#edit-vehicle-details-content-vehicle-year').val(vehicleYear);
        $('#edit-vehicle-details-content-vehicle-vehicle-type').val(vehicleType);
        $('#edit-vehicle-details-content-vehicle-value').val(vehicleValue);
        $('#edit-vehicle-details-content-vehicle-ncb').val(vehicleNCB);
        $('#edit-vehicle-details-content-vehicle-actions-add-vehicle').val('Update Vehicle');
        $(addVehicleFormSelector).find('input').change();
        $(addVehicleFormSelector).find('select').change();

        return this;
    };

    /**
     * Reset 'Add Vehicle' form function
     *
     * This function is triggered at various stages within the
     * vehicle details section of the form, one being when a user
     * has the 'add vehicle' form fieldset on display but rather
     * than saving it to the vehicles to quote against they click
     * the 'cancel' button.
     *
     * The function first removes any 'add vehicle' specific errors
     * that may have been generated (these are no longer needed)
     * before searching through the form finding all inputs/selects
     * and reseting (nulling) their values. After nulling the values
     * of all the form inputs we must trigger the change events for
     * them, this allows any jquery libraries wrapped around them
     * (floatlabel, fancyselect etc) to recognise the reset and act
     * accordingly. With any fancy select 'triggers' (the textbox
     * that is clicked to show the dropdown) we must forcefully
     * remove the selected class completing the reset of it. As the
     * form can also be used to add new vehicles to the quote and
     * edit existing vehicles we must also reset the value of the
     * form submit button to 'Add Vehicle'.
     */
    $.fn.avrt_resetAddVehicleForm = function(){
        var vehicleFormID = '#edit-vehicle-details-content-vehicle';
        $(formId + ' .vehicle-fieldset ' + ajaxMessageClass).remove();
        $(vehicleFormID).find('input:not(.form-submit)').val('').change();
        $(vehicleFormID).find('select').val('').change();
        $(formId + ' .vehicle-fieldset .fancy-select .trigger').removeClass('selected');
        $(formId + ' .vehicle-fieldset .fancy-select ul.options li').removeClass('selected');
        $('#edit-vehicle-details-content-vehicle-actions-add-vehicle').val('Add Vehicle');

        return this;
    };

    /**
     * Populate the premium breakdown section.
     *
     * This function is triggered when a premium has been generated
     * and is used to populate the various elements within the premium
     * breakdown section of the form. It is called in various ajax
     * callbacks including the initial premium generation and on
     * discount application.
     *
     * @param {float}     finalPremium   The final premium value
     * @param {float}     initPremium    The initial premium (before IPT + Fee added)
     * @param {int|float} iptPercent     The percentage value IPT is calculated at
     * @param {float}     iptValue       The calculated value of IPT from the initial premium
     * @param {float}     ruralFee       The rural fee that is added to every premium
     * @param {string}    discount       The discount applied to the premium
     * @param {string}    quoteReference The reference generated for this quote
     *
     * @returns {jQuery}
     */
    $.fn.avrt_populatePremiumBreakdown = function(finalPremium, initPremium, iptPercent, iptValue, ruralFee, discount, quoteReference){
        discount = (discount == 0) ? 'None' : discount + '%';

        var premiumBreakdownSelector = formId + ' .av-rating-tool-form-section.premium .premium-breakdown';

        $(premiumBreakdownSelector + ' .final-premium .val').html(finalPremium);
        $(premiumBreakdownSelector + ' .total-pre-ipt .val').html(initPremium);
        $(premiumBreakdownSelector + ' .ipt-value .percent').html(iptPercent);
        $(premiumBreakdownSelector + ' .ipt-value .val').html(iptValue);
        $(premiumBreakdownSelector + ' .fee .val').html(ruralFee);
        $(premiumBreakdownSelector + ' .discount .val').html(discount);

        $(premiumBreakdownSelector + ' .quote-reference .val').html(quoteReference);

        return this;
    };

    /**
     * Build the Premium Vehicle Breakdown Table
     *
     * This function is triggered when a premium is generated and
     * when a discount is applied to the premium. It works by firstly
     * removing the table from the vehicle premium breakdown container
     * and rebuilding it with the passed html. As the h2 (or the
     * container title) is an ever present we can after removing the
     * table call the method 'after' to append the html after the
     * title.
     *
     * @param {string} tableHtml The HTML to build the table with
     *
     * @returns {jQuery}
     */
    $.fn.avrt_buildPremiumVehicleBreakdownTable = function(tableHtml){
        var premiumVehicleBreakdownSelector = formId + ' .av-rating-tool-form-section.premium .vehicle-premium-breakdown';
        $(premiumVehicleBreakdownSelector + ' table').remove();
        $(premiumVehicleBreakdownSelector).html(tableHtml);

        return this;
    };

    /**
     * Bounce premium section's 'deposit' input.
     *
     * This function is triggered when an invalid deposit value
     * is attempted to be applied to the premium generated for an
     * AV Rating Tool quote/submission. It is used to show the
     * user there is a problem with the value they have entered
     * without the need to show the a conventional form error
     * message.
     *
     * If the input does not already have the class invalid
     * (remembering this is triggered on validation errors) then
     * the invalid class is added to it.
     */
    $.fn.avrt_bouncePremiumDiscountInput = function(){
        var premiumDiscountInputId = '#edit-premium-content-display-discount-value';
        $(premiumDiscountInputId).effect("bounce",
            { distance: 3, times: 3 },
            500
        );
        if(!$(premiumDiscountInputId).hasClass('invalid')){
            $(premiumDiscountInputId).addClass('invalid');
        }

        return this;
    };

    /**
     * Display modal function
     *
     * @todo: comment properly
     */
    $.fn.avrt_displayModal = function(reason, parameterString){

        var url  = Drupal.settings.basePath + 'unity/av-rating-tool/modal/' + reason + '/nojs' + parameterString;
        var link = $("<a></a>").attr('href', url).addClass('ctools-use-modal-processed').addClass('ctools-modal-rural-av-rating-tool-modal').click(Drupal.CTools.Modal.clickAjaxLink);
        Drupal.ajax[url] = new Drupal.ajax(url, link.get(0), {
            url: url,
            event: 'click',
            progress: { type: 'throbber' }
        });
        link.click();

        return this;
    };

    /**
     * Update page url
     *
     * @todo: comment properly
     */
    $.fn.avrt_updatePageUrl = function(url){
        History.pushState(null, $(document).find("title").text(), url);

        return this;
    };

    /**
     * Track submission
     *
     * @todo: comment properly
     */
    $.fn.avrt_GaSendSubmissionTransaction = function(quoteReference, premium, iptValue, ruralFee, transactionVehicles){
        if(typeof(ga) == "function"){
            ga('require', 'ecommerce');

            ga('ecommerce:addTransaction', {
                'id': quoteReference,
                'affiliation': 'Unity: AVRT Submission',
                'revenue': premium,
                'tax': iptValue,
                'shipping': ruralFee
            });

            $.each(transactionVehicles, function(index, vehicleData){
                ga('ecommerce:addItem', {
                    'id': quoteReference,
                    'name': vehicleData['name'],
                    'sku': vehicleData['sku'],
                    'category': vehicleData['category'],
                    'price': vehicleData['price'],
                    'quantity': '1'
                });
            });

            ga('ecommerce:send');
        }
        return this;
    };

    /**
     * Redirect to success page
     *
     * @param url
     * @param internal
     *
     * todo: comment properly
     */
    $.fn.avrt_redirectToPage = function(url, internal){
        if(internal){
            window.location.href = '/' + url;
        }
        else{
            window.location.href = url;
        }

        return this;
    };


})(jQuery);
