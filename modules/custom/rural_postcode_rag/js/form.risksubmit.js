(function($){
    Drupal.behaviors.RuralPostcodeRAGRiskSubmitForm = {
        attach: function(context, settings) {
            
            $('#rural-postcode-rag-risksubmit-form input').floatlabel({
                'labelClass': 'float-label-risksubmit-input'
            });

            $("#edit-submission-form").nicefileinput({
                label : 'Browse'
            });

            $("#edit-submit-risk").mousedown(function(){
                $(this).addClass('loading');
            });

            $(".form-item-files-submission-form input[type='text']").attr('placeholder', 'Upload your submission form');

            $("#send-submission-button").click(function(){
                $(this).slideUp();
                $('p.sub').slideUp();
                $("#send-submission-form").slideDown();
            });

            $(".close-rag").click(function(){
                $("#edit-postcode").val('').change();
                $(".rag-response").slideUp(function(){
                    $("#rag-response-container").html('');
                });
            });

        }
    };
})(jQuery);