(function($){

    /**
     * INIT
     */

    var form_id = "#rural-postcode-rag-search-form";
    var input_id = form_id + " #edit-postcode";
    var label_id = form_id + " label";

    /**
     * BEHAVIOUR
     */

    Drupal.behaviors.RuralPostcodeRAGSearchForm = {
        attach: function(context, settings) {

            $(form_id, context).once('ajax-search', function(){
                //Hide label
                $(label_id).hide();

                //Floatlabel the input
                $(input_id).floatlabel({
                    'labelClass': 'float-label-postcode-search'
                });

                //On form submit
                $(form_id).submit(function(e){
                    e.preventDefault();
                    //If empty no need to process
                    if(!$(input_id).val()){
                        _bounce_input();
                        _clear_rag_response();
                        $(input_id).removeClass('loading');
                        return;
                    }
                    //Search postcode
                    _search_postcode_ajax($(input_id).val());
                });
            });

            function _search_postcode_ajax($postcode){
                $(input_id).addClass('loading');
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: settings.RuralPostcodeRAG.ajax_path,
                    dataType: 'json',
                    data: {
                        "postcode": $postcode
                    },
                    success: function(data){
                        $(input_id).removeClass('loading');
                        if(typeof(data.html) == "undefined" || data.html === null){
                            _bounce_input();
                            _clear_rag_response();
                        }
                        else{
                            $('#rag-response-container').html(data.html).slideDown();
                            $(document).rbmpc_GaSendSearchEvent(data.status);
                            Drupal.attachBehaviors('#rag-response-container');
                        }
                    }
                });
            }

        }
    };

    /**
     * AJAX
     */

    /**
     * HELPER
     */

    function _bounce_input(){
        $(input_id).effect("bounce", { distance: 5, times: 2 }, 500);
    }

    function _clear_rag_response(){
        $("#rag-response-container").slideUp(500);
    }


})(jQuery);