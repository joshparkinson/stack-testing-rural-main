(function($){

    $.fn.rbmpc_GaSendSearchEvent = function(ragStatus){
        if(typeof(ga) == "function"){
            ga('send', 'event', 'RBM Postcode Checker', 'search', 'search-' + ragStatus);
        }
        return this;
    };

    $.fn.rbmpc_GaSendSubmissionEvent = function(ragStatus){
        if(typeof(ga) == "function"){
            ga('send', 'event', 'RBM Postcode Checker', 'submission', 'submission-' + ragStatus);
        }
        return this;
    }

})(jQuery);
