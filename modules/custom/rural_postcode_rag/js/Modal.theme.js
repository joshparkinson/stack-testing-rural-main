(function ($) {
    Drupal.behaviors.tsd_modal = {
        attach: function (context, settings) {

            Drupal.theme.prototype.rural_modal = function () {
                var html = '';
                html += '<div id="ctools-modal">';
                html += '  <div class="ctools-modal-content">';
                html += '    <a class="close" href="#">Close</a>';
                html += '    <h2 id="modal-title"></h2>';
                html += '    <div id="modal-content"> </div>';
                html += '  </div>';
                html += '</div>';
                return html;
            };


            Drupal.theme.prototype.rural_modal_throbber = function () {
                /* @TODO: do something here */
                //return '<img src="' + Drupal.settings.basePath + 'sites/all/themes/frameworks/assets/images/preloader.gif" class="preloader" />';
            };

            //Triggered on modal open, stops page scrolling
            $(document).on("CToolsAttachBehaviors", function(){
                $('body').css('overflow', 'hidden');
            });

            //Triggered on modal close, allows page scrolling
            $(document).on("CToolsDetachBehaviors", function(){
                $('body').css('overflow', 'auto');
            });

            //When the modal backdrop is clicked, close the modal.
            $(document).ready(function(){
                $('#modalBackdrop:not(.no-close)').click(function(){
                    Drupal.CTools.Modal.dismiss();
                });
            });

        }
    };
})(jQuery);
