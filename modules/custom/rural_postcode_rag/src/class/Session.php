<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 04/03/2016
 * Time: 11:37
 */

namespace RuralPostcodeRAG;


class Session{

    protected $session_key = 'rural_postcode_rag';

    public function get($key, $default = FALSE){
        return ($this->has($key)) ? $_SESSION[$this->session_key][$key] : $default;
    }

    public function set($key, $data){
        $_SESSION[$this->session_key][$key] = $data;
    }

    public function has($key){
        return (!empty($_SESSION[$this->session_key][$key]));
    }

}