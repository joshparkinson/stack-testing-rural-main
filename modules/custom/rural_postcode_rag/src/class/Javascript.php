<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 11:10
 */

namespace RuralPostcodeRAG;

use RuralPostcodeRAG\Config\ConfigFactory;

class Javascript {

    //Settings key
    private $settings_key = 'RuralPostcodeRAG';

    //Paths
    private $js_path;

    //Datastores
    private $files = array();
    private $settings = array();
    private $libraries = array();

    public function __construct(){
        $FilePathsConfig = ConfigFactory::build('FilePaths');

        $this->js_path = $FilePathsConfig->get('js');

        $this->addSetting('file_paths', array(
            'base' => '/'.$FilePathsConfig->get('module', FALSE),
            'ajax' => '/'.$FilePathsConfig->get('ajax'),
            'js' => '/'.$this->js_path,
        ));
    }

    public function addFile($filename){
        $this->files[] = $filename;
    }

    public function addLibrary($module, $name){
        $this->libraries[] = array(
            'module' => $module,
            'name' => $name,
        );
    }

    public function addSetting($key, $data){
        $this->settings[] = array(
            'key' => $key,
            'data' => $data,
        );
    }

    public function attach(){
        //Add Libraries
        foreach($this->libraries as $library){
            drupal_add_library($library['module'], $library['name']);
        }

        //Add Files
        foreach($this->files as $filename){
            drupal_add_js($this->js_path.'/'.$filename);
        }

        //Add settings
        $js_settings = array();
        foreach($this->settings as $setting){
            $js_settings[$setting['key']] = $setting['data'];
        }
        drupal_add_js(array($this->settings_key => $js_settings), 'setting');
    }

}