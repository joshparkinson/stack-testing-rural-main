<?php

namespace RuralPostcodeRAG\Config;

class DatabaseConfig extends AbstractConfig{

    protected $table_name = 'rural_postcode_rag';

    protected $postcode_column = 'postcode_sector';

    protected $rag_column = 'rag';

    protected $install_file = '/sql/install.sql';

}