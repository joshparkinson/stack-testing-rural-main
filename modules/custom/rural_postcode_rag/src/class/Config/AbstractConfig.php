<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 02/03/2016
 * Time: 16:16
 */

namespace RuralPostcodeRAG\Config;


abstract class AbstractConfig{

    public function get($param, $default = FALSE){
        return ($this->has($param)) ? $this->{$param} : $default;
    }

    public function has($param){
        return (!empty($this->{$param}));
    }

}