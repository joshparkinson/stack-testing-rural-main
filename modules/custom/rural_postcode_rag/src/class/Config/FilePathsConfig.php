<?php

namespace RuralPostcodeRAG\Config;


class FilePathsConfig extends AbstractConfig{

    protected $module_path;

    protected $ajax_path = "/src/ajax";

    protected $form_path = "/src/forms";

    protected $js_path = "/js";

    protected $page_path = "/src/pages";

    public function __construct(){
        $this->module_path = drupal_get_path('module', 'rural_postcode_rag');
    }

    public function get($path, $append = TRUE, $default = FALSE){
        $path = parent::get($path.'_path', $default);
        if(empty($path)){
            return $default;
        }
        return ($append) ? $this->module_path.$path : $path;
    }

}