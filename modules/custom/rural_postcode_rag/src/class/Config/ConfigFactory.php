<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 02/03/2016
 * Time: 15:38
 */

namespace RuralPostcodeRAG\Config;

use Exception;

class ConfigFactory{

    public static function build($type){
        $config_class = __NAMESPACE__.'\\'.$type.'Config';
        if(!class_exists($config_class)){
            throw new Exception('Invalid Config Type Recieved');
        }
        return new $config_class;
    }

}