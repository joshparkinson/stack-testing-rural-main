<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 16/03/2016
 * Time: 11:03
 */

namespace RuralPostcodeRAG\Config;


class PathConfig extends AbstractConfig{

    protected $path = 'rbm-postcode-checker';

    protected $overlay_path = 'rbm-postcode-checker/overlay';
    protected $overlay_path_wildcard_position = 2;

    protected $ajax_path = 'rbm-postcode-checker/ajax';

    protected $admin_path = 'rbm-postcode-checker/admin';

}