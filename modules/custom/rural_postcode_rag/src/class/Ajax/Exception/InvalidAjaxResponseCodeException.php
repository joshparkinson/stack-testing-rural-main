<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 12:38
 */

namespace RuralPostcodeRAG\Ajax\Exception;

use Exception;

class InvalidAjaxResponseCodeException extends Exception{

    public function __construct($code){
        parent::__construct('Invalid response code, must be numeric. Recieved: '.$code);
    }

}