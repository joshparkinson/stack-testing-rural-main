<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 12:33
 */

namespace RuralPostcodeRAG\Ajax\Exception;

use Exception;

class MalformedAjaxResponseException extends Exception{

    public function __construct(){
        parent::__construct('Attempted to return an incomplete response');
    }

}