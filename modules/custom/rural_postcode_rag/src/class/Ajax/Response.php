<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 12:27
 */

namespace RuralPostcodeRAG\Ajax;

use RuralPostcodeRAG\Ajax\Exception\MalformedAjaxResponseException;
use RuralPostcodeRAG\Ajax\Exception\InvalidAjaxResponseCodeException;

class Response{

    //Meta
    public $status = 'success';
    public $code = 0;
    public $data = array();

    //Keys
    private $keyError = 'error';

    public function setCode($code){
        if(!is_numeric($code)) throw new InvalidAjaxResponseCodeException($code);
        $this->code = $code;
        return $this;
    }
    public function getCode(){
        return $this->code;
    }

    public function setData($key, $data){
        $this->data[$key] = $data;
        return $this;
    }
    public function getData($key, $default = FALSE){
        return (!empty($this->data[$key])) ? $this->data[$key] : $default;
    }

    public function setError($message){
        $this->status = $this->keyError;
        $this->setData($this->keyError, $message);
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

}