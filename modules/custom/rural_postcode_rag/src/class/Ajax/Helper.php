<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 11:54
 */

namespace RuralPostcodeRAG\Ajax;


class Helper{
    public static function checkDirectAccess(){
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'){
            self::throwError('403', 'No direct access');
        }
    }

    public static function checkPostVariables($parameters){
        $parameters = (!is_array($parameters)) ? array($parameters) : $parameters;
        foreach($parameters as $parameter){
            if(empty($_POST[$parameter])){
                self::throwError('400', 'Invalid params');
                break;
            }
        }
    }

    public static function throwError($error_code = '403', $error_message = 'Nope'){
        header('HTTP/1.0 '.$error_code.' '.$error_message);
        die();
    }
}