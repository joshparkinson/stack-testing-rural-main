<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 12:00
 */

namespace RuralPostcodeRAG\Postcode;

class PostcodeSector{

    private $sector;
    private $rag;

    public function __construct($sector, $rag){
        $this->sector = $sector;
        $this->rag = $rag;
    }

    public function getSector(){
        return strtoupper($this->sector);
    }
    public function setSector($sector){
        $this->sector = $sector;
    }

    public function getRagStatus(){
        return ucfirst(strtolower($this->rag));
    }
    public function setRagStatus($rag){
        $this->rag = $rag;
    }

}