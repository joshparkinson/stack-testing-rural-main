<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 11:59
 */

namespace RuralPostcodeRAG\Postcode;

use RuralPostcodeRAG\Config\ConfigFactory;
use RuralPostcodeRAG\Cache;

class PostcodeSectorMapper{

    private $cache;

    private $table_name;
    private $postcode_column;
    private $rag_column;

    public function __construct(){
        //Add config
        $DatabaseConfig = ConfigFactory::build('Database');
        $this->table_name = $DatabaseConfig->get('table_name');
        $this->postcode_column = $DatabaseConfig->get('postcode_column');
        $this->rag_column = $DatabaseConfig->get('rag_column');
        unset($DatabaseConfig);

        //Initialise Cache
        $this->cache = new Cache();
    }

    public function findBySector($sector_string){
        //First attempt to get from cache
        if($cached_sector = $this->cache->get('sector_'.$sector_string)){
            return new PostcodeSector($cached_sector['postcode_sector'], $cached_sector['rag']);
        }

        //Build query
        $query = /** @lang text */ "SELECT * FROM @table_name WHERE CONCAT(REPLACE(@postcode_column, ' ', '')) = '@sector' LIMIT 1";
        $query = strtr($query, array(
            '@table_name' => $this->table_name,
            '@postcode_column' => $this->postcode_column,
            '@sector' => $sector_string
        ));

        //Attempt to fetch sector, return false if not found
        $db_sector = db_query($query)->fetchAssoc();
        if(!$db_sector) return FALSE;

        //Sector fetched, add to cache, return
        $this->cache->set('sector_'.$sector_string, $db_sector);
        return new PostcodeSector($db_sector['postcode_sector'], $db_sector['rag']);
    }

    public function saveSector(PostcodeSector $sector){
        db_merge($this->table_name)
            ->key(array($this->postcode_column => $sector->getSector()))
            ->fields(array(
                $this->postcode_column => $sector->getSector(),
                $this->rag_column => $sector->getRagStatus(),
            ))
            ->execute();
    }

}