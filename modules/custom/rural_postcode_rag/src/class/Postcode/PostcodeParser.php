<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 14:11
 */

namespace RuralPostcodeRAG\Postcode;


class PostcodeParser{

    public static function parse($postcode){
        //Clean postcode
        $postcode = strtoupper(preg_replace("[^A-Za-z0-9]", "", trim($postcode)));

        //Check postcode valid
        $regex = '/^([A-Z]{1,2})([0-9][0-9A-Z]?)\s*([0-9])([A-Z]{2})$/';
        if(!preg_match($regex, $postcode, $part) || strlen($postcode) < 5){
            return FALSE;
        }

        //Return parsed postcode
        return array(
            'full' => $part[1].$part[2].' '.$part[3].$part[4],
            'area'     => $part[1],
            'district' => $part[2],
            'outer'    => $part[1] . $part[2],
            'sector'   => $part[3],
            'walk'     => $part[4],
            'inner'    => $part[3] . $part[4]
        );
    }

}