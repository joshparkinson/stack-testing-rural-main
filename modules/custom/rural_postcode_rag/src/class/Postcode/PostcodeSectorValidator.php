<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 12:14
 */

namespace RuralPostcodeRAG\Postcode;


class PostcodeSectorValidator{

    private $postcode_regex = '/^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? ?[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/i';

    public function getPostcodePattern(){
        return $this->postcode_regex;
    }

    public function postcodeValid($postcode){
        return preg_match($this->postcode_regex, $postcode);
    }

}