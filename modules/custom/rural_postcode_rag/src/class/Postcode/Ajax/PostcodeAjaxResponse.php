<?php

namespace RuralPostcodeRAG\Postcode\Ajax;

use RuralPostcodeRAG\Ajax\Response as AjaxResponse;

class PostcodeAjaxResponse extends AjaxResponse{

    private $dataKey_Postcode = 'postcode';
    private $dataKey_Sector = 'sector';
    private $dataKey_RagStatus = 'rag_status';

    public function setPostcode($postcode){
        $this->setData($this->dataKey_Postcode, $postcode);
        return $this;
    }
    public function getPostcode(){
        return $this->getData($this->dataKey_Postcode);
    }

    public function setSector($sector){
        $this->setData($this->dataKey_Sector, $sector);
        return $this;
    }
    public function getSector(){
        return $this->getData($this->dataKey_Sector);
    }

    public function setRagStatus($rag_status){
        $this->setData($this->dataKey_RagStatus, strtolower($rag_status));
        return $this;
    }
    public function getRagStatus(){
        return $this->getData($this->dataKey_RagStatus);
    }

}