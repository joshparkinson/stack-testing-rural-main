<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 18/03/2016
 * Time: 09:34
 */

namespace RuralPostcodeRAG\Postcode\Ajax\Response;

use RuralPostcodeRAG\Config\ConfigFactory;


class PostcodeAjaxResponseOutputHtml extends AbstractPostcodeAjaxResponse{

    public function get(){
        switch ($this->response->getStatus()){
            case 'success':
                return $this->getSuccessOutput();
            case 'error':
                return $this->getErrorOutput();
        }
    }

    private function getErrorOutput(){
        $output = $this->top('red');
        $output .= $this->title($this->response->getData('error'));
        $output .= $this->subText('Please check another postcode.');
        $output .= $this->bottom();
        return $output;
    }

    private function getSuccessOutput(){
        $output = $this->top($this->response->getRagStatus());
        $output .= $this->buildSuccessInner($this->response->getRagStatus());
        $output .= $this->bottom();
        return $output;
    }

    private function buildSuccessInner($rag_status){
        switch ($rag_status){
            case 'red':
                return $this->buildSuccessRed();
            case 'amber':
                return $this->buildSuccessAmber();
            case 'green':
                return $this->buildSuccessGreen();
        }
    }

    private function buildSuccessRed(){
        $output = $this->title("We're sorry…");
        $output .= $this->subText('Unfortunately we are unable to provide a quotation for this risk.', 'mobile-reduce');
        $output .= $this->subText('Rural Business Motor insurance is exclusively for businesses operating in rural communities. We cover a wider range of trades and occupations so if you have a customer in a rural location please get in touch.', 'mobile-reduce rbm-about');
        return $output;
    }

    private function buildSuccessAmber(){
        $output = $this->title("We need to understand a bit more about this risk, please send us your submission and we’ll get back to you as soon as possible.");
        $output .= $this->riskForm();
        $output .= $this->subText('Or call us on 0344 55 77 177', 'call-us');
        return $output;
    }

    private function buildSuccessGreen(){
        $output = $this->title("Please send us your submission and we’ll get back to you as soon as possible.");
        $output .= $this->riskForm();
        return $output;
    }

    private function title($text){
        return "<h2>".$text."</h2>";
    }


    private function subText($text, $class = NULL){
        return "<p class='".trim('sub '.$class)."'>".$text."</p>";
    }

    private function riskForm(){

        $output = '<div id="send-submission-wrapper">';
        $output .= '<div id="send-submission-button">Send us your submission now</div>';
        $output .= '<div id="send-submission-form">'.drupal_render(drupal_get_form('rural_postcode_rag_risksubmit_form', $this->response->getPostcode(), $this->response->getRagStatus())).'</div>';
        $output .= '</div>';

        $javascript = drupal_add_js(NULL, NULL);
        $settings = FALSE;
        if(isset($javascript['settings']))
        {
            $settings = '<script type="text/javascript">jQuery.extend(Drupal.settings, '. drupal_json_encode(call_user_func_array('array_merge_recursive', $javascript['settings']['data'])) .');</script>';
        }
        $output .= $settings;

        return $output;
    }

    private function top($class = NULL){
        $class_string = trim("rag-response ".$class);
        return "<div class='".$class_string."'>
                    <div class='triangle'></div>";
    }

    private function bottom(){
        return "</div>";
    }

}