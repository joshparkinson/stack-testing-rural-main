<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 18/03/2016
 * Time: 10:09
 */

namespace RuralPostcodeRAG\Postcode\Ajax\Response;

use RuralPostcodeRAG\Postcode\Ajax\PostcodeAjaxResponse;

class PostcodeAjaxResponseFactory{

    private $response;
    private $classPrefix = 'RuralPostcodeRAG\\Postcode\\Ajax\\Response\\PostcodeAjaxResponseOutput';

    public function __construct(PostcodeAjaxResponse $response){
        $this->response = $response;
    }

    public function get($type){
        $className = $this->classPrefix.ucfirst(strtolower($type));
        if(!class_exists($className)){
            /* @todo throw type not found exception */
        }
        return new $className($this->response);
    }

}