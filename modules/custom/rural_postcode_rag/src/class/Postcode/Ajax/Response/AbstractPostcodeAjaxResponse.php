<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 18/03/2016
 * Time: 10:05
 */

namespace RuralPostcodeRAG\Postcode\Ajax\Response;

use RuralPostcodeRAG\Postcode\Ajax\PostcodeAjaxResponse;

abstract class AbstractPostcodeAjaxResponse{

    protected $keyOutput = 'output';

    protected $response;

    public function __construct(PostcodeAjaxResponse $response){
        $this->response = $response;
    }

    abstract public function get();

}