<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 03/03/2016
 * Time: 09:35
 */

namespace RuralPostcodeRAG;

use RuralPostcodeRAG\Config\ConfigFactory;

class Cache{

    private $bin;

    public function __construct(){
        $CacheConfig = ConfigFactory::build('Cache');
        $this->bin = $CacheConfig->get('bin');
    }

    public function getBin(){
        return $this->bin;
    }

    public function set($key, $data, $life = CACHE_TEMPORARY){
        cache_set($key, $data, $this->bin, $life);
        return $this;
    }

    public function get($key, $default = FALSE){
        $cache_object = cache_get($key, $this->bin);
        return (!empty($cache_object->data)) ? $cache_object->data : $default;
    }

    public function clear($key = NULL){
        if(!empty($key)){
            cache_clear_all($key, $this->bin);
            return $this;
        }
        cache_clear_all('*', $this->bin, TRUE);
        return $this;
    }

}