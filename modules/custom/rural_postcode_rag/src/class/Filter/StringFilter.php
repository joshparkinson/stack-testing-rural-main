<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 08/03/2016
 * Time: 10:25
 */

namespace RuralPostcodeRAG\Filter;

class StringFilter{

    public static function attributeString($attributes){
        $attribute_string = '';
        foreach($attributes as $key => $string){
            $attribute_string .= $key.'="'.trim($string).'" ';
        }
        return trim($attribute_string);
    }

    public static function forceArray($value){
        return (!is_array($value)) ? array($value) : $value;
    }

    public static function stripTags(&$string){
        if(is_array($string)){
            array_walk_recursive($string, 'self::transliterate');
        }
        else{
            $string = strip_tags($string);
        }
        return $string;
    }

    public static function transliterate($string){
        return (trim(preg_replace('@[^a-z0-9__]+@','_', strtolower(trim(self::stripTags($string)))), '_'));
    }

}