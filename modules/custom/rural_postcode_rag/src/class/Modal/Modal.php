<?php

namespace RuralPostcodeRAG\Modal;

use RuralPostcodeRAG\Filter\StringFilter;

//TSD Modal Window Class
class Modal{

    private $modalClass = 'ctools-use-modal';

    /* @var $style \RuralPostcodeRAG\Modal\Style\ModalStyleInterface */
    public $style;

    public function __construct($style = NULL){
        if(!empty($style)){
            $this->setStyle($style);
        }
    }

    public function setStyle($style){
        $this->style = ModalStyleFactory::build($style);
    }

    public function getLink($text, $href, $attributes = array()){
        $class_string = $this->modalClass.' '.$this->style->getClass();
        $attributes['class'] = (empty($attributes['class'])) ? $class_string :  $attributes['class'] .= ' '.$class_string;
        $attribute_string = StringFilter::attributeString($attributes);
        return '<a href="'.$href.'" '.$attribute_string.'>'.$text.'</a>';
    }

    public function attach(){
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();
        drupal_add_js(array($this->style->getKey() => $this->style->getStyle()), 'setting');
        ctools_add_js('Modal.theme', 'rural_postcode_rag', 'js');
    }

}