<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 24/03/2016
 * Time: 11:49
 */

namespace RuralPostcodeRAG\Modal\Style;


class FullPageOverlayModalStyle implements ModalStyleInterface {

    private $key = 'rural-full-page-overlay-modal';

    public function getClass(){
        return self::CLASS_PREFIX.$this->key;
    }

    public function getKey(){
        return $this->key;
    }

    public function getStyle(){
        return array(
            'modalOptions' => array(
                'opacity' => 0.9,
                'background-color' => '#000',
            ),
            'animation' => 'fadeIn',
            'modalClass' => 'rural-full-page-overlay',
            'modalTheme' => 'rural_modal',
            'throbberTheme' => 'rural_modal_throbber',
        );
    }

}