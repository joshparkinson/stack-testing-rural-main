<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 08/03/2016
 * Time: 10:02
 */

namespace RuralPostcodeRAG\Modal\Style;


interface ModalStyleInterface
{
    const CLASS_PREFIX = 'ctools-modal-';
    public function getClass();
    public function getKey();
    public function getStyle();
}