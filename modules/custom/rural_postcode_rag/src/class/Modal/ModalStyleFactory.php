<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 08/03/2016
 * Time: 09:52
 */

namespace RuralPostcodeRAG\Modal;

use Exception;

class ModalStyleFactory{

    private static $modalStyleNamespace = 'RuralPostcodeRAG\\Modal\\Style\\';

    public static function build($style){
        $modalStyle = self::$modalStyleNamespace.$style.'ModalStyle';
        if(!class_exists($modalStyle)){
            throw new Exception('Invalid Modal Style');
        }
        return new $modalStyle();
    }

}