<?php

function rural_postcode_rag_main_page(){
    $block = block_load('rural_postcode_rag', 'rural_postcode_rag');
    $renderArray = _block_get_renderable_array(_block_render_blocks(array($block)));
    $output = drupal_render($renderArray);
    return $output;
}