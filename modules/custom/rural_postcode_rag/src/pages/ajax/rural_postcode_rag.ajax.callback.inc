<?php

use RuralPostcodeRAG\Ajax\Helper as AjaxHelper;

use RuralPostcodeRAG\Postcode\PostcodeParser;
use RuralPostcodeRAG\Postcode\PostcodeSectorMapper;
use RuralPostcodeRAG\Postcode\PostcodeSectorValidator;
use RuralPostcodeRAG\Postcode\Ajax\PostcodeAjaxResponse;
use RuralPostcodeRAG\Postcode\Ajax\Response\PostcodeAjaxResponseFactory;

function rural_postcode_rag_ajax_callback_page(){

    //Check file not accessed directly and post vars present as expected
    AjaxHelper::checkDirectAccess();
    AjaxHelper::checkPostVariables('postcode');

    //Initialise classes
    $parser = new PostcodeParser();
    $validator = new PostcodeSectorValidator();
    $mapper = new PostcodeSectorMapper();
    $response = new PostcodeAjaxResponse();

    //Parse the postcode
    //Check passed postcode valid
    $postcode = $parser->parse($_POST['postcode']);
    if(!$postcode || !$validator->postcodeValid($postcode['full'])){
        $response->setError('Invalid Postcode');
    }

    //Load sector
    //If not found throw error
    $sector = null;
    if($response->getStatus() != "error"){
        $sector = $mapper->findBySector($postcode['outer'].$postcode['sector']);
        if(!$sector){
            $response->setError('Postcode not found');
        }
    }

    //Found, set data
    if($response->getStatus() != "error"){
        $response->setPostcode($postcode['full']);
        $response->setSector($sector->getSector());
        $response->setRagStatus($sector->getRagStatus());
    }

    //Build and return response output
    $outputFactory = new PostcodeAjaxResponseFactory($response);
    $htmlOutput = $outputFactory->get('html');
    drupal_json_output(array('status' => $response->getRagStatus(), 'html' => $htmlOutput->get()));


}