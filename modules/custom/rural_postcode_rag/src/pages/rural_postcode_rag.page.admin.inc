<?php

use RuralPostcodeRAG\Config\ConfigFactory;
use RuralPostcodeRAG\Modal\Modal;

function rural_postcode_rag_admin_page(){
    //Load form
    $FilePathsConfig = ConfigFactory::build('FilePaths');
    $PathConfig = ConfigFactory::build('Path');

    $modal = new Modal('FullPageOverlay');
    $modal->attach();

    $form_file = $FilePathsConfig->get('form', FALSE).'/rural_postcode_rag.form.admin';
    module_load_include('inc', 'rural_postcode_rag', $form_file);
    $form = drupal_get_form('rural_postcode_rag_admin_form');

    $output = drupal_render($form);
    $output .= $modal->getLink('test', '/'.$PathConfig->get('overlay_path').'/nojs');
    
    return $output;
}