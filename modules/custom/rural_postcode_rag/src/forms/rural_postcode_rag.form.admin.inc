<?php

use RuralPostcodeRAG\Cache;
use RuralPostcodeRAG\Postcode\PostcodeSector;
use RuralPostcodeRAG\Postcode\PostcodeSectorMapper;

function rural_postcode_rag_admin_form($form, &$form_state){

    $form['sector'] = array(
        '#type' => 'textfield',
        '#title' => 'Postcode Sector',
        '#description' => 'Include the space!',
        '#required' => TRUE,
    );

    $form['rag'] = array(
        '#title' => 'RAG Status',
        '#type' => 'select',
        '#options' => array(
            'red' => 'Red',
            'amber' => 'Amber',
            'green' => 'Green',
        ),
        '#required' => TRUE,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Save',
    );

    return $form;

}

function rural_postcode_rag_admin_form_submit($form, &$form_state){
    $cache = new Cache();
    $mapper = new PostcodeSectorMapper();
    $sector = new PostcodeSector($form_state['values']['sector'], $form_state['values']['rag']);
    $mapper->saveSector($sector);
    $cache->clear();
    drupal_set_message('Sector saved successfully');
}