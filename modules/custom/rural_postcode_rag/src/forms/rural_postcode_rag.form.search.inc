<?php

use RuralPostcodeRAG\Postcode\PostcodeParser;
use RuralPostcodeRAG\Postcode\PostcodeSectorMapper;
use RuralPostcodeRAG\Postcode\PostcodeSectorValidator;

use RuralPostcodeRAG\Session;

function rural_postcode_rag_search_form($form, &$form_state){

    $form['postcode'] = array(
        '#type' => 'textfield',
        '#title' => 'Postcode',
        '#attributes' => array(
            'placeholder' => "E.g. HG2 8RE",
            'autocomplete' =>'off',
        ),
    );

    $form['submit_search'] = array(
        '#type' => 'submit',
        '#value' => 'Search',
    );

    $form['#attributes']['class'] = array('clearfix');

    return $form;

}

function rural_postcode_rag_search_form_validate($form, &$form_state){
    $parser = new PostcodeParser();
    $validator = new PostcodeSectorValidator();
    $postcode = $parser->parse($form_state['values']['postcode']);

    if(!$postcode || !$validator->postcodeValid($postcode['full'])){
        form_set_error('invpc', 'Error: Invalid Postcode');
        return;
    }

    $form_state['values']['postcode'] = $postcode['outer'].$postcode['sector'];
}

function rural_postcode_rag_search_form_submit($form, &$form_state){
    $mapper = new PostcodeSectorMapper();
    $session = new Session();

    $sector = $form_state['values']['postcode'];
    $sector = $mapper->findBySector($sector);

    if(!$sector){
        $session->set('search_result', 'empty');
        return;
    }

    $session->set('search_result', array(
        'sector' => $sector->getSector(),
        'rag' => $sector->getRagStatus(),
    ));
    return;
}