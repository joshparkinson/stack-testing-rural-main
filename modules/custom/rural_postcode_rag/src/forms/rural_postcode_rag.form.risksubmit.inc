<?php

/**
 * FORM BUILDER
 */

function rural_postcode_rag_risksubmit_form($form, &$form_state, $postcode, $ragStatus){

    $form['#attributes']['class'] = array('clearfix');

    $form['postcode'] = array(
        '#type' => 'hidden',
        '#value' => $postcode,
    );

    $form['ragstatus'] = array(
        '#type' => 'hidden',
        '#value' => $ragStatus,
    );

    $form['name'] = array(
        '#type' => 'textfield',
        '#attributes' => array(
            'placeholder' => 'Name',
        ),
    );

    $form['email'] = array(
        '#type' => 'textfield',
        '#attributes' => array(
            'placeholder' => 'Email',
        ),
    );

    $form['deadline'] = array(
        '#type' => 'textfield',
        '#attributes' => array(
            'placeholder' => 'Deadline for quote (dd/mm/yyyy)'
        ),
    );

    $form['premium'] = array(
        '#type' => 'textfield',
        '#attributes' => array(
            'placeholder' => '£ Target Premium',
        ),
    );

    $form['submission_form'] = array(
        '#type' => 'file',
        '#attrbitues' => array(
            'placeholder' => 'Upload your submission form',
        ),
    );


    $form['submit_risk'] = array(
        '#type' => 'submit',
        '#value' => 'Confirm & Send',
        '#ajax' => array(
            'callback' => 'rural_postcode_rag_risksubmit_form_callback',
            'wrapper' => 'rural-postcode-rag-risksubmit-form',
            'method' => 'replace',
            'effect' => 'fade',
        ),
    );

    return $form;
}

/**
 * VALIDATION
 */

function rural_postcode_rag_risksubmit_form_validate($form, &$form_state){

    //Grab values
    $values = $form_state['values'];

    //Check hidden vars present
    if(empty($values['postcode']) || empty($values['ragstatus'])){
        form_set_error('ohno', 'Something went wrong, please refresh the page and try again.');
    }

    //Validate name
    if(empty($values['name'])){
        form_set_error('name', 'Please enter your name.');
    }

    //Validate email
    if(empty($values['email'])){
        form_set_error('email', 'Please enter your email.');
    }
    else if(!valid_email_address($values['email'])){
        form_set_error('email', 'Invalid email address.');
    }

    //Validate deadline
    if(empty($values['deadline'])){
        form_set_error('deadline', 'Please enter a quote deadline date.');
    }
    else{
        //DD/MM/YYYY == d/m/Y (Allow spaces in input)
        $deadline_input = str_replace(" / ", "/", $values['deadline']);
        $date = DateTime::createFromFormat('d/m/Y', str_replace(" / ", "/", $deadline_input));
        if(!$date || $date->format('d/m/Y') != $deadline_input){
            form_set_error('deadline', 'Invalid quote deadline.');
        }
        else if($date->getTimestamp() < time()){
            form_set_error('deadline', 'Quote deadline already passed.');
        }
    }

    //Validate premium
    if(!empty($values['premium'])){
        $values['premium'] = str_replace("£", "", str_replace(",", "", $values['premium']));
        if(!preg_match("/^([1-9][0-9]*|0)(\.[0-9]{2})?$/", $values['premium'])){
            form_set_error('premium', 'Invalid target premium');
        }
    }

    //Check submission form
    $file = file_save_upload('submission_form', array(
        'file_validate_extensions'    => array ('pdf doc docx rtf txt xls xlsx'),
        'file_validate_size' => array(5 * 1024 * 1024),
    ));
    if(!$file){
        form_set_error('submission_form', 'Please upload a submission form');
    }

}

/**
 * SUBMIT HANDLER
 */

function rural_postcode_rag_risksubmit_form_submit($form, &$form_state){

    //NB = New Buisness (internal) | TC = Thanks Confirmation (excternal)

    //Define keys
    //Must mirror values in rural_postcode_rag_mail()
    $nb_key = 'risksubmission-details-to-nb';
    $tc_key = 'risksubmission-thanks-confirmation';

    //Build attachment
    $file = file_save_upload('submission_form');
    $attachment = array(
        'filecontent' => file_get_contents($file->uri),
        'filename' => $file->filename,
        'filemime' => $file->filemime,
    );

    //Reformat date for email DD/MM/YYYY => (eg) 13th March 2016
    //Must match input format (also validation functionality)
    $deadline = DateTime::createFromFormat('d/m/Y', str_replace(" / ", "/", $form_state['values']['deadline']));
    $deadline = $deadline->format('jS F Y');

    //Reformat premium
    if(!empty($form_state['values']['premium'])){
        $premium = str_replace("£", "", str_replace(",", "", $form_state['values']['premium']));
        $premium = '£ '.number_format($premium, '2');
    }
    else{
        $premium = '<em>Not Specified</em>';
    }

    //Build form data
    $form_data = array(
        'postcode' => strtoupper($form_state['values']['postcode']),
        'ragstatus' => ucfirst(strtolower($form_state['values']['ragstatus'])),
        'name' => ucwords(strtolower($form_state['values']['name'])),
        'email' => strtolower($form_state['values']['email']),
        'deadline' => $deadline,
        'premium' => $premium,
    );

    //Build params array
    $tc_params = array(
        'subject' => 'Thank you for your risk submission',
        'form_data' => $form_data,
        'attachment' => $attachment,
    );

    $nb_params = array(
        'subject' => 'New risk submission via RBM Postcode Checker',
        'form_data' => $form_data,
        'attachment' => $attachment,
    );

    drupal_mail('rural_postcode_rag', $nb_key, 'newbusiness@ruralinsurance.co.uk', 'en', $nb_params);
    drupal_mail('rural_postcode_rag', $nb_key, 'marketing@ruralinsurance.co.uk', 'en', $nb_params);
    drupal_mail('rural_postcode_rag', $tc_key, $form_state['values']['email'], 'en', $tc_params);
}

/**
 * AJAX CALLBACK
 */

function rural_postcode_rag_risksubmit_form_callback($form, &$form_state){
    $commands = array();
    $commands[] = ajax_command_invoke('#edit-submit-risk', 'removeClass', array('loading'));

    if (form_get_errors()){
        $form_state['rebuild'] = TRUE;
        $commands[] = ajax_command_prepend(NULL, '<div class="ajax-form-response">'.theme('status_messages').'</div>');
        drupal_get_messages();
        return array('#type' => 'ajax', '#commands' => $commands);
    }

    drupal_get_messages();

    $output = '<h2 class="force-big smile">Thanks for submitting your risk</h2>';
    $output .= '<p class="sub">We’ll be in touch. If you need to get in touch in the meantime just call us on 0344 55 77 177</p>';
    $output .= '<div class="close-rag">Close</div>';

    $commands[] = ajax_command_replace(NULL, $output);
    $commands[] = ajax_command_invoke(NULL, 'rbmpc_GaSendSubmissionEvent', array($form_state['values']['ragstatus']));
    $commands[] = ajax_command_invoke('#send-submission-wrapper', 'addClass', array('no-border'));
    $commands[] = ajax_command_invoke('.rag-response > h2', 'hide');

    return array('#type' => 'ajax', '#commands' => $commands);
}