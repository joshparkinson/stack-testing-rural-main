<?php

function rural_postcode_rag_permission(){
    return array(
        'administer rural postcode rag' => array(
            'title' => t('Administer Rural Postcode RAG'),
            'description' => t('ADD/EDIT Postcode Sector RAG Status'),
        ),
    );
}