<?php

/* @param \Drupal\xautoload\Adapter\LocalDirectoryAdapter $adapter */

function rural_postcode_rag_xautoload($adapter){
    $adapter->addPsr4('RuralPostcodeRAG\\', 'src/class');
}