<?php

function rural_postcode_rag_mail($key, &$message, $params){

    if($key == 'risksubmission-details-to-nb' || $key == 'risksubmission-thanks-confirmation'){

        $template_path = drupal_get_path('module', 'rural_postcode_rag').'/templates/email';
        $template = ($key == 'risksubmission-details-to-nb') ? 'risksubmission-to-new-business.html' : 'risksubmission-thanks-confirmation.html';
        $template_contents = file_get_contents($template_path.'/'.$template);

        $email_body = strtr($template_contents, array(
            '[@postcode]' => $params['form_data']['postcode'],
            '[@ragstatus]' => $params['form_data']['ragstatus'],
            '[@name]' => $params['form_data']['name'],
            '[@email]' => $params['form_data']['email'],
            '[@deadline]' => $params['form_data']['deadline'],
            '[@premium]' => $params['form_data']['premium'],
        ));
        
        $message['subject'] = $params['subject'];
        $message['body'] = $email_body;

        // Add attachment when available.
        if (isset($params['attachment'])) {
            $message['params']['attachments'][] = $params['attachment'];
        }

    }
}