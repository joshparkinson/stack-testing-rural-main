<?php

use RuralPostcodeRAG\Cache;
use RuralPostcodeRAG\Config\ConfigFactory;

//Implements hook_flush_caches()
function rural_postcode_rag_flush_caches(){
    $CacheConfig = ConfigFactory::build('Cache');
    return array($CacheConfig->get('bin'));
}

//Implements hook_admin_menu_cache_info()
function rural_postcode_rag_admin_menu_cache_info(){
    $caches['rural_postcode_rag'] = array(
        'title' => t('Rural: Postcode RAG'),
        'callback' => 'rural_postcode_rag_cache_clear',
    );
    return $caches;
}

//Local helper to trigger cache clear function
function rural_postcode_rag_cache_clear(){
    $cache = new Cache();
    $cache->clear();
    unset($cache);
}