<?php

use RuralPostcodeRAG\Config\ConfigFactory;
use RuralPostcodeRAG\Javascript;
use RuralPostcodeRAG\Postcode\PostcodeSectorValidator;

use RuralPostcodeRAG\Session;


/**
 * Implements hook_block_info().
 * Define blocks implemented by this module
 */
function rural_postcode_rag_block_info(){
    $blocks = array();
    $blocks['rural_postcode_rag'] = array(
        'info' => t('Rural Postcode RAG'),
    );
    return $blocks;
}

function rural_postcode_rag_block_view($delta = ''){
    $block = array();
    switch($delta){
        case 'rural_postcode_rag':
            $block['subject'] = '';
            $block['content'] = _build_rural_postcode_rag_block();
            break;
    }
    return $block;
}

function _build_rural_postcode_rag_block(){

    //Dont initialise block fully on admin pages
    /* @TODO: move floatlabel etc etc etc into libraries rather than themes / new build */
    if(path_is_admin(current_path())){
        return "RBM Postcode Checker Block";
    }

    //Init Session
    $session = new Session();

    //Load form
    $FilePathsConfig = ConfigFactory::build('FilePaths');
    $form_file = $FilePathsConfig->get('form', FALSE).'/rural_postcode_rag.form.search';
    module_load_include('inc', 'rural_postcode_rag', $form_file);
    $form = drupal_get_form('rural_postcode_rag_search_form');

    //Initialise javascript,
    // leverage postcode validator to get regex pattern
    // leverage config to get ajax path
    $Validator = new PostcodeSectorValidator();
    $PathConfig = ConfigFactory::build('Path');
    $Javascript = new Javascript();
    $Javascript->addFile('form.search.js');
    $Javascript->addFile('form.risksubmit.js');
    $Javascript->addFile('shared_callbacks.js');
    $Javascript->addLibrary('system', 'drupal.ajax');
    $Javascript->addLibrary('system', 'jquery.form');
    $Javascript->addLibrary('system', 'effects.bounce');
    $Javascript->addSetting('ajax_path', '/'.$PathConfig->get('ajax_path'));
    $Javascript->addSetting('postcode_regex', $Validator->getPostcodePattern());
    $Javascript->attach();

    $output = "<div class='rag-intro'><p>Enter your client's postcode to find out if we can provide a quote for the location.</p></div>";

    //Build form output
    $output .=  drupal_render($form);

    //Build response output
    $output .= "<div id='rag-response-container'>";
    if($search_result = $session->get('search_result', FALSE)){
        if($search_result == "empty"){
            $output .= "<div class='error'>Couldnt find status for the chosen postcode</div>";
        }
        else if(is_array($search_result)){
            $output .= "<div class='rag'>".$search_result['rag']."</div>";
            $output .= "<div class='sector'>".$search_result['sector']."</div>";
        }
        $session->set('search_result', FALSE);
    }
    $output .= "</div>";

    return $output;
}