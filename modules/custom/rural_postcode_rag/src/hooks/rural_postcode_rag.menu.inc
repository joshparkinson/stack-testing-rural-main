<?php

use RuralPostcodeRAG\Config\ConfigFactory;

function rural_postcode_rag_menu(){

    //Build Configs
    $FilePathsConfig = ConfigFactory::build('FilePaths');
    $PathConfig = ConfigFactory::build('Path');

    //Return Paths
    $items = array();

    /*
    $items[$PathConfig->get('path')] = array(
        'title' => 'Rural Business Motor postcode checker',
        'page callback' => 'rural_postcode_rag_main_page',
        'file' => $FilePathsConfig->get('page', FALSE).'/rural_postcode_rag.page.main.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );
    */

    $items[$PathConfig->get('ajax_path')] = array(
        'title' => 'Rural Business Motor postcode checker ajax callback',
        'page callback' => 'rural_postcode_rag_ajax_callback_page',
        'file' => $FilePathsConfig->get('page', FALSE).'/ajax/rural_postcode_rag.ajax.callback.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );

    $items[$PathConfig->get('overlay_path').'/%ctools_js'] = array(
        'title' => 'Rural Business Motor postcode checker overlay',
        'page callback' => 'rural_postcode_rag_overlay_page',
        'page arguments' => array($PathConfig->get('overlay_path_wildcard_position')),
        'file' => $FilePathsConfig->get('page', FALSE).'/rural_postcode_rag.page.overlay.inc',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );

    $items[$PathConfig->get('admin_path')] = array(
        'title' => 'Rural Business Motor postcode checker administration',
        'page callback' => 'rural_postcode_rag_admin_page',
        'file' => $FilePathsConfig->get('page', FALSE).'/rural_postcode_rag.page.admin.inc',
        'access arguments' => array('administer rural postcode rag'),
        'type' => MENU_CALLBACK,
    );
    
    return $items;

}