<?php

use RuralPostcodeRAG\Config\ConfigFactory;

//Implements hook_preprocess_page()
function rural_postcode_rag_preprocess_html(&$vars){
    
    //Nothing to do if we're not on the postcode rag main page
    $PathConfig = ConfigFactory::build('Path');
    if(drupal_get_path_alias() == $PathConfig->get('path')){
        $vars['classes_array'][] = 'rural-postcode-rag broker-area';
    }

    if(drupal_get_path_alias() == $PathConfig->get('overlay_path/ajax')){
        $vars['classes_array'][] = 'rural-postcode-overlay-rag';
    }
    
}