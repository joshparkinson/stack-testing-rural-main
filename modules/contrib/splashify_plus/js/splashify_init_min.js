var idleTimer = null;
var modalSeen = false;
var ns = false;
var cbr = true;

jQuery(document).ready(function(e) {
    
    var t = Drupal.settings.splashify.js_mode;
    if (t == "redirect") {
        hidepage()
    }
    var n = new Date;
    var r = n.getTime() / 1e3;
    var i = document.referrer + "";
    var s = window.location.hostname + "";
    var o = e.jStorage.get("splash", 0);
    var u = Drupal.settings.splashify.js_splash_always;
    var a = Drupal.settings.splashify.js_mode_settings.urls;
    var f = Drupal.settings.splashify.js_disable_referrer_check;
    var l = Number(Drupal.settings.splashify.js_mode_settings.delay);
    var c = Number(Drupal.settings.splashify.js_mode_settings.how_autoclose);
    var z = Drupal.settings.splashify.js_mode_settings.never_show;
    var zo = Drupal.settings.splashify.js_mode_settings.ns_option;
    var sp = Drupal.settings.splashify.js_mode_settings.specific;
    var cp = Drupal.settings.splashify.js_mode_settings.curpage;
    var cps = Drupal.settings.splashify.js_mode_settings.curpage;
    var ifrm = true;

    if(Drupal.settings.splashify.js_mode_settings.reposition){
        cbr = false;
    }

    if(Drupal.settings.splashify.js_mode_settings.mode == "form" || (Drupal.settings.splashify.js_mode_settings.mode == "specific" && Drupal.settings.splashify.js_mode_settings.specific_type == "form")){
        ifrm = false;
    }

    if (e.isArray(cp)) {
        cps = cp.join('_');
    }

    if (i.indexOf("?") != -1) {
        i = i.substr(0, i.indexOf("?"));
    }
    if (i.search(s) != -1 && !f || jQuery.inArray(window.location.pathname, a) > -1) {
        showpage();
        return;
    }
    var h = false;
    if (!o || o < r || u == "1") {
        h = true;
    }
    if (h) {
        var p = Drupal.settings.splashify.js_expire_after;
        var d = e.jStorage.get("splashlasturl", "");
        var v = "";
        e.jStorage.set("splash", r + p);
        if (Drupal.settings.splashify.js_mode_settings.system_splash != "") {
            v = Drupal.settings.splashify.js_mode_settings.system_splash;
        } else if (Drupal.settings.splashify.js_mode_settings.mode == "sequence") {
            var m = 0;
            var g = jQuery.inArray(d, a);
            if (g > -1 && g + 1 < Drupal.settings.splashify.js_mode_settings.total_urls) {
                m = g + 1
            }
            v = a[m]
        } else if (Drupal.settings.splashify.js_mode_settings.mode == "random") {
            var m = Math.floor(Math.random() * Drupal.settings.splashify.js_mode_settings.total_urls);
            v = a[m]
        } else if (Drupal.settings.splashify.js_mode_settings.mode == "specific") {
            v = sp;
        } else if (Drupal.settings.splashify.js_mode_settings.mode == "node") {
            v = Drupal.settings.splashify.js_mode_settings.node_path;
        }
        e.jStorage.set("splashlasturl", v);
        if (t == "redirect") {
            setTimeout(function() {
                window.location.replace(v)
            }, l)
        } else if (t == "colorbox" && modalSeen == false) {
            idleTimer = setTimeout(function() {
                e.colorbox({
                    transition: "elastic",
                    speed: 1,
                    fixed: true,
                    href: v,
                    iframe: ifrm,
                    initialWidth: Drupal.settings.splashify.js_mode_settings.size_width,
                    initialHeight: Drupal.settings.splashify.js_mode_settings.size_height,
                    innerWidth: Drupal.settings.splashify.js_mode_settings.size_width,
                    innerHeight: Drupal.settings.splashify.js_mode_settings.size_height,
                    className: Drupal.settings.splashify.js_mode_settings.box_class,
                    reposition: cbr,
                    onOpen: function() {
                        e("#colorbox").hide();
                    },
                    onComplete: function() {
                        if (z) {
                            e('#cboxContent').append('<div class="cookieWrapper"><input name="nevershow" type="checkbox" value="true" onClick="setNS()" />Never show me this again</div>');
                        }
                        if (c != 0) {
                            setTimeout(function() {
                                e.colorbox.close()
                            }, c);
                        }
                        e('body').css({'overflow':'hidden'});
                        setCboxPosition();
                        e("#colorbox").show();
                    },
                    onClosed: function() {
                        checkNS();
                        e('body').css({'overflow':'visible'});
                        e('#colorbox').hide();
                    }
                })
                if (Drupal.settings.splashify.js_mode_settings.showonce == 1) {
                    modalSeen = true;
                }
            }, l);
        } else if (t == "window") {
            window.open(v, "splash", Drupal.settings.splashify.js_mode_settings.size)
        }
    } else if (t == "redirect") {
        showpage()
    }

    e(document).bind('mousemove keydown scroll', function() {
        if (Drupal.settings.splashify.js_mode_settings.inactive == 1 && t == "colorbox") {
            clearTimeout(idleTimer);
            if (modalSeen == false) {
                idleTimer = setTimeout(function() {
                    e.colorbox({
                        transition: "elastic",
                        speed: 1,
                        fixed: true,
                        href: v,
                        iframe: ifrm,
                        initialWidth: Drupal.settings.splashify.js_mode_settings.size_width,
                        initialHeight: Drupal.settings.splashify.js_mode_settings.size_height,
                        innerWidth: Drupal.settings.splashify.js_mode_settings.size_width,
                        innerHeight: Drupal.settings.splashify.js_mode_settings.size_height,
                        className: Drupal.settings.splashify.js_mode_settings.box_class,
                        reposition: cbr,
                        onOpen: function() {
                            e("#colorbox").hide();
                        },
                        onComplete: function() {
                            if (z) {
                                e('#cboxContent').append('<div class="cookieWrapper"><input name="nevershow" type="checkbox" value="true" onClick="setNS()" />Never show me this again</div>');
                            }
                            if (c != 0) {
                                setTimeout(function() {
                                    e.colorbox.close()
                                }, c);
                            }
                            e('body').css({'overflow':'hidden'});
                            setCboxPosition(false);
                            e("#colorbox").show();
                        },
                        onClosed: function() {
                            checkNS();
                            e('#colorbox').hide();
                            e('body').css({'overflow':'visible'});
                        }
                    })
                    if (Drupal.settings.splashify.js_mode_settings.showonce == 1) {
                        modalSeen = true;
                    }
                }, l);
            }
        }
    });

    e(window).unbind('resize').resize(function(){
        if(t == "colorbox"){
            setCboxPosition(true);
        }
    });

    e("#colorbox").click(function(){
        e.colorbox.close();
    });
    
    e("#colorbox #cboxWrapper").click(function(e){
        e.stopPropagation();
    });

    function checkNS() {
        if (ns == true) {
            if (zo == "global") {
                e.cookie('no_splash', true, {
                    path: '/'
                });
            } else if (zo == "specific") {
                e.cookie('no_splash_' + cps, true, {
                    path: '/'
                });
            }
        }
    }

});

function showpage() {
    jQuery("html").show();
}

function hidepage() {
    jQuery("html").hide();
}

function setCboxPosition(checkVisible){

    var repos = true;

    if(checkVisible == true){

        if(jQuery("#colorbox").is(':visible')){
            repos = true;
        }
        else{
            repos = false;
        }

    }

    if(cbr == false && repos == true){

        //Store window height
        var win_height = jQuery(window).height();

        var cbox_wrapper_height = jQuery("#cboxWrapper").css('height').slice(0, -2);

        var top_pos = Math.round((win_height / 2) - (cbox_wrapper_height / 2));

        jQuery("#colorbox").offset({ top: top_pos, left: 0 });

        jQuery("#cboxWrapper").offset({ top: top_pos, left: 0 });

        jQuery('#colorbox').css('cssText', 'top: ' + top_pos + 'px !important');

        jQuery('#cboxWrapper').css('cssText', 'top: ' + top_pos + 'px !important');

    }

}

function setNS() {
    if (ns == false) {
        ns = true;
    } else if (ns == true) {
        ns = false;
    }
}