<?php

/**
 * @file
 * The admin "What" tab.
 *
 * Admin functionality that determines what should show up as the splash
 * page.
 */

/**
 * "What" settings tab.
 */
function splashify_admin_what_form($form, &$form_state) {
  //Get Values From Where Form
  $where_page = variable_get('splashify_where_desktop_page', '');
  $where_exclude = variable_get('splashify_where_desktop_opposite', '');
  
  //Get Values From How Form
  $how_desktop_mode = variable_get('splashify_how_desktop_mode', '');

  //Check if list pages option is chosen and not to exclude.
  $valid_list = false;
  if($where_page == "list" && !$where_exclude){
    $list_pages_string = variable_get('splashify_where_desktop_listpages', '');
    $list_pages = explode("\n", $list_pages_string);
    if(strlen($list_pages_string) != 0 && !empty($list_pages)){
      $valid_list = true;
    } 
  }

  $form = array();

  $form['description'] = array(
    '#markup' => '<p>' . t('What should show up as the splash page?') . '</p>',
  );

  $form['desktop'] = array(
    '#type' => 'fieldset',
    '#title' => t('Desktop Settings'),
  );

  $form['desktop']['splashify_what_desktop_mode'] = array(
    '#type' => 'select',
    '#title' => t('Splash Mode'),
    '#options' => array(
      'random' => t('Pick random path or URL from the list'),
      'sequence' => t('Pick next path or URL from the list'),
      'node' => t('Display a Full Node'),
      'node-content' => t('Display a Nodes Content'),
      'text' => t('Display Custom Text/HTML'),
    ),
    '#default_value' => variable_get('splashify_what_desktop_mode', ''),
    '#description' => t('Determines how the content field below will be used with the splash page.'),
    '#ajax' => array(
      'callback' => 'splashify_ajax_what_desktop_mode_callback',
      'wrapper' => 'what-mode-desktop-value',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  //Add Entity Form Option if module installed
  if(module_exists('entityform')){
    $form['desktop']['splashify_what_desktop_mode']['#options']['form'] = t('Display an EntityForm by "Type"');
  }

  //Add View Option if module installed
  if(module_exists('views')){
    $form['desktop']['splashify_what_desktop_mode']['#options']['view'] = t('Display a View');
  }

  //Add specific content per page option is using a non exclude list
  if($valid_list){
    $form['desktop']['splashify_what_desktop_mode']['#options']['specific'] = t('Display specific content for each "Where" page');
  }

  // Set a variable that is either defined by the selection from the ajax
  // dropdown menu, or a previously saved value.
  if (isset($form_state['values']['splashify_what_desktop_mode'])) {
    $what_desktop_mode_set = $form_state['values']['splashify_what_desktop_mode'];
  }
  else {
    $what_desktop_mode_set = variable_get('splashify_what_desktop_mode', '');
  }

  $splashify_what_desktop_content = variable_get('splashify_what_desktop_content', '');

  $form['desktop']['mode_value']['begin'] = array('#markup' => '<div id="what-mode-desktop-value">');

  if($what_desktop_mode_set == 'node-content' || $what_desktop_mode_set == 'text' || $what_desktop_mode_set == 'view' || $what_desktop_mode_set == 'form'){
    $form['desktop']['mode_value']['splashify_what_splash_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Splash Title'),
      '#default_value' => variable_get('splashify_what_splash_title', ''),
      '#description' => t('NOT REQUIRED. If left blank, no title, or conent default will be used.'),
    );
  }
  if($what_desktop_mode_set == 'node'){
    $form['desktop']['mode_value']['splashify_what_full_node'] = array(
      '#type' => 'textfield',
      '#title' => t('Node ID/Alias'),
      '#default_value' => variable_get('splashify_what_full_node', ''),
    );
  }
  else if($what_desktop_mode_set == 'node-content'){
    $form['desktop']['mode_value']['splashify_what_node_content'] = array(
      '#type' => 'textfield',
      '#title' => t('Node ID/Alias'),
      '#default_value' => variable_get('splashify_what_node_content', ''),
    );
  }
  else if($what_desktop_mode_set == 'form'){
    $form['desktop']['mode_value']['splashify_what_entityform_type'] = array(
      '#type' => 'textfield',
      '#title' => t('EntityForm Type'),
      '#description' => t('Enter the machine of the entityform type to display'),
      '#default_value' => variable_get('splashify_what_entityform_type', ''),
    );
    if($how_desktop_mode = "lightbox"){
      $form['desktop']['mode_value']['splashify_what_entityform_close_lightbox'] = array(
        '#type' => 'checkbox',
        '#title' => 'Close Lightbox On Submission',  
        '#description' => t('Alters form submission process to leading to redirect to splash parent page. Simulating lightbox closing.<br />If left unchecked the form will redirect to page specificed in the
                              entityform\'s "redirect path" setting.'),
        '#default_value' => variable_get('splashify_what_entityform_close_lightbox', ''),
      );
    }
  }
  else if($what_desktop_mode_set == 'view'){
    $form['desktop']['mode_value']['what_view']['splashify_what_view_name'] = array(
      '#type' => 'textfield',
      '#title' => t('View Machine Name'),
      '#default_value' => variable_get('splashify_what_view_name', ''),
    );
    $form['desktop']['mode_value']['what_view']['splashify_what_view_display'] = array(
      '#type' => 'textfield',
      '#title' => t('View Display ID'),
      '#default_value' => variable_get('splashify_what_view_display', 'default'), 
      '#description' => t("The display mode for the view. Usually: 'page','block' etc etc"),
    );
    $form['desktop']['mode_value']['what_view']['splashify_what_view_arg'] = array(
      '#type' => 'textfield',
      '#title' => t('View Arguments'),
      '#default_value' => variable_get('splashify_what_view_arg', ''),
      '#description' => t("Any arguments needing to be passed to the view. If contextual filters are used for example.<br />Multiple values allowed, should be seperated with a '/'<br />Example:1/14/3"), 
    );
  }
  else if($what_desktop_mode_set == 'text'){
      //Get Previous Entry Defaults
      $what_custom_text_default = '';
      if(module_exists('ckeditor')){
        $what_custom_format_default = 'ckeditor';
      }
      else{
        $what_custom_format_default = 'plain_text';
      }
      $what_custom_text = variable_get('splashify_what_custom_text', '');
      $what_custom_text_default = $what_custom_text['value'];
      $what_custom_format_default = $what_custom_text['format'];
      $form['desktop']['mode_value']['splashify_what_custom_text'] = array(
        '#type' => 'text_format',
        '#title' => t('Display Content'),
        '#default_value' => $what_custom_text_default,
        '#format' => $what_custom_format_default,
      );
      $form['desktop']['mode_value']['splashify_what_custom_text_type'] = array(
        '#type' => 'checkbox',
        '#title' => 'Show in Site Template',  
        '#description' => t('Entered content will be displayed in a full site page.'),
        '#default_value' => variable_get('splashify_what_custom_text_type', ''),
      );
  }
  else if($what_desktop_mode_set == 'specific'){
    
    //Get saved values if avaiable
    $what_specific_saved = variable_get('splashify_what_specific_list_pages', '');
    
    //Display all list pages
    foreach($list_pages as $list_page){
      $list_page = trim($list_page);

      //Get Custom Output Field Defaults
      if(module_exists('ckeditor')){
        $what_specific_format_default = 'ckeditor';
      }
      else{
        $what_specific_format_default = 'plain_text';
      }
      if(isset($what_specific_saved[$list_page]) && $what_specific_saved[$list_page]['type'] == "text"){
        $what_specific_text_default = $what_specific_saved[$list_page]['data'];
        $what_specific_format_default = variable_get('text_pick_'.$list_page);
        $what_specific_format_default = $what_specific_format_default['format'];
      }
      else{
        $what_specific_text_default = '';
      }

      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page] = array(
        '#type' => 'fieldset',
        '#title' => t('"'.$list_page.'" Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#states' => array(
          'visible' => array(
            ':input[name="splashify_what_desktop_mode"]' => array('value' => 'specific'),
          ),
        ),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['type_'.$list_page] = array(
        '#type' => 'select',
        '#title' => t('Display Type'),
        '#options' => array(
          'node' => t('Display a Full Node'),
          'node-content' => t('Display a Nodes Content'),
          'text' => t('Display Custom Text/HTML'),
        ),
        '#description' => t('Determins what to display for '.$list_page.' page popup.'),
        '#default_value' => variable_get('type_'.$list_page, ''),
      );
      if(module_exists('entityform')){
        $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['type_'.$list_page]['#options']['form'] = t('Display an EntityForm by "Type"');
      }
      if(module_exists('views')){
        $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['type_'.$list_page]['#options']['view'] = t('Display a View');
      }
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['splash_title_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('Splash Title'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array(
              array('value' => 'node-content'),
              array('value' => 'text'),
              array('value' => 'view'),
              array('value' => 'form'),
            ),
          ),
        ),
        '#default_value' => variable_get('splash_title_'.$list_page, ''),
        '#description' => t('NOT REQUIRED. If left blank, no title, or conent default will be used.'),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['node_pick_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('Node ID/Alias'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'node'),
          ),
        ),
        '#default_value' => variable_get('node_pick_'.$list_page, ''),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['node_content_pick_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('Node ID/Alias'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'node-content'),
          ),
        ),
        '#default_value' => variable_get('node_content_pick_'.$list_page, ''),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['form_pick_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('EntityForm Type'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'form'),
          ),
        ),
        '#description' => t('Enter the machine of the entityform type to display'),
        '#default_value' => variable_get('form_pick_'.$list_page, ''),
      );
      if($how_desktop_mode == "lightbox"){
        $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['form_close_lightbox_'.$list_page] = array(
          '#type' => 'checkbox',
          '#title' => 'Close Lightbox On Submission',
          '#states' => array(
            'visible' => array(
              ':input[name="type_'.$list_page.'"]' => array('value' => 'form'),
            ),
          ),  
          '#description' => t('Alters form submission process to leading to redirect to splash parent page. Simulating lightbox closing.<br />If left unchecked the form will redirect to page specificed in the
                                entityform\'s "redirect path" setting.'),
          '#default_value' => variable_get('form_close_lightbox_'.$list_page, ''),
        );
      }
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['container_'.$list_page] = array(
        '#type' => 'container',
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'text'),
          ),
        ),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['container_'.$list_page]['text_pick_'.$list_page] = array(
        '#type' => 'text_format',
        '#title' => t('Display Content'),
        '#default_value' => $what_specific_text_default,
        '#format' => $what_specific_format_default,
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['container_'.$list_page]['text_type_'.$list_page] = array(
        '#type' => 'checkbox',
        '#title' => 'Show in Site Template',  
        '#description' => t('Entered content will be displayed in a full site page.'),
        '#default_value' => variable_get('text_type_'.$list_page, ''),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'text'),
          ),
        ),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['view_name_pick_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('View Machine Name'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'view'),
          ),
        ),
        '#default_value' => variable_get('view_name_pick_'.$list_page, ''),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['view_display_pick_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('View Display ID'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'view'),
          ),
        ),
        '#default_value' => variable_get('view_display_pick_'.$list_page, 'default'), 
        '#description' => t("The display mode for the view. Usually: 'page','block' etc etc"),
      );
      $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['view_arg_pick_'.$list_page] = array(
        '#type' => 'textfield',
        '#title' => t('View Arguments'),
        '#states' => array(
          'visible' => array(
            ':input[name="type_'.$list_page.'"]' => array('value' => 'view'),
          ),
        ),
        '#default_value' => variable_get('view_arg_pick_'.$list_page, ''),
        '#description' => t("Any arguments needing to be passed to the view. If contextual filters are used for example.<br />Multiple values allowed, should be seperated with a '/'<br />Example:1/14/3"), 
      );
      if($how_desktop_mode == "lightbox" || $how_desktop_mode == "window"){
        $form['desktop']['mode_value']['what_specific_list_pages']['list_page_'.$list_page]['dimensions_'.$list_page] = array(
          '#type' => 'textfield',
          '#title' => t('New Window / Lightbox Dimensions'),
          '#default_value' => variable_get('dimensions_'.$list_page, ''),
          '#description' => t("Values specified here will override the default value entered under the 'How' tab.<br />Format: [WIDTH]x[HEIGHT] | Example: 600x400"), 
        );
      }
    }

  }
  else {
    // Display a textarea field.
    $form['desktop']['mode_value']['splashify_what_urls'] = array(
      '#type' => 'textarea',
      '#title' => t('Paths or Url Values'),
      '#default_value' => variable_get('splashify_what_urls', ''),
      '#description' => t('Enter the site paths or URLs (one on each line) to use. You can use either the drupal source path, or the alias. <br />To open a non-Drupal path, use an absolute URL, i.e. http://example.com/splash.html'),
    );
  }

  $form['desktop']['mode_value']['end'] = array('#markup' => '</div>');


  /*
   * Mobile settings.
   */

  $form['mobile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile Settings'),
  );

  // If the mobile splash is enabled, display the mobile options.
  if (variable_get('splashify_when_mobile', 0) == 1) {
    $form['mobile']['splashify_what_mobile_mode'] = array(
      '#type' => 'select',
      '#title' => t('Splash Mode'),
      '#options' => array(
        'random' => t('Pick random path or URL from the list'),
        'sequence' => t('Pick next path or URL from the list'),
        'template' => t('Display entered text in the site template'),
        'fullscreen' => t('Display entered text/HTML full screen'),
      ),
      '#default_value' => variable_get('splashify_what_mobile_mode', 'random'),
      '#description' => t('Determines how the content field below will be used with the splash page.'),
      '#ajax' => array(
        'callback' => 'splashify_ajax_what_mobile_mode_callback',
        'wrapper' => 'what-mode-mobile-value',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

    // Set a variable that is either defined by the selection from the ajax
    // dropdown menu, or a previously saved value.
    if (isset($form_state['values']['splashify_what_mobile_mode'])) {
      $what_mobile_mode_set = $form_state['values']['splashify_what_mobile_mode'];
    }
    else {
      $what_mobile_mode_set = variable_get('splashify_what_mobile_mode', '');
    }

    $splashify_what_mobile_content = variable_get('splashify_what_mobile_content', '');

    $form['mobile']['mode_value']['begin'] = array('#markup' => '<div id="what-mode-mobile-value">');

    // If they specified the template or fullscreen option, we want to hide the
    // window size text field.
    if ($what_mobile_mode_set == 'template' || $what_mobile_mode_set == 'fullscreen') {
      $form['mobile']['mode_value']['splashify_what_mobile_content'] = array(
        '#type' => 'text_format',
        '#title' => t('Content'),
        '#default_value' => (is_array($splashify_what_mobile_content)) ? $splashify_what_mobile_content['value'] : '',
        '#format' => (is_array($splashify_what_mobile_content)) ? $splashify_what_mobile_content['format'] : '',
        '#description' => t('Text to show or paths/URLs (one on each line) to use.  To open a non-Drupal path, use an absolute URL, i.e. http://example.com/splash.html'),
      );

      // If the template mode is selected, allow them to set a page title value.
      if ($what_mobile_mode_set == 'template') {
        $form['mobile']['mode_value']['splashify_what_mobile_pagetitle'] = array(
          '#type' => 'textfield',
          '#title' => t('Page Title'),
          '#default_value' => variable_get('splashify_what_mobile_pagetitle', ''),
        );
      }
    }
    else {
      // If this value is an array, the mode was changed and we want to empty
      // out the value.
      if (is_array($splashify_what_desktop_content)) {
        $splashify_what_desktop_content = '';
      }

      // Display a textarea field.
      $form['mobile']['mode_value']['splashify_what_mobile_content'] = array(
        '#type' => 'textarea',
        '#title' => t('Paths or Url Values'),
        '#default_value' => $splashify_what_mobile_content,
        '#description' => t('Enter the site paths or URLs (one on each line) to use. You can use either the drupal source path, or the alias. <br />To open a non-Drupal path, use an absolute URL, i.e. http://example.com/splash.html'),
      );
    }

    $form['mobile']['mode_value']['end'] = array('#markup' => '</div>');

  }
  else {
    $form['mobile']['splashify_what_mobile_mode'] = array(
      '#markup' => '<p>' . t('In order to specify mobile options, you need to enable the "When: Enable Unique Mobile Splash" option.') . '</p>',
    );
  }

  $form['#validate'][] = 'splashify_admin_what_form_validate';
  $form['#submit'][] = 'splashify_admin_what_form_submit';
  return system_settings_form($form);
}

/**
 * Implements form validation handler.
 */
function splashify_admin_what_form_validate($form, &$form_state) {
  // If they entered paths, make sure the path values are valid.
  $desktop_mode = $form_state['values']['splashify_what_desktop_mode'];
  if ($desktop_mode == 'random' || $desktop_mode == 'sequence') {
    splashify_what_paths_check('splashify_what_urls', $form_state['values']['splashify_what_urls']);
  }
  else if($desktop_mode == 'text'){
    //Ensure Text Field Isn't Empty
    if($form_state['values']['splashify_what_custom_text']['value'] == NULL){
      form_set_error("custom display empty", "Display Content Can't Be Empty");
    }
  }
  else if($desktop_mode == 'node'){
    splashify_validate_node($form_state['values']['splashify_what_full_node']);
  }
  else if($desktop_mode == 'node-content'){
    splashify_validate_node($form_state['values']['splashify_what_node_content']);
  }
  else if($desktop_mode == 'form'){
    splashify_validate_entityform($form_state['values']['splashify_what_entityform_type']);
  }
  else if($desktop_mode == 'view'){
    splashify_validate_view($form_state['values']['splashify_what_view_name'], $form_state['values']['splashify_what_view_display']);
  }
  else if($desktop_mode == 'specific'){
    //Init Variables
    $specific = array();
    $errors = 0;
    $data = NULL;

    //Loop through all specific options
    foreach($form_state['complete form']['desktop']['mode_value']['what_specific_list_pages'] as $listkey => $listval){
      //Only loop over required array parts
      if(strpos($listkey, '#') === false){
        //Get Page Alias
        $page = explode('_', $listkey);
        $page = $page[2];

        //Get Form Values
        $type = $listval['type_'.$page]['#value'];
        $node = $listval['node_pick_'.$page]['#value'];
        $node_content = $listval['node_content_pick_'.$page]['#value'];
        $form = $listval['form_pick_'.$page]['#value'];
        $view_name = $listval['view_name_pick_'.$page]['#value'];
        $view_display = strtolower($listval['view_display_pick_'.$page]['#value']);
        $view_arg = $listval['view_arg_pick_'.$page]['#value'];
        $cont = $listval['container_'.$page]['text_pick_'.$page]['value']['#value'];
        $intemplate = $listval['container_'.$page]['text_type_'.$page]['#value'];
        $dimensions = $listval['dimensions_'.$page]['#value'];
        $dims = '';
        $title = $listval['splash_title_'.$page]['#value'];

        //Validate type choice, this should never fail.
        if($type != "node" && $type != "node-content" && $type != "form" && $type != "text" && $type != "view"){
          $errors++;
          form_set_error($page." type invalid", "Type choice for ".$page." is invalid");
        }

        //Force Site Template Option To False If Not Text
        if($type != "text"){
          $keyArray = explode('list_page_', $listkey);
          $key = $keyArray[1];
          $form_state['values']['text_type_'.$key] = 0;
        }

        //Validate node type
        if($type == "node" || $type == "node-content"){
          if($type == "node-content"){
            $node = $node_content;
          }
          //$errors += splashify_validate_node($node, $page);
        }

        //Validate form related type fields
        if($type == "form"){
          $errors += splashify_validate_entityform($form, $page);
        }

        //Validate text related type fields
        if($type == "text" && $cont == NULL){
          form_set_error($page." text empty error", "'".$page."' Text Field is Required.");
          $errors++;
        }
        else{
          $format = $listval['container_'.$page]['text_pick_'.$page]['#format'];
        }

        //Validate view related type fields
        if($type == "view"){
          $errors += splashify_validate_view($view_name, $view_display, $page);
        }

        //Validate Dimensions Field If Not Blank
        if($dimensions != NULL){
          $vd = splashify_validate_dimensions($dimensions, $page);
          $errors += $vd[0];
          $dims = $vd[1];
        }

        //Determins what to pass to the specific array
        if($type == "node" || $type == "node-content"){
          $data = $node;
        }
        else if($type == "form"){
          $data = $form;
        }
        else if($type == "view"){
          $data = $view_name."|".$view_display."|".$view_arg;
          $data = str_replace(" ", "", $data);
        }
        else if($type == "text"){
          $data = $cont;
        }

        $specific[$page] = array (
          "title" => $title,
          "type" => $type,
          "data" => $data,
          "dims" => $dims,
        );

      }
    }

    //Set variable if passed validation
    if($errors == 0){
      variable_set("splashify_what_specific_list_pages", $specific);
    }
    else{
      variable_set("spashify_what_specific_list_pages", '');
    }

  }

  if (isset($form_state['values']['splashify_what_mobile_mode'])) {
    $mobile_mode = $form_state['values']['splashify_what_mobile_mode'];
    if ($mobile_mode == 'random' || $mobile_mode == 'sequence') {
      splashify_what_paths_check('splashify_what_mobile_content', $form_state['values']['splashify_what_mobile_content']);
    }
  }
}

/**
 * Implements form submit handler.
 */
function splashify_admin_what_form_submit($form, &$form_state) {
  // Clear out the title value if we aren't using the template option.
  if ($form_state['values']['splashify_what_desktop_mode'] != 'template') {
    variable_set('splashify_what_desktop_pagetitle', '');
  }
}

/**
 * Ajax callback for the desktop mode dropdown.
 *
 * The $form array element that is returned is the updated field that should
 * be displayed.
 */
function splashify_ajax_what_desktop_mode_callback($form, &$form_state) {
  variable_set('splashify_what_desktop_mode', $form_state['values']['splashify_what_desktop_mode']);
  return $form['desktop']['mode_value'];
}

/**
 * Ajax callback for the mobile mode dropdown.
 *
 * The $form array element that is returned is the updated field that should
 * be displayed.
 */
function splashify_ajax_what_mobile_mode_callback($form, &$form_state) {
  return $form['mobile']['mode_value'];
}

/**
 * Validate the what:paths field.
 *
 * We put this in a function so it can handle both the desktop and mobile
 * settings. This assumes that each path in $paths is separated by a new line
 * character. We also assume $paths cannot be blank.
 */
function splashify_what_paths_check($field, $paths) {
  if (empty($paths)) {
    form_set_error($field, t('You must enter at least one path.'));
    return;
  }

  // Make sure each path is valid.
  $what_paths = preg_split('/[\n\r]+/', $paths);
  $errors = array();
  foreach ($what_paths as $path) {
    // If this is a valid Drupal path.
    if (drupal_valid_path($path)) {
      continue;
    }

    // If this path is an alias, we know this is a valid path.
    if (drupal_lookup_path('source', $path)) {
      continue;
    }

    // If this path is a source url, we know this is a valid path.
    if (drupal_lookup_path('alias', $path)) {
      continue;
    }

    // Now check if this is a url value.
    if (substr($path, 0, 7) == 'http://') {
      continue;
    }

    // This path is not an alias or the source url.
    $errors[] .= t('The path "@path" is not valid.', array('@path' => $path));;
  }

  // Since there could be multiple errors for this one field, we want to
  // break each error into a separate line.
  if (count($errors) > 0) {
    form_set_error($field, implode('<br />', $errors));
  }
}

/**
 * Validate The Node Entries
 *
 * Place into a function so it can be handled by both the specific and global options
 * $page vairable used to distinguish errors when using specific splash options
 * Returns the number of errors generated.
 */
function splashify_validate_node($node, $page = NULL){
  $errors = 0;
  //Format page properly if exists
  if($page != NULL){
    $page = "'".$page."' - ";
  }
  //Check Not Empty
  if($node == NULL){
    form_set_error($page." node empty", $page."No Node Selected");
    $errors++;
  }
  //Validate Node if ID passed
  else if(is_numeric($node)){
    $ln = node_load($node);
    if($ln == false){
      form_set_error($page." node doesnt exist error", $page."Node ID: '".$node."' does not exist");
      $errors++;
    }
  }
  //Validate Node Alias
  else{
    $path = drupal_lookup_path("source", $node);
    $ln = menu_get_object("node", 1, $path);
    if($ln == false){
      form_set_error($page." node alias not found", $page."Node Alias: '".$node."' not found");
      $errors++;
    }
  }
  return $errors;
}

/**
 * Validate The EntityForm Entries
 *
 * Place into a function so it can be handled by both the specific and global options
 * $page vairable used to distinguish errors when using specific splash options
 * Returns the number of errors generated.
 */
function splashify_validate_entityform($entityform, $page = NULL){
  $errors = 0;
  if($page != NULL){
    $page = "'".$page."' - ";
  }
  //Check Not Empty
  if($entityform == NULL){
    form_set_error($page." form empty", $page."No Form Selected");
    $errors++;
  }
  //Validate entityform exists
  else{
    module_load_include('inc', 'entityform', 'entityform.admin');
    $entity_form = entityform_empty_load($entityform);
    if(!is_object($entity_form)){
      form_set_error($page. " entityform doesnt exist error", $page."EntityForm Type: '".$entityform."' does not exist");
      $errors++;
    }
  }
  return $errors;
}

/**
 * Validate The View Entries
 *
 * Place into a function so it can be handled by both the specific and global options
 * $page vairable used to distinguish errors when using specific splash options
 * Returns the number of errors generated.
 */
function splashify_validate_view($view_name, $view_display, $page = NULL){
  $errors = 0;
  if($page != NULL){
    $page = "'".$page."' - ";
  }
  //Check Required Fields Are Present
  if($view_name == NULL || $view_display == NULL){
    form_set_error($page." view details empty error", $page."View Name & Display Fields Are Required.");
    $errors++;
  }
  else{
    //Load view with form variabled
    $v = views_get_view($view_name);
    //Check view exists
    if(is_object($v)){
      //Check display type for view exists
      if(!isset($v->display[$view_display]) || !is_object($v->display[$view_display])){
        form_set_error($page." view display not found", $page."View Display: '".$view_display."' Not Found For '".$view_name."'.");
        $errors++;
      }
    }
    else{
      form_set_error($page." view not found", $page."View Name: '".$view_name."' Not Found.");
      $errors++;
    }
  }
  return $errors;
}

function splashify_validate_dimensions($dimensions, $page = NULL){
  $errors = 0;
  $dims = '';
  if($page != NULL){
    $page = "'".$page."' - ";
  }
  //Check String In Required Format
  if(strpos($dimensions, 'x') === FALSE){
    form_set_error($page." dimension format error", "'".$page."' - Dimensions: Incorrect Format. Example: 600x400");
    $errors++;
  }
  else{
    //String Correct Format, Validate Width Height Elements
    $dimensions_array = explode("x", $dimensions);
    //Check only contains Width Height
    if(sizeof($dimensions_array) != 2){
      form_set_error($page." dimension size error", "'".$page."' - Dimensions: Incorrect Format. Example: 600x400");
      $errors++;
    }
    else{
      //Contains Only Width Height, Validate Each
      if(!is_numeric($dimensions_array[0]) || !is_numeric($dimensions_array[1])){
        form_set_error($page." dimension not number error", "'".$page."' - Dimensions: Incorrect Format. Example: 600x400");
        $errors++;
      }
    }
  }
  //Check dimensions array properties exists and set dims variable.
  if(isset($dimensions_array[0]) && isset($dimensions_array[1])){
    $dims = array(
      'width' => $dimensions_array[0],
      'height' => $dimensions_array[1],
    );
  }
  return array($errors, $dims);
}
