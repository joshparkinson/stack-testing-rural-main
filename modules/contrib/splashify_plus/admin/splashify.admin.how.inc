<?php

/**
 * @file
 * The admin "How" tab.
 *
 * Admin functionality that determines how the splash page shows up to the
 * user.
 */

/**
 * "How" settings tab.
 */
function splashify_admin_how_form($form, &$form_state) {
  $form = array();

  $form['description'] = array(
    '#markup' => '<p>' . t('How should the splash page come up?') . '</p>',
  );

  // Specify the display mode options for the splash page.
  $splashify_how_mode_options = array(
    'redirect' => t('Redirect'),
    'window' => t('Open in new window'),
  );

  if (module_exists('colorbox')) {
    $splashify_how_mode_options['lightbox'] = t('Open in a Lightbox (colorbox)');
  }
  else {
    $colorbox_warning = t('In order to access the lightbox option, you need to have the <a href="@colorbox" target="_blank">Colorbox</a> module installed.', array(
      '@colorbox' => 'http://drupal.org/project/colorbox',
    ));

    drupal_set_message($colorbox_warning, 'warning');
  }

  $form['desktop'] = array(
    '#type' => 'fieldset',
    '#title' => t('Desktop Settings'),
  );

  // Determines how the splash page shows up.
  $form['desktop']['splashify_how_desktop_mode'] = array(
    '#type' => 'select',
    '#title' => t('Splash Display Mode'),
    '#options' => $splashify_how_mode_options,
    '#default_value' => variable_get('splashify_how_desktop_mode', 'redirect'),
    '#description' => t('Determines how the splash page should show up. If you want to use the lightbox option, you need to have the Colorbox module installed.'),
    '#ajax' => array(
      'callback' => 'splashify_ajax_how_desktop_mode_callback',
      'wrapper' => 'how-mode-desktop-size',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );

  // Set a variable that is either defined by the selection from the ajax
  // dropdown menu, or a previously saved value.
  if (isset($form_state['values']['splashify_how_desktop_mode'])) {
    $how_desktop_mode_set = $form_state['values']['splashify_how_desktop_mode'];
  }
  else {
    $how_desktop_mode_set = variable_get('splashify_how_desktop_mode', '');
  }

  $form['desktop']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-desktop-size">');

  // If they specified the redirect option, we want to hide the window size
  // text field.
  if ($how_desktop_mode_set != 'redirect') {
    $form['desktop']['mode_value']['splashify_how_desktop_size'] = array(
      '#type' => 'textfield',
      '#title' => t('Window/Box size'),
      '#default_value' => variable_get('splashify_how_desktop_size', ''),
      '#description' => t('Size (<code>WIDTHxHEIGHT</code>, e.g. 400x300) of the Window or Lightbox.'),
    );
  }

  $form['desktop']['mode_value']['end'] = array('#markup' => '</div>');

  $form['cbox_class'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'),
      ),
    ),
  );
  $form['cbox_class']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-splashify-class">');
  $form['cbox_class']['mode_value']['splashify_how_box_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Window class'),
    '#default_value' => variable_get('splashify_how_box_class', ''),
    '#description' => t('Use this if you plan on styling the lightbox with css'),
  );
  $form['cbox_class']['mode_value']['end'] = array('#markup' => '</div>');

  $form['delay'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'),
      ),
    ),
  );
  $form['delay']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-splashify-delay">');
  $form['delay']['mode_value']['splashify_how_long_delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Splashify Delay'),
      '#default_value' => variable_get('splashify_how_long_delay', ''),
      '#description' => t('Enter how long in milliseconds to wait before Splashify does it\'s thing.'),
  );
  $form['delay']['mode_value']['end'] = array('#markup' => '</div>');
  $form['autoclose'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'),
      ),
    ),
  );
  $form['autoclose']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-splashify-autoclose">');
  $form['autoclose']['mode_value']['splashify_how_long_before_autoclose'] = array(
      '#type' => 'textfield',
      '#title' => t('Splashify Autoclose'),
      '#default_value' => variable_get('splashify_how_long_before_autoclose', ''),
      '#description' => t('Enter how long in milliseconds to wait before Splashify autocloses.<br />Not Required, leave blank to never autoclose'),
  );
  $form['autoclose']['mode_value']['end'] = array('#markup' => '</div>');
  $form['reposition']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-splashify-reposition">');
  $form['reposition']['mode_value']['splashify_how_reposition'] = array(
    '#type' => 'checkbox',
    '#title' => 'Disable Repositioning',
    '#description' => 'Setting this will turn off colorbox\'s automatic repositioning, only recommended for use when styling colorbox height etc with css',
    '#default_value' => variable_get('splashify_how_reposition', 0),
    '#state' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'), 
      ),
    ),
  );
  $form['reposition']['mode_value']['end'] = array('#markup' => '</div>');
  $form['inactive'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'),
      ),
    ),
  );
  $form['inactive']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-spashify-inactive">');
  $form['inactive']['mode_value']['splashify_how_on_inactive'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show on Inactivity',  
    '#description' => t('Enabling will mean splashify runs when the user is not active on the current page. Left unchecked splashify will run regardless. Only enable if using the lightbox display mode.'),
    '#default_value' => variable_get('splashify_how_on_inactive', ''),
  );
  $form['inactive']['mode_value']['end'] = array('#markup' => '</div>');
  $form['show_once'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'),
      ),
    ),
  );
  $form['show_once']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-spashify-show-once">');
  $form['show_once']['mode_value']['splashify_how_show_once'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show only Once',  
    '#description' => t('Enabling will mean the splash screen will show only once per page load no matter how many times the inactivity trigger is activated.'),
    '#default_value' => variable_get('splashify_how_show_once', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_on_inactive"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['show_once']['mode_value']['end'] = array('#markup' => '</div>');
  $form['never_show'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_desktop_mode"]' => array('value' => 'lightbox'),
      ),
    ),
  );
  $form['never_show']['mode_value']['begin'] = array('#markup' => '<div id="how-mode-splashify-never-show">');
  $form['never_show']['mode_value']['splashify_how_never_show'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable Never Show Option',
    '#description' => t('Allows users to set cookie stopping the display of the splash page'),
    '#default_value' => variable_get('splashify_how_never_show', ''),
  );
  $form['never_show']['mode_value']['splashify_how_ns_option'] = array(
    '#type' => 'radios',
    '#options' => array(
      'global' => t('Global Cookie'),
      'specific' => t('Specific Splash Cookie'),
    ),
    '#description' => t('Defines Never Show Functionality.<br />Global: If enabled no spashes will be shown for that user across the site.<br />Specific: If enabled only the splash where the cookie was enabled will not be shown. A user will have to specify not to show for all splashes.<br />Using where page wildcards and specific cookies will lead to a user having to set the cookie for the same
      splash page multiple times.<br />Use this setting wisely.'),
    '#default_value' => variable_get('splashify_how_ns_option', 'global'),
    '#states' => array(
      'visible' => array(
        ':input[name="splashify_how_never_show"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['never_show']['mode_value']['end'] = array('#markup' => '</div>');

  //Begin Mobile Settings
  $form['mobile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile Settings'),
  );

  // If the mobile splash is enabled, display the mobile options.
  if (variable_get('splashify_when_mobile', 0) == 1) {
    // Determines how the splash page shows up.
    $form['mobile']['splashify_how_mobile_mode'] = array(
      '#type' => 'select',
      '#title' => t('Splash Display Mode'),
      '#options' => $splashify_how_mode_options,
      '#default_value' => variable_get('splashify_how_mobile_mode', 'redirect'),
      '#description' => t('How should we load the splash page? Note: Redirect is currently the only option.'),

      // For the time being, the only option we are allowing for mobile
      // devices is "redirect". We display this so that it is obvious the
      // method that is used.
      '#disabled' => TRUE,
    );
  }
  else {
    $form['mobile']['splashify_how_mobile_mode'] = array(
      '#markup' => '<p>' . t('In order to specify mobile options, you need to enable the "When: Enable Unique Mobile Splash" option.') . '</p>',
    );
  }

  return system_settings_form($form);
}

/**
 * Ajax callback for the desktop mode dropdown.
 */
function splashify_ajax_how_desktop_mode_callback($form, &$form_state) {
  return $form['desktop']['mode_value'];
}
