<?php
/**
 * @file
 * rural_unity_av_rating_tool.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function rural_unity_av_rating_tool_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-broker-area-menu_agri-vehicle-quick-quote-:unity/av-rating-tool.
  $menu_links['menu-broker-area-menu_agri-vehicle-quick-quote-:unity/av-rating-tool'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/av-rating-tool',
    'router_path' => 'unity/av-rating-tool',
    'link_title' => 'Agri vehicle quick quote ',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-broker-area-menu_agri-vehicle-quick-quote-:unity/av-rating-tool',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'av-quickquote',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Agri vehicle quick quote ');

  return $menu_links;
}
