<?php
/**
 * @file
 * rural_unity_av_rating_tool.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function rural_unity_av_rating_tool_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'broker_area_av_rating_tool';
  $page->task = 'page';
  $page->admin_title = 'Broker Area: AV Rating Tool';
  $page->admin_description = '';
  $page->path = 'unity/av-rating-tool';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_broker_area_av_rating_tool__panel_context_d9aadf5e-f544-48e1-9000-03278f323d70';
  $handler->task = 'page';
  $handler->subtask = 'broker_area_av_rating_tool';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'broker-area full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Agricultural vehicle quick quote';
  $display->uuid = '4feae87b-95a2-40d3-a276-1e80c191cd96';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_broker_area_av_rating_tool__panel_context_d9aadf5e-f544-48e1-9000-03278f323d70';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '320cb04f-8d51-4f35-b831-9236605da5d7';
  $display->content['new-320cb04f-8d51-4f35-b831-9236605da5d7'] = $pane;
  $display->panels['left'][0] = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane = new stdClass();
  $pane->pid = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2e138423-a36c-43c6-b282-8f62172b8c7c';
  $display->content['new-2e138423-a36c-43c6-b282-8f62172b8c7c'] = $pane;
  $display->panels['right'][0] = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane = new stdClass();
  $pane->pid = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => '<none>',
    'body' => '<h2 class="pane-title">Agricultural vehicle quick quote</h2>',
    'format' => 'php_code',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro covernotes clearfix',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b3287135-76cf-4a30-9b5b-044c602d6018';
  $display->content['new-b3287135-76cf-4a30-9b5b-044c602d6018'] = $pane;
  $display->panels['right'][1] = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane = new stdClass();
  $pane->pid = 'new-949b6ad7-031d-41cc-b4a1-98215b98beae';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'rural_av_rating_tool-rural_avrt_new_quote';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '949b6ad7-031d-41cc-b4a1-98215b98beae';
  $display->content['new-949b6ad7-031d-41cc-b4a1-98215b98beae'] = $pane;
  $display->panels['right'][2] = 'new-949b6ad7-031d-41cc-b4a1-98215b98beae';
  $pane = new stdClass();
  $pane->pid = 'new-b654fbd4-2803-4a1c-8f39-f3481768d4d4';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'rural_av_rating_tool-rural_avrt_saved_quotes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'b654fbd4-2803-4a1c-8f39-f3481768d4d4';
  $display->content['new-b654fbd4-2803-4a1c-8f39-f3481768d4d4'] = $pane;
  $display->panels['right'][3] = 'new-b654fbd4-2803-4a1c-8f39-f3481768d4d4';
  $pane = new stdClass();
  $pane->pid = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $display->content['new-10c08a48-3cf2-4f7b-825b-9ea21547e879'] = $pane;
  $display->panels['top'][0] = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane = new stdClass();
  $pane->pid = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->content['new-a4a361ed-f925-4327-b8dc-dc085f72f7f8'] = $pane;
  $display->panels['top'][1] = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['broker_area_av_rating_tool'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'broker_area_av_rating_tool_quote_form';
  $page->task = 'page';
  $page->admin_title = 'Broker Area: AV Rating Tool - Quote form';
  $page->admin_description = '';
  $page->path = 'unity/av-rating-tool/quote/!quote_id';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'quote_id' => array(
      'id' => '',
      'identifier' => '',
      'argument' => '',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_broker_area_av_rating_tool_quote_form__panel_context_68fc1947-b4be-4d3d-a3f9-1d0644dd6f63';
  $handler->task = 'page';
  $handler->subtask = 'broker_area_av_rating_tool_quote_form';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'broker-area full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Agricultural vehicle quick quote';
  $display->uuid = '4feae87b-95a2-40d3-a276-1e80c191cd96';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_broker_area_av_rating_tool_quote_form__panel_context_68fc1947-b4be-4d3d-a3f9-1d0644dd6f63';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '320cb04f-8d51-4f35-b831-9236605da5d7';
  $display->content['new-320cb04f-8d51-4f35-b831-9236605da5d7'] = $pane;
  $display->panels['left'][0] = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane = new stdClass();
  $pane->pid = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2e138423-a36c-43c6-b282-8f62172b8c7c';
  $display->content['new-2e138423-a36c-43c6-b282-8f62172b8c7c'] = $pane;
  $display->panels['right'][0] = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane = new stdClass();
  $pane->pid = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => '<none>',
    'body' => '<h2 class="pane-title">Agricultural vehicle quick quote</h2>',
    'format' => 'php_code',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro covernotes clearfix',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b3287135-76cf-4a30-9b5b-044c602d6018';
  $display->content['new-b3287135-76cf-4a30-9b5b-044c602d6018'] = $pane;
  $display->panels['right'][1] = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane = new stdClass();
  $pane->pid = 'new-a66d2ab9-cae7-48cb-a00b-e946470a39a7';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'rural_av_rating_tool-rural_avrt_quote_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a66d2ab9-cae7-48cb-a00b-e946470a39a7';
  $display->content['new-a66d2ab9-cae7-48cb-a00b-e946470a39a7'] = $pane;
  $display->panels['right'][2] = 'new-a66d2ab9-cae7-48cb-a00b-e946470a39a7';
  $pane = new stdClass();
  $pane->pid = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $display->content['new-10c08a48-3cf2-4f7b-825b-9ea21547e879'] = $pane;
  $display->panels['top'][0] = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane = new stdClass();
  $pane->pid = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->content['new-a4a361ed-f925-4327-b8dc-dc085f72f7f8'] = $pane;
  $display->panels['top'][1] = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['broker_area_av_rating_tool_quote_form'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'broker_area_av_rating_tool_quote_success_page';
  $page->task = 'page';
  $page->admin_title = 'Broker Area: AV Rating Tool - Quote Success Page';
  $page->admin_description = '';
  $page->path = 'unity/av-rating-tool/quote/success';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_broker_area_av_rating_tool_quote_success_page__panel_context_287b3534-337d-4282-aa60-f560fd8e994b';
  $handler->task = 'page';
  $handler->subtask = 'broker_area_av_rating_tool_quote_success_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'broker-area full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Agricultural vehicle quick quote';
  $display->uuid = '4feae87b-95a2-40d3-a276-1e80c191cd96';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_broker_area_av_rating_tool__panel_context_d9aadf5e-f544-48e1-9000-03278f323d70';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '320cb04f-8d51-4f35-b831-9236605da5d7';
  $display->content['new-320cb04f-8d51-4f35-b831-9236605da5d7'] = $pane;
  $display->panels['left'][0] = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane = new stdClass();
  $pane->pid = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2e138423-a36c-43c6-b282-8f62172b8c7c';
  $display->content['new-2e138423-a36c-43c6-b282-8f62172b8c7c'] = $pane;
  $display->panels['right'][0] = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane = new stdClass();
  $pane->pid = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => '<none>',
    'body' => '<h2 class="pane-title">Agricultural vehicle quick quote</h2>',
    'format' => 'php_code',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro covernotes clearfix',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b3287135-76cf-4a30-9b5b-044c602d6018';
  $display->content['new-b3287135-76cf-4a30-9b5b-044c602d6018'] = $pane;
  $display->panels['right'][1] = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane = new stdClass();
  $pane->pid = 'new-50772f37-e313-444f-b036-e79be6dac96c';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'rural_av_rating_tool-rural_avrt_quote_success';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '50772f37-e313-444f-b036-e79be6dac96c';
  $display->content['new-50772f37-e313-444f-b036-e79be6dac96c'] = $pane;
  $display->panels['right'][2] = 'new-50772f37-e313-444f-b036-e79be6dac96c';
  $pane = new stdClass();
  $pane->pid = 'new-df25b2ff-2da2-47d2-95f1-37c3d35710e8';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'What next?',
    'title' => 'What next?',
    'body' => '<div class="what-next-block-wrapper clearfix">
    <div class="what-next-block covernotes-login">
        <p>Place your customer on cover by logging onto our <span>Covernotes</span> system to Generate a cover note</p>
        <a href="https://covernotes.ruralinsurance.co.uk/Console/Login.action">Log in</a>
    </div>
    <div class="what-next-block covernotes-account"> 
        <p>If you do not have a <span>Covernotes</span> account, you can request one here</p>
        <a href="/unity/covernotes">Set up account</a>
    </div>
</div>
<div class="meantime">
    <p>In the meantime, our underwriters will process your submission and send your customers policy documentation directly to you.</p>
</div>
<div class="return">
    <a href="/unity/leave-us-your-feedback" class="leave-feedback">Leave feedback</a>
    <a href="/unity/av-rating-tool" class="back-to-main">View submissions</a>
</div>',
    'format' => 'php_code',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding pane-rural-av-rating-tool-rural-avrt-success-whats-next',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'df25b2ff-2da2-47d2-95f1-37c3d35710e8';
  $display->content['new-df25b2ff-2da2-47d2-95f1-37c3d35710e8'] = $pane;
  $display->panels['right'][3] = 'new-df25b2ff-2da2-47d2-95f1-37c3d35710e8';
  $pane = new stdClass();
  $pane->pid = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $display->content['new-10c08a48-3cf2-4f7b-825b-9ea21547e879'] = $pane;
  $display->panels['top'][0] = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane = new stdClass();
  $pane->pid = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->content['new-a4a361ed-f925-4327-b8dc-dc085f72f7f8'] = $pane;
  $display->panels['top'][1] = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['broker_area_av_rating_tool_quote_success_page'] = $page;

  return $pages;

}
