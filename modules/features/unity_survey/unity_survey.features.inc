<?php
/**
 * @file
 * unity_survey.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function unity_survey_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function unity_survey_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function unity_survey_default_entityform_type() {
  $items = array();
  $items['unity_survey_another_survey'] = entity_import('entityform_type', '{
    "type" : "unity_survey_another_survey",
    "label" : "Unity Survey: How can we support your marketing activities?",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "ckeditor" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "ckeditor" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "2" : "2", "1" : 0, "3" : 0, "4" : 0, "5" : 0, "6" : 0, "7" : 0 },
      "resubmit_action" : "confirm",
      "redirect_path" : "unity\\/your-voice\\/thank-you",
      "instruction_pre" : { "value" : "", "format" : "ckeditor" }
    },
    "weight" : "0",
    "paths" : []
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function unity_survey_node_info() {
  $items = array(
    'unity_survey' => array(
      'name' => t('Unity: Survey'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
