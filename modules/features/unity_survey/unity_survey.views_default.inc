<?php
/**
 * @file
 * unity_survey.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function unity_survey_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'unity_survey';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Unity: Survey';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'clearfix';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Survey form reference */
  $handler->display->display_options['relationships']['field_entityform_reference_target_id']['id'] = 'field_entityform_reference_target_id';
  $handler->display->display_options['relationships']['field_entityform_reference_target_id']['table'] = 'field_data_field_entityform_reference';
  $handler->display->display_options['relationships']['field_entityform_reference_target_id']['field'] = 'field_entityform_reference_target_id';
  $handler->display->display_options['relationships']['field_entityform_reference_target_id']['ui_name'] = 'Survey form reference';
  $handler->display->display_options['relationships']['field_entityform_reference_target_id']['label'] = 'Survey form reference';
  $handler->display->display_options['relationships']['field_entityform_reference_target_id']['required'] = TRUE;
  /* Relationship: Survey form submission */
  $handler->display->display_options['relationships']['entityform']['id'] = 'entityform';
  $handler->display->display_options['relationships']['entityform']['table'] = 'entityform_type';
  $handler->display->display_options['relationships']['entityform']['field'] = 'entityform';
  $handler->display->display_options['relationships']['entityform']['relationship'] = 'field_entityform_reference_target_id';
  $handler->display->display_options['relationships']['entityform']['ui_name'] = 'Survey form submission';
  $handler->display->display_options['relationships']['entityform']['label'] = 'Survey form submission';
  /* Field: Data token (Path) */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['ui_name'] = 'Data token (Path)';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Data token (Title) */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['ui_name'] = 'Data token (Title)';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Data token (Body) */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['ui_name'] = 'Data token (Body)';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Data token (Submission details / form link) */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'entityform';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['relationship'] = 'entityform';
  $handler->display->display_options['fields']['created']['ui_name'] = 'Data token (Submission details / form link)';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['created']['alter']['text'] = '<p class="completed-date">You completed this survery on: [created].</p>
<p class="thanks">Thanks for participating!</p>';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['empty'] = '<a href="[path] ">Complete this survey now</a>';
  $handler->display->display_options['fields']['created']['date_format'] = 'd_m_y';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'd_m_y';
  /* Field: View output */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['ui_name'] = 'View output';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<h3>[title]</h3>
<!--<div class="intro">[body]</div>-->
<div class="submission">[created]</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'unity_survey' => 'unity_survey',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'unity_survey_list_pane');
  $export['unity_survey'] = $view;

  return $export;
}
