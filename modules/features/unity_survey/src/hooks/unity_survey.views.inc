<?php

/**
 * Alter the coditions of the survery submission join statement.
 *
 * The following hook is used to alter the views query for the
 * 'unity_suvey_list_pane' view, the view used to display a list of views of
 * available surveys within Unity. The query alter is used to add aditional
 * conditions to the join statmenet used to attach survery submissions to the
 * survey. Only the submission from the currently logged in user should be
 * joined to the survey record but due to limitiations within the Views
 * administration UI this has to be done programatically.
 *
 * To add the extra paramaters to the join statement we must first find it
 * knowing the statement is used to conntect the 'entityform' table to the
 * result set, so search through the tables attahced to this view to find it.
 * Once found add the extra join condition which limits joined submission to
 * only the currently logged in user (via their user id).
 *
 * @param $view
 * @param $query
 *
 * @return bool Did alter reach completion
 */
function unity_survey_views_query_alter(&$view, &$query){
    if($view->name != "unity_survey" || $view->current_display != "unity_survey_list_pane"){
        return FALSE;
    }

    global $user;

    foreach($query->table_queue as $tableAlias => &$tableDetails){
        if($tableDetails['table'] != 'entityform'){
            continue;
        }

        $tableDetails['join']->extra[] = array(
            'table' => $tableAlias,
            'field' => 'uid',
            'value' => $user->uid,
            'operator' => '='
        );

    }

    return TRUE;
}