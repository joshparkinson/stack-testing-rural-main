<?php

/**
 * Alter the 'How can we support your marketing activities?' survey entityform.
 *
 * Add in a custom validation handler for this form that can be used to merge
 * multiple form errors into a single error message.
 *
 * Note: the id as defined in this hook is
 * unity_survey_another_survey_entityform_edit
 * due to intial configuration errors.
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function unity_survey_form_unity_survey_another_survey_entityform_edit_form_alter(&$form, &$form_state, $form_id){
    $form['#validate'][] = 'unity_survey_another_survey_entityform_edit_form_custom_validate';
}

/**
 * Custom 'How can we support your marketing activities?' validation handler.
 *
 * As only missing required field errors are thrown if any of these errors are
 * caught using 'form_get_errors' remove them and replace with a generic
 * 'oops' erorr message.
 *
 * Note: this catch will catch multiple errors and minimise to the 1 oops error.
 *
 * @param $form
 * @param $form_state
 */
function unity_survey_another_survey_entityform_edit_form_custom_validate($form, &$form_state){
    if(form_get_errors()){
        drupal_get_messages();
        form_set_error('qmissing', 'Oops! It looks like you missed a question. Please check back through your responses and try again.');
    }
}