<?php
/**
 * @file
 * unity_survey.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function unity_survey_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'unity_survey';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:unity_survey:default';
  $panelizer->css_id = '';
  $panelizer->css_class = 'full-width broker-area';
  $panelizer->css = '';
  $panelizer->no_blocks = TRUE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'b7c185be-c323-4162-8b35-f4e814fdb5b6';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:unity_survey:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f8cea55b-717b-4b75-a861-68031f3b9c0b';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f8cea55b-717b-4b75-a861-68031f3b9c0b';
  $display->content['new-f8cea55b-717b-4b75-a861-68031f3b9c0b'] = $pane;
  $display->panels['left'][0] = 'new-f8cea55b-717b-4b75-a861-68031f3b9c0b';
  $pane = new stdClass();
  $pane->pid = 'new-3bb7abee-32c4-4a17-9b7a-ab7cf07aed42';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3bb7abee-32c4-4a17-9b7a-ab7cf07aed42';
  $display->content['new-3bb7abee-32c4-4a17-9b7a-ab7cf07aed42'] = $pane;
  $display->panels['right'][0] = 'new-3bb7abee-32c4-4a17-9b7a-ab7cf07aed42';
  $pane = new stdClass();
  $pane->pid = 'new-f62d4027-8839-4829-ab52-7d89a990e889';
  $pane->panel = 'right';
  $pane->type = 'page_tabs';
  $pane->subtype = 'page_tabs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'both',
    'id' => 'node-tabs',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f62d4027-8839-4829-ab52-7d89a990e889';
  $display->content['new-f62d4027-8839-4829-ab52-7d89a990e889'] = $pane;
  $display->panels['right'][1] = 'new-f62d4027-8839-4829-ab52-7d89a990e889';
  $pane = new stdClass();
  $pane->pid = 'new-c73f84c4-3091-4664-b6e0-1239e5ad9e85';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => '<none>',
    'body' => '<h2 class="pane-title">%node:title</h2>',
    'format' => 'php_code',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro broker-survey',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'c73f84c4-3091-4664-b6e0-1239e5ad9e85';
  $display->content['new-c73f84c4-3091-4664-b6e0-1239e5ad9e85'] = $pane;
  $display->panels['right'][2] = 'new-c73f84c4-3091-4664-b6e0-1239e5ad9e85';
  $pane = new stdClass();
  $pane->pid = 'new-aa613983-02a0-40ea-9b00-32f1216bc350';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area unity-survey introduction',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'aa613983-02a0-40ea-9b00-32f1216bc350';
  $display->content['new-aa613983-02a0-40ea-9b00-32f1216bc350'] = $pane;
  $display->panels['right'][3] = 'new-aa613983-02a0-40ea-9b00-32f1216bc350';
  $pane = new stdClass();
  $pane->pid = 'new-fcd731ad-634a-4bb7-ab28-f054b5567567';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_entityform_reference';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'entityreference_entity_view',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'link' => FALSE,
      'view_mode' => 'default',
      'links' => 1,
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => 'Pist',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area unity-survey survey',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'fcd731ad-634a-4bb7-ab28-f054b5567567';
  $display->content['new-fcd731ad-634a-4bb7-ab28-f054b5567567'] = $pane;
  $display->panels['right'][4] = 'new-fcd731ad-634a-4bb7-ab28-f054b5567567';
  $pane = new stdClass();
  $pane->pid = 'new-371b3993-8a63-4474-bc10-e2799fc8fdb9';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '371b3993-8a63-4474-bc10-e2799fc8fdb9';
  $display->content['new-371b3993-8a63-4474-bc10-e2799fc8fdb9'] = $pane;
  $display->panels['top'][0] = 'new-371b3993-8a63-4474-bc10-e2799fc8fdb9';
  $pane = new stdClass();
  $pane->pid = 'new-9b11e6b2-b23d-4f3c-8410-eb270ffeaf77';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9b11e6b2-b23d-4f3c-8410-eb270ffeaf77';
  $display->content['new-9b11e6b2-b23d-4f3c-8410-eb270ffeaf77'] = $pane;
  $display->panels['top'][1] = 'new-9b11e6b2-b23d-4f3c-8410-eb270ffeaf77';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-aa613983-02a0-40ea-9b00-32f1216bc350';
  $panelizer->display = $display;
  $export['node:unity_survey:default'] = $panelizer;

  return $export;
}
