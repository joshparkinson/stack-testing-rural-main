<?php
/**
 * @file
 * unity_survey.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function unity_survey_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_about_rural_leaflet_includ'.
  $field_instances['entityform-unity_survey_another_survey-field_about_rural_leaflet_includ'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_about_rural_leaflet_includ',
    'label' => 'About Rural leaflet, including information on our claims & products',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_about_rural_powerpoint_pre'.
  $field_instances['entityform-unity_survey_another_survey-field_about_rural_powerpoint_pre'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_about_rural_powerpoint_pre',
    'label' => 'About Rural powerpoint presentation',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_an_introduction_to_agricul'.
  $field_instances['entityform-unity_survey_another_survey-field_an_introduction_to_agricul'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_an_introduction_to_agricul',
    'label' => 'An introduction to agricultural insurance powerpoint presentation',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_benefits_of_business_inter'.
  $field_instances['entityform-unity_survey_another_survey-field_benefits_of_business_inter'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 12,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_benefits_of_business_inter',
    'label' => 'Benefits of Business Interruption (BI) cover leaflet',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 13,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_benefits_of_environmental_'.
  $field_instances['entityform-unity_survey_another_survey-field_benefits_of_environmental_'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 11,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_benefits_of_environmental_',
    'label' => 'Benefits of Environmental Impairment Liability (EIL) cover leaflet',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 12,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_caption'.
  $field_instances['entityform-unity_survey_another_survey-field_caption'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 26,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_caption',
    'label' => 'What\'s your biggest challenge when marketing to your clients and how might we help?',
    'placeholder' => '',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 0,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 29,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_case_studies_of_customers_'.
  $field_instances['entityform-unity_survey_another_survey-field_case_studies_of_customers_'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 13,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_case_studies_of_customers_',
    'label' => 'Case studies of customers who have needed to make a claim ',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 14,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_email_letter_templates_int'.
  $field_instances['entityform-unity_survey_another_survey-field_email_letter_templates_int'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 16,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_email_letter_templates_int',
    'label' => 'Email & letter templates introducing Rural insurance to prospective clients',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 17,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_email_letter_templates_to_'.
  $field_instances['entityform-unity_survey_another_survey-field_email_letter_templates_to_'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 17,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_email_letter_templates_to_',
    'label' => 'Email & letter templates to promote the multi-policy discount & help with cross selling ',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 18,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_farm_combined_leaflet'.
  $field_instances['entityform-unity_survey_another_survey-field_farm_combined_leaflet'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_farm_combined_leaflet',
    'label' => 'Farm combined leaflet',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_farm_motor_leaflet'.
  $field_instances['entityform-unity_survey_another_survey-field_farm_motor_leaflet'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_farm_motor_leaflet',
    'label' => 'Farm motor leaflet',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 10,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_how_to_guide_on_social_med'.
  $field_instances['entityform-unity_survey_another_survey-field_how_to_guide_on_social_med'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 20,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_how_to_guide_on_social_med',
    'label' => 'How to guide on social media',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 21,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_how_to_guides_for_optimisi'.
  $field_instances['entityform-unity_survey_another_survey-field_how_to_guides_for_optimisi'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 19,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_how_to_guides_for_optimisi',
    'label' => 'How to guides for optimising your website for Google.',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 20,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_how_to_guides_on_content_m'.
  $field_instances['entityform-unity_survey_another_survey-field_how_to_guides_on_content_m'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 21,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_how_to_guides_on_content_m',
    'label' => 'How to guides on content marketing',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 22,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_introduction_new_starters_'.
  $field_instances['entityform-unity_survey_another_survey-field_introduction_new_starters_'] = array(
    'bundle' => 'unity_survey_another_survey',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_introduction_new_starters_',
    'label' => 'Introduction new starters to Rural',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_markup'.
  $field_instances['entityform-unity_survey_another_survey-field_markup'] = array(
    'bundle' => 'unity_survey_another_survey',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_markup',
    'label' => 'Intro',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_message'.
  $field_instances['entityform-unity_survey_another_survey-field_message'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 25,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_message',
    'label' => 'Have we missed anything? Let us know if you have any other suggestions.',
    'placeholder' => '',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 0,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 27,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_planning_delivering_market'.
  $field_instances['entityform-unity_survey_another_survey-field_planning_delivering_market'] = array(
    'bundle' => 'unity_survey_another_survey',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_planning_delivering_market',
    'label' => 'Planning & delivering marketing activity',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 15,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_posters_to_display_in_your'.
  $field_instances['entityform-unity_survey_another_survey-field_posters_to_display_in_your'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 15,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_posters_to_display_in_your',
    'label' => 'Posters to display in your offices, instore or at auction marts',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 16,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_product_training_powerpoin'.
  $field_instances['entityform-unity_survey_another_survey-field_product_training_powerpoin'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_product_training_powerpoin',
    'label' => 'Product training powerpoint presentations',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_promotional_tweets_and_fac'.
  $field_instances['entityform-unity_survey_another_survey-field_promotional_tweets_and_fac'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 23,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_promotional_tweets_and_fac',
    'label' => 'Promotional tweets and facebook posts about our products for you to use.',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 24,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_quiz_to_test_your_rural_kn'.
  $field_instances['entityform-unity_survey_another_survey-field_quiz_to_test_your_rural_kn'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_quiz_to_test_your_rural_kn',
    'label' => 'Quiz to test your rural knowledge',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_recommed_a_friend_leaflet_'.
  $field_instances['entityform-unity_survey_another_survey-field_recommed_a_friend_leaflet_'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 18,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_recommed_a_friend_leaflet_',
    'label' => 'Recommed a friend leaflet templates should you wish to offer these discounts',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 19,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_risk_prevention_tips_and_c'.
  $field_instances['entityform-unity_survey_another_survey-field_risk_prevention_tips_and_c'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 22,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_risk_prevention_tips_and_c',
    'label' => 'Risk prevention tips and checklists that you can use on your website or client newsletters',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 23,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_rural_business_motor_leafl'.
  $field_instances['entityform-unity_survey_another_survey-field_rural_business_motor_leafl'] = array(
    'bundle' => 'unity_survey_another_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_rural_business_motor_leafl',
    'label' => 'Rural Business Motor leaflet',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 11,
    ),
  );

  // Exported field_instance:
  // 'entityform-unity_survey_another_survey-field_talking_to_clients_about_r'.
  $field_instances['entityform-unity_survey_another_survey-field_talking_to_clients_about_r'] = array(
    'bundle' => 'unity_survey_another_survey',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_talking_to_clients_about_r',
    'label' => 'Talking to clients about Rural',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-unity_survey-body'.
  $field_instances['node-unity_survey-body'] = array(
    'bundle' => 'unity_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'placeholder' => '',
    'required' => 1,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 0,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-unity_survey-field_entityform_reference'.
  $field_instances['node-unity_survey-field_entityform_reference'] = array(
    'bundle' => 'unity_survey',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_entityform_reference',
    'label' => 'Entityform reference',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About Rural leaflet, including information on our claims & products');
  t('About Rural powerpoint presentation');
  t('An introduction to agricultural insurance powerpoint presentation');
  t('Benefits of Business Interruption (BI) cover leaflet');
  t('Benefits of Environmental Impairment Liability (EIL) cover leaflet');
  t('Body');
  t('Case studies of customers who have needed to make a claim ');
  t('Email & letter templates introducing Rural insurance to prospective clients');
  t('Email & letter templates to promote the multi-policy discount & help with cross selling ');
  t('Entityform reference');
  t('Farm combined leaflet');
  t('Farm motor leaflet');
  t('Have we missed anything? Let us know if you have any other suggestions.');
  t('How to guide on social media');
  t('How to guides for optimising your website for Google.');
  t('How to guides on content marketing');
  t('Intro');
  t('Introduction new starters to Rural');
  t('Planning & delivering marketing activity');
  t('Posters to display in your offices, instore or at auction marts');
  t('Product training powerpoint presentations');
  t('Promotional tweets and facebook posts about our products for you to use.');
  t('Quiz to test your rural knowledge');
  t('Recommed a friend leaflet templates should you wish to offer these discounts');
  t('Risk prevention tips and checklists that you can use on your website or client newsletters');
  t('Rural Business Motor leaflet');
  t('Talking to clients about Rural');
  t('What\'s your biggest challenge when marketing to your clients and how might we help?');

  return $field_instances;
}
