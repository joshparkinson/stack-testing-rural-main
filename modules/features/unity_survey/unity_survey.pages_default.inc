<?php
/**
 * @file
 * unity_survey.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function unity_survey_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'unity_survey_entry_page';
  $page->task = 'page';
  $page->admin_title = 'Unity: Survey - Entry Page';
  $page->admin_description = '';
  $page->path = 'unity/your-voice';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_unity_survey_entry_page__panel_context_b2d740b6-638d-440b-8019-76b4c27ec7fa';
  $handler->task = 'page';
  $handler->subtask = 'unity_survey_entry_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'broker-area full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Your voice';
  $display->uuid = '4feae87b-95a2-40d3-a276-1e80c191cd96';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_broker_area_covernotes__panel_context_bfc5d184-afb8-4865-b101-b4588ababd07';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '320cb04f-8d51-4f35-b831-9236605da5d7';
  $display->content['new-320cb04f-8d51-4f35-b831-9236605da5d7'] = $pane;
  $display->panels['left'][0] = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane = new stdClass();
  $pane->pid = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2e138423-a36c-43c6-b282-8f62172b8c7c';
  $display->content['new-2e138423-a36c-43c6-b282-8f62172b8c7c'] = $pane;
  $display->panels['right'][0] = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane = new stdClass();
  $pane->pid = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => 'Your voice',
    'body' => '<h2 class="intro">Your views are really important to us and we want to get your feedback in as many ways as possible. Please feel free to complete any of the open surveys below. Or <a href="/unity/leave-us-your-feedback">contact us directly</a> with your thoughts.</h2>
',
    'format' => 'ckeditor',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro unity-survey clearfix',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b3287135-76cf-4a30-9b5b-044c602d6018';
  $display->content['new-b3287135-76cf-4a30-9b5b-044c602d6018'] = $pane;
  $display->panels['right'][1] = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane = new stdClass();
  $pane->pid = 'new-cfd277db-c4a8-4fae-8c92-b0bb10bd2d5b';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'unity_survey-unity_survey_list_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area unity-survey entry-page list-view-pane',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'cfd277db-c4a8-4fae-8c92-b0bb10bd2d5b';
  $display->content['new-cfd277db-c4a8-4fae-8c92-b0bb10bd2d5b'] = $pane;
  $display->panels['right'][2] = 'new-cfd277db-c4a8-4fae-8c92-b0bb10bd2d5b';
  $pane = new stdClass();
  $pane->pid = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $display->content['new-10c08a48-3cf2-4f7b-825b-9ea21547e879'] = $pane;
  $display->panels['top'][0] = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane = new stdClass();
  $pane->pid = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->content['new-a4a361ed-f925-4327-b8dc-dc085f72f7f8'] = $pane;
  $display->panels['top'][1] = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['unity_survey_entry_page'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'unity_survey_thank_you';
  $page->task = 'page';
  $page->admin_title = 'Unity: Survey - Thank you';
  $page->admin_description = '';
  $page->path = 'unity/your-voice/thank-you';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_unity_survey_thank_you__panel_context_d41c35f1-c6ce-493c-95ab-928a72d45c33';
  $handler->task = 'page';
  $handler->subtask = 'unity_survey_thank_you';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'broker-area full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Your voice - Thank you';
  $display->uuid = '4feae87b-95a2-40d3-a276-1e80c191cd96';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_broker_area_covernotes__panel_context_bfc5d184-afb8-4865-b101-b4588ababd07';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '320cb04f-8d51-4f35-b831-9236605da5d7';
  $display->content['new-320cb04f-8d51-4f35-b831-9236605da5d7'] = $pane;
  $display->panels['left'][0] = 'new-320cb04f-8d51-4f35-b831-9236605da5d7';
  $pane = new stdClass();
  $pane->pid = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2e138423-a36c-43c6-b282-8f62172b8c7c';
  $display->content['new-2e138423-a36c-43c6-b282-8f62172b8c7c'] = $pane;
  $display->panels['right'][0] = 'new-2e138423-a36c-43c6-b282-8f62172b8c7c';
  $pane = new stdClass();
  $pane->pid = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => 'Thanks for participating in this survey',
    'body' => '<h3>Your feedback is really valuable to us.</h3>
<p><a href="/unity/latest-from-rural">Click here</a> to find out the latest from Rural Insurance</p>',
    'format' => 'php_code',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro unity-survey clearfix',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b3287135-76cf-4a30-9b5b-044c602d6018';
  $display->content['new-b3287135-76cf-4a30-9b5b-044c602d6018'] = $pane;
  $display->panels['right'][1] = 'new-b3287135-76cf-4a30-9b5b-044c602d6018';
  $pane = new stdClass();
  $pane->pid = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $display->content['new-10c08a48-3cf2-4f7b-825b-9ea21547e879'] = $pane;
  $display->panels['top'][0] = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $pane = new stdClass();
  $pane->pid = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->content['new-a4a361ed-f925-4327-b8dc-dc085f72f7f8'] = $pane;
  $display->panels['top'][1] = 'new-a4a361ed-f925-4327-b8dc-dc085f72f7f8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-10c08a48-3cf2-4f7b-825b-9ea21547e879';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['unity_survey_thank_you'] = $page;

  return $pages;

}
