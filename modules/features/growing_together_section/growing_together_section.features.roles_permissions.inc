<?php
/**
 * @file
 * growing_together_section.features.roles_permissions.inc
 */

/**
 * Implements hook_default_roles_permissions().
 */
function growing_together_section_default_roles_permissions() {
  $roles = array();

  // Exported role: broker
  $roles['broker'] = array(
    'name' => 'broker',
    'weight' => 5,
    'permissions' => array(),
  );

  return $roles;
}
