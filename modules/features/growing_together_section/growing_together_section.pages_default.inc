<?php
/**
 * @file
 * growing_together_section.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function growing_together_section_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'growing_together_login';
  $page->task = 'page';
  $page->admin_title = 'Growing Together: Login';
  $page->admin_description = 'Growing together login page';
  $page->path = 'growingtogether/login';
  $page->access = array(
    'plugins' => array(),
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_growing_together_login__panel';
  $handler->task = 'page';
  $handler->subtask = 'growing_together_login';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'growing-together-login-page',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Growing Together';
  $display->uuid = 'dc0ae280-a027-4b1a-91bd-d9f362a055ee';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-8f397f42-71a8-403b-8047-8e6b85454883';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Growing Together Logo',
      'title' => '<none>',
      'body' => '<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/logo.jpg" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-logo',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8f397f42-71a8-403b-8047-8e6b85454883';
    $display->content['new-8f397f42-71a8-403b-8047-8e6b85454883'] = $pane;
    $display->panels['middle'][0] = 'new-8f397f42-71a8-403b-8047-8e6b85454883';
    $pane = new stdClass();
    $pane->pid = 'new-a0aa614f-9151-4968-8027-98d922e1c03b';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Member login',
      'title' => '<none>',
      'body' => '<div class="login-header"><p>Broker login</p></div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'login-header',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'a0aa614f-9151-4968-8027-98d922e1c03b';
    $display->content['new-a0aa614f-9151-4968-8027-98d922e1c03b'] = $pane;
    $display->panels['middle'][1] = 'new-a0aa614f-9151-4968-8027-98d922e1c03b';
    $pane = new stdClass();
    $pane->pid = 'new-f2ddb87e-b4d1-49ba-90dc-8af3a108e03d';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'user-login';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-login-block',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f2ddb87e-b4d1-49ba-90dc-8af3a108e03d';
    $display->content['new-f2ddb87e-b4d1-49ba-90dc-8af3a108e03d'] = $pane;
    $display->panels['middle'][2] = 'new-f2ddb87e-b4d1-49ba-90dc-8af3a108e03d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-f2ddb87e-b4d1-49ba-90dc-8af3a108e03d';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['growing_together_login'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'growing_together_main';
  $page->task = 'page';
  $page->admin_title = 'Growing Together: Main';
  $page->admin_description = 'The main landing page for growing together collateral';
  $page->path = 'growingtogether';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_growing_together_main__panel';
  $handler->task = 'page';
  $handler->subtask = 'growing_together_main';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'growing-together-main-page',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Growing Together';
  $display->uuid = '9d1a9d0a-a342-43b2-b045-f5e6ffe14c23';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0836af41-5513-4c92-bea9-a640f24b5fa4';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Growing together logo',
      'title' => '<none>',
      'body' => '<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/logo.jpg" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'no-padding growing-together-logo',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0836af41-5513-4c92-bea9-a640f24b5fa4';
    $display->content['new-0836af41-5513-4c92-bea9-a640f24b5fa4'] = $pane;
    $display->panels['center'][0] = 'new-0836af41-5513-4c92-bea9-a640f24b5fa4';
    $pane = new stdClass();
    $pane->pid = 'new-e6b51938-ab37-4db1-8f47-3b793c6f8ae8';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Entry Blocks',
      'title' => '<none>',
      'body' => '<div class="growing-together-entry-block our-journey-so-far first odd">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/icons/our_journey.png" />
    <h2>Our journey so far</h2>
    <p>With Maria Leighton, Finance Director</p>
    <a href="/growingtogether/our-journey-so-far">More info</a>
</div>
<div class="growing-together-entry-block tools-to-help-you even">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/icons/tools_to_help_you.png" />
    <h2>Tools to help you</h2>
    <p>With Gary O’Brien, Director of Digital & Marketing</p>
    <a href="/growingtogether/tools-to-help-you">More info</a>
</div>
<div class="growing-together-entry-block our-brand odd">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/icons/our_brand.png" />
    <h2>Our brand (and yours…)</h2>
    <p>With Anna Drabble, Head of Marketing</p>
    <a href="/growingtogether/our-brand-and-yours">More info</a>
</div>
<div class="growing-together-entry-block your-team-your-products last even">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/icons/your_team_your_products.png" />
    <h2>Your team and your products</h2>
    <p>With Kirsty Walker, Commercial Director & Matthew Washer, Technical Director</p>
    <a href="/growingtogether/your-team-and-your-products">More info</a>
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-entry-blocks clearfix',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e6b51938-ab37-4db1-8f47-3b793c6f8ae8';
    $display->content['new-e6b51938-ab37-4db1-8f47-3b793c6f8ae8'] = $pane;
    $display->panels['center'][1] = 'new-e6b51938-ab37-4db1-8f47-3b793c6f8ae8';
    $pane = new stdClass();
    $pane->pid = 'new-a5427ef6-c288-4853-bb9d-e1f4aaa93e5e';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'growing_together_section-growing_together_feedback';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'a5427ef6-c288-4853-bb9d-e1f4aaa93e5e';
    $display->content['new-a5427ef6-c288-4853-bb9d-e1f4aaa93e5e'] = $pane;
    $display->panels['center'][2] = 'new-a5427ef6-c288-4853-bb9d-e1f4aaa93e5e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['growing_together_main'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'growing_together_our_brand_and_yours';
  $page->task = 'page';
  $page->admin_title = 'Growing Together: Our brand and yours';
  $page->admin_description = '';
  $page->path = 'growingtogether/our-brand-and-yours';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_growing_together_our_brand_and_yours__panel';
  $handler->task = 'page';
  $handler->subtask = 'growing_together_our_brand_and_yours';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'growing-together-section-page growing-together-our-brand-and-yours',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Our brand and yours';
  $display->uuid = 'ba0ea8d7-a3a1-4206-91c7-a0eeec513baf';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-534cfb47-6599-4aee-997d-5edf32baeb27';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Section Header',
      'title' => '<none>',
      'body' => '<div class="sections clearfix">
    <a class="our-journey-so-far" href="/growingtogether/our-journey-so-far">Our journey so far</a>
    <a class="tools-to-help-you" href="/growingtogether/tools-to-help-you">Tools to help you</a>
    <a class="our-brand-and-yours active" href="/growingtogether/our-brand-and-yours">Our brand and yours</a>
    <a class="your-team-and-your-products" href="/growingtogether/your-team-and-your-products">your team and your products</a>
</div>
<h2>Our brand (and yours…)</h2>
<p>Anna Drabble, Head of Marketing</p>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-section-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '534cfb47-6599-4aee-997d-5edf32baeb27';
    $display->content['new-534cfb47-6599-4aee-997d-5edf32baeb27'] = $pane;
    $display->panels['middle'][0] = 'new-534cfb47-6599-4aee-997d-5edf32baeb27';
    $pane = new stdClass();
    $pane->pid = 'new-9aeb5b01-8eb2-4523-9348-5e6f54acd8ce';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Content',
      'title' => '<none>',
      'body' => '<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/abay_01.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/abay_02.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/abay_03.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/abay_04.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/abay_05.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/abay_06.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9aeb5b01-8eb2-4523-9348-5e6f54acd8ce';
    $display->content['new-9aeb5b01-8eb2-4523-9348-5e6f54acd8ce'] = $pane;
    $display->panels['middle'][1] = 'new-9aeb5b01-8eb2-4523-9348-5e6f54acd8ce';
    $pane = new stdClass();
    $pane->pid = 'new-17b3640b-6731-4e3d-a33a-559d1645378b';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'growing_together_section-growing_together_feedback';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '17b3640b-6731-4e3d-a33a-559d1645378b';
    $display->content['new-17b3640b-6731-4e3d-a33a-559d1645378b'] = $pane;
    $display->panels['middle'][2] = 'new-17b3640b-6731-4e3d-a33a-559d1645378b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-17b3640b-6731-4e3d-a33a-559d1645378b';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['growing_together_our_brand_and_yours'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'growing_together_our_journey_so_far';
  $page->task = 'page';
  $page->admin_title = 'Growing Together: Our journey so far';
  $page->admin_description = '';
  $page->path = 'growingtogether/our-journey-so-far';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_growing_together_our_journey_so_far__panel';
  $handler->task = 'page';
  $handler->subtask = 'growing_together_our_journey_so_far';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'growing-together-section-page growing-together-our-journey-so-far',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Our journey so far';
  $display->uuid = 'a8a545ce-b19a-48bb-99aa-6b097ad1a6cb';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f8de19b0-1ac1-4a5a-8d92-ec87e0cea13e';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Section Header',
      'title' => '<none>',
      'body' => '<div class="sections clearfix">
    <a class="our-journey-so-far active" href="/growingtogether/our-journey-so-far">Our journey so far</a>
    <a class="tools-to-help-you" href="/growingtogether/tools-to-help-you">Tools to help you</a>
    <a class="our-brand-and-yours" href="/growingtogether/our-brand-and-yours">Our brand and yours</a>
    <a class="your-team-and-your-products" href="/growingtogether/your-team-and-your-products">your team and your products</a>
</div>
<h2>Our journey so far</h2>
<p>Maria Leighton, Finance Director</p>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-section-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f8de19b0-1ac1-4a5a-8d92-ec87e0cea13e';
    $display->content['new-f8de19b0-1ac1-4a5a-8d92-ec87e0cea13e'] = $pane;
    $display->panels['middle'][0] = 'new-f8de19b0-1ac1-4a5a-8d92-ec87e0cea13e';
    $pane = new stdClass();
    $pane->pid = 'new-82a2b442-3b4d-449a-8cb7-677621ddf582';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Timeline',
      'title' => '<none>',
      'body' => '<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/timeline-pg01.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/timeline-pg02.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-content',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '82a2b442-3b4d-449a-8cb7-677621ddf582';
    $display->content['new-82a2b442-3b4d-449a-8cb7-677621ddf582'] = $pane;
    $display->panels['middle'][1] = 'new-82a2b442-3b4d-449a-8cb7-677621ddf582';
    $pane = new stdClass();
    $pane->pid = 'new-df87a973-9e3f-44b1-b01d-e0bd879cdbf0';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'growing_together_section-growing_together_feedback';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'df87a973-9e3f-44b1-b01d-e0bd879cdbf0';
    $display->content['new-df87a973-9e3f-44b1-b01d-e0bd879cdbf0'] = $pane;
    $display->panels['middle'][2] = 'new-df87a973-9e3f-44b1-b01d-e0bd879cdbf0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-df87a973-9e3f-44b1-b01d-e0bd879cdbf0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['growing_together_our_journey_so_far'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'growing_together_tools_to_help_you';
  $page->task = 'page';
  $page->admin_title = 'Growing Together: Tools to help you';
  $page->admin_description = '';
  $page->path = 'growingtogether/tools-to-help-you';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_growing_together_tools_to_help_you__panel';
  $handler->task = 'page';
  $handler->subtask = 'growing_together_tools_to_help_you';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'growing-together-section-page growing-together-tools-to-help-you',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tools to help you';
  $display->uuid = '5429c184-c65d-4906-b900-96b1cc1c800d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-8e46f741-d1f0-429c-aa30-79a4cb9910ff';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Section Header',
      'title' => '<none>',
      'body' => '<div class="sections clearfix">
    <a class="our-journey-so-far" href="/growingtogether/our-journey-so-far">Our journey so far</a>
    <a class="tools-to-help-you active" href="/growingtogether/tools-to-help-you">Tools to help you</a>
    <a class="our-brand-and-yours" href="/growingtogether/our-brand-and-yours">Our brand and yours</a>
    <a class="your-team-and-your-products" href="/growingtogether/your-team-and-your-products">your team and your products</a>
</div>
<h2>Tools to help you</h2>
<p>Gary O’Brien, Director of Digital & Marketing</p>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-section-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8e46f741-d1f0-429c-aa30-79a4cb9910ff';
    $display->content['new-8e46f741-d1f0-429c-aa30-79a4cb9910ff'] = $pane;
    $display->panels['middle'][0] = 'new-8e46f741-d1f0-429c-aa30-79a4cb9910ff';
    $pane = new stdClass();
    $pane->pid = 'new-665d4b99-652b-493f-aece-3d7eddea5c2c';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Content',
      'title' => '<none>',
      'body' => '<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/tthy_01.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/tthy_02.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/tthy_03.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '665d4b99-652b-493f-aece-3d7eddea5c2c';
    $display->content['new-665d4b99-652b-493f-aece-3d7eddea5c2c'] = $pane;
    $display->panels['middle'][1] = 'new-665d4b99-652b-493f-aece-3d7eddea5c2c';
    $pane = new stdClass();
    $pane->pid = 'new-187a165c-6d82-4b4b-90ed-5faa3158f23e';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'growing_together_section-growing_together_feedback';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '187a165c-6d82-4b4b-90ed-5faa3158f23e';
    $display->content['new-187a165c-6d82-4b4b-90ed-5faa3158f23e'] = $pane;
    $display->panels['middle'][2] = 'new-187a165c-6d82-4b4b-90ed-5faa3158f23e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-187a165c-6d82-4b4b-90ed-5faa3158f23e';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['growing_together_tools_to_help_you'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'growing_together_your_team_and_your_products';
  $page->task = 'page';
  $page->admin_title = 'Growing Together: Your team and your products';
  $page->admin_description = '';
  $page->path = 'growingtogether/your-team-and-your-products';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_growing_together_your_team_and_your_products__panel';
  $handler->task = 'page';
  $handler->subtask = 'growing_together_your_team_and_your_products';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'growing-together-section-page growing-together-your-team-and-your-products',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Your team and your products';
  $display->uuid = 'acd7b0af-243c-42d0-bcf7-4c6abec240bc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0e5a3da7-85fb-40d9-be2b-0742f24dcfc1';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Section Header',
      'title' => '<none>',
      'body' => '<div class="sections clearfix">
    <a class="our-journey-so-far" href="/growingtogether/our-journey-so-far">Our journey so far</a>
    <a class="tools-to-help-you" href="/growingtogether/tools-to-help-you">Tools to help you</a>
    <a class="our-brand-and-yours" href="/growingtogether/our-brand-and-yours">Our brand and yours</a>
    <a class="your-team-and-your-products active" href="/growingtogether/your-team-and-your-products">your team and your products</a>
</div>
<h2>Your team and your products</h2>
<p>Kirsty Walker, Commercial Director</p>
<p>Matthew Washer, Technical Director</p>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'growing-together-section-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0e5a3da7-85fb-40d9-be2b-0742f24dcfc1';
    $display->content['new-0e5a3da7-85fb-40d9-be2b-0742f24dcfc1'] = $pane;
    $display->panels['middle'][0] = 'new-0e5a3da7-85fb-40d9-be2b-0742f24dcfc1';
    $pane = new stdClass();
    $pane->pid = 'new-3419eeb3-1ae5-4d39-82de-bc2058fc8baf';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Content',
      'title' => '<none>',
      'body' => '<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/ytyp_01.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />
<img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/growing_together/content/ytyp_02.jpg" galleryimg="no" draggable="false" oncontextmenu="return false;" onmousedown="return false;" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3419eeb3-1ae5-4d39-82de-bc2058fc8baf';
    $display->content['new-3419eeb3-1ae5-4d39-82de-bc2058fc8baf'] = $pane;
    $display->panels['middle'][1] = 'new-3419eeb3-1ae5-4d39-82de-bc2058fc8baf';
    $pane = new stdClass();
    $pane->pid = 'new-268c1632-c428-4cc3-83dd-f88b1d1e1338';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'growing_together_section-growing_together_feedback';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '268c1632-c428-4cc3-83dd-f88b1d1e1338';
    $display->content['new-268c1632-c428-4cc3-83dd-f88b1d1e1338'] = $pane;
    $display->panels['middle'][2] = 'new-268c1632-c428-4cc3-83dd-f88b1d1e1338';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-268c1632-c428-4cc3-83dd-f88b1d1e1338';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['growing_together_your_team_and_your_products'] = $page;

  return $pages;

}
