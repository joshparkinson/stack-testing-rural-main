<?php
/**
 * @file
 * growing_together_section.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function growing_together_section_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
