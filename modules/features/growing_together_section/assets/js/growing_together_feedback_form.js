(function($) {

    $(document).ready(function(){

        var submitFeedbackButton = $(".submit-feedback-button");
        var submitFeedbackButtonText = $(".submit-feedback-button p");
        var feedbackFormWrapper = $('.feedback-form-wrapper');


        $(submitFeedbackButton).click(function(){
            if($(feedbackFormWrapper).is(':visible')){
                $(feedbackFormWrapper).slideUp('slow');
                $(submitFeedbackButtonText).html('Leave feedback');
            }
            else{
                $(feedbackFormWrapper).slideDown('slow');
                $(submitFeedbackButtonText).html('Close');
            }
        });

    });

})(jQuery);