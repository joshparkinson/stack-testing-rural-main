<?php
/**
 * @file
 * growing_together_section.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function growing_together_section_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Site Map';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'site_map_block';
  $fe_block_boxes->body = '<ul class="site-map-menu clearfix">
    <li class="first"><a href="/about">About Rural</a>
        <ul>
            <li><a href="/about/our-history">Our history</a></li>
            <li><a href="/about/our-partners">Our partners</a></li>
            <li><a href="/about/careers">Careers</a></li>
        </ul>
    </li>
    <li><a href="/products">Our products</a>
        <ul>
            <li><a href="/products/farm-combined-insurance">Farm Combined insurance</a></li>
            <li><a href="/products/farm-motor-insurance">Farm Motor insurance</a></li>
            <li><a href="/products/rural-business-motor-insurance">Rural Business Motor insurance</a></li>
            <li><a href="/products/renewable-energy-insurance">Renewable Energy insurance</a></li>
            <li><a href="/products/livestock-insurance">Livestock insurance</a></li>
            <li><a href="/products/smallholders-insurance">Smallholders insurance</a></li>
            <li><a href="/products/motor-breakdown-insurance">Motor Breakdown insurance</a></li>
        </ul>      
    </li> 
    <li><a href="/claims">Claims</a>
        <ul>
            <li><a href="/claims/make-a-claim">Make a claim</a></li>
            <li><a href="/claims/claims-documents">Claims documents</a></li>
            <li><a href="/claims/our-claims-partners">Our claims partners</a></li>
            <li><a href="/claims/what-to-expect">What to expect</a></li>
        </ul>        
    </li> 
    <li class="last"><a href="/produts">More information</a>
        <ul>
            <li><a href="/service-standards">Service</a></li>
            <li><a href="/documentation">Documentation</a></li>
            <li><a href="/news">News</a></li>
            <li><a href="/contact">Contact</a></li>
        </ul>        
    </li> 
</ul>';

  $export['site_map_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Tool Tabs';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'tool_tabs';
  $fe_block_boxes->body = '<a class="tool-tab smallholders" target="_blank" href="https://ruralinsurance.transactorhosting.com/">Smallholders login</a>
<a class="tool-tab covernotes" target="_blank" href="https://covernotes.ruralinsurance.co.uk/Console/Login.action">Rural CoverNotes login</a>
<a class="tool-tab find-a-broker" href="/find-a-rural-insurance-broker">Find a Rural Broker</a>';

  $export['tool_tabs'] = $fe_block_boxes;

  return $export;
}
