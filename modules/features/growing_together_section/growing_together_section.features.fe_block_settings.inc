<?php
/**
 * @file
 * growing_together_section.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function growing_together_section_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-site_map_block'] = array(
    'cache' => -1,
    'css_class' => 'site-map clearfix',
    'custom' => 0,
    'machine_name' => 'site_map_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-tool_tabs'] = array(
    'cache' => -1,
    'css_class' => 'tool-tabs',
    'custom' => 0,
    'machine_name' => 'tool_tabs',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -49,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
