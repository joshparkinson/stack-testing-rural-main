<?php
/**
 * @file
 * rural_rebrand_light.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function rural_rebrand_light_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Min length';
  $rule->name = 'min_length';
  $rule->field_name = 'field_message';
  $rule->col = 'value';
  $rule->entity_type = 'entityform';
  $rule->bundle = 'a_new_chapter_feedback_form';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '10',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Message must be atleast 10 characters';
  $export['min_length'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Plain text';
  $rule->name = 'plain_text';
  $rule->field_name = 'field_first_name';
  $rule->col = 'value';
  $rule->entity_type = 'entityform';
  $rule->bundle = 'a_new_chapter_feedback_form';
  $rule->validator = 'field_validation_plain_text_validator';
  $rule->settings = array(
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Name should be plain text only';
  $export['plain_text'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Valid email';
  $rule->name = 'valid_email';
  $rule->field_name = 'field_email';
  $rule->col = 'value';
  $rule->entity_type = 'entityform';
  $rule->bundle = 'a_new_chapter_feedback_form';
  $rule->validator = 'field_validation_email_validator';
  $rule->settings = array(
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = 'Please enter a valid email address';
  $export['valid_email'] = $rule;

  return $export;
}
