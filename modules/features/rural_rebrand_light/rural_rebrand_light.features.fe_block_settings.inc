<?php
/**
 * @file
 * rural_rebrand_light.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function rural_rebrand_light_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-site_map_block'] = array(
    'cache' => -1,
    'css_class' => 'site-map clearfix',
    'custom' => 0,
    'machine_name' => 'site_map_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['block-tool_tabs'] = array(
    'cache' => -1,
    'css_class' => 'tool-tabs',
    'custom' => 0,
    'machine_name' => 'tool_tabs',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -50,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
