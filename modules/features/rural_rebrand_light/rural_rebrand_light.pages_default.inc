<?php
/**
 * @file
 * rural_rebrand_light.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function rural_rebrand_light_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'a_new_chapter_on_stand_feedback';
  $page->task = 'page';
  $page->admin_title = 'A New Chapter: Brand Feedback';
  $page->admin_description = '';
  $page->path = 'brand-feedback';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_a_new_chapter_on_stand_feedback__panel';
  $handler->task = 'page';
  $handler->subtask = 'a_new_chapter_on_stand_feedback';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Our brand. What do you think?';
  $display->uuid = 'a66f686f-0dfc-4a25-abd1-d408546f82c1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a0dfb79a-822d-41bc-bc34-22b42d044766';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Intro copy',
      'title' => '<none>',
      'body' => '<h2>Our brand. What do you think?</h2>
<p>Rural Insurance has very recently rebranded. Our new brand reflects the optimism and opportunity our customers saw in the future of the rural economy – and it supports our ambition to become the leading specialist insurer for rural communities.</p>
<p>We would love to hear your thoughts…</p>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'new-chapter-on-stand-feedback-intro',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a0dfb79a-822d-41bc-bc34-22b42d044766';
    $display->content['new-a0dfb79a-822d-41bc-bc34-22b42d044766'] = $pane;
    $display->panels['middle'][0] = 'new-a0dfb79a-822d-41bc-bc34-22b42d044766';
    $pane = new stdClass();
    $pane->pid = 'new-12adda2d-bf8a-4acb-a9a9-4fa11f414eb0';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'rural_new_chapter_feedback-rural_new_chapter_feedback';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'a-new-chapter-feedback',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '12adda2d-bf8a-4acb-a9a9-4fa11f414eb0';
    $display->content['new-12adda2d-bf8a-4acb-a9a9-4fa11f414eb0'] = $pane;
    $display->panels['middle'][1] = 'new-12adda2d-bf8a-4acb-a9a9-4fa11f414eb0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-12adda2d-bf8a-4acb-a9a9-4fa11f414eb0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['a_new_chapter_on_stand_feedback'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home_page';
  $page->task = 'page';
  $page->admin_title = 'Home page';
  $page->admin_description = 'The rural homepage';
  $page->path = 'home';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'home_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'opportunity-home full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'A new chapter';
  $display->uuid = '56fb0860-6bb0-46d8-8315-82103997241a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9878a321-e90a-4554-9eb8-98f6e71acdce';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Hero image',
      'title' => '<none>',
      'body' => '<div class="title-block">
    <h1 class="thick">Don\'t forget…</h1>
    <p>From Thursday 7th July you’ll only be able to access our broker area with a username and password.</p> 
    <a href="/unity/register">Pre-register now</a>
</div>
<img class="logo" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/unity_logo.png" />
<img class="scroll" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/scroll.png" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'no-padding hero-image',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9878a321-e90a-4554-9eb8-98f6e71acdce';
    $display->content['new-9878a321-e90a-4554-9eb8-98f6e71acdce'] = $pane;
    $display->panels['middle'][0] = 'new-9878a321-e90a-4554-9eb8-98f6e71acdce';
    $pane = new stdClass();
    $pane->pid = 'new-561583a4-631a-450f-bba8-776212154532';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'About rural',
      'title' => 'About Rural Insurance',
      'body' => '<p class="lead">Rural Insurance is a leading specialist insurer of rural communities. We enable you and your clients to look to the future through the support we provide today.</p>
<p class="lead last">At Rural we know that progress doesn’t just happen. It’s strived for by people who know that the rural economy never stands still. It’s achieved by those who work together to take the steps, however small, to adapt, improve and grow. And it’s insured by a partner that understands how to make change work for you and your clients quickly and decisively.</p>
<h3>Why Rural Insurance?</h3>
<div class="stat-block-container clearfix">
    <div class="stat-block">
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/stat_years.png" />
        <p>20 years\' experience providing specialist insurance</p>
    </div>
    <div class="stat-block">
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/stat_customers.png" />
        <p>Over 24,500 customers nationwide choose rural to protect their assets</p>
    </div>
    <div class="stat-block">
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/stat_brokers.png" />
        <p>A network of more than 600 trusted brokers</p>
    </div>
</div>
<div style="text-align:center;"><a href="/about">Find out more</a></div>
<img class="scroll" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/icons/arrow_down.png">',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'about-rural',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '561583a4-631a-450f-bba8-776212154532';
    $display->content['new-561583a4-631a-450f-bba8-776212154532'] = $pane;
    $display->panels['middle'][1] = 'new-561583a4-631a-450f-bba8-776212154532';
    $pane = new stdClass();
    $pane->pid = 'new-9514c2e2-3fc3-4451-bf8c-95d0aea12a61';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'products-products_homepage_content_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '9514c2e2-3fc3-4451-bf8c-95d0aea12a61';
    $display->content['new-9514c2e2-3fc3-4451-bf8c-95d0aea12a61'] = $pane;
    $display->panels['middle'][2] = 'new-9514c2e2-3fc3-4451-bf8c-95d0aea12a61';
    $pane = new stdClass();
    $pane->pid = 'new-71cb00d1-3cb2-4e1e-bef8-658b7574a1b6';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Promo hero',
      'title' => '<none>',
      'body' => '<div class="title-block">
    <h2>Rural Business Motor postcode<br /><span>checker</span></h2> 
    <p>Find out if we can provide a quote for your client’s location with our handy postcode checker</p>
    <div class="link-wrapper"><a href="/rbm-postcode-checker">Find out more</a></div>
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'promo-hero',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '71cb00d1-3cb2-4e1e-bef8-658b7574a1b6';
    $display->content['new-71cb00d1-3cb2-4e1e-bef8-658b7574a1b6'] = $pane;
    $display->panels['middle'][3] = 'new-71cb00d1-3cb2-4e1e-bef8-658b7574a1b6';
    $pane = new stdClass();
    $pane->pid = 'new-a0231ee7-b912-4388-adba-6d8ef62b0dd2';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Find a broker',
      'title' => '',
      'body' => '<h3>Looking for a Rural Broker?</h3>
<p>Rural Insurance products are only available through our trusted broker network. We have over <span>600 partners</span> across the UK who can help you get the right cover for your business.</p>
<a href="/find-a-rural-insurance-broker">Start your search</a>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'find-a-broker-bar clearfix',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'a0231ee7-b912-4388-adba-6d8ef62b0dd2';
    $display->content['new-a0231ee7-b912-4388-adba-6d8ef62b0dd2'] = $pane;
    $display->panels['middle'][4] = 'new-a0231ee7-b912-4388-adba-6d8ef62b0dd2';
    $pane = new stdClass();
    $pane->pid = 'new-61c054dd-af6b-4d4f-afad-c3adaf4b0b0f';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Navigation blocks',
      'title' => '<none>',
      'body' => '<div class="stat-block-container clearfix">
    <div class="stat-block">
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/nav_documentation.png" />
        <p>Documentation</p>
        <a href="/documentation">More info</a>
    </div>
    <div class="stat-block">
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/nav_claims.png" />
        <p>Claims</p>
        <a href="/claims">More info</a>
    </div>
    <div class="stat-block">
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/nav_get_in_touch.png" />
        <p>Get in touch</p>
        <a href="/contact">More info</a>
    </div>
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'nav-blocks',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '61c054dd-af6b-4d4f-afad-c3adaf4b0b0f';
    $display->content['new-61c054dd-af6b-4d4f-afad-c3adaf4b0b0f'] = $pane;
    $display->panels['middle'][5] = 'new-61c054dd-af6b-4d4f-afad-c3adaf4b0b0f';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-9878a321-e90a-4554-9eb8-98f6e71acdce';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home_page'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'landing_page_a_new_chapter';
  $page->task = 'page';
  $page->admin_title = 'Landing Page: A new chapter';
  $page->admin_description = '';
  $page->path = 'new-chapter';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_landing_page_a_new_chapter__panel';
  $handler->task = 'page';
  $handler->subtask = 'landing_page_a_new_chapter';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'landing-page-a-new-chapter full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'A new chapter';
  $display->uuid = '9015277a-9ea2-4ee3-8b09-92123e618091';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d01278d0-8b17-45c4-8544-e82fe38b0ad2';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Hero image',
      'title' => '<none>',
      'body' => '<div class="title-block">
    <h1>A new<br /><span>chapter…</span></h1> 
</div>
<img class="logo" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/rural_logo.png" />
<img class="scroll" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/home/scroll.png" />',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'no-padding hero-image a-new-chapter',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd01278d0-8b17-45c4-8544-e82fe38b0ad2';
    $display->content['new-d01278d0-8b17-45c4-8544-e82fe38b0ad2'] = $pane;
    $display->panels['middle'][0] = 'new-d01278d0-8b17-45c4-8544-e82fe38b0ad2';
    $pane = new stdClass();
    $pane->pid = 'new-f1b50b85-5d16-4a62-a3b5-7ede53e49038';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Intro copy',
      'title' => '<none>',
      'body' => '<h2>Rural Insurance is a leading specialist insurer of rural communities. We enable you and your clients to look to the future through the support we provide today.</h2>
<p>At Rural we know that progress doesn’t just happen. It’s strived for by people who know that the rural economy never stands still. It’s achieved by those who work together to take the steps, however small, to adapt, improve and grow. And it’s insured by a partner that understands how to make change work for you and your clients quickly and decisively.</p>
<p>That’s why the rural economy needs Rural Insurance.</p>
<img class="scroll" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/icons/arrow_down.png">',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'a-new-chapter-intro',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f1b50b85-5d16-4a62-a3b5-7ede53e49038';
    $display->content['new-f1b50b85-5d16-4a62-a3b5-7ede53e49038'] = $pane;
    $display->panels['middle'][1] = 'new-f1b50b85-5d16-4a62-a3b5-7ede53e49038';
    $pane = new stdClass();
    $pane->pid = 'new-367c50f0-d3df-49a6-9f1e-206f4e4bbdf7';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Brand Video',
      'title' => '<none>',
      'body' => '<h2>A new chapter for Rural Insurance</h2>

<iframe width="560" height="315" src="https://www.youtube.com/embed/rFi6MpphfhA?rel=0&vq=hd720&showinfo=0" frameborder="0" allowfullscreen></iframe>
<img class="scroll" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/icons/arrow_down_grey.png">',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'new-chapter-brand-video',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '367c50f0-d3df-49a6-9f1e-206f4e4bbdf7';
    $display->content['new-367c50f0-d3df-49a6-9f1e-206f4e4bbdf7'] = $pane;
    $display->panels['middle'][2] = 'new-367c50f0-d3df-49a6-9f1e-206f4e4bbdf7';
    $pane = new stdClass();
    $pane->pid = 'new-afb09909-aebd-4817-beff-f19bfba1924d';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Why we\'ve changed',
      'title' => '<none>',
      'body' => '<div class="block-one">
    <h3>Why we’ve changed</h3>
    <p class="intro">Rural Insurance has been looking after rural businesses for more than 20 years – with more than 24,500 customer choosing us to protect their farms and vehicles. We have strong partnerships with 600 brokers up and down the UK who sell Rural’s products on our behalf. But we want to do more.</p>
    <p>We have an ambition to establish Rural as the number one specialist insurer for rural communities. We want to challenge established market leaders through our ability to be flexible and enable progression.</p>
    <p>Customers have told us they see insurance as an important part of running their business. A way of enabling future plans and keeping today’s assets covered.</p>
    <a href="/news/a-new-chapter-for-rural-insurance">Find out more</a>
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/new_chapter/why_we_changed.jpg" />
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'new-chapter-why-we-changed',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'afb09909-aebd-4817-beff-f19bfba1924d';
    $display->content['new-afb09909-aebd-4817-beff-f19bfba1924d'] = $pane;
    $display->panels['middle'][3] = 'new-afb09909-aebd-4817-beff-f19bfba1924d';
    $pane = new stdClass();
    $pane->pid = 'new-88232eba-1db4-4860-ab51-bdb42c0bc36b';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'We are we will',
      'title' => '<none>',
      'body' => '<div class="we-are-border"></div>
<p class="section-intro we-are">We are…</p>
<ul>
    <li>Independent</li>
    <li>Energetic</li>
    <li>Agile</li>
    <li>Specialist</li>
    <li>Commercial</li>
</ul>

<div class="we-will-border"></div>
<p class="section-intro we-will">We will…</p>
<div class="clearfix">
<div class="will-block">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/new_chapter/status_quo.jpg" />
    <p class="sub">challenge the</p>
    <p>status quo</p>
</div>
<div class="will-block">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/new_chapter/supporting_change.jpg" />
    <p class="sub">invest in</p>
    <p>change</p>
</div>
<div class="will-block">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/new_chapter/think_digital.jpg" />
    <p class="sub">think</p>
    <p>digital</p>
</div>
<div class="will-block">
    <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/new_chapter/new_products.jpg" />
    <p class="sub">commit to developing</p>
    <p>new products</p>
</div>
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'new-chapter-we-are-we-will',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '88232eba-1db4-4860-ab51-bdb42c0bc36b';
    $display->content['new-88232eba-1db4-4860-ab51-bdb42c0bc36b'] = $pane;
    $display->panels['middle'][4] = 'new-88232eba-1db4-4860-ab51-bdb42c0bc36b';
    $pane = new stdClass();
    $pane->pid = 'new-853414a7-318d-4103-9321-b979481639a0';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Same people same service',
      'title' => '<none>',
      'body' => '<h3>Same great people delivering the same great products and service.</h3>
<img class="scroll" src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/icons/arrow_down_dark_grey.png">',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'new-chapter-same-people-same-service',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '853414a7-318d-4103-9321-b979481639a0';
    $display->content['new-853414a7-318d-4103-9321-b979481639a0'] = $pane;
    $display->panels['middle'][5] = 'new-853414a7-318d-4103-9321-b979481639a0';
    $pane = new stdClass();
    $pane->pid = 'new-c7066a93-ab51-46d7-b720-f1a9b799bc59';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'entityform_panel-a_new_chapter_feedback_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'new-chapter-feedback-form',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'c7066a93-ab51-46d7-b720-f1a9b799bc59';
    $display->content['new-c7066a93-ab51-46d7-b720-f1a9b799bc59'] = $pane;
    $display->panels['middle'][6] = 'new-c7066a93-ab51-46d7-b720-f1a9b799bc59';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-d01278d0-8b17-45c4-8544-e82fe38b0ad2';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['landing_page_a_new_chapter'] = $page;

  return $pages;

}
