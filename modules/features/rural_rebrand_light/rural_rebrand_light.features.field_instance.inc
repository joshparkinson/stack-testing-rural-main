<?php
/**
 * @file
 * rural_rebrand_light.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rural_rebrand_light_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'entityform-a_new_chapter_feedback_form-field_email'.
  $field_instances['entityform-a_new_chapter_feedback_form-field_email'] = array(
    'bundle' => 'a_new_chapter_feedback_form',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_email',
    'label' => 'Email',
    'placeholder' => 'Email*',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 0,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 33,
    ),
  );

  // Exported field_instance:
  // 'entityform-a_new_chapter_feedback_form-field_first_name'.
  $field_instances['entityform-a_new_chapter_feedback_form-field_first_name'] = array(
    'bundle' => 'a_new_chapter_feedback_form',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_first_name',
    'label' => 'Name',
    'placeholder' => 'Name*',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 0,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 32,
    ),
  );

  // Exported field_instance:
  // 'entityform-a_new_chapter_feedback_form-field_message'.
  $field_instances['entityform-a_new_chapter_feedback_form-field_message'] = array(
    'bundle' => 'a_new_chapter_feedback_form',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'entityform',
    'field_name' => 'field_message',
    'label' => 'Message',
    'placeholder' => '',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'use_title_as_placeholder' => 1,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 34,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Email');
  t('Message');
  t('Name');

  return $field_instances;
}
