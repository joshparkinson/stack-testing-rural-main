<?php
/**
 * @file
 * rural_rebrand_light.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rural_rebrand_light_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_validation" && $api == "default_field_validation_rules") {
    return array("version" => "2");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function rural_rebrand_light_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function rural_rebrand_light_default_entityform_type() {
  $items = array();
  $items['a_new_chapter_feedback_form'] = entity_import('entityform_type', '{
    "type" : "a_new_chapter_feedback_form",
    "label" : "A New Chapter: Feedback Form",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "ckeditor" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "ckeditor" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : 0, "3" : 0, "4" : 0, "5" : 0, "6" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "[current-page:url]",
      "instruction_pre" : { "value" : "", "format" : "ckeditor" }
    },
    "weight" : "0",
    "paths" : []
  }');
  return $items;
}
