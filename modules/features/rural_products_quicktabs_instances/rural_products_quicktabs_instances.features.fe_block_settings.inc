<?php
/**
 * @file
 * rural_products_quicktabs_instances.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function rural_products_quicktabs_instances_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['quicktabs-farm_combined'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'farm_combined',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/farm-combined-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -37,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['quicktabs-farm_motor_quicktab'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'farm_motor_quicktab',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/farm-motor-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -36,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['quicktabs-livestock_quicktabs'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'livestock_quicktabs',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/livestock-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -35,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['quicktabs-motor_breakdown_insurance_tabs'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'motor_breakdown_insurance_tabs',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/motor-breakdown-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -11,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['quicktabs-renewable_energy_quicktab'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'renewable_energy_quicktab',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/renewable-energy-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -34,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['quicktabs-rural_business_motor_quicktab'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'rural_business_motor_quicktab',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/rural-business-motor-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -32,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['quicktabs-smallholders_quicktabs'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'smallholders_quicktabs',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'products/smallholders-insurance',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -31,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
