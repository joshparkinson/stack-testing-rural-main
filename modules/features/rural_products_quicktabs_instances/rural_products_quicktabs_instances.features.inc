<?php
/**
 * @file
 * rural_products_quicktabs_instances.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rural_products_quicktabs_instances_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
}
