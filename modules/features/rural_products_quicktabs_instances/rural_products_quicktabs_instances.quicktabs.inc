<?php
/**
 * @file
 * rural_products_quicktabs_instances.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function rural_products_quicktabs_instances_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'farm_combined';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Farm Combined quicktab';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '2961',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Selling points',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '100',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Covers',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '101',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'vid' => 'documentation',
      'display' => 'policy_documents_product_specific',
      'args' => '',
      'title' => 'Policy documents',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'nid' => '103',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-96',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Covers');
  t('Farm Combined quicktab');
  t('Making a claim');
  t('Policy documents');
  t('Selling points');
  t('Trading');

  $export['farm_combined'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'farm_motor_quicktab';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Farm Motor quicktab';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '3053',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Selling points',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '104',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Covers',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '105',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'vid' => 'documentation',
      'display' => 'policy_documents_product_specific',
      'args' => '',
      'title' => 'Policy documents',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'nid' => '107',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-96',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Covers');
  t('Farm Motor quicktab');
  t('Making a claim');
  t('Policy documents');
  t('Selling points');
  t('Trading');

  $export['farm_motor_quicktab'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'livestock_quicktabs';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Livestock quicktabs';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '120',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Features & Benefits',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '121',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'vid' => 'documentation',
      'display' => 'policy_documents_product_specific',
      'args' => '',
      'title' => 'Policy documents',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'nid' => '123',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-97',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Features & Benefits');
  t('Livestock quicktabs');
  t('Making a claim');
  t('Policy documents');
  t('Trading');

  $export['livestock_quicktabs'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'motor_breakdown_insurance_tabs';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 999999;
  $quicktabs->title = 'Motor Breakdown Insurance tabs';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '3084',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Covers',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '3085',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Selling points',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '3086',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'vid' => 'documentation',
      'display' => 'policy_documents_product_specific',
      'args' => '',
      'title' => 'Policy documents',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'nid' => '3088',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-96',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Covers');
  t('Making a claim');
  t('Motor Breakdown Insurance tabs');
  t('Policy documents');
  t('Selling points');
  t('Trading');

  $export['motor_breakdown_insurance_tabs'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'renewable_energy_quicktab';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Renewable Energy quicktab';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '116',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Features & Benefits',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '117',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'vid' => 'documentation',
      'display' => 'policy_documents_product_specific',
      'args' => '',
      'title' => 'Policy documents',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'nid' => '119',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-97',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Features & Benefits');
  t('Making a claim');
  t('Policy documents');
  t('Renewable Energy quicktab');
  t('Trading');

  $export['renewable_energy_quicktab'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'rural_business_motor_quicktab';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 2;
  $quicktabs->title = 'Rural business motor quicktab';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '2874',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Selling points',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '108',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Covers',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '2875',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trades & Locations',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'vid' => 'documentation',
      'display' => 'policy_documents_product_specific',
      'args' => '',
      'title' => 'Policy documents',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'nid' => '109',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-96',
      'type' => 'node',
    ),
    5 => array(
      'nid' => '111',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-95',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Covers');
  t('Making a claim');
  t('Policy documents');
  t('Rural business motor quicktab');
  t('Selling points');
  t('Trades & Locations');
  t('Trading');

  $export['rural_business_motor_quicktab'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'smallholders_quicktabs';
  $quicktabs->ajax = 1;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Smallholders quicktabs';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '3047',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Selling points',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '124',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Covers',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '125',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Trading',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'nid' => '126',
      'view_mode' => 'full',
      'hide_title' => 1,
      'title' => 'Making a claim',
      'weight' => '-97',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Covers');
  t('Making a claim');
  t('Selling points');
  t('Smallholders quicktabs');
  t('Trading');

  $export['smallholders_quicktabs'] = $quicktabs;

  return $export;
}
