<?php
/**
 * @file
 * large_farm_campaign_jan_2016.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function large_farm_campaign_jan_2016_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
