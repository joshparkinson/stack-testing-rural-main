<?php
/**
 * @file
 * large_farm_campaign_jan_2016.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function large_farm_campaign_jan_2016_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'large_farm_campaign';
  $page->task = 'page';
  $page->admin_title = 'Large Farm Campaign';
  $page->admin_description = '';
  $page->path = 'largefarm';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_large_farm_campaign__panel';
  $handler->task = 'page';
  $handler->subtask = 'large_farm_campaign';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Large Farm Campaign';
  $display->uuid = 'a220213b-2fc4-4a9b-afa0-8c1fa0714778';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ecf080cd-542d-4b25-b4b5-66adb0423298';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Helping you secure more <span>large farm risks</span> with Rural',
      'body' => '<p>Historically we’re aware that Rural has had a reputation for farm risks in the smaller and medium end of the market. Over the last 18 months we’ve dramatically increased our risk appetite towards the larger end of the market, <strong>specifically £5k+ premium risks</strong>, creating a more rounded proposition for a range of customers from hobby farmers right up to larger more complex working farms.</p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'large-farm intro-panel clearfix',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ecf080cd-542d-4b25-b4b5-66adb0423298';
    $display->content['new-ecf080cd-542d-4b25-b4b5-66adb0423298'] = $pane;
    $display->panels['middle'][0] = 'new-ecf080cd-542d-4b25-b4b5-66adb0423298';
    $pane = new stdClass();
    $pane->pid = 'new-4cf1786c-fc35-48a5-a9bb-eec5b34d1731';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'We know <span>large farm business…</span>',
      'body' => '<p>We\'ve been consistently strengthening our&nbsp; team to ensure we have the rights skills and capability to be successful in this market:</p>

<ul>
	<li>Appointment of James Leathley to Underwriting Manager, utilising a wealth of experience in this market to develop our large farm proposition</li>
	<li>Ongoing recruitment of experienced underwriters who know your market.</li>
	<li>20 strong new business underwriting team to providing the service and turnaround you need.</li>
</ul>

<p><img alt="" src="/sites/default/files/landing_page_images/field_cow_sheep.png" style="height:250px; width:500px" /></p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'large-farm we-know-large-farm-business',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4cf1786c-fc35-48a5-a9bb-eec5b34d1731';
    $display->content['new-4cf1786c-fc35-48a5-a9bb-eec5b34d1731'] = $pane;
    $display->panels['middle'][1] = 'new-4cf1786c-fc35-48a5-a9bb-eec5b34d1731';
    $pane = new stdClass();
    $pane->pid = 'new-6ed83e15-526f-4b19-900c-ad4093524677';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'You asked, <span>we did: product improvements</span>',
      'body' => '<p>Through a process of benchmarking and reviewing feedback, we\'re evolving our farm product in line with your needs. Previous changes have included:</p>

<ul>
	<li>Unoccupied definition extended from 30 days to 60 days</li>
	<li>Single article limit under Private House Contents sections increased to £5,000</li>
	<li>Waiver of average subject to conditions*</li>
	<li>Accidental Damage in respect of Increased Cost of Working*</li>
	<li>Milk in Tanks: Breakdown Cover*.</li>
</ul>

<p><a href="/sites/default/files/latest_documentation/farm_policy_risk_guide.pdf">Download risk guide</a> <a href="/node/55">Product details</a></p>

<p>*available on request</p>

<p><img alt="" src="/sites/default/files/landing_page_images/product_improvements_image.png" style="height:350px; margin-right:-480px; width:590px" /></p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'large-farm product-improvements',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '6ed83e15-526f-4b19-900c-ad4093524677';
    $display->content['new-6ed83e15-526f-4b19-900c-ad4093524677'] = $pane;
    $display->panels['middle'][2] = 'new-6ed83e15-526f-4b19-900c-ad4093524677';
    $pane = new stdClass();
    $pane->pid = 'new-c218c1bc-3f17-4460-8394-854309e412f1';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'We\'re here <span>to trade…</span>',
      'body' => '<p>Getting the right result for you and your customers takes teamwork:</p>

<ul>
	<li>Our underwriters work in partnership with you</li>
	<li>We align underwriters to our supporting brokers to buildsuccessful relationships</li>
	<li>We take a flexible approach based on a clear understanding of each risk</li>
	<li>All £5k+ Farm quotes will be reviewed by an experienced largefarm underwriter.</li>
</ul>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'large-farm two-col left here-to-trade',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'c218c1bc-3f17-4460-8394-854309e412f1';
    $display->content['new-c218c1bc-3f17-4460-8394-854309e412f1'] = $pane;
    $display->panels['middle'][3] = 'new-c218c1bc-3f17-4460-8394-854309e412f1';
    $pane = new stdClass();
    $pane->pid = 'new-7e98cf9a-cacd-41c8-8fc7-12f03895f6e8';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'We’ve reviewed our rating on <span>Agricultural Vehicles</span>',
      'body' => '<ul>
	<li>Our underwriters now have increased pricing flexibility</li>
	<li>We can now be more supportive on Agricultural&nbsp;Vehicle heavy risks</li>
	<li>Up to 15% new business discount available when Farm and Farm Motor are placed together.</li>
</ul>

<p><a href="/node/56">Find out more</a> <img alt="" src="/sites/default/files/landing_page_images/farm_motor_image.png" style="height:105px; width:350px" /></p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'large-farm two-col right competitive-farm-motor',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '7e98cf9a-cacd-41c8-8fc7-12f03895f6e8';
    $display->content['new-7e98cf9a-cacd-41c8-8fc7-12f03895f6e8'] = $pane;
    $display->panels['middle'][4] = 'new-7e98cf9a-cacd-41c8-8fc7-12f03895f6e8';
    $pane = new stdClass();
    $pane->pid = 'new-ac843384-d8fa-41c9-967e-0b1159b08d3f';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '24hr response on large cases',
      'body' => '<p>If you require a quick response, simply email your submission to&nbsp;<a href="mailto:jleathley@ruralinsurance.co.uk">jleathley@ruralinsurance.co.uk</a>, you\'ll then receive a call within 24hrs to discuss the risk further. If you have any questions or require further support then please speak to&nbsp;<a href="http://ruralinsurance.co.uk/contact/regional-contacts">your regional contact</a>.</p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'large-farm quick-response',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'ac843384-d8fa-41c9-967e-0b1159b08d3f';
    $display->content['new-ac843384-d8fa-41c9-967e-0b1159b08d3f'] = $pane;
    $display->panels['middle'][5] = 'new-ac843384-d8fa-41c9-967e-0b1159b08d3f';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = 'new-ecf080cd-542d-4b25-b4b5-66adb0423298';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['large_farm_campaign'] = $page;

  return $pages;

}
