<?php
/**
 * @file
 * agriculutral_vehicle_campaign_jan_2016_.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function agriculutral_vehicle_campaign_jan_2016__ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
