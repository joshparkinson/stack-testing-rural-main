<?php
/**
 * @file
 * agriculutral_vehicle_campaign_jan_2016_.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function agriculutral_vehicle_campaign_jan_2016__default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'aggricultural_vehicles_campaign';
  $page->task = 'page';
  $page->admin_title = 'Aggricultural Vehicles Campaign';
  $page->admin_description = '';
  $page->path = 'agriculturalvehicles';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_aggricultural_vehicles_campaign__panel_context_173e57ec-25ad-4dad-a262-b1e3da7db0fc';
  $handler->task = 'page';
  $handler->subtask = 'aggricultural_vehicles_campaign';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Large Farm Campaign';
  $display->uuid = 'a220213b-2fc4-4a9b-afa0-8c1fa0714778';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ecf080cd-542d-4b25-b4b5-66adb0423298';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'We\'ve reviewed our rating on <span>Agricultural Vehicles</span>',
      'body' => '<p>It’s important to us that we receive and act upon regular feedback from our trading partners to ensure our products and appetite continue to keep up with your needs and those of your customers. Following a recent review of our Farm Motor rating we’re delighted to be able to make some changes to broaden the range of risks we can support you on.</p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'agricultural-vehicle intro-panel clearfix',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ecf080cd-542d-4b25-b4b5-66adb0423298';
    $display->content['new-ecf080cd-542d-4b25-b4b5-66adb0423298'] = $pane;
    $display->panels['middle'][0] = 'new-ecf080cd-542d-4b25-b4b5-66adb0423298';
    $pane = new stdClass();
    $pane->pid = 'new-4cf1786c-fc35-48a5-a9bb-eec5b34d1731';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '<span>You said…</span>',
      'body' => '<p>“We use you a lot for Private Car and Commercial Vehicle heavy risks... but not&nbsp; so much for Agricultural Vehicle heavy ones”</p>

<p><img alt="" src="/sites/default/files/landing_page_images/speech_marks.png" style="height:160px; width:350px" /></p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'agricultural-vehicle you-said',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4cf1786c-fc35-48a5-a9bb-eec5b34d1731';
    $display->content['new-4cf1786c-fc35-48a5-a9bb-eec5b34d1731'] = $pane;
    $display->panels['middle'][1] = 'new-4cf1786c-fc35-48a5-a9bb-eec5b34d1731';
    $pane = new stdClass();
    $pane->pid = 'new-6ed83e15-526f-4b19-900c-ad4093524677';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '<span>We did…</span>',
      'body' => '<p>We\'ve given our underwriters increased pricing flexibility on Agricultural Vehicles</p>

<p><img alt="" src="/sites/default/files/landing_page_images/av_image.png" style="height:140px; width:350px" /></p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'agricultural-vehicle we-did',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '6ed83e15-526f-4b19-900c-ad4093524677';
    $display->content['new-6ed83e15-526f-4b19-900c-ad4093524677'] = $pane;
    $display->panels['middle'][2] = 'new-6ed83e15-526f-4b19-900c-ad4093524677';
    $pane = new stdClass();
    $pane->pid = 'new-c218c1bc-3f17-4460-8394-854309e412f1';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Agricultural Vehicle heavy risk? <span>No problem!</span>',
      'body' => '<p>This increased flexibility means we\'re able to be more supportive on a broader range of Farm Motor risks - including Agricultural Vehicle heavy risks, even on single vehicles.</p>

<p><a href="/sites/default/files/latest_documentation/farm_motor_risk_guide.pdf">Download our risk guide</a> <a href="/products/farm-motor-insurance">Product details</a> <img alt="" src="/sites/default/files/landing_page_images/vehicles_image.png" style="height:105px; width:280px" /></p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'agricultural-vehicle two-col left heavy-risk-no-problem',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'c218c1bc-3f17-4460-8394-854309e412f1';
    $display->content['new-c218c1bc-3f17-4460-8394-854309e412f1'] = $pane;
    $display->panels['middle'][3] = 'new-c218c1bc-3f17-4460-8394-854309e412f1';
    $pane = new stdClass();
    $pane->pid = 'new-7e98cf9a-cacd-41c8-8fc7-12f03895f6e8';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Up to <span>15% multi-policy discount</span>',
      'body' => '<p>Make our Farm Motor even more competitive by adding Farm Combined to receive up to 15% multi-policy discount on new business.</p>

<a href="/multi-policy-discount">Find out more</a>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'agricultural-vehicle two-col right multi-policy-discount',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '7e98cf9a-cacd-41c8-8fc7-12f03895f6e8';
    $display->content['new-7e98cf9a-cacd-41c8-8fc7-12f03895f6e8'] = $pane;
    $display->panels['middle'][4] = 'new-7e98cf9a-cacd-41c8-8fc7-12f03895f6e8';
    $pane = new stdClass();
    $pane->pid = 'new-ac843384-d8fa-41c9-967e-0b1159b08d3f';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '<span>Our underwriters are waiting to help…</span>',
      'body' => '<p>Turn to our team to help with your next Agricultural Vehicle risk. With a 20 strong team of new business underwriters, we\'re ready and waiting to provide a fast and efficient service. Simply send your submissions to <a href="mailto:newbusiness@ruralinsurance.co.uk">newbusiness@ruralinsurance.co.uk</a> or speak to your <a href="/contact/regional-contacts">regional contact</a> for further information.</p>
',
      'format' => 'ckeditor',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'agricultural-vehicle underwriters',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'ac843384-d8fa-41c9-967e-0b1159b08d3f';
    $display->content['new-ac843384-d8fa-41c9-967e-0b1159b08d3f'] = $pane;
    $display->panels['middle'][5] = 'new-ac843384-d8fa-41c9-967e-0b1159b08d3f';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = 'new-ecf080cd-542d-4b25-b4b5-66adb0423298';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['aggricultural_vehicles_campaign'] = $page;

  return $pages;

}
