<?php
/**
 * @file
 * rural_unity.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function rural_unity_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-broker-area-menu_:<view>.
  $menu_links['menu-broker-area-menu_:<view>'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => '<view>',
    'router_path' => '<view>',
    'link_title' => '',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 6052,
        'type' => 'view',
        'original_path' => '',
        'view' => array(
          'name' => 'broker_news_promotions',
          'display' => 'broker_news_promotions_menu_pane',
          'arguments' => '',
          'settings' => array(
            'wrapper_classes' => 'menu-views latest-from-rural',
            'breadcrumb' => 1,
            'breadcrumb_title' => '',
            'breadcrumb_path' => '<front>',
            'title' => 1,
            'title_wrapper' => 0,
            'title_classes' => '',
            'title_override' => '',
          ),
        ),
      ),
      'alter' => TRUE,
      'identifier' => 'menu-broker-area-menu_:<view>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_agency-agreement:unity/documentation/agency-agreement.
  $menu_links['menu-broker-area-menu_agency-agreement:unity/documentation/agency-agreement'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/documentation/agency-agreement',
    'router_path' => 'unity/documentation',
    'link_title' => 'Agency agreement',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'agency-agreement',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_agency-agreement:unity/documentation/agency-agreement',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
  );
  // Exported menu link: menu-broker-area-menu_claims-forms:unity/documentation/claims-forms.
  $menu_links['menu-broker-area-menu_claims-forms:unity/documentation/claims-forms'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/documentation/claims-forms',
    'router_path' => 'unity/documentation/claims-forms',
    'link_title' => 'Claims forms',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'claims-forms',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_claims-forms:unity/documentation/claims-forms',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
  );
  // Exported menu link: menu-broker-area-menu_covernotes:unity/covernotes.
  $menu_links['menu-broker-area-menu_covernotes:unity/covernotes'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/covernotes',
    'router_path' => 'unity',
    'link_title' => 'Covernotes',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'covernotes',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_covernotes:unity/covernotes',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_documentation:unity/documentation.
  $menu_links['menu-broker-area-menu_documentation:unity/documentation'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/documentation',
    'router_path' => 'unity/documentation',
    'link_title' => 'Documentation',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'documentation',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'alter' => TRUE,
      'identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_key-contacts:unity/key-contacts.
  $menu_links['menu-broker-area-menu_key-contacts:unity/key-contacts'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/key-contacts',
    'router_path' => 'unity/key-contacts',
    'link_title' => 'Key contacts',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'key-contacts',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_key-contacts:unity/key-contacts',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_leave-us-your-feedback:unity/leave-us-your-feedback.
  $menu_links['menu-broker-area-menu_leave-us-your-feedback:unity/leave-us-your-feedback'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/leave-us-your-feedback',
    'router_path' => 'unity/leave-us-your-feedback',
    'link_title' => 'Leave us your feedback',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'leave-us-your-feedback',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_leave-us-your-feedback:unity/leave-us-your-feedback',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_policy-documents:unity/documentation/policy-documents.
  $menu_links['menu-broker-area-menu_policy-documents:unity/documentation/policy-documents'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/documentation/policy-documents',
    'router_path' => 'unity/documentation/policy-documents',
    'link_title' => 'Policy documents',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'policy-documents',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_policy-documents:unity/documentation/policy-documents',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
  );
  // Exported menu link: menu-broker-area-menu_proposals--submissions:unity/documentation/proposal-documents.
  $menu_links['menu-broker-area-menu_proposals--submissions:unity/documentation/proposal-documents'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/documentation/proposal-documents',
    'router_path' => 'unity/documentation/proposal-documents',
    'link_title' => 'Proposals & submissions',
    'options' => array(
      'identifier' => 'menu-broker-area-menu_proposals--submissions:unity/documentation/proposal-documents',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'proposal-documents',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
  );
  // Exported menu link: menu-broker-area-menu_rbm-postcode-checker:unity/rbm-postcode-checker.
  $menu_links['menu-broker-area-menu_rbm-postcode-checker:unity/rbm-postcode-checker'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/rbm-postcode-checker',
    'router_path' => 'unity/rbm-postcode-checker',
    'link_title' => 'RBM postcode checker',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'rbm-postcode-checker',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_rbm-postcode-checker:unity/rbm-postcode-checker',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_risk-guides:unity/documentation/risk-guides.
  $menu_links['menu-broker-area-menu_risk-guides:unity/documentation/risk-guides'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/documentation/risk-guides',
    'router_path' => 'unity/documentation/risk-guides',
    'link_title' => 'Risk guides',
    'options' => array(
      'identifier' => 'menu-broker-area-menu_risk-guides:unity/documentation/risk-guides',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'risk-guides',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
  );
  // Exported menu link: menu-broker-area-menu_smallholders-online:unity/smallholders-online.
  $menu_links['menu-broker-area-menu_smallholders-online:unity/smallholders-online'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'unity/smallholders-online',
    'router_path' => 'unity/smallholders-online',
    'link_title' => 'Smallholders online',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'smallholders-online',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-menu_smallholders-online:unity/smallholders-online',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-menu_unity-documentation-menu-position-rule:menu-position/1.
  $menu_links['menu-broker-area-menu_unity-documentation-menu-position-rule:menu-position/1'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'menu-position/1',
    'router_path' => 'menu-position/%',
    'link_title' => 'Unity: Documentation (menu position rule)',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'class' => array(
          0 => 'menu-position-link',
        ),
      ),
      'identifier' => 'menu-broker-area-menu_unity-documentation-menu-position-rule:menu-position/1',
    ),
    'module' => 'menu_position',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_documentation:unity/documentation',
  );
  // Exported menu link: menu-broker-area-menu_unity-latest-from-rural-menu-position-rule:menu-position/2.
  $menu_links['menu-broker-area-menu_unity-latest-from-rural-menu-position-rule:menu-position/2'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'link_path' => 'menu-position/2',
    'router_path' => 'menu-position/%',
    'link_title' => 'Unity: Latest From Rural (menu position rule)',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'class' => array(
          0 => 'menu-position-link',
        ),
      ),
      'identifier' => 'menu-broker-area-menu_unity-latest-from-rural-menu-position-rule:menu-position/2',
    ),
    'module' => 'menu_position',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-broker-area-menu_:<view>',
  );
  // Exported menu link: menu-broker-area-top-menu_home:unity.
  $menu_links['menu-broker-area-top-menu_home:unity'] = array(
    'menu_name' => 'menu-broker-area-top-menu',
    'link_path' => 'unity',
    'router_path' => 'unity',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'broker-area-home',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-top-menu_home:unity',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-top-menu_pre-register-now:unity/register.
  $menu_links['menu-broker-area-top-menu_pre-register-now:unity/register'] = array(
    'menu_name' => 'menu-broker-area-top-menu',
    'link_path' => 'unity/register',
    'router_path' => 'unity',
    'link_title' => 'Pre-register now',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'identifier' => 'menu-broker-area-top-menu_pre-register-now:unity/register',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-broker-area-top-menu_what-is-spanunityspan:unity/what-is-unity.
  $menu_links['menu-broker-area-top-menu_what-is-spanunityspan:unity/what-is-unity'] = array(
    'menu_name' => 'menu-broker-area-top-menu',
    'link_path' => 'unity/what-is-unity',
    'router_path' => 'unity',
    'link_title' => 'What is <span>Unity</span>?',
    'options' => array(
      'html' => 1,
      'identifier' => 'menu-broker-area-top-menu_what-is-spanunityspan:unity/what-is-unity',
      'attributes' => array(
        'class' => array(
          0 => 'what-is-unity',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Agency agreement');
  t('Claims forms');
  t('Covernotes');
  t('Documentation');
  t('Home');
  t('Key contacts');
  t('Leave us your feedback');
  t('Policy documents');
  t('Pre-register now');
  t('Proposals & submissions');
  t('RBM postcode checker');
  t('Risk guides');
  t('Smallholders online');
  t('Unity: Documentation (menu position rule)');
  t('Unity: Latest From Rural (menu position rule)');
  t('What is <span>Unity</span>?');

  return $menu_links;
}
