<?php
/**
 * @file
 * rural_unity.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function rural_unity_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-broker-area-menu.
  $menus['menu-broker-area-menu'] = array(
    'menu_name' => 'menu-broker-area-menu',
    'title' => 'Broker Area Menu',
    'description' => '',
  );
  // Exported menu: menu-broker-area-top-menu.
  $menus['menu-broker-area-top-menu'] = array(
    'menu_name' => 'menu-broker-area-top-menu',
    'title' => 'Broker Area Top Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Broker Area Menu');
  t('Broker Area Top Menu');

  return $menus;
}
