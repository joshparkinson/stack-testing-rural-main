(function($){

    $(document).ready(function(){

        if($('.collapsible').length){
            $(".collapsible").each(function(index, pane){
                processCollapse($(this), false);
            });

            $(".collapsible .pane-title").click(function(){
                processCollapse($(this), true);
            });
        }



    });

    function processCollapse(element, animate){
        var collapsiblePane = $(element).closest(".collapsible");
        var collapsibleTitle = collapsiblePane.find(".pane-title");
        var collapsibleContent = collapsiblePane.find(".pane-content");

        if(animate){
            if(collapsibleContent.is(':visible')){
                collapsibleContent.slideUp('slow').removeClass('open').addClass('closed');
                collapsibleTitle.removeClass('open').addClass('closed');
                collapsiblePane.removeClass('open').addClass('closed');
            }
            else{
                collapsibleContent.slideDown('slow').removeClass('closed').addClass('open');
                collapsibleTitle.removeClass('closed').addClass('open');
                collapsiblePane.removeClass('closed').addClass('open');
            }
        }
        else{
            if(collapsibleContent.is(':visible')){
                collapsibleContent.removeClass('closed').addClass('open');
                collapsibleTitle.removeClass('closed').addClass('open');
                collapsiblePane.removeClass('closed').addClass('open');
            }
            else{
                collapsibleContent.removeClass('open').addClass('closed');
                collapsibleTitle.removeClass('open').addClass('closed');
                collapsiblePane.removeClass('open').addClass('closed');
            }
        }
    }

})(jQuery);