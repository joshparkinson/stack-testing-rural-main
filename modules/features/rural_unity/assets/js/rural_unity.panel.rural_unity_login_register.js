(function($){

    var tabId;
    var tabHeight;
    var forgotPassword;
    var fotgotPasswordAjaxOutputClass;

    $(document).ready(function(){
        tabId = '#rural_unity_login_register-responsive-tabs';
        tabHeight = $(tabId).height() + 2;
        forgotPassword = '#rural_unity_login_register-responsive-tabs .forgot-password';
        fotgotPasswordAjaxOutputClass = 'fgtp-output';
        
        _initialiseForgotPasswordContent(fotgotPasswordAjaxOutputClass);

        $(forgotPassword).find('a').click(function(e){
            e.preventDefault();
            $('.fgtp-hide').fadeOut();
            $(forgotPassword).animate({
                'padding': 0,
                'height': tabHeight + 'px'
            }, function(){
                $(forgotPassword + ' .' + fotgotPasswordAjaxOutputClass).fadeIn();
            });
        });
    });

    $(document).ajaxComplete(function(){
        $(forgotPassword + ' .fgtp-output .close').click(function(e){
            $(forgotPassword + ' .' + fotgotPasswordAjaxOutputClass).fadeOut();
            $(forgotPassword).animate({
                'padding': '20px 0',
                'height': '22px'
            }, function(){
                $('.fgtp-hide').fadeIn();
                $('.' + fotgotPasswordAjaxOutputClass + ' #ajax-errors').remove();
            });
        });
    });

    function _initialiseForgotPasswordContent(outputClass){

        $(forgotPassword + ' p').wrap('<div class="fgtp-hide"></div>');

        $.ajax({
            type: 'POST',
            url: '/ajax/unity/forgot-password/load',
            dataType: 'json',
            success: function(data) {
                $(forgotPassword).append('<div class="' + outputClass + '">' + data.output + '</div>');
                $(forgotPassword).append(data.settings);
                Drupal.attachBehaviors();
            }
        });

    }

})(jQuery);