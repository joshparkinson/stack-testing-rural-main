(function($) {
    Drupal.behaviors.rural_unity_notifications_check = {
        attach: function(context, settings) {

            //Trigger the minute check for notifications
            $('body').once('notifications-check', function(){
                checkNotifications();
                setInterval(function(){
                    checkNotifications()
                }, 60000);
            });

            //Check the notifications
            function checkNotifications(){
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/ajax/unity/notifications/check',
                    dataType: 'json',
                    success: function(response){
                        if(response.status == "success"){

                            //Set the unread notifications count
                            $("#activity-toggle .activity-count").html(response.response.count);

                            //If the activity loader is still in the hover box hide and remove it
                            if($("#latest-activity #activity-loader").length){
                                $("#latest-activity #activity-loader").hide().remove();
                            }

                            //Set the list of alerts/notifications
                            $("#latest-activity ul").html(response.response.output);

                            //If we're on the activity page and the unread-alerts-counter is available
                            //Set the count and then show the counter if more than 0
                            if($(".unread-alerts-counter").length){
                                $(".unread-alerts-counter .unread-alerts span").html(response.response.count);
                                if(response.response.count > 0){
                                    $(".unread-alerts-counter").fadeIn("slow");
                                }
                            }
                        }

                    }
                });
            }

        }
    };
})(jQuery);