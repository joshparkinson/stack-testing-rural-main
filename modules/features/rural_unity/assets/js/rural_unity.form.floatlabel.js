(function($){

    $(document).ready(function(){
        $("#user-login-form input").floatlabel({
            'labelClass': 'float-label'
        });
        $("#unity-registration-entityform-edit-form input").floatlabel({
            'labelClass': 'float-label'
        });
    });

    $(document).ajaxComplete(function(){
        $("#user-pass input").floatlabel({
            'labelClass': 'float-label'
        });
    });


})(jQuery);
