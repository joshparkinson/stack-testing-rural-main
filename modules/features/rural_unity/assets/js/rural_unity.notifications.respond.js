(function($) {
    Drupal.behaviors.rural_unity_notifications_respond = {
        attach: function(context, settings) {

            //Initialis script vars
            var hover_delay = 800; //Milliseconds
            var hide_timeout;

            $(document).ready(function(){

                $("#activity-toggle, #latest-activity").hover(
                    function(){
                        clearTimeout(hide_timeout);
                        $("#latest-activity").fadeIn("fast");
                    },
                    function(){
                        clearTimeout(hide_timeout);
                        hide_timeout = setTimeout(function(){
                            $("#latest-activity").fadeOut("fast");
                        }, hover_delay);
                    }
                );

                if($(".view-id-unity_alerts.view-display-id-main_page_pane").length){
                    $(".view-id-unity_alerts.view-display-id-main_page_pane .views-row.unread .views-field-title a").unbind('mousedown').on('mousedown', function(){
                        _viewNotification($(this).closest('.views-row').find('.notification-id').data('notification-id'));
                    });
                }

            });

            //On ajax callback completion attach the notification link handlers
            $(document).ajaxComplete(function(){
                $("#latest-activity li.unread a").unbind('mousedown').on('mousedown', function(){
                    _viewNotification($(this).closest('li').data('notification-id'));
                });
            });

            //View a notification
            //Fires an ajax call to register the notification/alert as seen.
            function _viewNotification(nid){
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/ajax/unity/notifications/read',
                    dataType: 'json',
                    data: {
                        nid: nid
                    },
                    success: function(response){
                        //console.log(response);
                    }
                });
            }

        }
    };
})(jQuery);
