<?php
/**
 * @file
 * rural_unity.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rural_unity_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'view any unity_notification content'.
  $permissions['view any unity_notification content'] = array(
    'name' => 'view any unity_notification content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node_view_permissions',
  );

  // Exported permission: 'view own unity_notification content'.
  $permissions['view own unity_notification content'] = array(
    'name' => 'view own unity_notification content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node_view_permissions',
  );

  return $permissions;
}
