<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

function rural_unity_install(){

    //Check broker role exists
    if(!user_role_load_by_name('broker')){
        $brokerRole = new stdClass();
        $brokerRole->name = 'broker';
        user_role_save($brokerRole);
    }

    //Checker broker_onboarded role exists
    if(!user_role_load_by_name('broker_onboarded')){
        $brokerOnboardedRole = new stdClass();
        $brokerOnboardedRole->name = 'broker_onboarded';
        user_role_save($brokerOnboardedRole);
    }

}

/**
 * Implements hook_schema().
 * Defines module/feature dependent database tables.
 */
function rural_unity_schema(){

    /**
     * Unity registrants/applicants table
     */

    $schema['unity_user_registrants'] = array(
        'description' => 'Holds Unity registrant data',
        'fields' => array(
            'id' => array(
                'description' => 'Registrant ID',
                'type' => 'serial',
                'not null' => TRUE,
            ),
            'email' => array(
                'description' => 'Registrant email address',
                'type' => 'varchar',
                'length' => '120',
                'not null' => TRUE,
            ),
            'first_name' => array(
                'description' => 'Registrant first name',
                'type' => 'varchar',
                'length' => '120',
                'not null' => TRUE,
            ),
            'last_name' => array(
                'description' => 'Registrant last name',
                'type' => 'varchar',
                'length' => '120',
                'not null' => TRUE,
            ),
            'company_name' => array(
                'description' => 'Registrant company name',
                'type' => 'varchar',
                'length' => '120',
                'not null' => TRUE,
            ),
            'town_city' => array(
                'description' => 'Registrant town/city',
                'type' => 'varchar',
                'length' => '120',
                'not null' => TRUE,
            ),
            'created' => array(
                'description' => 'The date the registrant data was submitted',
                'type' => 'int',
                'not null' => TRUE,
            ),
            'active' => array(
                'description' => 'Is the registrant application active. 1 = acitve, 0 = closed',
                'type' => 'int',
                'not null' => TRUE,
                'default' => 1,
            ),
            'status' => array(
                'description' => 'Registrant application status. p = pending, v = verified, r = rejected',
                'type' => 'varchar',
                'length' => '1',
                'not null' => TRUE,
                'default' => 'p',
            ),
            'user_id' => array(
                'description' => 'The user account id this data was used to create if verified',
                'type' => 'int',
                'not null' => FALSE,
            ),
        ),
        'primary key' => array('id'),
    );
    
    /**
     * Unity notifications views
     */

    $schema['unity_notifications_views'] = array(
        'description' => 'Holds notifcation view data',
        'fields' => array(
            'id' => array(
                'description' => 'The unique id for the notification view',
                'type' => 'serial',
                'not null' => TRUE,
            ),
            'notification_id' => array(
                'description' => 'The ID of the notication being viewed',
                'type' => 'int',
                'not null' => TRUE,
            ),
            'user_id' => array(
                'description' => 'The ID of the user viewing the notification',
                'type' => 'int',
                'not null' => TRUE,
            ),
        ),
        'primary key' => array('id'),
    );


    return $schema;
}