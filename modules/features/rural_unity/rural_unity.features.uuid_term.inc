<?php
/**
 * @file
 * rural_unity.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function rural_unity_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Underwriting Support',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 3,
    'uuid' => '15b40c92-5226-4e0a-b094-8a480c6e0c88',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Finance',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 10,
    'uuid' => '3e938b16-08bb-429e-863e-34aa87e48ac7',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'New Business Underwriting',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 0,
    'uuid' => '48b6d377-a6bd-4241-a051-dc788a083d03',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Claims',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 5,
    'uuid' => '65221525-5d30-421c-a991-203a23d13c09',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Accounts',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 6,
    'uuid' => '689e5dda-3c76-4184-9122-7793626ba2f8',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Business Development',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 4,
    'uuid' => '765175bb-dbff-4a17-b755-e893cd44c8c9',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Development Underwriters',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 2,
    'uuid' => '76ff1237-c0b0-4aea-8c90-53472ec8c09c',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Agency',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 7,
    'uuid' => '9237e235-44c2-4cff-a23f-d4b5175981d0',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Sales',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 9,
    'uuid' => 'b01a0fbb-4a8f-49f2-aec6-a97a3bae5df9',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Complaints',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 0,
    'uuid' => 'e8d92efd-a8ec-41a1-b852-a0f77f2080db',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Directors',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 8,
    'uuid' => 'ef51b687-0604-49a7-9f2f-c361a6d8309d',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Existing Business Underwriting',
    'description' => '',
    'format' => 'ckeditor',
    'weight' => 1,
    'uuid' => 'f5c7dc88-d180-4c8b-b01b-fd7846543f5f',
    'vocabulary_machine_name' => 'departments',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
