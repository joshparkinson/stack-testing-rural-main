<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Rural unity registrants main page callback
 * - Display the stats table
 * - Add the main registrant table
 *
 * @return string The html to output the page
 *
 */
function rural_unity_registrants_main_page(){
    $output = _get_registrants_stats_table();
    $output .= _get_registrants_main_table();
    return $output;
}