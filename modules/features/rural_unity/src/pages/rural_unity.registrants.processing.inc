<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Registrant processing page
 *
 * @param int $registrantId The id of the registrant data
 *
 * @return string The page html output
 */
function rural_unity_registrant_processing_page($registrantId){
    module_load_include('inc', 'rural_unity', 'src/forms/rural_unity.form.registrant_processing');

    //Fetch registrant data from url id
    $query = db_select('unity_user_registrants', 'u');
    $query->fields('u');
    $query->condition('id', intval($registrantId), '=');
    $registrantData = $query->execute()->fetchAssoc();

    //If empty, no registrant with passed id found, throw 404
    if(empty($registrantData)){
        drupal_not_found();
        drupal_exit();
    }

    //If registrant application pending show form
    $output = drupal_get_form('rural_unity_registrant_processing_form', $registrantData);
    return drupal_render($output);
}