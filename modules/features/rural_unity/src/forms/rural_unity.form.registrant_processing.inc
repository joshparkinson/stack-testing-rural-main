<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_form()
 * Define the form used to process registrant data
 *
 * @param array $form           The drupal form building array
 * @param array $form_state     The drupal form state array
 * @param array $registrantData The registrant data array (sent via page callback)
 *
 * @return array The drupal form definition array
 */
function rural_unity_registrant_processing_form($form, &$form_state, $registrantData){

    $form['first_name'] = array(
        '#type' => 'textfield',
        '#title' => 'First name',
        '#default_value' => $registrantData['first_name'],
        '#disabled' => ($registrantData['status'] == 'v') ? TRUE : FALSE,
        '#required' => TRUE
    );

    $form['last_name'] = array(
        '#type' => 'textfield',
        '#title' => 'Last name',
        '#default_value' => $registrantData['last_name'],
        '#disabled' => ($registrantData['status'] == 'v') ? TRUE : FALSE,
        '#required' => TRUE
    );

    $form['email'] = array(
        '#type' => 'textfield',
        '#title' => 'Email address',
        '#default_value' => $registrantData['email'],
        '#disabled' => ($registrantData['status'] == 'v') ? TRUE : FALSE,
        '#required' => TRUE
    );

    $form['company_name'] = array(
        '#type' => 'textfield',
        '#title' => 'Company name',
        '#default_value' => $registrantData['company_name'],
        '#disabled' => ($registrantData['status'] == 'v') ? TRUE : FALSE,
        '#required' => TRUE
    );

    $form['town_city'] = array(
        '#type' => 'textfield',
        '#title' => 'Town/City',
        '#default_value' => $registrantData['town_city'],
        '#disabled' => ($registrantData['status'] == 'v') ? TRUE : FALSE,
        '#required' => TRUE
    );

    $form['action'] = array(
        '#type' => 'select',
        '#title' => 'Action',
        '#options' => array(
            'v' => 'Verify Account',
            'r' => 'Reject Account'
        ),
        '#disabled' => ($registrantData['status'] == 'v') ? TRUE : FALSE,
        '#required' => TRUE
    );

    //Create system settings form
    $form = system_settings_form($form);
    $form['#validate'] = array('rural_unity_registrant_processing_form_validate');
    $form['#submit'] = array('rural_unity_registrant_processing_form_submit');
    $form['actions']['submit']['#value'] = 'Process registrant';

    //Add registrant ID to form state for processing
    $form_state['unity'] = array(
        'registrantID' => $registrantData['id']
    );

    //If a decision hasn't been made we can return the form in its current state
    //There no further processing to do.
    if($registrantData['status'] == 'p'){
        return $form;
    }

    //Set the chosen action as the default for the action default
    $form['action']['#default_value'] = $registrantData['status'];

    //Decision has been made so kill of the form submit button
    unset($form['actions']['submit']);

    //Add in the message outlining what has happened
    $form['actions']['outcome'] = array(
        '#markup' => strtr(
            '<p>The following registrant has been: <span class=":class">:outcome</span></p>',
            array(
                ':class' => ($registrantData['status'] == 'v') ? 'verified' : 'rejected',
                ':outcome' => ($registrantData['status'] == 'v') ? 'Verified' : 'Rejected'
            )
        )
    );

    //If the user was verfieid provide link to there user account
    if($registrantData['status'] == 'v'){
        $form['actions']['user_link'] = array(
            '#markup' => strtr(
                '<div class="user-link"><a href="/:link">View user profile</a></div>',
                array(':link' => drupal_get_path_alias('user/'.$registrantData['user_id']))
            )
        );
    }

    return $form;

}

/**
 * Implements hook_form_validate().
 * Validate the registrant data processing form
 *
 * @param array $form       The drupal form building array
 * @param array $form_state The drupal form state array
 */
function rural_unity_registrant_processing_form_validate($form, &$form_state){
    //Ensure registrant id was passed succesfully
    if(empty($form_state['unity']['registrantID'])){
        form_set_error('error', 'Error submitting data, please refresh and try again.');
    }
    //Ensure action is valid
    if($form_state['values']['action'] != 'v' && $form_state['values']['action'] != 'r'){
        form_set_error('action', 'Invalid action');
    }
}

/**
 * Implements hook_form_submit().
 * Process a successful submission of the registrant data processing form
 *
 * @param array $form       The drupal form building array
 * @param array $form_state The drupal form state array
 */
function rural_unity_registrant_processing_form_submit($form, &$form_state){

    //Grab registrant data
    $registrantId = $form_state['unity']['registrantID'];
    $firstName = $form_state['values']['first_name'];
    $lastName = $form_state['values']['last_name'];
    $email = $form_state['values']['email'];
    $companyName = $form_state['values']['company_name'];
    $townCity = $form_state['values']['town_city'];
    $action = $form_state['values']['action'];

    //Update registrant data row with sanitised data
    //Set action
    //Set as no longer active
    $updateQuery = db_update('unity_user_registrants');
    $updateQuery->fields(array(
        'email' => $email,
        'first_name' => $firstName,
        'last_name' => $lastName,
        'company_name' => $companyName,
        'town_city' => $townCity,
        'active' => 0,
        'status' => $action
    ));
    $updateQuery->condition('id', $registrantId, '=');
    $updateQuery->execute();

    //Rejecting the users application, send rejection email
    if($action == 'r'){
        drupal_set_message('Please contact the broker/applicant explaining why this application has been rejected');
        return;
    }

    //Verifiying the users application
    //Create the user
    //Generate one time login link
    //Send the link to the end user
    if($action == 'v'){

        //Load broker role
        $brokerRole = user_role_load_by_name('broker');

        //Create the user
        $userData = array(
            'name' => 'ruTempBrokerName',
            'mail' => $email,
            'pass' => user_password(20),
            'status' => 1,
            'init' => 'email address',
            'field_first_name' => array(LANGUAGE_NONE => array(array('value' => $firstName))),
            'field_last_name' => array(LANGUAGE_NONE => array(array('value' => $lastName))),
            'field_company_name' => array(LANGUAGE_NONE => array(array('value' => $companyName))),
            'field_region' => array(LANGUAGE_NONE => array(array('value' => $townCity))),
            'roles' => array(
                $brokerRole->rid => 'broker'
            )
        );
        $newUserAccount = user_save(NULL, $userData);

        //Fix user name
        $fixedName = preg_replace('/@.*$/', '', $email);
        $newUserAccount->name = email_registration_cleanup_username($fixedName, $newUserAccount->uid);
        user_save($newUserAccount);

        //Add the user id to the verified registrants row
        $updateQuery = db_update('unity_user_registrants');
        $updateQuery->fields(array(
            'user_id' => $newUserAccount->uid
        ));
        $updateQuery->condition('id', $registrantId, '=');
        $updateQuery->execute();

        /* @TODO: send this to the new user */
        //dpm($newUserAccount);

        $passwordResetLink = user_pass_reset_url($newUserAccount);

        $emailParams = array(
            'firstname' => $firstName,
            'reglink' => $passwordResetLink
        );

        drupal_mail(
            'rural_unity',                               //Module
            'broker_verification_external_notification', //Mail key
            $email,                                      //To
            'en',                                        //Language
            $emailParams                                //Email parameters
        );
    

    }


}