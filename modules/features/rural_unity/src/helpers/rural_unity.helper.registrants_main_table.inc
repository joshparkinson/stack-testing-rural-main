<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

function _get_registrants_main_table(){

    //Fetch all registrants from database
    $query = db_select('unity_user_registrants', 'u');
    $query->addField('u', 'id');
    $query->addField('u', 'first_name');
    $query->addField('u', 'last_name');
    $query->addField('u', 'company_name');
    $query->addField('u', 'status');
    $query->orderBy('created', 'DESC');
    $result = $query->execute();

    //Build table header
    $header = array(
        array('data' => 'Name', 'class' => 'name'),
        array('data' => 'Company', 'class' => 'company'),
        array('data' => 'Status', 'class' => 'status'),
        array('data' => 'Actions', 'class' => 'actions')
    );

    //Add table rows from fetched db data
    $rows = array();
    while($record = $result->fetchAssoc()){

        //Generate status
        $status = 'Unknown';
        $linkText = 'View';
        switch($record['status']){
            case 'p':
                $status = 'pending';
                $linkText = 'Process';
                break;
            case 'v':
                $status = 'verified';
                break;
            case 'r':
                $status = 'rejected';
                break;
        }

        $rows[] = array(
            'data' => array(
                array('data' => $record['first_name'].' '.$record['last_name']),
                array('data' => $record['company_name']),
                array('data' => ucfirst($status)),
                array('data' => l($linkText, '/admin/people/unity/registrants/'.$record['id']))
            ),
            'class' => array('status-'.$status)
        );
    }

    if(empty($rows)){
        $rows[] = array(
            'data' => array(
                array('data' => 'No registrants found.', 'colspan' => 4)
            ),
            'class' => array('table-empty')
        );
    }

    return theme('table', array('header' => $header, 'rows' => $rows));

}