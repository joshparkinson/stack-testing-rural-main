<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 20/06/2016
 * Time: 09:39
 */

function _get_registrants_stats_table(){

    $result = db_query("SELECT 
	    sum(case when active = 1 then 1 else 0 end) as open,
        sum(case when active = 0 then 1 else 0 end) as closed,
        sum(case when status = 'p' then 1 else 0 end) as pending,
        sum(case when status = 'v' then 1 else 0 end) as verified,
        sum(case when status = 'r' then 1 else 0 end) as rejected
        FROM unity_user_registrants")->fetchAssoc();

    //Build table header
    $header = array(
        array('data' => 'Open', 'class' => 'open'),
        array('data' => 'Closed', 'class' => 'closed'),
        array('data' => 'Pending', 'class' => 'pending'),
        array('data' => 'Verified', 'class' => 'verified'),
        array('data' => 'Rejected', 'class' => 'rejected')
    );

    $rows = array(array(
        (!empty($result['open'])) ? $result['open'] : 0,
        (!empty($result['closed'])) ? $result['closed'] : 0,
        (!empty($result['pending'])) ? $result['pending'] : 0,
        (!empty($result['verified'])) ? $result['verified'] : 0,
        (!empty($result['rejected'])) ? $result['rejected'] : 0
    ));

    $caveat = '<p style="font-size:10px;position:relative;top:-14px;">
    Note: If taking cumlative stats from the above table please
    add <strong>154</strong> to the total registrants (open + closed) and 
    <strong>150</strong> to the verified value.';

    return theme('table', array('header' => $header, 'rows' => $rows)).$caveat;

}