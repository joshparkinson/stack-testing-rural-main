<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_permission().
 * Defines permissions implemented by this feature.
 *
 * @return array An array of permission definitions.
 */
function rural_unity_permission(){
    return array(
        'process unity registrants' => array(
            'title' => t('Process Unity Registrants'),
            'description' => t('Can verify and create user accounts for unity registrants, or reject them.')
        )
    );
}