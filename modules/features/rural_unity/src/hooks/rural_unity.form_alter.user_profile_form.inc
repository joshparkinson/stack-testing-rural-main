<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rural_unity_form_user_profile_form_alter(&$form, &$form_state, $form_id){
    //Get the current user global
    //If current user isnt an admin, hide username and email fields
    global $user;
    if(!in_array('admin', $user->roles)){
        $form['account']['name']['#access'] = FALSE;
        $form['account']['mail']['#access'] = FALSE;
        $form['field_company_name']['#access'] = FALSE;
        $form['account']['current_pass']['#description'] = 'Enter your current password to change the password for your account.';
    }
}