<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * FEEDBACK FORM
 */

function rural_unity_form_unity_initial_feedback_form_entityform_edit_form_alter(&$form, &$form_state, $form_id){
    $form['#attributes']['autocomplete'] = 'off';
    $form['actions']['submit']['#ajax'] = array(
        'callback' => 'rural_unity_feedback_entityform_callback',
        'method' => 'replace',
        'effect' => 'fade'
    );
}

function rural_unity_feedback_entityform_callback($form, &$form_state){
    $commands = array();

    //If the form has errors, get them, display them
    if(form_get_errors()){
        $form_state['rebuild'] = TRUE;
        $commands[] = ajax_command_replace('#ajax-errors', '');
        $commands[] = ajax_command_prepend('#unity-initial-feedback-form-entityform-edit-form', '<div id="ajax-errors" class="ajax-form-response">'.theme('status_messages').'</div>');
        drupal_get_messages();
        return array('#type' => 'ajax', '#commands' => $commands);
    }
    drupal_get_messages();

    $output = '<div class="feedback-success"><p>Thanks for your feedback – we appreciate it and will get back to you if required.</p></div>';
    $commands[] = ajax_command_replace('#unity-initial-feedback-form-entityform-edit-form', $output);

    return array('#type' => 'ajax', '#commands' => $commands);
}