<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rural_unity_form_user_pass_alter(&$form, &$form_state, $form_id){
    $form['actions']['submit']['#value'] = 'Submit';
    if(current_path() != 'user/password'){
        $form['name']['#attributes']['placeholder'] = 'Email';

        $form['actions']['submit']['#ajax'] = array(
            'callback' => 'rural_unity_user_pass_form_callback',
            'method' => 'replace',
            'effect' => 'fade'
        );

        $form['actions']['submit_forgot_pass'] = $form['actions']['submit'];
        unset($form['actions']['submit']);
    }
}

/**
 * User password reset form ajax callback for unity entry page
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax commands to carry out
 */
function rural_unity_user_pass_form_callback($form, &$form_state){
    $commands = array();

    //If the form has errors get them display them
    if(form_get_errors()){
        $form_state['rebuild'] = TRUE;
        $commands[] = ajax_command_replace('#ajax-errors', '');
        $commands[] = ajax_command_prepend('#user-pass', '<div id="ajax-errors" class="ajax-form-response">'.theme('status_messages').'</div>');
        drupal_get_messages();
        return array('#type' => 'ajax', '#commands' => $commands);
    }

    drupal_get_messages();
    $email = $form_state['values']['name'];
    $output = '<div class="user-pass-feedback-success" style="height:213px;"><h2>Thank you.</h2><p>We’ll send a new link to '.$email.' shortly.</div>';
    $commands[] = ajax_command_replace('#user-pass', $output);
    return array('#type' => 'ajax', '#commands' => $commands);
}