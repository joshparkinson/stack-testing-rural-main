<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_mail().
 *
 * @param string $key     The identifier of the mail
 * @param array  $message An array of message components to be filled in
 * @param array  $params  An array of parameters supplied by the caller of drupal mail
 */
function rural_unity_mail($key, &$message, $params){

    //New registrant internal notification
    if($key == 'new_registrant_internal_notification'){
        $message['subject'] = 'Unity: New broker registration';
        $message['body'] = _rural_unity_get_email_template_contents($key, array(
            '[@brokercompanyname]' => $params['company'],
            '[@registrantlink]' => $params['link']
        ));
    }

    //Broker verification external notification email
    //This is the email sent to brokers when their accounts has been
    //confirmed by existing business / business support etc etc
    if($key == 'broker_verification_external_notification'){
        $message['subject'] = 'Activate your Unity account now';
        $message['body'] = _rural_unity_get_email_template_contents($key, array(
            '[@firstname]' => $params['firstname'],
            '[@reglink]' => $params['reglink']
        ));
    }

    //Marketing onboarding notification email
    if($key == 'marketing_onboarding_notification'){
        $user = user_load($params['uid']);
        $message['subject'] = 'Unity: New broker verfied. Begin onboarding';
        $message['body'] = _rural_unity_get_email_template_contents($key, array(
            '[@email]' => $user->mail,
            '[@firstname]' => (!empty($user->field_first_name['und'][0]['safe_value'])) ? $user->field_first_name['und'][0]['safe_value'] : 'Unknown',
            '[@lastname]' => (!empty($user->field_last_name['und'][0]['safe_value'])) ? $user->field_last_name['und'][0]['safe_value'] : 'Unknown',
            '[@companyname]' => (!empty($user->field_company_name['und'][0]['safe_value'])) ? $user->field_company_name['und'][0]['safe_value'] : 'Unknown',
            '[@towncity]' => (!empty($user->field_region['und'][0]['safe_value'])) ? $user->field_region['und'][0]['safe_value'] : 'Unknown',
        ));
    }

}

/**
 * Helper function to load and return
 *
 * @param string $templateKey  The email template key
 * @param array  $replacements A key value pair array of replacement tokens and values
 *
 * @return string The email contents
 */
function _rural_unity_get_email_template_contents($templateKey, $replacements = array()){
    $templatePath = drupal_get_path('module', 'rural_unity').'/templates/email';
    $templateContents = file_get_contents($templatePath.'/'.$templateKey.'.html');
    return strtr($templateContents, $replacements);
}