<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_user_login()
 * - Fires after use login
 * - Catch user login, check if we need to onboard brokers
 *
 * @param array    $edit    An array of form values submitted by the user.
 * @param stdClass $account The user object for the user being logged in
 *
 * @return bool
 */
function rural_unity_user_login(&$edit, $account){

    //Only handling broker logins
    if(!in_array('broker', $account->roles)){
        return FALSE;
    }

    //Nothing to do if broker already onboarded
    if(in_array('broker', $account->roles) && in_array('broker_onboarded', $account->roles)){
        return FALSE;
    }

    //If this is reacher, the user must have the broker role but not have the onboarded role.
    //Add that role so this check doesnt pass again
    $brokerOnboardedRole = user_role_load_by_name('broker_onboarded');
    user_multiple_role_edit(array($account->uid), 'add_role', $brokerOnboardedRole->rid);

    //drupal_get_messages();
    //dpm("ONBOARD USER NOW!");
    //drupal_set_message('Please change your password below and you\'re all set.');

    $emailParams = array(
        'uid' => $account->uid
    );

    drupal_mail(
        'rural_unity',                               //Module
        'marketing_onboarding_notification',         //Mail key
        'marketing@ruralinsurance.co.uk',            //To
        'en',                                        //Language
        $emailParams                                 //Email parameters
    );

}