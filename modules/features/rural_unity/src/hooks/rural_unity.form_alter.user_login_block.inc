<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rural_unity_form_user_login_block_alter(&$form, &$form_state, $form_id){

    if(current_path() != 'unity'){
        return FALSE;
    }

    //Update username/password labels/placeholders
    $form['name']['#title'] = 'Email';
    $form['name']['#attributes']['placeholder'] = 'Email';
    $form['pass']['#attributes']['placeholder'] = 'Password';

    //Add the intro text to the login form
    $nameIndex = array_search('name', array_keys($form));
    $insertIntro = array(
        'intro' => array(
            '#markup' => '<p class="intro">Please enter your details below</p>'
        )
    );
    $formEnd = array_splice($form, $nameIndex);
    $formStart = array_splice($form, 0, $nameIndex);
    $form = array_merge($formStart, $insertIntro, $formEnd);

    //Add the forgot password link after the submit button
    $form['actions']['submit']['#suffix'] = '<div class="forgot-password">
            <p>Forgot your password? <a href="/user/password">Click here</a></p>
        </div>';

    //Unset drupal default links
    unset($form['links']);

    //Ajaxify
    $form['actions']['submit']['#ajax'] = array(
        'callback' => 'rural_unity_user_login_form_callback',
        'method' => 'replace',
        'effect' => 'fade'
    );

    //Add the required javascript
    drupal_add_js(drupal_get_path('module', 'rural_unity').'/assets/js/rural_unity.form.user_login.js');
    drupal_add_js(drupal_get_path('module', 'rural_unity').'/assets/js/rural_unity.panel.rural_unity_login_register.js');

}

/**
 * User login form ajax callback for unity entry page
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal form state array
 *
 * @return array An array of ajax commands to carry out
 */
function rural_unity_user_login_form_callback($form, &$form_state){
    $commands = array();

    //If the form has errors, get them, display them
    if(form_get_errors()){
        $form_state['rebuild'] = TRUE;
        $commands[] = ajax_command_replace('#ajax-errors', '');
        $commands[] = ajax_command_prepend('#user-login-form', '<div id="ajax-errors" class="ajax-form-response">'.theme('status_messages').'</div>');
        drupal_get_messages();
        return array('#type' => 'ajax', '#commands' => $commands);
    }

    ctools_include('ajax');
    $output = '<div class="login-feedback-success" style="height:213px;"><p>Success</p><div class="loader"></div></div>';
    $commands[] = ajax_command_replace('#user-login-form', $output);
    $commands[] = ajax_command_invoke(NULL, 'rural_unity_refresh_page');
    return array('#type' => 'ajax', '#commands' => $commands);
}