<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

function rural_unity_preprocess_page(&$vars){

    if(drupal_match_path(drupal_get_path_alias(), 'unity') && !user_is_logged_in()){
        drupal_add_js(drupal_get_path('module', 'rural_unity').'/assets/js/rural_unity.form.floatlabel.js');
    }

    //If a user hits a unity sub page but isnt logged in redirect them
    //to the unity entry page.. which will show the login/registration form
    if(strpos(drupal_get_path_alias(), 'unity/') !== FALSE && !user_is_logged_in() && drupal_get_path_alias() != 'unity'){
        drupal_goto('unity');
        return;
    }

    if(drupal_match_path(drupal_get_path_alias(), 'user/reset/*')){
        drupal_set_title('Access your account');
        $vars['page']['content']['system_main']['message']['#markup'] = '<p>Click on this button to log in to Unity and change your password.</p>';
    }

    if(drupal_match_path(drupal_get_path_alias(), 'unity') || drupal_match_path(drupal_get_path_alias(), 'unity/*') || strpos(drupal_get_path_alias(), 'unity/') !== FALSE){
        drupal_add_js(drupal_get_path('module', 'rural_unity').'/assets/js/rural_unity.notifications.check.js');
        drupal_add_js(drupal_get_path('module', 'rural_unity').'/assets/js/rural_unity.notifications.respond.js');
    }

}

function rural_unity_preprocess_panels_pane(&$vars){
    /* @TODO: move out of unity.. not unity specific */
    if(!empty($vars['classes_array'])){
        foreach($vars['classes_array'] as $classString){
            if($classString == "collapsible" || strpos($classString, 'collapsible') !== FALSE){
                drupal_add_js(drupal_get_path('module', 'rural_unity').'/assets/js/rural_unity.pane.collapsible.js');
                break;
            }
        }
    }
}