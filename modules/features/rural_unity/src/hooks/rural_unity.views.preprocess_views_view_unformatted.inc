<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

function rural_unity_preprocess_views_view_unformatted(&$vars){

    //Add unread/read classes to the notifications list results
    if($vars['view']->name == "unity_alerts" && $vars['view']->current_display == "main_page_pane" && user_is_logged_in()){
        global $user;

        //Get number unread notification for current user
        $unreadNotifications = db_query("SELECT n.nid
                                     FROM node n 
                                     WHERE NOT EXISTS ( 
                                         SELECT nv.id 
                                         FROM unity_notifications_views nv 
                                         WHERE n.nid = nv.notification_id 
                                         AND nv.user_id = :uid
                                     )
                                     AND n.type = 'unity_notification'", array(':uid' => $user->uid))->fetchAllAssoc('nid');

        //Loop over view result
        //Check whether unread, store row index if it is
        $unreadRows = array();
        foreach($vars['view']->result as $rowIndex => $row){
            if(array_key_exists($row->nid, $unreadNotifications)){
                $unreadRows[] = $rowIndex;
            }
        }

        //Add unread class to all unread rows
        foreach($unreadRows as $rowIndex){
            $vars['classes_array'][$rowIndex] .= ' unread';
        }
    }

}