<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_form_FORM_ID_alter()
 *
 * @param array  $form       Drupal form array
 * @param array  $form_state Drupal form state array
 * @param string $form_id    The form id
 */
function rural_unity_form_unity_registration_entityform_edit_form_alter(&$form, &$form_state, $form_id){
    
    //Dont autocomplete the form
    $form['#attributes']['autocomplete'] = 'off';

    //Add custom validation/submission handlers
    $form['#validate'][] = 'rural_unity_register_entityform_validate';
    $form['actions']['submit']['#submit'][] = 'rural_unity_register_entityform_submit';

    //AJAXify form
    $form['actions']['submit']['#ajax'] = array(
        'callback' => 'rural_unity_register_entityform_callback',
        'method' => 'replace',
        'effect' => 'fade'
    );
}

/**
 * Implements hook_form_validate()
 *
 * @param array $form       The drupal form array
 * @param array $form_state The drupal formstate array
 *
 * @return bool Indicates if validation was passed or not
 */
function rural_unity_register_entityform_validate($form, &$form_state){

    //Grab email address
    //If its empty, end checks, other validation methods will flag this.
    $emailAddress = $form_state['values']['field_email_address']['und'][0]['email'];
    if(empty($emailAddress)){
        return FALSE;
    }

    //Ensure no user currently exists with entered email
    if(user_load_by_mail($emailAddress)){
        form_set_error('email', 'User already exists with provided email address.');
        return FALSE;
    }

    //Ensure no current registration process in place for user with email
    //Check that the registration is active
    //Registrations are marked as no longer active after a decision has been made
    //If a decision has been made and the registration is no longer active, the same email can be used, and this check will pass
    $query = db_select('unity_user_registrants', 'u');
    $query->addField('u', 'id');
    $query->condition('email', strtolower($emailAddress), '=');
    $query->condition('active', 1, '=');
    $result = $query->execute();
    if($result->rowCount() > 0){
        form_set_error('email', 'Registration in progress for provided email address.');
        return FALSE;
    }

    //Checks passed
    return TRUE;

}

/**
 * Implements hook_form_submit()
 *
 * @param $form
 * @param $form_state
 */
function rural_unity_register_entityform_submit($form, &$form_state){

    global $base_url;

    //No errors, form submitted successfully, final processing.
    //Catch all form values
    $emailAddress = $form_state['values']['field_email_address']['und'][0]['email'];
    $firstName = $form_state['values']['field_first_name']['und'][0]['value'];
    $lastName = $form_state['values']['field_last_name']['und'][0]['value'];
    $companyName = $form_state['values']['field_company_name']['und'][0]['value'];
    $townCity = $form_state['values']['field_region']['und'][0]['value'];

    //Add registrant details to the form
    $query = db_insert('unity_user_registrants');
    $query->fields(array(
        'email' => strtolower($emailAddress),
        'first_name' => $firstName,
        'last_name' => $lastName,
        'company_name' => $companyName,
        'town_city' => $townCity,
        'created' => time(),
    ));
    $registrantId = $query->execute();

    //Store email parameters
    $emailParams = array(
        'company' => $companyName,
        'link' => $base_url.'/admin/people/unity/registrants/'.$registrantId
    );

    /* @todo: fix from and to emails */

    //Send email
    drupal_mail(
        'rural_unity',                              //Module
        'new_registrant_internal_notification',     //Mail key
        'enquiries@ruralinsurance.co.uk',           //To
        'en',                                       //Language
        $emailParams                                //Email parameters
    );

}

/**
 * AJAX Callback function for the unity registration form
 *
 * @param array $form       Drupal form array
 * @param array $form_state Drupal form state array
 *
 * @return array An array of drupal readable ajax commands
 */
function rural_unity_register_entityform_callback($form, &$form_state){

    //Initialise commands array and entityform id
    $commands = array();
    $entityFormId = '#unity-registration-entityform-edit-form';

    //If the form has errors, get them, clear them, display them
    if(form_get_errors()){
        $form_state['rebuild'] = TRUE;
        $formErrors = theme('status_messages');
        drupal_get_messages();
        return array(
            '#type' => 'ajax',
            '#commands' => array(
                ajax_command_replace('#ajax-errors', ''),
                ajax_command_prepend($entityFormId, '<div id="ajax-errors" class="ajax-form-response">'.$formErrors.'</div>')
            )
        );
    }

    //No errors return the success message
    drupal_get_messages();
    return array(
        '#type' => 'ajax',
        '#commands' => array(
            ajax_command_replace(
                $entityFormId,
                '<div class="reg-feedback-success">
                    <h2>Thank you for registering for Unity.</h2>
                    <p>Your details have been submitted and we\'ll be in touch soon</p>
                    <p>Confirmation will be sent to <span>'.$form_state['values']['field_email_address']['und'][0]['email'].'</span></p>
                </div>'
            ),
        )
    );

}