<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_block_info().
 * Defines blocks implemented by this module/feature
 *
 * @return array Block definitions
 */
function rural_unity_block_info(){
    $blocks = array();
    $blocks['rural_unity_user_details'] = array(
        'info' => t('Rural Unity: User Details')
    );
    $blocks['rural_unity_notifications'] = array(
        'info' => t('Rural Unity: Notifications')
    );
    return $blocks;
}

/**
 * Implements hook_block_view()
 * Define blocks title/body based off of machine name
 *
 * @param string $delta The blocks machine name
 *
 * @return array An array defining the title/body of the block being viewed
 */
function rural_unity_block_view($delta = ''){
    $block = array();
    switch($delta){
        case 'rural_unity_user_details':
            $block['subject'] = '';
            $block['content'] = _build_rural_unity_user_details_block();
            break;
        case 'rural_unity_notifications':
            $block['subject'] = '';
            $block['content'] = _build_rural_unity_notifications_block();
            break;
    }
    return $block;
}

/**
 * Builds the 'rural_unity_user_details' block
 *
 * @return string
 */
function _build_rural_unity_user_details_block(){
    if(!user_is_logged_in()){
        return FALSE;
    }

    global $user;

    $currentUser = user_load($user->uid);
    $firstName = (!empty($currentUser->field_first_name['und'][0]['safe_value']))
        ? '<span>'.$currentUser->field_first_name['und'][0]['safe_value'].'</span>'
        : NULL;

    $output = '<div class="user-details clearfix">';
    $output .= '<div class="user-icon"></div>';
    $output .= '<div class="user-intro">';
    $output .= '<p class="hello">Hello '.$firstName.'</p>';
    $output .= '<a class="update-details" href="/user/'.$currentUser->uid.'/edit">Update details</a>';
    $output .= '<p class="spacer">|</p>';
    $output .= '<a class="logout" href="/user/logout">Logout</a>';
    $output .= '</div></div>';

    return $output;
}

/**
 * Builds the 'rural_unity_notifications' block
 *
 * @return bool|string
 */
function _build_rural_unity_notifications_block(){
    if(!user_is_logged_in()){
        return FALSE;
    }

    $output =    '<div id="activity-toggle">';
    $output .=        '<a href="/unity/alerts">My alerts <span class="activity-count">0</span></a>';
    $output .=    '</div>';
    $output .=    '<div id="latest-activity">';
    $output .=        '<h3>Latest alerts</h3>';
    $output .=        '<div id="activity-loader"></div>';
    $output .=        '<ul></ul>';
    $output .=        '<div id="activity-actions">';
    $output .=            '<a href="/unity/alerts">View all alerts</a>';
    $output .=        '</div>';
    $output .=     '</div>';

    return $output;

}