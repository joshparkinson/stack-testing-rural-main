<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 11/07/2016
 * Time: 13:24
 */

/**
 * Implements hook_form_alter().
 *
 * @param array  $form       The drupal form definition array
 * @param array  $form_state The drupal form state array
 * @param string $form_id    The unique form id
 *
 * @return bool Whether form alterations happened or not
 */
function rural_unity_form_broker_news_promotions_node_form_alter(&$form, &$form_state, $form_id){

    //Notifcations can only be created during node creation, not during updates
    if($form['nid']['#value'] != NULL){
        return FALSE;
    }

    //Create notifications form fieldset
    $notificationForm = array();
    $notificationForm['notifications'] = array(
        '#tree' => TRUE,
        '#type' => 'fieldset',
        '#title' => t('Notifcation settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 50
    );
    $notificationForm['notifications']['create'] = array(
        '#type' => 'checkbox',
        '#title' => t('Create notifcation?')
    );
    $notificationForm['notifications']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Notifcation title'),
        '#states' => array(
            'disabled' => array(
                ':input[name="notifications[create]"]' => array('checked' => FALSE)
            ),
        )
    );

    //Merge the notifications form into the main form
    $form = array_merge($form, $notificationForm);

    //Add notification validation/submission handlers, finish
    array_unshift($form['#validate'], 'rural_unity_form_broker_news_promotions_node_form_notification_validate');
    array_unshift($form['#submit'], 'rural_unity_form_broker_news_promotions_node_form_notificiation_submit');
    return TRUE;
}

/**
 * The notificiation form validation handler
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether validaiton handler was triggered or not
 */
function rural_unity_form_broker_news_promotions_node_form_notification_validate($form, &$form_state){

    //Nothing to do if not creating a notification
    if(!$form_state['values']['notifications']['create']){
        return FALSE;
    }

    //Creating a notification..ensure we have a title for it
    if(empty($form_state['values']['notifications']['title'])){
        form_set_error('notifications][title', 'Notification title is required.');
    }

    dpm($form_state);

    return TRUE;

}

/**
 * The nofication form submission handler
 *
 * @param array $form       The drupal form definition array
 * @param array $form_state The drupal form state array
 *
 * @return bool Whether submission handler was triggered or not
 */
function rural_unity_form_broker_news_promotions_node_form_notificiation_submit($form, &$form_state){
    //Nothing to do if not creating a notification
    if(!$form_state['values']['notifications']['create']){
        return FALSE;
    }



    return TRUE;
}
