<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

/**
 * Implements hook_menu()
 * Defines menu items/routes implemented by this feature
 */
function rural_unity_menu(){
    $items = array();

    //Forgot password form loader
    $items['ajax/unity/forgot-password/load'] = array(
        'title' => 'Unity: Forgot Password Form Loader',
        'page callback' => 'rural_unity_ajax_forgot_password_loader',
        'delivery callback' => 'rural_unity_ajax_forgot_password_loader_delivery',
        'access arguments' => array('access content'),
        'file' => 'src/ajax/rural_unity.ajax.forgot_password.inc',
    );

    //Notifications ajax checker
    $items['ajax/unity/notifications/check'] = array(
        'title' => 'Unity: Notifications Checker',
        'page callback' => 'rural_unity_ajax_notifications_check',
        'delivery callback' => 'rural_unity_ajax_notifications_check_delivery',
        'access arguments' => array('access content'),
        'file' => 'src/ajax/rural_unity.ajax.notifications_check.inc'
    );

    //Notifications read link
    $items['ajax/unity/notifications/read'] = array(
        'title' => 'Unity: Read Notification',
        'page callback' => 'rural_unity_ajax_notifications_read',
        'delivery callback' => 'rural_unity_ajax_notifications_read_delivery',
        'access arguments' => array('access content'),
        'file' => 'src/ajax/rural_unity.ajax.notifications_read.inc'
    );

    //Registrants overview page
    $items['admin/people/unity/registrants'] = array(
        'title' => 'Unity Registrants',
        'page callback' => 'rural_unity_registrants_main_page',
        'access arguments' => array('process unity registrants'),
        'file' => 'src/pages/rural_unity.registrants.main.inc',
    );
    //Registrants processing page
    $items['admin/people/unity/registrants/%'] = array(
        'title' => 'Unity Registrant Processing',
        'page callback' => 'rural_unity_registrant_processing_page',
        'page arguments' => array(4),
        'access arguments' => array('process unity registrants'),
        'file' => 'src/pages/rural_unity.registrants.processing.inc',
    );

    return $items;
}