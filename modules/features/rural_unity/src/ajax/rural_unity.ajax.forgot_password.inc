<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

function rural_unity_ajax_forgot_password_loader_delivery($result){
    print json_encode($result);
}

function rural_unity_ajax_forgot_password_loader(){

    drupal_add_library('system', 'drupal.ajax');
    drupal_add_library('system', 'drupal.form');

    module_load_include('inc', 'user', 'user.pages');
    $output = '<div class="close"></div><h2>Forgot password</h2><p>Just tell us your email address and we’ll send you an email to reset your password</p>';
    $output .= drupal_render(drupal_get_form('user_pass'));


    $settings = FALSE;
    
    $javascript = drupal_add_js(NULL, NULL);
    if(isset($javascript['settings'], $javascript['settings']['data']))
    {
        $settings = '<script type="text/javascript">jQuery.extend(Drupal.settings, ';
        $settings .= drupal_json_encode(call_user_func_array('array_merge_recursive', $javascript['settings']['data']));
        $settings .=  ');</script>';
    }

    return array('output' => $output, 'settings' => $settings);
}