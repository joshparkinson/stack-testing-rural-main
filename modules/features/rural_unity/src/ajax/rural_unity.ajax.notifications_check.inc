<?php
/**
 * Custom Feature File
 * Feature: rural_unity
 * Developed by: Josh Parkinson
 */

function rural_unity_ajax_notifications_check_delivery($result){
    print json_encode($result);
}

function rural_unity_ajax_notifications_check(){

    //No direct access
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'){
        return array(
            "status" => "error",
            "response" => "No direct access"
        );
    }

    //Make sure we have a logged in user we can check notifications for
    if(!user_is_logged_in()){
        return array(
            "status" => "error",
            "response" => "No logged in user"
        );
    }

    global $user;

    //Get number unread notification for current user
    $unreadNotifications = db_query("SELECT n.nid
                                     FROM node n 
                                     WHERE NOT EXISTS ( 
                                         SELECT nv.id 
                                         FROM unity_notifications_views nv 
                                         WHERE n.nid = nv.notification_id 
                                         AND nv.user_id = :uid
                                     )
                                     AND n.type = 'unity_notification'", array(':uid' => $user->uid));
    
    //Grab notications
    $notificationsQuery = db_select('node', 'n');
    $notificationsQuery->leftJoin('field_data_field_link', 'l', 'l.entity_id = n.nid');
    $notificationsQuery->leftJoin('field_data_field_notification_type', 'nt', 'nt.entity_id = n.nid');
    $notificationsQuery->leftJoin('unity_notifications_views', 'nv', 'nv.notification_id = n.nid AND nv.user_id = '.$user->uid);
    $notificationsQuery->addField('n', 'nid', 'nid');
    $notificationsQuery->addField('n', 'title', 'title');
    $notificationsQuery->addField('n', 'created', 'created');
    $notificationsQuery->addField('l', 'field_link_url', 'link');
    $notificationsQuery->addField('nt', 'field_notification_type_value', 'type');
    $notificationsQuery->addField('nv', 'id', 'notification_read');
    $notificationsQuery->condition('n.type', 'unity_notification', '=');
    $notificationsQuery->groupBy('n.nid');
    $notificationsQuery->orderBy('n.created', 'DESC');
    $notificationsQuery->range(0, 8);
    $notificationsResult = $notificationsQuery->execute();

    //Build notification markup
    $i = 0;
    $oe = "odd";
    $output = '';
    while($notification = $notificationsResult->fetchAssoc()){

        $classes = array();
        $classes[] = $oe;
        $classes[] = (empty($notification['notification_read'])) ? 'unread' : 'read';
        $classes[] = $notification['type'];
        if($i == 0){
            $classes[] = 'first';
        }
        if($i == ($notificationsResult->rowCount() - 1)){
            $classes[] = 'last';
        }

        $output .= '<li data-notification-id="'.$notification['nid'].'" class="'.implode(" ", $classes).'">';
        $output .= '<a href="/'.$notification['link'].'">'.$notification['title'].'</a>';
        $output .= '<p>'.format_date($notification['created'], 'custom', 'd-M-y').'</p>';
        $output .= '</li>';

        $oe = ($oe == 'odd') ? 'even' : 'odd';
        $i++;
    }

    //If no output built (thus no results) build the no results output
    if(empty($output)){
        $output = '<li class="first last odd no-results">';
        $output .= '<p>No alerts found.</p>';
        $output .= '</li>';
    }

    return array(
        "status" => "success",
        "response" => array(
            'output' => $output,
            'count' => $unreadNotifications->rowCount()
        )
    );

}