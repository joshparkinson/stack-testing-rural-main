<?php

function rural_unity_ajax_notifications_read_delivery($result){
    print json_encode($result);
}

function rural_unity_ajax_notifications_read(){

    //No direct access
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest'){
        return array(
            "status" => "error",
            "response" => "No direct access"
        );
    }

    //Make sure we have a logged in user we can attach view to user id
    if(!user_is_logged_in()){
        return array(
            "status" => "error",
            "response" => "No logged in user"
        );
    }

    //Check we have a notification id and it is for a alert
    if(empty($_POST['nid']) || intval($_POST['nid'] == 0)){
        return array(
            "status" => "error",
            "response" => "Invalid nid"
        );
    }

    //Load the node from its nid
    $node = node_load($_POST['nid']);
    if(!$node || $node->type != 'unity_notification'){
        return array(
            "status" => "error",
            "response" => "Invalid nid"
        );
    }

    //Checks passed, add the notification view
    //Grab the user
    //Run the query
    //Return success message
    global $user;
    $insertView = db_insert('unity_notifications_views');
    $insertView->fields(array(
        'notification_id' => intval($_POST['nid']),
        'user_id' => $user->uid
    ));
    $insertView->execute();

    return array(
        "status" => "success",
        "response" => "success"
    );
}