<?php
/**
 * @file
 * rural_unity.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function rural_unity_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'rural_unity_login_register';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Rural Unity: Login/Register';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'user_delta_login',
      'hide_title' => 1,
      'title' => 'Log in',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'vid' => 'entityform_panel',
      'display' => 'panel_pane_2',
      'args' => 'Unity Registration',
      'title' => 'Register',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Log in');
  t('Register');
  t('Rural Unity: Login/Register');

  $export['rural_unity_login_register'] = $quicktabs;

  return $export;
}
