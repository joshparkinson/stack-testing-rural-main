<?php
/**
 * @file
 * rural_unity.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function rural_unity_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-broker-area-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'menu-broker-area-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'rural_insurance',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-broker-area-top-menu'] = array(
    'cache' => -1,
    'css_class' => 'broker-area-top-menu',
    'custom' => 0,
    'delta' => 'menu-broker-area-top-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'rural_insurance',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['rural_unity-rural_unity_user_details'] = array(
    'cache' => 1,
    'css_class' => 'rural-unity-user-details',
    'custom' => 0,
    'delta' => 'rural_unity_user_details',
    'module' => 'rural_unity',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'authenticated user' => 2,
    ),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -49,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
