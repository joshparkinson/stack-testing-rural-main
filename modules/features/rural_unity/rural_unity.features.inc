<?php
/**
 * @file
 * rural_unity.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rural_unity_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function rural_unity_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function rural_unity_default_entityform_type() {
  $items = array();
  $items['unity_initial_feedback_form'] = entity_import('entityform_type', '{
    "type" : "unity_initial_feedback_form",
    "label" : "Unity Initial Feedback Form",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "ckeditor" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "ckeditor" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0, "5" : 0, "6" : 0, "7" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "[current-page:url]",
      "instruction_pre" : { "value" : "", "format" : "ckeditor" }
    },
    "weight" : "0",
    "paths" : []
  }');
  $items['unity_registration'] = entity_import('entityform_type', '{
    "type" : "unity_registration",
    "label" : "Unity Registration",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "ckeditor" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "ckeditor" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : 0, "3" : 0, "4" : 0, "5" : 0, "6" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "[current-page:url]",
      "instruction_pre" : { "value" : "", "format" : "ckeditor" }
    },
    "weight" : "0",
    "paths" : []
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function rural_unity_node_info() {
  $items = array(
    'broker_news_promotions' => array(
      'name' => t('Unity: Latest from Rural'),
      'base' => 'node_content',
      'description' => t('Broker only news and promtional content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'key_contact' => array(
      'name' => t('Unity: Key Contact'),
      'base' => 'node_content',
      'description' => t('Add a Key contact to the Unity area'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'unity_notification' => array(
      'name' => t('Unity: Notification'),
      'base' => 'node_content',
      'description' => t('Create content alerts for brokers in the Unity section'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
