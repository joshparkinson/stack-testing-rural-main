<?php
/**
 * @file
 * rural_unity.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function rural_unity_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'broker_news_promotions';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:broker_news_promotions:default';
  $panelizer->css_id = '';
  $panelizer->css_class = 'full-width broker-area';
  $panelizer->css = '';
  $panelizer->no_blocks = TRUE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '7c8ab205-efef-4b47-b120-408d3c8eb0a5';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:broker_news_promotions:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-25943c04-459b-430a-acf1-8b59b341c71d';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '25943c04-459b-430a-acf1-8b59b341c71d';
  $display->content['new-25943c04-459b-430a-acf1-8b59b341c71d'] = $pane;
  $display->panels['left'][0] = 'new-25943c04-459b-430a-acf1-8b59b341c71d';
  $pane = new stdClass();
  $pane->pid = 'new-03aff0c5-21c3-48c2-a8f1-c728d2c58438';
  $pane->panel = 'right';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '03aff0c5-21c3-48c2-a8f1-c728d2c58438';
  $display->content['new-03aff0c5-21c3-48c2-a8f1-c728d2c58438'] = $pane;
  $display->panels['right'][0] = 'new-03aff0c5-21c3-48c2-a8f1-c728d2c58438';
  $pane = new stdClass();
  $pane->pid = 'new-39eeff44-d120-40f0-91ba-a1c349211732';
  $pane->panel = 'right';
  $pane->type = 'page_tabs';
  $pane->subtype = 'page_tabs';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'both',
    'id' => 'node-tabs',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '39eeff44-d120-40f0-91ba-a1c349211732';
  $display->content['new-39eeff44-d120-40f0-91ba-a1c349211732'] = $pane;
  $display->panels['right'][1] = 'new-39eeff44-d120-40f0-91ba-a1c349211732';
  $pane = new stdClass();
  $pane->pid = 'new-1500b65c-fa6e-48c5-82df-1ebe09899f55';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Page Intro',
    'title' => '<none>',
    'body' => '<h2 class="pane-title">%node:title</h2>',
    'format' => 'php_code',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-page-intro broker-news-item',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '1500b65c-fa6e-48c5-82df-1ebe09899f55';
  $display->content['new-1500b65c-fa6e-48c5-82df-1ebe09899f55'] = $pane;
  $display->panels['right'][2] = 'new-1500b65c-fa6e-48c5-82df-1ebe09899f55';
  $pane = new stdClass();
  $pane->pid = 'new-6475161b-350d-46b4-93d5-cfaf9004c6b9';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_news_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => '',
      'image_link' => '',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-news-item-image',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '6475161b-350d-46b4-93d5-cfaf9004c6b9';
  $display->content['new-6475161b-350d-46b4-93d5-cfaf9004c6b9'] = $pane;
  $display->panels['right'][3] = 'new-6475161b-350d-46b4-93d5-cfaf9004c6b9';
  $pane = new stdClass();
  $pane->pid = 'new-7e4cf670-d29f-4f5c-ab34-49f5ba10806f';
  $pane->panel = 'right';
  $pane->type = 'node_created';
  $pane->subtype = 'node_created';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'format' => 'd_m_y',
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-news-item-created-date',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '7e4cf670-d29f-4f5c-ab34-49f5ba10806f';
  $display->content['new-7e4cf670-d29f-4f5c-ab34-49f5ba10806f'] = $pane;
  $display->panels['right'][4] = 'new-7e4cf670-d29f-4f5c-ab34-49f5ba10806f';
  $pane = new stdClass();
  $pane->pid = 'new-e0e3245b-bcff-4931-aaaa-949fc3aac2dc';
  $pane->panel = 'right';
  $pane->type = 'node_body';
  $pane->subtype = 'node_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-news-item-body',
  );
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'e0e3245b-bcff-4931-aaaa-949fc3aac2dc';
  $display->content['new-e0e3245b-bcff-4931-aaaa-949fc3aac2dc'] = $pane;
  $display->panels['right'][5] = 'new-e0e3245b-bcff-4931-aaaa-949fc3aac2dc';
  $pane = new stdClass();
  $pane->pid = 'new-7d722183-4f6e-4f68-99c5-95f8403e3d66';
  $pane->panel = 'right';
  $pane->type = 'views_panes';
  $pane->subtype = 'broker_news_promotions-broker_news_promotions_aa_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'clearfix no-padding',
  );
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '7d722183-4f6e-4f68-99c5-95f8403e3d66';
  $display->content['new-7d722183-4f6e-4f68-99c5-95f8403e3d66'] = $pane;
  $display->panels['right'][6] = 'new-7d722183-4f6e-4f68-99c5-95f8403e3d66';
  $pane = new stdClass();
  $pane->pid = 'new-d18ada7f-a5b9-479b-8677-1b3e84339ef4';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-broker-area-top-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-top-menu-pane',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd18ada7f-a5b9-479b-8677-1b3e84339ef4';
  $display->content['new-d18ada7f-a5b9-479b-8677-1b3e84339ef4'] = $pane;
  $display->panels['top'][0] = 'new-d18ada7f-a5b9-479b-8677-1b3e84339ef4';
  $pane = new stdClass();
  $pane->pid = 'new-473e364e-78f4-49a8-bad9-eb4b054ed715';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'rural_unity-rural_unity_notifications';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-padding broker-area-notifications-pane',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '473e364e-78f4-49a8-bad9-eb4b054ed715';
  $display->content['new-473e364e-78f4-49a8-bad9-eb4b054ed715'] = $pane;
  $display->panels['top'][1] = 'new-473e364e-78f4-49a8-bad9-eb4b054ed715';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:broker_news_promotions:default'] = $panelizer;

  return $export;
}
