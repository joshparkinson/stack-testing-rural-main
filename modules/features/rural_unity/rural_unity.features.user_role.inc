<?php
/**
 * @file
 * rural_unity.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function rural_unity_user_default_roles() {
  $roles = array();

  // Exported role: broker.
  $roles['broker'] = array(
    'name' => 'broker',
    'weight' => 5,
  );

  return $roles;
}
