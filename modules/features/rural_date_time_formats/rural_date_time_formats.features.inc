<?php
/**
 * @file
 * rural_date_time_formats.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rural_date_time_formats_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function rural_date_time_formats_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  $custom_date_formats['d M Y'] = 'd M Y';
  $custom_date_formats['d-m-Y'] = 'd-m-Y';
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function rural_date_time_formats_date_format_types() {
  $format_types = array();
  // Exported date format type: d_m_y
  $format_types['d_m_y'] = 'd M Y';
  // Exported date format type: d_m_y_dashed
  $format_types['d_m_y_dashed'] = 'd-m-Y';
  return $format_types;
}
