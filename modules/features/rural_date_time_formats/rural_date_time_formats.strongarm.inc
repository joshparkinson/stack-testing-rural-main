<?php
/**
 * @file
 * rural_date_time_formats.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function rural_date_time_formats_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_d_m_y';
  $strongarm->value = 'd M Y';
  $export['date_format_d_m_y'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_d_m_y_dashed';
  $strongarm->value = 'd-m-Y';
  $export['date_format_d_m_y_dashed'] = $strongarm;

  return $export;
}
