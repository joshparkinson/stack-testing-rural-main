<?php
/**
 * @file
 * rural_documentation_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rural_documentation_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function rural_documentation_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function rural_documentation_feature_node_info() {
  $items = array(
    'document' => array(
      'name' => t('Document'),
      'base' => 'node_content',
      'description' => t('Add a document of various types'),
      'has_title' => '1',
      'title_label' => t('Title (Do not include document type in title)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
