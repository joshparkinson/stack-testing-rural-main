<?php
/**
 * Feature Source File
 * Feature: rural_documentation_feature
 * Developed by: Josh Parkinson
 * Created: 23/05/2016
 */

/**
 * Implements hook_preprocess_page().
 * Catch the document node form, add required javascript.
 *
 * @param array $vars An array of drupal page variables
 *
 * @return bool Indicates whether preprocess hook was required/used
 */
function rural_documentation_feature_preprocess_page(&$vars){

    //Nothing to do if no form id present
    if(empty($vars['page']['content']['system_main']['#form_id'])){
        return FALSE;
    }

    //Nothing to do if not document node form
    if($vars['page']['content']['system_main']['#form_id'] != 'document_node_form'){
        return FALSE;
    }

    //Reached thus on a document node form, add required javascript
    $featurePath = drupal_get_path('module', 'rural_documentation_feature');
    $jsFilepath = $featurePath.'/src/assets/js/rural_documentation_feature.document_node_form.js';
    drupal_add_js($jsFilepath, 'file');
    return TRUE;

}