<?php
/**
 * Feature Source File
 * Feature: rural_documentation_feature
 * Developed by: Josh Parkinson
 * Created: 23/05/2016
 */

/**
 * Implements hook_form_alter().
 * Set the policy documents date filter date format dont auto complete
 *
 * @param array  $form       Nested array of form elements that comprise the form.
 * @param array  $form_state A keyed array containing the current state of the form. The arguments that drupal_get_form() was originally called with are available in the array $form_state['build_info']['args'].
 * @param string $form_id    String representing the name of the form itself. Typically this is the name of the function that generated the form.
 *
 * @return bool Indicates whether hook was leveraged or not
 */
function rural_documentation_feature_form_alter(&$form, &$form_state, $form_id){

    if(empty($form['#id'])){
        return FALSE;
    }
    
    if($form['#id'] != "views-exposed-form-documentation-policy-documents"
        && $form['#id'] != "views-exposed-form-documentation-policy-documents-product-specific"){
        return FALSE;
    }

    $form['between_date_filter']['value']['#date_format'] = 'd M Y';
    $form['#attributes']['autocomplete'] = 'off';

}