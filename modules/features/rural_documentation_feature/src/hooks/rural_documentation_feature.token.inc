<?php
/**
 * Feature Source File
 * Feature: rural_documentation_feature
 * Developed by: Josh Parkinson
 * Created: 23/05/2016
 */

/**
 * Implements hook_token_info()
 * Defines the tokens created/used by this feature.
 *
 * @return array An array of token information for tokens implemented by this feature
 */
function rural_documentation_feature_token_info(){
    $info['tokens']['node']['documentation_file_name'] = array(
        'name' => t('Documentation File Name'),
        'description' => t('Formats/Standardises Documentation File Names')
    );
    $info['tokens']['node']['documentation_title'] = array(
        'name' => t('Documentation Title'),
        'description' => t('Formats/Standardises Documentation Titles')
    );
    return $info;
}

/**
 * Impements hook_tokens().
 * Used to process token replacement.
 *
 * @param string $type    The machine-readable name of the type (group) of token being replaced, such as 'node', 'user', or another type defined by a hook_token_info() implementation.
 * @param array  $tokens  An array of tokens to be replaced. The keys are the machine-readable token names, and the values are the raw [type:token] strings that appeared in the original text.
 * @param array  $data    An associative array of data objects to be used when generating replacement values, as supplied in the $data parameter to token_replace().
 * @param array  $options An associative array of options for token replacement; see token_replace() for possible values.
 *
 * @return array An associated array of replacement values keyed by the raw [type:token] strings from the original text.
 *               An empty array is returned when no replacements have taken place
 */
function rural_documentation_feature_tokens($type, $tokens, array $data = array(), array $options = array()){

    //Only need to handle node token replacements
    if($type != 'node' || empty($data['node'])){
        return array();
    }

    //Store node locally
    $node = $data['node'];

    //Only handling document node
    if($node->type != "document"){
        return array();
    }

    //Initialise replacements array
    $replacements = array();

    //Get the type of document we are dealing with
    $document_type = $node->field_type['und'][0]['value'];

    //IF dealing with a policy or risk guide type
    //  A bit more work to do, title and filename tokens need to be built,
    //  Replacements will be built using 'document info type', 'related product', 'valid from' date.
    //ELSE
    //  Dont worry about building title replacement, it has been set by user
    //  Build standard filename replacement
    if($document_type == "policy" || $document_type == "risk"){

        //Determine the product this document is related to
        $related_product_node = (intval($node->field_related_product['und'][0]['target_id']) > 0) ? node_load($node->field_related_product['und'][0]['target_id']) : NULL;
        $title_related_product = (!empty($related_product_node)) ? $related_product_node->title : NULL;
        $filename_related_product = (!empty($related_product_node)) ? strtolower(str_replace(" ", "_", $related_product_node->title)) : NULL;

        //Determine type vars
        //If were dealing with a risk guide, set risk guide types
        //Else we check wehter we're dealing with a policy wording or policy summary document
        if($document_type == "risk"){
            $filename_type = "risk_guide";
            $title_type = "risk guide";
        }
        else if($node->field_policy_document_type['und'][0]['value'] == "policy_other"){
            $filename_type = strtolower(str_replace(" ", "_", $node->field_policy_document_name['und'][0]['value']));
            $title_type = $node->field_policy_document_name['und'][0]['value'];
        }
        else{
            $filename_type = ($node->field_policy_document_type['und'][0]['value'] == "policy_wording") ? "policy_wording" : "policy_summary";
            $title_type = ($node->field_policy_document_type['und'][0]['value'] == "policy_wording") ? "policy wording" : "policy summary";
        }

        //Build the valid from date
        //Policy uses valid from, risk guides use created date
        $filename_date = ($document_type == "policy") ? date('dmY', strtotime($node->field_valid_from['und'][0]['value'])) : date('dmY', $node->created);

        //Build the replacement variables
        $filename_replacement = trim(str_replace("__", "_", "ruralinsurance_".$filename_related_product."_".$filename_type."_".$filename_date));
        $title_replacement = trim($title_related_product." ".$title_type);

    }
    else{
        //Build the filename based on title and upload date
        $filename_replacement = str_replace(" ", "_", strtolower($node->title)."-".date('dmY', $node->created));
        $title_replacement = NULL;
    }

    //Do the token replacement
    foreach($tokens as $name => $original){
        switch($name){
            case "documentation_file_name":
                $replacements[$original] = $filename_replacement;
                break;
            case "documentation_title":
                $replacements[$original] = $title_replacement;
                break;
        }
    }

    return $replacements;

}