<?php
/**
 * Feature Source File
 * Feature: rural_documentation_feature
 * Developed by: Josh Parkinson
 * Created: 23/05/2016
 */

/**
 * Implements hook_views_query_alter().
 * Alter the policy documentation views to improve handling of the
 * views between data filter
 *
 * @param stdClass $view  The view object about to processed
 * @param stdCLass $query The view query object
 *
 * @return bool Indicates whether hook was leveraged fully or not
 */
function rural_documentation_feature_views_query_alter(&$view, &$query){

    //Only dealing with policy document documentation views
    if($view->name != 'documentation'
        || ($view->current_display != 'policy_documents'
            && $view->current_display != 'policy_documents_product_specific')){
        return FALSE;
    }

    //If the user hasnt specified an inception date
    // -- We must add the valid to field to the view so we can add a where condition to the query using it
    // -- We also hide the valid to field, as without an inception date.. we are only showing the current (non legacy docs)
    //Else our user is searching by inception date so:
    // -- We remove the policy status where clause (which by default searches only for current non legacy docs)
    // -- We then add a fallback where clause to the view allowing is to deal with 'views between date filter' shortcomings
    //    This fallback where clause allows us to show documents that are current (no valid to value)
    //    which normally would not be displayed using the 'views between date filter' module alone.
    if($view->exposed_raw_input['between_date_filter']['value'] == NULL){

        //Build join statment
        $join = new views_join;
        $join->table = "field_data_field_valid_to";
        $join->field = "entity_id";
        $join->left_table = "node";
        $join->left_field = "nid";
        $join->type = "left";

        //Add join statement to the query
        $view->query->add_relationship('field_data_field_valid_to',$join,'node');

        //Change query to get all products that are currently valid (ie/ no valid to date)
        $view->query->add_where_expression(1, 'field_data_field_valid_to.field_valid_to_value IS NULL');

        //Hide the display of the valid to field.
        unset($view->display_handler->handlers['field']["field_valid_to"]);

    }
    else{
        //Get date entered by users
        //Add a where clause which searched for the documents which are still valid but have no valid to date
        $input_date = $view->exposed_raw_input['between_date_filter']['value'];
        $view->query->add_where_expression(2, "(DATE_FORMAT(field_data_field_valid_from.field_valid_from_value, '%Y-%m-%d') <= '".$input_date."' AND field_data_field_valid_to.field_valid_to_value IS NULL)");
    }

    return TRUE;

}