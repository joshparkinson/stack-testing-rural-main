(function($) {

    $(document).ready(function(){

        //Grab the documentation type select
        var doc_type_dropdown = $("#edit-field-type-und");

        //Check if we should be showing the title or not on page load
        if(doc_type_dropdown.val() == "policy" || doc_type_dropdown.val() == "risk") hide_title(false);

        //When the selctbox is changed check wether we need to show/hide the title
        $(doc_type_dropdown).change(function(){
            if($(this).val() == "policy" || $(this).val() == "risk"){
                hide_title(true);
            }
            else{
                show_title();
            }
        });

    });

    /**
     * HELPERS
     */

    //Helper function to hide the title of the form
    //Get the title value and store in local variable
    //Hide the title and reset value, slideup if animate
    function hide_title(animate){
        if(animate){
            $(".form-item-title").slideUp(function(){
                $("#edit-title").val('');
            });
        }
        else{
            $(".form-item-title").hide();
            $("#edit-title").val('');
        }
    }

    //Helper function to show the title
    function show_title(){
        $(".form-item-title").slideDown();
    }

})(jQuery);