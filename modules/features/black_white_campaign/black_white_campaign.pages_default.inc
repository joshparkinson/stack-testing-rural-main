<?php
/**
 * @file
 * black_white_campaign.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function black_white_campaign_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'black_white_risks_bigger_picture_';
  $page->task = 'page';
  $page->admin_title = 'Black & White Risks (Bigger Picture)';
  $page->admin_description = '';
  $page->path = 'blackandwhite';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_black_white_risks_bigger_picture___panel';
  $handler->task = 'page';
  $handler->subtask = 'black_white_risks_bigger_picture_';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'full-width',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'All risks are black & white on paper…';
  $display->uuid = 'a2b589dd-5b24-473a-8247-e0ad1e4a0c07';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f6e9d22f-1b59-4689-ae82-0c88d5495c6f';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Banner',
      'title' => '',
      'body' => '<div class="banner-image"></div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'black-white banner',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f6e9d22f-1b59-4689-ae82-0c88d5495c6f';
    $display->content['new-f6e9d22f-1b59-4689-ae82-0c88d5495c6f'] = $pane;
    $display->panels['middle'][0] = 'new-f6e9d22f-1b59-4689-ae82-0c88d5495c6f';
    $pane = new stdClass();
    $pane->pid = 'new-860ddd31-9d78-4cbc-8e05-b7a0a3774a2f';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Intro Copy',
      'title' => '',
      'body' => '<h2>As March is traditionally one of the busiest periods of the year, now more than ever we understand how important it is to you that your insurer partners use a flexible approach to secure each risk.<br /><br />We pride ourselves on developing strong relationships with trading partners which allow us to work together in partnership to secure each risk. We don’t apply a one size fits all approach, we empower our underwriters to use discretion based on knowledge and experience – because we understand that not everything is black and white.</h2>
<div class="seperator"></div>
<div class="call-us">Call our team on 0344 55 77 177 to discuss the support you need to secure your next risk</div>
',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'black-white intro-copy',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '860ddd31-9d78-4cbc-8e05-b7a0a3774a2f';
    $display->content['new-860ddd31-9d78-4cbc-8e05-b7a0a3774a2f'] = $pane;
    $display->panels['middle'][1] = 'new-860ddd31-9d78-4cbc-8e05-b7a0a3774a2f';
    $pane = new stdClass();
    $pane->pid = 'new-61cc24ac-2125-4d77-9e4f-7039582a1d70';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Picture Blocks',
      'title' => '',
      'body' => '<div class="picture-block new-business-team"><div class="gradient">
  <h3>We\'ve got a <span>new business team</span> of the required <span>size</span> and <span>experience</span> to deliver</h3>
</div></div>
<div class="picture-block dedicated-underwriters"><div class="gradient">
  <h3>We write each risk individually never <span>\'one size fits all\'</span></h3>
</div></div>
<div class="picture-block empowered-underwriters"><div class="gradient">
  <h3>We empower our underwriters with the <span>flexibility</span> to <span>make decisions</span></h3>
</div></div>
<div class="picture-block work-with-you"><div class="gradient">
  <h3>We\'ll <span>work with you</span> to greater understand a risk that you think needs a closer look</h3>
</div></div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'black-white picture-blocks clearfix',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '61cc24ac-2125-4d77-9e4f-7039582a1d70';
    $display->content['new-61cc24ac-2125-4d77-9e4f-7039582a1d70'] = $pane;
    $display->panels['middle'][2] = 'new-61cc24ac-2125-4d77-9e4f-7039582a1d70';
    $pane = new stdClass();
    $pane->pid = 'new-a04afff6-2f82-496f-a2bb-30f5c293883f';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Key Contacts',
      'title' => '',
      'body' => '<a name="profiles"></a>
<h3>Meet some of our experienced underwriters:</h3>
<div class="key-contact">
  <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/black_white/profiles/anthea_mouser.png" />
  <p class="bio">With over 16 years underwriting experience at Rural, Anthea is one of our longest serving and the most experienced members of the team. She writes both Farm and Motor risks and has experience dealing with your renewals and new quotes as well as MTAs and referrals. A self-confessed country girl Anthea grew up next to a farm in the Yorkshire dales.</p>
  <p class="name">Anthea Mouser</p>
  <p class="position">Senior Underwriter</p>
</div>
<div class="key-contact">
  <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/black_white/profiles/sarah_robinson.png" />
  <p class="bio">Sarah began her insurance career with<br />Ageas as a claims handler before moving to FarmWeb in 2006, and Rural Insurance in 2013. For the last 10 years Sarah has specialised in agricultural insurance – building a wealth of knowledge across both farm and farm motor product areas. Living in the rural South West, Sarah brings a great deal of experience to our brokers in her role as Development Underwriter. </p>
  <p class="name">Sarah Robinson</p>
  <p class="position">Development Underwriter</p>
</div>
<div class="key-contact">
  <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/landing_page/black_white/profiles/mark_hughes.png" />
  <p class="bio">For the last 18 years Mark has gathered a wide range of experience in the insurance industry, including time in claims and broking roles. Mark joined Rural 4 years ago as a Motor Underwriter and has since gone on to lead our new business team and now supports brokers in field as a motor Development Underwriter.</p>
  <p class="name">Mark Hughes</p>
  <p class="position">Development Underwriter</p>
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'black-white key-contacts',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'a04afff6-2f82-496f-a2bb-30f5c293883f';
    $display->content['new-a04afff6-2f82-496f-a2bb-30f5c293883f'] = $pane;
    $display->panels['middle'][3] = 'new-a04afff6-2f82-496f-a2bb-30f5c293883f';
    $pane = new stdClass();
    $pane->pid = 'new-f6cf6951-bb45-4b49-9678-1794ebe040e7';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Contact ',
      'title' => '',
      'body' => '<p>Let us help you cope with the increased demand at this time of year, <span>call our team on 0344 55 77 177</span> to discuss your next risk</p>
',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'black-white contact',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'f6cf6951-bb45-4b49-9678-1794ebe040e7';
    $display->content['new-f6cf6951-bb45-4b49-9678-1794ebe040e7'] = $pane;
    $display->panels['middle'][4] = 'new-f6cf6951-bb45-4b49-9678-1794ebe040e7';
    $pane = new stdClass();
    $pane->pid = 'new-1329a78b-43e6-4ab8-a661-e543b3e5cb96';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Cross sell',
      'title' => '',
      'body' => '<h3>Take a look at just some of the ways in which we can support you</h3>
<div class="support-block farm-risk">
  <p>Have you seen how we can help you secure more <span>£5k+ farm risks</span></p>
  <a href="/largefarm">Find out more</a>
</div>
<div class="support-block agricultural-risk">
  <p>See how we can now be more supportive of <span>Agricultural Vehicle heavy risks</span></p>
  <a href="/agriculturalvehicles">Find out more</a>
</div>
<div class="support-block farm-motor">
  <p>Partner Farm and Farm Motor for up to <span>15%</span> new business discount</p>
  <a href="/multi-policy-discount">Find out more</a>
</div>',
      'format' => 'php_code',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'black-white other-ways clearfix',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '1329a78b-43e6-4ab8-a661-e543b3e5cb96';
    $display->content['new-1329a78b-43e6-4ab8-a661-e543b3e5cb96'] = $pane;
    $display->panels['middle'][5] = 'new-1329a78b-43e6-4ab8-a661-e543b3e5cb96';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['black_white_risks_bigger_picture_'] = $page;

  return $pages;

}
