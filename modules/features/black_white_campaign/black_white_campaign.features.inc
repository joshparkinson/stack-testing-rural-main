<?php
/**
 * @file
 * black_white_campaign.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function black_white_campaign_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
