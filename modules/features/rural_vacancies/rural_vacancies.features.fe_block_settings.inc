<?php
/**
 * @file
 * rural_vacancies.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function rural_vacancies_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-vacancy_application_text'] = array(
    'cache' => -1,
    'css_class' => 'block-vacancy-application-text',
    'custom' => 0,
    'machine_name' => 'vacancy_application_text',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'about/careers/*',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => 0,
      ),
    ),
    'title' => 'Apply for this role',
    'visibility' => 1,
  );

  $export['views-vacancies-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'vacancies-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'about/careers',
    'roles' => array(),
    'themes' => array(
      'rural_insurance' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'rural_insurance',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
