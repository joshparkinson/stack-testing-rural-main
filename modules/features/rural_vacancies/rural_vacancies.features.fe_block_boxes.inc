<?php
/**
 * @file
 * rural_vacancies.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function rural_vacancies_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Vacancy application text';
  $fe_block_boxes->format = 'ckeditor';
  $fe_block_boxes->machine_name = 'vacancy_application_text';
  $fe_block_boxes->body = '<p>To apply for this role, please email: <a href="mailto:careers@ruralinsurance.co.uk">careers@ruralinsurance.co.uk</a></p>
';

  $export['vacancy_application_text'] = $fe_block_boxes;

  return $export;
}
