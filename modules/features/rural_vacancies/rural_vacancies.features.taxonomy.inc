<?php
/**
 * @file
 * rural_vacancies.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function rural_vacancies_taxonomy_default_vocabularies() {
  return array(
    'vacancy_locataion' => array(
      'name' => 'Vacancy locataion',
      'machine_name' => 'vacancy_locataion',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
