<?php
/**
 * @file
 * rural_vacancies.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function rural_vacancies_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'vacancies';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Vacancies';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Current vacancies';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_vacancy_location' => 'field_vacancy_location',
    'field_closing_date' => 'field_closing_date',
    'path' => 'path',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_vacancy_location' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_closing_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'path' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No open vacancies found.';
  $handler->display->display_options['empty']['area']['format'] = 'ckeditor';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Job title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Vacancy location */
  $handler->display->display_options['fields']['field_vacancy_location']['id'] = 'field_vacancy_location';
  $handler->display->display_options['fields']['field_vacancy_location']['table'] = 'field_data_field_vacancy_location';
  $handler->display->display_options['fields']['field_vacancy_location']['field'] = 'field_vacancy_location';
  $handler->display->display_options['fields']['field_vacancy_location']['label'] = 'Location';
  $handler->display->display_options['fields']['field_vacancy_location']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Closing date */
  $handler->display->display_options['fields']['field_closing_date']['id'] = 'field_closing_date';
  $handler->display->display_options['fields']['field_closing_date']['table'] = 'field_data_field_closing_date';
  $handler->display->display_options['fields']['field_closing_date']['field'] = 'field_closing_date';
  $handler->display->display_options['fields']['field_closing_date']['empty'] = 'Ongoing';
  $handler->display->display_options['fields']['field_closing_date']['settings'] = array(
    'format_type' => 'd_m_y',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['path']['alter']['text'] = '<a href="[path]">More info</a>';
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'vacancy' => 'vacancy',
  );
  /* Filter criterion: Content: Vacancy location (field_vacancy_location) */
  $handler->display->display_options['filters']['field_vacancy_location_tid']['id'] = 'field_vacancy_location_tid';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['table'] = 'field_data_field_vacancy_location';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['field'] = 'field_vacancy_location_tid';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_vacancy_location_tid']['expose']['operator_id'] = 'field_vacancy_location_tid_op';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['expose']['label'] = 'Vacancy location';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['expose']['operator'] = 'field_vacancy_location_tid_op';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['expose']['identifier'] = 'field_vacancy_location_tid';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_vacancy_location_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_vacancy_location_tid']['vocabulary'] = 'vacancy_locataion';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['vacancies'] = $view;

  return $export;
}
