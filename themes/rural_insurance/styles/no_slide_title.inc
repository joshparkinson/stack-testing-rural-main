<?php

// Plugin definition.
$plugin = array(
    'title' => t('No Slide Title'),
    'description' => t('Hide default slide title.'),
    'render pane' => 'rural_insurance_no_slide_title_style_render_pane',
);

function theme_rural_insurance_no_slide_title_style_render_pane($vars) {
    $content = &$vars['content'];

    if ($content) {
        $output = theme('panels_pane', $vars);
        $classes = 'panel-pane-no-slide-title';
        $output = '<div class="' . $classes . '">' . $output . '</div>';
        return $output;
    }
}