<?php print render($page['admin']); ?>

<div class="clearfix" id="pageRow1">
  <div class="clearfix" id="header">
    <div id="logo"><a href="<?php print $front_page; ?>" title="<?php print t('Return to homepage'); ?>" rel="home"><img src="<?php print $base_path . $directory; ?>/assets/images/logo.png" alt="Rural Insurance" /></a></div>
    <div id="header-right" class="clearfix">
      <div id="phoneNumberTop">0344 55 77 177</div>
      <div id="openingTimes">08:30 - 17:30 | Mon-Fri</div>
    </div>
    <div id="socialShare">
      <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
      <script type="IN/FollowCompany" data-id="1986177" data-counter="right"></script>
    </div>
    <?php print render($page['header']); ?>
    <nav id="mobile-menu"><?php print render($page['navigation']); ?></nav>
  </div>
  <!-- /header -->
  <div id="navigationInner">
      <div id="navigation"><?php print render($page['navigation']); ?></div>
</div><!-- nav-->
  <!-- /main-menu -->
</div>
<!-- /pageRow1 -->
<div class="clearfix" id="pageRow2">
  <div id="content-top"><?php print render($page['content_top']); ?></div>
</div>
<!-- /pageRow2 -->
<div class="clearfix" id="pageRow3">
  <div id="pageRow3Inner" class="clearfix">
    <div class="clearfix" id="content">
      <?php if ((!$is_front) && ($page['sidebar_first'])): ?>
      <div id="contentColumn-1" class="clearfix">
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <a id="main-content"></a><?php print render($title_prefix); ?>
        <?php if ((!$is_front) && ($title)): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?>
        <div class="tabs main-tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
        <?php endif; ?>
        <div id="content-content"><?php print render($page['content']); ?></div>
        <!-- /content-content -->
        <?php if ((!$is_front) && ($page['sidebar_first'])): ?>
      </div>
      <!-- /contentColumn-1 -->
      <div id="contentColumn-2" class="clearfix"><?php print render($page['sidebar_first']); ?></div>
      <!-- /contentColumn-2 -->
      <?php endif; ?>
    </div>
    <!-- /content -->
  </div>
</div>
<div id="footer" class="clearfix">
  <?php print render($page['footer']); ?>
  <!-- <img src="<?php print $base_path . $directory; ?>/assets/images/footerShape.png" alt="*" width="50" height="60" id="footerShape" /> -->
  <div id="legal">
    <div id="legalContent" class="clearfix">
      <p>
        Rural Insurance Group Limited is authorised and regulated by the Financial Conduct Authority. Calls may be recorded for training and monitoring purposes
        Registered address: The Hamlet, Hornbeam Park, Harrogate HG2 8RE. Registered in England and Wales. Registered no. 2207611
      </p>
      <p>
        Copyright &copy; <?php print $site_name . date(' Y'); ?> Rural Insurance Group Limited. All rights reserved. <a href="legal">Privacy & Cookies</a>
      </p>
        <img src="/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/footer_logo.png" />
    </div>
  </div>
</div>
<!-- /footer -->
