<body style="background:#fff !important;min-height:100%;">
  <?php print $messages; ?>
  <a id="main-content"></a><?php print render($title_prefix); ?>
  <?php if ((!$is_front) && ($title)): ?>
  <h1 class="title" id="page-title"><?php print $title; ?></h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php print render($page['help']); ?>
  <div id="content-content"><?php print render($page['content']); ?></div>
</body>