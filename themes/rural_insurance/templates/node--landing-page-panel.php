<?php print render($page['admin']); ?>

<div class="clearMe" id="pageRow1">
  <div class="clearMe" id="header">
    <div id="logo"><a href="<?php print $front_page; ?>" title="<?php print t('Return to homepage'); ?>" rel="home"><img src="<?php print $base_path . $directory; ?>/assets/images/logo.png" alt="Rural Insurance" /></a></div>
    <div id="headerTag">Rural and Argicultural specialists</div>
    <div id="phoneNumberTop">0844 55 77 177</div>
    <div id="openingTimes">08:30 - 17:30 | Mon-Fri</div>
    <div id="socialShare">
      <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
      <script type="IN/FollowCompany" data-id="1986177" data-counter="right"></script>
    </div>
    <div id="navigation"><?php print render($page['navigation']); ?></div>
  </div>
  <!-- /header -->
  <!-- /main-menu -->
</div>
<!-- /pageRow1 -->
<div class="clearMe" id="pageRow2">
  <div id="content-top"><?php print render($page['content_top']); ?></div>
  <div id="curve"></div>
</div>
<!-- /pageRow2 -->
<div class="clearMe" id="pageRow3">
  <div id="pageRow3Inner">
    <div class="clearMe" id="content">
      <?php if ((!$is_front) && ($page['sidebar_first'])): ?>
      <div id="contentColumn-1">
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <a id="main-content"></a><?php print render($title_prefix); ?>
        <?php if ((!$is_front) && ($title)): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?>
        <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
        <?php endif; ?>
        <div id="content-content"><?php print render($page['content']); ?></div>
        <!-- /content-content -->
        <?php if ((!$is_front) && ($page['sidebar_first'])): ?>
      </div>
      <!-- /contentColumn-1 -->
      <div id="contentColumn-2"><?php print render($page['sidebar_first']); ?></div>
      <!-- /contentColumn-2 -->
      <?php endif; ?>
    </div>
    <!-- /content -->
  </div>
</div>
<div id="footer" class="clearMe">
  <div id="footerMenu" class="clearMe"><?php print render($page['footer']); ?></div>
  <!-- <img src="<?php print $base_path . $directory; ?>/assets/images/footerShape.png" alt="*" width="50" height="60" id="footerShape" /> -->
  <div id="legal">
    <div id="legalContent" class="clearMe">
      <p>
        Rural Insurance Group Limited is authorised and regulated by the Financial Conduct Authority.<br />
        Calls may be recorded for training and monitoring purposes<br />
        Registered address: The Hamlet, Hornbeam Park, Harrogate HG2 8RE<br />
        Registered in England and Wales. Registered no. 2207611<br />
        Copyright &copy; <?php print $site_name . date(' Y'); ?> Rural Insurance Group Limited. All rights reserved. <a href="legal">Cookies</a>
      </p>
      <img src="<?php print $base_path . $directory; ?>/assets/images/footerShape.png" alt="*" width="50" height="60" />
    </div>
  </div>
</div>
<!-- /footer -->
