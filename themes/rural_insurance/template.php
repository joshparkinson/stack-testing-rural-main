<?php
/*
// Load reset.css above all other styles
function rural_insurance_preprocess_html(&$variables) {
  $reset = drupal_get_path('theme', 'rural_insurance') . '/assets/css/reset.css';
  $options = array(
    'group'  => CSS_SYSTEM -1,
    'weight' => -10,
  );
  drupal_add_css($reset, $options);
		
		  // Add conditional stylesheets for IE
  //drupal_add_css(path_to_theme() . '/assets/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/assets/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
	
}
*/

function rural_insurance_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode('', $vars['panes']);
  return $output;
}


function rural_insurance_preprocess_panels_pane(&$variables){

    /*
    //Front Page or New chapter
    if(drupal_is_front_page() || current_path() == 'new-chapter'){
        drupal_add_js(drupal_get_path('theme', 'rural_insurance').'/assets/scripts/rural_insurance.front_page.js');
        return TRUE;
    }
    */

    //RBM Campaign Page, 'Our Rural Business Motor insurance offers:' pane
    if(!empty($variables['pane']->css['css_id']) && $variables['pane']->css['css_id'] == "rbmc-rural-business-motor-offers"){
        drupal_add_js(drupal_get_path('theme', 'rural_insurance').'/assets/scripts/rural_insurance.panel.rbmc_rural_business_motor_offers.js');
        return TRUE;
    }

    return FALSE;
}

/**
 * Uncomment to flip the true/false ordering for boolean radio button form items
 *
function rural_insurance_field_widget_form_alter(&$element, &$form_state, $context){
    if ($context['field']['type'] == 'list_boolean' && isset($element['#options'])) {
        $element['#options'] = array_reverse($element['#options'], TRUE);
    }
}
 */

