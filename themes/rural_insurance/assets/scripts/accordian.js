(function ($) {

    $(document).ready(function () {

        //Check An Accordian Container Exists On The Page
        if ($("div.accordian-container").length) {
            //Add accordian buttons to each accordian header on the page
            $(".accordian-header").append("<div class='accordian-button'><img alt='open' src='/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/arrow_down_16x16.png' /></div>");

            //React to an accordian button being clicked
            $(".accordian-button").click(function () {

                //Check if the buttons corresponding body text is already visible. If it is close it, and remove open class.
                if ($(this).closest(".accordian-header").next(".accordian-body").hasClass("accordian-open")) {
                    $(this).closest(".accordian-header").next(".accordian-body").slideUp("slow", function () {
                        $(this).removeClass("accordian-open");
                        $(this).prevAll(".accordian-header:first").find(".accordian-button").removeClass('active').html("<img alt='open' src='/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/arrow_down_16x16.png' />");
                        //$(this).prevAll(".accordian-header:first").find(".accordian-button").find('img').animate({rotate: '360'}, 600); //Arrow Rotation
                    });
                }
                else {
                    //Find all open accordian bodys within the accordian container and close them.
                    $(this).closest(".accordian-container").find('.accordian-open').slideUp("slow", function () {
                        $(this).removeClass("accordian-open");
                        $(this).prevAll(".accordian-header:first").find(".accordian-button").removeClass('active').html("<img alt='open' src='/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/arrow_down_16x16.png' />");
                        //$(this).prevAll(".accordian-header:first").find(".accordian-button").children('img').animate({rotate: '360'}, 600); //Arrow Rotation
                    });

                    //Open the buttons corresponding body text.
                    $(this).closest(".accordian-header").next(".accordian-body").slideDown("slow", function () {
                        $(this).addClass("accordian-open");
                        $(this).prevAll(".accordian-header:first").find(".accordian-button").addClass('active').html("<img alt='close' src='/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/arrow_up_16x16.png' />");
                        //$(this).prevAll(".accordian-header:first").find(".accordian-button").children('img').animate({rotate: '180'}, 600); //Arrow Rotation
                    });
                }

            });
        }

    });
})
(jQuery);