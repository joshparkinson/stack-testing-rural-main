/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */

if (typeof (CKEDITOR) !== 'undefined') {
  CKEDITOR.stylesSet.add('drupal', [{
    name: 'Intro (h2)',
    element: 'h2',
    attributes: {
      'class': 'intro'
    }
  }, {
    name: 'Heading 2',
    element: 'h2'
  }, {
    name: 'Heading 2 Centered',
    element: 'h2',
    attributes: {
      'class': 'center'
    }
  }, {
    name: 'Heading 3',
    element: 'h3'
  }, {
    name: 'Heading 4',
    element: 'h4'
  }, {
    name: 'Heading 5',
    element: 'h5'
  }, {
    name: 'Heading 6',
    element: 'h6'
  }, {
    name: 'Big',
    element: 'span',
    attributes: {
      'class': 'big'
    }
  }, {
    name: 'Small',
    element: 'span',
    attributes: {
      'class': 'small'
    }
  }, {
    name: 'Green Bullet Header',
    element: 'h2',
    attributes: {
      'class': 'green-bullet-header'
    }
  }, {
    name: 'Green Bullet List',
    element: 'ul',
    attributes: {
      'class': 'green-bullet-list'
    }
  }, {
    name: 'Accordian Container',
    element: 'div',
    attributes: {
      'class': 'accordian-container'
    }
  }, {
    name: 'Accordian Header',
    element: 'div',
    attributes: {
      'class': 'accordian-header'
    }
  }, {
    name: 'Accordian Body',
    element: 'div',
    attributes: {
      'class': 'accordian-body'
    }
  }]);
}