/**
 *	CKEDITOR TEMPLATES
 */

CKEDITOR.addTemplates( 'default', {
	imagesPath: '/sites/ruralinsurance.co.uk/themes/rural_insurance/assets/images/ckeditor_template_icons/',
	templates: [
        
        /** 2 Column Template **/

        {
            title: '2 Column',
            //image: 'my-template.png',
            description: '2 Columns that sit side by side / stack on mobile.',
            html: '<div class="column-wrapper two-col clearfix">' +
            	  '<div class="column first">Left Column</div>' +
            	  '<div class="column last">Right Column</div>' +
            	  '</div>'
        }
	]
});