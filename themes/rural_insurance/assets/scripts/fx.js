(function ($) {

    /**
     * Responsive views slideshow
     */
    Drupal.theme.prototype.slideshowResizer = function(target) {
        
        var slideheight = 0;
        $(target + ' .views-slideshow-cycle-main-frame-row').each(function(){
            slideheight = $(this).find('.views-slideshow-cycle-main-frame-row-item').innerHeight();
            if(slideheight != 0){
                $(target + ' .views-slideshow-cycle-main-frame').css('height',slideheight+'px');
                $(target + ' .views-slideshow-cycle-main-frame-row').css('height',slideheight+'px');
                return false;
            }
        });

    };

    Drupal.behaviors.slideshowResize = {
        attach: function(context, settings) {
            
            $('.view-front-page-banner', context).once('processed', function() {
                //Drupal.theme('slideshowResizer', '.view-front-page-banner');
            });

            $(window).resize(function() {
                Drupal.theme('slideshowResizer', '.view-front-page-banner');
            });
    
            $(window).load(function() {
                //Drupal.theme('slideshowResizer', '.view-front-page-banner');
            });

        }
    };


})(jQuery);