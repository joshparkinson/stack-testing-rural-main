(function($){

    var ruralBusinessMotorOffersPane;

    $(document).ready(function(){

        ruralBusinessMotorOffersPane = "#rbmc-rural-business-motor-offers";

        $(ruralBusinessMotorOffersPane).find(".show-more p").click(function(){
            $(ruralBusinessMotorOffersPane).find(".show-more").slideUp();
            $(".more-offers").slideDown();
        });

    });

})(jQuery);