(function($){

    $(document).ready(function(){

        if($(window).width() > 980){
            _fullscreen_hero();
        }
        /*
        if($(window).width() > 760){
            $(".hero-image img.logo").hide();
        }
        */

    });

    /*
    $(window).load(function(){
        if($(window).width() > 760) {
            $(".hero-image img.logo").fadeIn('slow');
        }
    });

    $(window).resize(function(){
        if($(window).width() <= 760 && $(".hero-image img.logo").is(':visible')){
            $(".hero-image img.logo").hide();
        }
        else if($(window).width() > 760 && !$(".hero-image img.logo").is(':visible')){
            $(".hero-image img.logo").show();
        }
    });
    */


    function _fullscreen_hero(){
        var heroHeight = $(window).height() - 190;
        if(heroHeight < 700){

            if(heroHeight < 500){
                heroHeight = 500;
            }

            var scrollImage = $(".hero-image img.scroll");
            var heroImage = $(".hero-image");
            var heroPane = $(".hero-image .pane-content");
            var bigLogo = $(".hero-image img.logo");
            var logoNudge = 700 - heroHeight;
            var nudge = 700 - heroHeight;
            var originalLogoTop = $(bigLogo).css('top').replace('px', '');
            var newLogoTop = originalLogoTop - (logoNudge / 2);

            if(newLogoTop < 120){
                newLogoTop = 120;
            }

            $(heroImage).css({
                'height': heroHeight + 'px',
                'background-position': 'center bottom -' + nudge + 'px'
            });

            $(heroPane).css({
                'height': heroHeight + 'px'
            });

            $(bigLogo).css({
                'top': newLogoTop + 'px'
            });

            $(scrollImage).css({
                'bottom': '5px'
            });

        }
    }

})(jQuery);